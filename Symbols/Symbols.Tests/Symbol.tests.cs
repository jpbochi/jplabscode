﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.IO;
using Xunit;
using Xunit.Sdk;
using System.Xml;
using System.ComponentModel;

namespace JpLabs.Symbols.Tests
{
	/// Features to be implemented:
	/// - Cast to int/long (or not!);
	/// - Variance (i.e.: pass a more-specific-than-required type for a method/property)
	///		- PROBLEM: real variance only works on interfaces, but interfaces equality operator can't be overriden!
	///		- SOLUTION: make variance through convertion
	/// - Flags (e.g.: CarOptions options = CarOptions.SunRoof | CarOptions.FogLights; )
	///		- create an AggregatedSymbol class
	/// 

	public class Symbol_Tests
	{
		abstract class Color : SymbolEnum
		{
			static Color() { SymbolEnum.InitEnum<Color>(); }

			public static readonly Symbol Black = null;
			public static readonly Symbol<Color> White = null;
		}

		abstract class LowColor : Color
		{
			static LowColor() { SymbolEnum.InitEnum<LowColor>(); }
			public static readonly Symbol Infrared = null;
		}

		abstract class HighColor : Color
		{
			static HighColor() { SymbolEnum.InitEnum<HighColor>(); }
			public static readonly Symbol Ultraviolet = null;
		}

		abstract class EnumWithReadWriteField : SymbolEnum
		{
			public static Symbol Invalid = null;
		}

		[Fact]
		void TestEquality()
		{
			Color.Black.AssertEqual(Color.Black);
			Color.White.AssertEqual(Color.White);
			LowColor.Infrared.AssertEqual(LowColor.Infrared);
			HighColor.Ultraviolet.AssertEqual(HighColor.Ultraviolet);

			Symbol.Declare(typeof(Color), "Black").AssertEqual(Color.Black);
			Symbol.Declare(typeof(Color), "White").AssertEqual(Color.White);
			Symbol.Declare(typeof(LowColor), "Infrared").AssertEqual(LowColor.Infrared);
			Symbol.Declare(typeof(HighColor), "Ultraviolet").AssertEqual(HighColor.Ultraviolet);

			Symbol.Declare<Color>("Black").AssertEqual(Color.Black);
			Symbol.Declare<Color>("White").AssertEqual(Color.White);
			Symbol.Declare<LowColor>("Infrared").AssertEqual(LowColor.Infrared);
			Symbol.Declare<HighColor>("Ultraviolet").AssertEqual(HighColor.Ultraviolet);
		}

		[Fact]
		void TestInequality()
		{
			Color.Black
				.AssertNotEqual(Color.White)
				.AssertNotEqual(LowColor.Infrared)
				.AssertNotEqual(HighColor.Ultraviolet);
			Color.White
				.AssertNotEqual(Color.Black)
				.AssertNotEqual(LowColor.Infrared)
				.AssertNotEqual(HighColor.Ultraviolet);
			LowColor.Infrared
				.AssertNotEqual(Color.Black)
				.AssertNotEqual(Color.White)
				.AssertNotEqual(HighColor.Ultraviolet);
			HighColor.Ultraviolet
				.AssertNotEqual(Color.Black)
				.AssertNotEqual(Color.White)
				.AssertNotEqual(LowColor.Infrared);

			((Symbol)null)
				.AssertNotEqual(Color.Black)
				.AssertNotEqual(Color.White)
				.AssertNotEqual(LowColor.Infrared)
				.AssertNotEqual(HighColor.Ultraviolet);
		}

		[Fact]
		void StringConverstion()
		{
			Color.Black.ToString().AssertEqual(":JpLabs.Symbols.Tests.Symbol_Tests+Color:Black");
			Color.White.ToString().AssertEqual(":JpLabs.Symbols.Tests.Symbol_Tests+Color:White");
			LowColor.Infrared.ToString().AssertEqual(":JpLabs.Symbols.Tests.Symbol_Tests+LowColor:Infrared");
			HighColor.Ultraviolet.ToString().AssertEqual(":JpLabs.Symbols.Tests.Symbol_Tests+HighColor:Ultraviolet");

			Convert.ToString(Color.Black).AssertEqual(Color.Black.ToString());
			Convert.ToString(Color.White).AssertEqual(Color.White.ToString());
			Convert.ToString(LowColor.Infrared).AssertEqual(LowColor.Infrared.ToString());
			
		}

		[Fact]
		void ParseByTypeAndName()
		{
			Symbol.Parse(typeof(Color), "Black").AssertEqual(Color.Black);
			Symbol.Parse(typeof(Color), "White").AssertEqual(Color.White);
			Symbol.Parse(typeof(LowColor), "Infrared").AssertEqual(LowColor.Infrared);
			Symbol.Parse(typeof(HighColor), "Ultraviolet").AssertEqual(HighColor.Ultraviolet);
		}

		[Fact]
		void ParseByFullName()
		{
			Symbol.Parse(Color.Black.ToString()).AssertEqual(Color.Black);
			Symbol.Parse(Color.White.ToString()).AssertEqual(Color.White);
			Symbol.Parse(LowColor.Infrared.ToString()).AssertEqual(LowColor.Infrared);
			Symbol.Parse(HighColor.Ultraviolet.ToString()).AssertEqual(HighColor.Ultraviolet);

			Symbol.Parse(":" + typeof(Color).FullName + ":Bogus").AssertEqual(Symbol.Declare<Color>("Bogus"));

			Symbol.Parse("::Global").AssertEqual(Symbol.Declare(null, "Global"));
		}

		[Fact]
		void TryParseByFullName()
		{
			Symbol outSymbol;

			Assert.True(Symbol.TryParse(Color.Black.ToString(), out outSymbol));
			outSymbol.AssertEqual(Color.Black);

			Assert.True(Symbol.TryParse(Color.White.ToString(), out outSymbol));
			outSymbol.AssertEqual(Color.White);

			Assert.True(Symbol.TryParse(LowColor.Infrared.ToString(), out outSymbol));
			outSymbol.AssertEqual(LowColor.Infrared);

			Assert.True(Symbol.TryParse(HighColor.Ultraviolet.ToString(), out outSymbol));
			outSymbol.AssertEqual(HighColor.Ultraviolet);

			Assert.True(Symbol.TryParse(":" + typeof(Color).FullName + ":Bogus", out outSymbol));
			outSymbol.AssertEqual(Symbol.Declare<Color>("Bogus"));

			Assert.True(Symbol.TryParse("::Global", out outSymbol));
			outSymbol.AssertEqual(Symbol.Declare(null, "Global"));

			var symbolWithNoName = ":" + typeof(Color).FullName + ":";
			Assert.False(Symbol.TryParse(symbolWithNoName, out outSymbol));

			Assert.False(Symbol.TryParse(":InvalidEnumType:Name", out outSymbol));
		}

		[Fact]
		void GetEnumValues()
		{
			Symbol.GetEnumValues(typeof(Color), false).ToArray().AssertEqual(new [] {Color.Black, Color.White});
			Symbol.GetEnumValues(typeof(Color), true).ToArray().AssertEqual(new [] {Color.Black, Color.White});

			Symbol.GetEnumValues(typeof(LowColor), false).ToArray().AssertEqual(new [] {LowColor.Infrared});
			Symbol.GetEnumValues(typeof(LowColor), true).ToArray().AssertEqual(new [] {LowColor.Infrared, Color.Black, Color.White});

			Symbol.GetEnumValues(typeof(HighColor), false).ToArray().AssertEqual(new [] {HighColor.Ultraviolet});
			Symbol.GetEnumValues(typeof(HighColor), true).ToArray().AssertEqual(new [] {HighColor.Ultraviolet, Color.Black, Color.White});
		}

		[Fact]
		void GetEnumValuesOfEnumType()
		{
			Symbol.GetEnumValues<Color>(false).ToArray().AssertEqual(new [] {Color.Black, Color.White});
			Symbol.GetEnumValues<Color>(true).ToArray().AssertEqual(new [] {Color.Black, Color.White});

			Symbol.GetEnumValues<LowColor>(false).ToArray().AssertEqual(new [] {LowColor.Infrared});
			Symbol.GetEnumValues<LowColor>(true).ToArray().AssertEqual(new [] {LowColor.Infrared, Color.Black, Color.White});

			Symbol.GetEnumValues<HighColor>(false).ToArray().AssertEqual(new [] {HighColor.Ultraviolet});
			Symbol.GetEnumValues<HighColor>(true).ToArray().AssertEqual(new [] {HighColor.Ultraviolet, Color.Black, Color.White});
		}

		[Fact]
		void GetAttributeProvider()
		{
			Color.Black.GetAttributeProvider().AssertEqual(typeof(Color).GetField("Black"));
			Color.White.GetAttributeProvider().AssertEqual(typeof(Color).GetField("White"));
			LowColor.Infrared.GetAttributeProvider().AssertEqual(typeof(LowColor).GetField("Infrared"));
			HighColor.Ultraviolet.GetAttributeProvider().AssertEqual(typeof(HighColor).GetField("Ultraviolet"));
		}

		[Fact]
		void TestCloning()
		{
			Color.Black.Clone().AssertEqual(Color.Black);
			Color.White.Clone().AssertEqual(Color.White);
			LowColor.Infrared.Clone().AssertEqual(LowColor.Infrared);
			HighColor.Ultraviolet.Clone().AssertEqual(HighColor.Ultraviolet);
		}

		[Fact]
		void IsOperator()
		{
			Assert.True(Color.Black.Is<Color>());
			Assert.True(Color.White.Is<Color>());

			Assert.False(Color.Black.Is<LowColor>());
			Assert.False(Color.White.Is<LowColor>());

			Assert.True(LowColor.Black.Is<Color>());
			Assert.True(LowColor.White.Is<Color>());
			Assert.True(LowColor.Infrared.Is<Color>());
			Assert.True(LowColor.Infrared.Is<LowColor>());

			Assert.True(HighColor.Black.Is<Color>());
			Assert.True(HighColor.White.Is<Color>());
			Assert.True(HighColor.Ultraviolet.Is<Color>());
			Assert.True(HighColor.Ultraviolet.Is<HighColor>());
		}

		[Fact]
		void AsOperator()
		{
			Color.Black.As<Color>().AssertNotNull().EnumType.AssertEqual(typeof(Color));
			Color.Black.As<LowColor>().AssertNull();
			Color.Black.As<HighColor>().AssertNull();

			Color.White.As<Color>().AssertNotNull().EnumType.AssertEqual(typeof(Color));
			Color.White.As<LowColor>().AssertNull();
			Color.White.As<HighColor>().AssertNull();

			LowColor.Infrared.As<Color>().AssertNotNull().EnumType.AssertEqual(typeof(LowColor));
			LowColor.Infrared.As<LowColor>().AssertNotNull().EnumType.AssertEqual(typeof(LowColor));
			LowColor.Infrared.As<HighColor>().AssertNull();

			HighColor.Ultraviolet.As<Color>().AssertNotNull().EnumType.AssertEqual(typeof(HighColor));
			HighColor.Ultraviolet.As<LowColor>().AssertNull();
			HighColor.Ultraviolet.As<HighColor>().AssertNotNull().EnumType.AssertEqual(typeof(HighColor));
		}

		[Fact]
		void EqualityBetweenOriginalAndCastValues()
		{
			Color.Black.As<Color>().AssertEqual(Color.Black);
			Color.White.As<Color>().AssertEqual(Color.White);
			LowColor.Infrared.As<Color>().AssertEqual(LowColor.Infrared);
			LowColor.Infrared.As<LowColor>().AssertEqual(LowColor.Infrared);
		}

		[Fact]
		void ConversionToValidTypes()
		{
			Type targetType = typeof(object);
			Symbol.Converter.ConvertTo(Color.Black, targetType).AssertEqual(Color.Black);
			Symbol.Converter.ConvertTo(Color.White, targetType).AssertEqual(Color.White);
			Symbol.Converter.ConvertTo(LowColor.Infrared, targetType).AssertEqual(LowColor.Infrared);

			targetType = typeof(string);
			Symbol.Converter.ConvertTo(Color.Black, targetType).AssertEqual(Color.Black.ToString());
			Symbol.Converter.ConvertTo(Color.White, targetType).AssertEqual(Color.White.ToString());
			Symbol.Converter.ConvertTo(LowColor.Infrared, targetType).AssertEqual(LowColor.Infrared.ToString());
			Symbol.Converter.ConvertTo(HighColor.Ultraviolet, targetType).AssertEqual(HighColor.Ultraviolet.ToString());
			
			targetType = typeof(Symbol);
			Symbol.Converter.ConvertTo(Color.Black, targetType).AssertEqual(Color.Black).AssertIsAssignableFrom(targetType);
			Symbol.Converter.ConvertTo(Color.White, targetType).AssertEqual(Color.White).AssertIsAssignableFrom(targetType);
			Symbol.Converter.ConvertTo(LowColor.Infrared, targetType).AssertEqual(LowColor.Infrared).AssertIsAssignableFrom(targetType);

			targetType = typeof(Symbol<Color>);
			Symbol.Converter.ConvertTo(Color.Black, targetType).AssertEqual(Color.Black).AssertIsAssignableFrom(targetType);
			Symbol.Converter.ConvertTo(Color.White, targetType).AssertEqual(Color.White).AssertIsAssignableFrom(targetType);
			Symbol.Converter.ConvertTo(LowColor.Infrared, targetType).AssertEqual(LowColor.Infrared).AssertIsAssignableFrom(targetType);

			targetType = typeof(Symbol<LowColor>);
			Symbol.Converter.ConvertTo(LowColor.Infrared, targetType).AssertEqual(LowColor.Infrared).AssertIsAssignableFrom(targetType);

			targetType = typeof(Symbol<HighColor>);
			Symbol.Converter.ConvertTo(HighColor.Ultraviolet, targetType).AssertEqual(HighColor.Ultraviolet).AssertIsAssignableFrom(targetType);
		}

		[Fact]
		void ConversionFromValidValues()
		{
			Symbol.Converter.ConvertFrom(Color.Black).AssertEqual(Color.Black);
			Symbol.Converter.ConvertFrom(Color.White).AssertEqual(Color.White);
			Symbol.Converter.ConvertFrom(LowColor.Infrared).AssertEqual(LowColor.Infrared);
			Symbol.Converter.ConvertFrom(HighColor.Ultraviolet).AssertEqual(HighColor.Ultraviolet);

			Symbol.Converter.ConvertFrom(Color.Black.ToString()).AssertEqual(Color.Black);
			Symbol.Converter.ConvertFrom(Color.White.ToString()).AssertEqual(Color.White);
			Symbol.Converter.ConvertFrom(LowColor.Infrared.ToString()).AssertEqual(LowColor.Infrared);
			Symbol.Converter.ConvertFrom(HighColor.Ultraviolet.ToString()).AssertEqual(HighColor.Ultraviolet);
		}

		[Fact]
		void ConversionToInvalidTypeFails()
		{
			Assert.Throws<InvalidCastException>(() => Symbol.Converter.ConvertTo(Color.Black, typeof(Symbol<LowColor>)));
			Assert.Throws<InvalidCastException>(() => Symbol.Converter.ConvertTo(Color.White, typeof(Symbol<LowColor>)));

			Assert.Throws<InvalidCastException>(() => Symbol.Converter.ConvertTo(LowColor.Infrared, typeof(Symbol<HighColor>)));
			Assert.Throws<InvalidCastException>(() => Symbol.Converter.ConvertTo(HighColor.Ultraviolet, typeof(Symbol<LowColor>)));

			Assert.Throws<InvalidCastException>(() => Convert.ChangeType(Color.Black, typeof(IEnumerable<Symbol>), null));
		}

		[Fact]
		void ConversionFromInvalidValueFails()
		{
			Assert.Throws<NotSupportedException>(() => Symbol.Converter.ConvertFrom(null));
		}

		[Fact]
		void SymbolFieldsMustBeReadonly()
		{
			Assert.Throws<InvalidOperationException>(() => SymbolEnum.InitEnum<EnumWithReadWriteField>());
		}

		[Fact]
		void Serialization()
		{
			var symbol = Color.Black;
			var clone = CloneBySerialization(symbol);

			clone.AssertEqual(symbol);
		}

		[Fact]
		void SerializationOfTypedSymbol()
		{
			var symbol = Color.White;
			var clone = CloneBySerialization(symbol);

			clone.AssertEqual(symbol);
		}

		/*
		public static Stream Serialize(object obj)
		{
			IFormatter formatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
			Stream stream = new MemoryStream();
			formatter.Serialize(stream, obj);
			stream.Seek(0, SeekOrigin.Begin);
			return stream;
		}

		public static T Deserialize<T>(Stream stream)
		{
			IFormatter formatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
			
			return (T)formatter.Deserialize(stream);
		}

		public static T CloneBySerialization<T>(T obj)
		{
			return Deserialize<T>(Serialize(obj));
		}

		/*/
		public static string Serialize<T>(T obj, params Type[] knownTypes)
		{
			var output = new StringBuilder();
			using (var textWriter = new StringWriter(output))
			using (var xmlWriter = new XmlTextWriter(textWriter) { Formatting = Formatting.Indented } )
			{
				XmlObjectSerializer serializer = new DataContractSerializer(typeof(T), knownTypes);

				serializer.WriteObject(xmlWriter, obj);
			}
			
			return output.ToString();
		}

		public static T Deserialize<T>(string serializedSymbol, params Type[] knownTypes)
		{
			using (var textReader = new StringReader(serializedSymbol))
			using (var xmlReader = new XmlTextReader(textReader))
			{
				XmlObjectSerializer serializer = new DataContractSerializer(typeof(T), knownTypes);
				return (T)serializer.ReadObject(xmlReader);
			}
		}

		public static T CloneBySerialization<T>(T obj)
		{
			return Deserialize<T>(Serialize(obj));
		}
		//*/
	}

	public static class AssertExt
	{
		private static bool SymbolEqual(Symbol expected, Symbol actual)
		{
			//about object equality: http://www.ikriv.com/en/prog/info/dotnet/ObjectEquality.html

			if ((expected != actual) || (actual != expected)) return false;

			if (!(expected == actual) || !(actual == expected)) return false;

			if (!expected.Equals(actual) || !actual.Equals(expected)) return false;

			if (!Symbol.Comparer.Equals(actual, expected)) return false;
			if (!Symbol.Comparer.Equals(expected, actual)) return false;

			return expected.GetHashCode() == actual.GetHashCode();
		}

		public static void AssertNull(this object obj)
		{
			Assert.Null(obj);
		}

		public static T AssertNotNull<T>(this T obj) where T : class
		{
			Assert.NotNull(obj);
			return obj;
		}

		public static T AssertEqual<T>(this T actual, T expected)
		{
			if (expected is Symbol) {
				if (!SymbolEqual(expected as Symbol, actual as Symbol)) throw new EqualException(expected, actual);
			} else {
				Assert.Equal(expected, actual);
			}
			return actual;
		}

		public static T AssertNotEqual<T>(this T actual, T expected)
		{
			if (expected is Symbol) {
				if (SymbolEqual(expected as Symbol, actual as Symbol)) throw new NotEqualException();
			} else {
				Assert.Equal(expected, actual);
			}
			return actual;
		}

		public static T AssertIsType<T>(this object obj)
		{
			return Assert.IsType<T>(obj);
		}

		public static T AssertIsAssignableFrom<T>(this T obj, Type expectedType)
		{
			Assert.IsAssignableFrom(expectedType, obj);
			return obj;
		}
	}
}
