﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JpLabs.Symbols
{
	public abstract class SymbolEnum
	{
		public SymbolEnum()
		{
			throw new InvalidOperationException("SymbolEnum should not be instantiated");
		}

		public static void InitEnum<TDeclaringType>()
		{
			var declaringType = typeof(TDeclaringType);

			foreach (var fi in Symbol.GetSymbolFields(declaringType, false)) {
				if (!fi.IsInitOnly) throw new InvalidOperationException("Symbol fields must be readonly");

				//TOOD: Should I care if the field was initialized?
				//object currentValue = fi.GetValue(null);
				//if (currentValue == null) throw new InvalidOperationException("Symbol fields must be initialized with Symbol.Auto");

				var symbol = Symbol.Declare(declaringType, fi.Name).ConvertTo(fi.FieldType);
				fi.SetValue(null, symbol);
			}
		}
	}
}
