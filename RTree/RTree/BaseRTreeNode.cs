﻿using System;
using System.Collections.Generic;
using System.Linq;
using JpLabs.Geometry;

namespace JpLabs.RTree
{
	/// <summary>
	/// Base class for R-tree-nodes
	/// </summary>
	internal abstract class BaseRTreeNode : IRTreeNode
	{
		public ITree Tree			{ get { return rTree; } }
		public IRTreeNode Parent	{ get; set; }
		public IShape Shape			{ get { return bBox; } }
		public virtual int Level	{ get; protected set; }
		public bool IsLeaf			{ get {return Level == 0;} }
		public bool IsBranch		{ get {return Level != 0;} }

		ITreeNode ITreeNode.Parent
		{
			get { return this.Parent; }
			set { this.Parent = (IRTreeNode)value; }
		}
		
		public abstract IEnumerable<IRTreeEntry> Query(ISpatialQuery query);
		public abstract IRTreeNode Add(ITreeNode child);
		public abstract int TotalCount { get; }

		protected RStarTree rTree	{ get; private set; }
		protected IBoundingBox bBox	{ get; private set; }

		private ICollection<ITreeNode> ChildrenColl	{ get; set; }
		public IEnumerable<ITreeNode> Children	{ get { return ChildrenColl; } }
		public int ChildrenCount				{ get { return ChildrenColl.Count; } }
		
		public BaseRTreeNode(RStarTree tree)
		{
			ChildrenColl = new RChildrenNodeCollection(this);
			this.rTree = tree;
			bBox = BoundingBoxExt.GetVoidBoundingBox();
		}

		public BaseRTreeNode(RStarTree tree, IEnumerable<ITreeNode> children) : this(tree)
		{
			if (children.Count() > rTree.Max) throw new ArgumentException("Number of children exceeds tree threshold");

			ChildrenColl = new RChildrenNodeCollection(this, children);
			
			UpdateSelfShapeFromChildren();
		}

		protected void UpdateSelfShapeFromChildren()
		{
			bBox = Children.GetBoundingBox();
		}
		
		protected IRTreeNode AddChild(ITreeNode child)
		{
			//1. Add child to children
			ChildrenColl.Add(child);
			
			if (ChildrenCount <= rTree.Max) {
				//1.1. No overflow occurred => update shape
				UpdateSelfShapeFromChildren();
				return null;
			} else if (rTree.CanReinsertAtLevel(this.Level)) {
				//1.2. Overflow occurred AND can reinsert at this level => perform reinsert
				PerformReinsert();
				return null;
			} else {
				//1.3. Else => Split this node in two and return new node
				return SplitNode();
			}
		}
		
		public virtual IEnumerable<ITreeNode> Remove(ITreeNode child)
		{
			//1. Remove child from this node
			if (ChildrenColl.Remove(child)) {
				if (Parent != null && ChildrenCount < rTree.Min) {
					//1.1. If this node is not a root AND became too uncrowded
					//	   Then remove this from parent and ask to reinsert the remaining children
					return Parent.Remove(this).Concat(Children);
				} else {
					//1.2. If this node is a root OR has enough children
					//	   Then simply update shape
					UpdateSelfShapeFromChildren();
					return Enumerable.Empty<ITreeNode>();
				}
			} else {
				throw new InvalidOperationException("A node to be removed was not found");
			}
		}

		protected IRTreeNode SplitNode()
		{
			//1. Partition children in two groups
			Pair<IEnumerable<ITreeNode>> partition = this.Tree.Partition.Partition(ChildrenColl);
			
			//2. Remove one group from this node and move them to a new node
			foreach (var n in partition.Two) ChildrenColl.Remove(n);
			IRTreeNode splitNode
				= (this.IsLeaf)
				? (IRTreeNode)new RTreeLeaf(rTree, partition.Two.Cast<IRTreeEntry>())
				: (IRTreeNode)new RTreeBranch(rTree, partition.Two.Cast<IRTreeNode>());
			
			//3. Adjust shape of this node
			UpdateSelfShapeFromChildren();
			
			//4. Return a split node {should be sibling to 'this'}
			return splitNode;
		}
		
		protected void PerformReinsert()
		{
			//1. Get farthest N nodes
			var center = this.bBox.Center;
			var nodesToReinsert =
				Children.Select((n) =>
					new { Node = n, Distance = center.DistanceSquared(n.Shape.GetBoundingBox().Center) }
				)
				.OrderByDescending(s => s.Distance)
				.Take(rTree.ResinsertNodeCount)
				.Select(s => s.Node)
				.ToArray();
			
			//2. Remove them from this node
			foreach (var n in nodesToReinsert) ChildrenColl.Remove(n);
			
			//3. Update shape
			UpdateSelfShapeFromChildren();
			
			//4. Reinsert nodes into the tree
			rTree.Reinsert(nodesToReinsert, this.Level);
		}
	}
}
