﻿using System;
using System.Collections.Generic;
using JpLabs.Geometry;

namespace JpLabs.RTree
{
	public interface ITree
	{
		//Constants
		int Dimension { get; }
		int Max { get; }
		int Min { get; }
		ITreePartition Partition { get; }
		
		//Variables
		IBoundingBox Box { get; }
		int Height { get; }
		int Count { get; }
		
		//Functions
		void Add(IRTreeEntry item);
		bool Remove(IRTreeEntry entry, Func<IRTreeEntry,IRTreeEntry,bool> comparer); //bool Remove(IRTreeEntry item);
		IEnumerable<IRTreeEntry> Query(ISpatialQuery query);
	}
	
	public interface ITreeNode
	{
	    IShape Shape { get; }
		ITreeNode Parent { get; set; }
	}

	public interface IRTreeEntry : ITreeNode
	{
	    object Data { get; }
	}
	
	public class RTreeEntry : IRTreeEntry
	{
		public object Data		{ get; set; }
		public ITreeNode Parent	{ get; set; }
	    public IShape Shape		{ get; set; }
		
		public RTreeEntry(object data, IShape shape)
		{
			Data = data;
			Shape = shape;
		}
	}
	
	public interface IRTreeNode : ITreeNode
	{
		ITree Tree { get; }
		new IRTreeNode Parent { get; set; }
		bool IsLeaf { get; }
		bool IsBranch { get; }
		int Level { get; }
		
		IEnumerable<ITreeNode> Children { get; }
		int ChildrenCount { get; }
		int TotalCount { get; }
		
		IEnumerable<IRTreeEntry> Query(ISpatialQuery query);
		
		/// <summary>Insert a new entry in this node or a child node</summary>
		/// <remarks>Is called in a top-down fashion</remarks>
		/// <returns>A new sibling splitted node, if any</returns>
		IRTreeNode Add(ITreeNode child);
		
		/// <summary>Remove a child from this node</summary>
		/// <remarks>Is called in a bottom-up fashion</remarks>
		/// <returns>A list of nodes that should be reinserted</returns>
		IEnumerable<ITreeNode> Remove(ITreeNode child);
	}

	public interface ITreePartition
	{
		Pair<IEnumerable<ITreeNode>> Partition(IEnumerable<ITreeNode> source);
	}

	//internal interface IRTreeNode : ITreeNode
	//{
	//    //IEnumerable<ITreePath> QueryPath(ISpatialQuery query, IEnumerable<ITreeNode> initialPath);
	//}
	//public interface ITreePath
	//{
	//    IEnumerable<IRTreeNode> Path { get; }
	//    IEntry Entry { get; }
	//}

	//internal class RTreePath
	//{
	//    IEnumerable<IRTreeNode> Path { get; set; }
	//    IRTreeEntry Entry { get; set; }
	//    public RTreePath(IEnumerable<IRTreeNode> path, IRTreeEntry entry)
	//    {
	//        Path = path;
	//        Entry = entry;
	//    }
	//}
	
	/*
	public class RTreeInsertResult
	{
		public RTreeInsertResult(ITreeNode sourceNode)
		{
			SourceNode = sourceNode;
		}

		public RTreeInsertResult(ITreeNode sourceNode, ITreeNode splitNode)
		{
			SourceNode = sourceNode;
			SplitNode = splitNode;
		}
		
		public bool IsSplit { get { return SplitNode != null; } }
		
		public ITreeNode SourceNode	{ get; private set; }
		public ITreeNode SplitNode	{ get; private set; }
		
		public IEnumerable<ITreeNode> Nodes
		{
			get {
				yield return SourceNode;
				if (SplitNode != null) yield return SplitNode;
			}
		}
	}//*/
}
