﻿using System;
using System.Collections.Generic;
using System.Linq;
using JpLabs.Geometry;

namespace JpLabs.RTree
{
	// R* Tree approach
		
	public class RStarTree : ITree
	{
		protected IRTreeNode root;

		public int Dimension { get; protected set; }
		
		public int Max { get; protected set; }
		public int Min { get; protected set; }
		
		public ITreePartition Partition { get; protected set; }
		public int LeastOverlapEnlargementTests { get; protected set; }
		public int ResinsertNodeCount { get; protected set; }
		public bool ResinsertEnabled { get; protected set; }

		public IBoundingBox Box	{ get { return root.Shape.GetBoundingBox(); } }
		public int Height		{ get { return root.Level; } }
		public int Count		{ get { return root.TotalCount; } }
		
		private ReinsertContext currentReinsertContext;

		public RStarTree(int dimension, int maxChildrenPerNode) : this(dimension, maxChildrenPerNode, 0)
		{}
		
		public RStarTree(int dimension, int maxChildrenPerNode, double reinsertRatio)
		{
			Dimension = dimension;
			Max = maxChildrenPerNode;
			Min = (int)Math.Round((Max + 1) / 3d);
			LeastOverlapEnlargementTests = (int)Math.Ceiling(Math.Log(Max, 2));
			ResinsertNodeCount			 = (int)Math.Round((Max + 1) * reinsertRatio);;
			ResinsertEnabled			 = (ResinsertNodeCount != 0);
			
			root = new RTreeLeaf(this);
			
			Partition = new RStarPartition(this);
			//Partition = new ExaustivePartition(this); //exastive partition is slower for adding, but faster for searching
		}
		
		public void Add(IRTreeEntry entry)
		{
			AddNode(entry, ResinsertEnabled);
		}

		protected void AddNode(ITreeNode node)
		{
			AddNode(node, ResinsertEnabled);
		}
		
		protected void AddNode(ITreeNode node, bool newReinsertContext)
		{
			if (root == null) throw new ArgumentNullException();
			
			if (newReinsertContext) currentReinsertContext = new ReinsertContext(this);
			
			//1. Insert node on root
			IRTreeNode splitNode = root.Add(node);
			
			//2. If split happened, create a branch and put it in the root
			if (splitNode != null) root = new RTreeBranch(this, EnumerableExt.YieldMany(root, splitNode));
			
			if (newReinsertContext) currentReinsertContext = null;
		}

		public bool Remove(IRTreeEntry entry)
		{
			return Remove(entry, null);
		}
		
		public bool Remove(IRTreeEntry entry, Func<IRTreeEntry,IRTreeEntry,bool> equalityComparer)
		{
			//1. Find node to remove
			if (equalityComparer == null) equalityComparer = Object.Equals;//RTreeEntryExt.EntryDataEquals;
			var q = root.Query(new EntryQuery(equalityComparer){ Entry = entry});
			IRTreeEntry e = q.SingleOrDefault();
			
			if (e == null) {
				//Not was not found => return false
				return false;
			} else {
				//2. Remove node from its parent
				var parent = (IRTreeNode)e.Parent;
				var reinsertList = parent.Remove(e).ToArray(); //.ToArray() is optional
				
				if (reinsertList.Any()) {
					//3. When a shrinking happens, a list of removed nodes is returned. They are reinserted here
					foreach (var n in reinsertList) this.AddNode(n);
					
					//4. Test if root became a single child Branch
					if (root.ChildrenCount == 1 && root is RTreeBranch) {
						root = ((RTreeBranch)root).Nodes.Single();
						root.Parent = null;
					}
				}
				return true;
			}
		}
		
		internal bool CanReinsertAtLevel(int level)
		{
			return (ResinsertEnabled) ? currentReinsertContext.CanReinsertAtLevel(level) : false;
		}
		
		internal void Reinsert(IEnumerable<ITreeNode> nodes, int level)
		{
			currentReinsertContext.Reinsert(nodes, level);
		}
	
		public IEnumerable<IRTreeEntry> Query(ISpatialQuery query)
		{
			if (root == null) throw new ArgumentNullException();
			
			//TOREVIEW: should I use a .ToArray() here or not?
			//	- Advantage [of returning an array]: release the tree to be updated right after the query
			//	- Disadvantage: possibly enumerate more items than necessary
			return root.Query(query).ToArray();
		}
		
		class ReinsertContext
		{
			bool[] reinsertUsedTable;
			RStarTree rStarTree;
			
			public ReinsertContext(RStarTree tree)
			{
				rStarTree = tree;
				reinsertUsedTable = new bool[tree.Height + 1]; //Enumerable.Range(0, tree.RootLevel + 1).Select((i) => true).ToArray();
			}
			
			public bool CanReinsertAtLevel(int level)
			{
				return !reinsertUsedTable[level];
			}
			
			public void Reinsert(IEnumerable<ITreeNode> nodes, int level)
			{
				reinsertUsedTable[level] = true;
				
				foreach (var n in nodes) rStarTree.AddNode(n, false);
			}
		}
	}
}
