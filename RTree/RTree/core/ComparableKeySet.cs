﻿using System;

namespace JpLabs.Geometry
{
	internal class ComparableKeySet : IComparable<ComparableKeySet>
	{
		IComparable[] keys;

		public ComparableKeySet(params IComparable[] keys)
		{
			this.keys = keys;
		}
		
		public IComparable GetKey(int index)
		{
			return keys[index];
		}
		
		/// <remarks> Only works when the other key set has the same set of keys, in the same order </remarks>
		public int CompareTo(ComparableKeySet other)
		{
			for (int i=0; i<keys.Length; i++) {
				var comp = this.GetKey(i).CompareTo(other.GetKey(i));
				if (comp != 0) return comp;
			}
			return 0; //all provided keys all equal
		}
	}
}
