﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace JpLabs.Geometry
{
	internal static class EnumerableExt
	{
		static public IEnumerable<T> ToEnumerable<T>(T item)
		{
			return new [] { item }; //yield return item;
		}

		static public IEnumerable<T> YieldMany<T>(params T[] items)
		{
			return items;
		}

		static public IEnumerable<T> Concat<T>(this IEnumerable<T> source, params T[] other)
		{
			return source.Concat((IEnumerable<T>)other);
		}
		
		//static public void ForEach<TSource>(this IEnumerable<TSource> source, Action<TSource> action)
		//{
		//    foreach (var i in source) action(i);
		//}

		/*
		[Obsolete]
		static public int MinIndex<TSource,TResult>(this IEnumerable<TSource> source, Func<TSource,TResult> selector)
		where TResult : IComparable<TResult>
		{
			var select = source.Select(
				(item, index) => new {Index = index, Value = selector(item)}
			);
			
			var min = select.Aggregate(
				(i1, i2) => (i1.Value.CompareTo(i2.Value) < 0) ? i1 : i2
			);
			
			return min.Index;
		}
		
		[Obsolete]
		static public TSource MinItem<TSource,TValue>(this IEnumerable<TSource> source, Func<TSource,TValue> valueSelector)
		where TValue : IComparable<TValue>
		{
			//var select = source.Select(
			//    (item) => new {Item = item, Value = selector(item)}
			//);
			//var min = select.Aggregate(
			//    (i1, i2) => (i1.Value.CompareTo(i2.Value) < 0) ? i1 : i2
			//);
			//return min.Item;
			
			return 
				source.Select(
					(item) => new {Item = item, Value = valueSelector(item)}
				)
				.Aggregate(
					(i1, i2) => (i1.Value.CompareTo(i2.Value) < 0) ? i1 : i2
				).Item;
		}
		//*/
		
		/// <summary>
		/// Computes a key for each item in the <paramref name="source"/> using <paramref name="keySelector"/>;
		/// Find the item with the smaller key (according to the <paramref name="keyComparer"/>);
		/// Return the item (and throws away the key)
		/// </summary>
		static public TSource MinItem<TSource,TKey>(
		    this IEnumerable<TSource> source,
		    Func<TSource,TKey> keySelector,
		    Func<TKey,TKey,int> keyComparer
		)
		{
			return
			    source.Select(
			        (item) => new {Item = item, Key = keySelector(item)}
			    )
			    .Aggregate(
			        (s1, s2) => (keyComparer(s1.Key, s2.Key) < 0) ? s1 : s2
			    )
			    .Item;
		}

		/// <summary>
		/// Computes a key for each item in the <paramref name="source"/> using <paramref name="keySelector"/>;
		/// Find the item with the smaller key (key has to be <see cref="IComparable"/> to itself);
		/// Return the item (and throws away the key)
		/// </summary>
		static public TSource MinItem<TSource,TKey>(
			this IEnumerable<TSource> source,
			Func<TSource,TKey> keySelector
		)
			where TKey : IComparable<TKey>
		{
			return
			    source.Select(
			        (item) => new {Item = item, Key = keySelector(item)}
			    )
			    .Aggregate(
			        (s1, s2) => (s1.Key.CompareTo(s2.Key) < 0) ? s1 : s2
			    )
			    .Item;
		}
	}
}
