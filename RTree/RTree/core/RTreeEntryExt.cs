﻿
namespace JpLabs.RTree
{
	public static class RTreeEntryExt
	{
		//static public EqualityComparer<IRTreeEntry> CreateComparer()
		//{
		//    Type c = typeof(T);
		//    if (c == typeof(byte))
		//    {
		//        return (EqualityComparer<T>) new ByteEqualityComparer();
		//    }
		//    if (typeof(IEquatable<T>).IsAssignableFrom(c))
		//    {
		//        return (EqualityComparer<T>) typeof(GenericEqualityComparer<int>).TypeHandle.CreateInstanceForAnotherGenericParameter(c);
		//    }
		//    if (c.IsGenericType && (c.GetGenericTypeDefinition() == typeof(Nullable<>)))
		//    {
		//        Type type2 = c.GetGenericArguments()[0];
		//        if (typeof(IEquatable<>).MakeGenericType(new Type[] { type2 }).IsAssignableFrom(type2))
		//        {
		//            return (EqualityComparer<T>) typeof(NullableEqualityComparer<int>).TypeHandle.CreateInstanceForAnotherGenericParameter(type2);
		//        }
		//    }
		//    return new ObjectEqualityComparer<T>();
		//}
		
		static public bool EntryDataEquals(this IRTreeEntry x, IRTreeEntry y)
		{
			//TODO: deal correctly (and rapidly) IEquatable<int> entries
			
			return (x != null)
				? ((y != null) && ObjEquals(x.Data, y.Data))
				: (y == null);
		}

		static public bool ObjEquals(object x, object y)
		{
			return (x != null)
				? ((y != null) && (x == y))
				: (y == null);
		}
	}
}
