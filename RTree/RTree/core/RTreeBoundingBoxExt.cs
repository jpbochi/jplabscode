﻿using System;
using System.Collections.Generic;
using System.Linq;
using JpLabs.Geometry;

namespace JpLabs.RTree
{
	public static class RTreeBoundingBoxExt
	{
		//static public T Enlargement<T>(this IBoundingBox<T> b, IBoundingBox<T> other) where T : struct, IComparable
		//{
		//    if (typeof(T) == typeof(int))	 return (T)Enlargement((IBoundingBox<int>)b, (IBoundingBox<int>)other);
		//    if (typeof(T) == typeof(double)) return (T)Enlargement((IBoundingBox<double>)b, (IBoundingBox<double>)other);
		//    throw new NotSupportedException();
		//}

		static public IComparable Enlargement(this IBoundingBox b, IBoundingBox other)
		{
		    return b.Merge(other).Volume - b.Volume;
		}

		//static public double Enlargement(this IBoundingBox<double> b, IBoundingBox<double> other)
		//{
		//    return b.Merge(other).Volume - b.Volume;
		//}

		static public IBoundingBox GetBoundingBox(this IEnumerable<ITreeNode> source)
		{
			//return GetBoundingBox(source.Select(node => node.Shape));
			return GetBoundingBox(source.Select(node => node.Shape.GetBoundingBox()));
		}

		static public IBoundingBox GetBoundingBox(this IEnumerable<IShape> source)
		{
			//return source.Aggregate(
			//    //(IBoundingBox)null,
			//    BoundingBoxExt.GetVoidBoundingBox(),
			//    (partial, s) => partial.Merge(s.GetBoundingBox())
			//);
			return GetBoundingBox(source.Select(shape => shape.GetBoundingBox()));
		}

		static public IBoundingBox GetBoundingBox(this IEnumerable<IBoundingBox> source)
		{
			/*
			return source.Aggregate(
				(IBoundingBox)null, //IBoundingBoxExt.GetVoidBoundingBox(),
				(partial, b) => partial.Merge(b)
			);
			/*/
			return source.MergeAll();
			//*/
		}
	}

}
