﻿using System.Collections;
using System.Collections.Generic;

namespace JpLabs.RTree
{
	/// <summary>
	/// A collection of children R-tree-nodes
	/// Main purpose is controlling the Parent property of items
	/// </summary>
	internal class RChildrenNodeCollection : ICollection<ITreeNode>
	{
		ICollection<ITreeNode> innerColl;
		ITreeNode owner;
		
		public RChildrenNodeCollection(ITreeNode owner)
		{
			this.owner = owner;
			this.innerColl = new List<ITreeNode>();
		}

		public RChildrenNodeCollection(ITreeNode owner, IEnumerable<ITreeNode> children)
		{
			this.owner = owner;
			this.innerColl = new List<ITreeNode>(children);
			foreach (var c in innerColl) c.Parent = owner;
		}

		void ICollection<ITreeNode>.Add(ITreeNode item)
		{
			item.Parent = owner;
			innerColl.Add(item);
		}

		void ICollection<ITreeNode>.Clear()
		{
			foreach (var c in innerColl) c.Parent = null;
			innerColl.Clear();
		}

		bool ICollection<ITreeNode>.Contains(ITreeNode item)
		{
			return innerColl.Contains(item);
		}

		void ICollection<ITreeNode>.CopyTo(ITreeNode[] array, int arrayIndex)
		{
			innerColl.CopyTo(array, arrayIndex);
		}

		int ICollection<ITreeNode>.Count
		{
			get { return innerColl.Count; }
		}

		bool ICollection<ITreeNode>.IsReadOnly
		{
			get { return false; }
		}

		bool ICollection<ITreeNode>.Remove(ITreeNode item)
		{
			if (innerColl.Remove(item)) {
				item.Parent = null;
				return true;
			} else {
				return false;
			}
		}

		IEnumerator<ITreeNode> IEnumerable<ITreeNode>.GetEnumerator()
		{
			return innerColl.GetEnumerator();
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return innerColl.GetEnumerator();
		}
	}
}
