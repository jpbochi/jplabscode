﻿using System;
using System.Collections.Generic;
using System.Linq;
using JpLabs.Geometry;

namespace JpLabs.RTree
{
	public interface ISpatialQuery
	{
		//bool RequestPath { get; }

		bool TestBoundingBox(IBoundingBox b);
		bool TestEntry(IRTreeEntry e);
	}

	public static class NearestNeighborsExt
	{
	    static public IEnumerable<TNode> Nearest<TNode>(this IEnumerable<TNode> source, IPoint point, int neighbors)
			where TNode : ITreeNode
	    {
			//TODO: implement a more correct way of fiding the distance between a point and a box
			
	        return source
				.Select(
					n => new { Node = n, Distance = n.Shape.GetBoundingBox().Center.DistanceSquared(point) }
	            )
	            .OrderBy(s => s.Distance)
	            .Take(neighbors)
	            .Select(s => s.Node);
	    }
	}

	public class GetAllQuery : ISpatialQuery
	{
	    public bool TestBoundingBox(IBoundingBox b)
	    {
			return true;
	    }

		public bool TestEntry(IRTreeEntry entry)
		{
			return true;
		}
	}

	public class IntersectsShapeQuery : ISpatialQuery
	{
	    public IShape Shape { get; private set; }
	    public IBoundingBox BoundingBox { get; private set; }
		
	    public IntersectsShapeQuery(IShape shape)
	    {
	        Shape = shape;
	        BoundingBox = shape.GetBoundingBox();
	    }
	    
	    public bool TestBoundingBox(IBoundingBox b)
	    {
			return b.Intersects(BoundingBox);
	    }

		public bool TestEntry(IRTreeEntry entry)
		{
			return Shape.Intersects(entry.Shape);
		}
	}

	/*
	public class FuncSpatialQuery : ISpatialQuery
	{
		Func<IBoundingBox,bool> funcTestBoundingBox;
		Func<IRTreeEntry,bool> funcTestEntry;
		
	    public SpatialQuery(Func<IBoundingBox,bool> testBoundingBox, Func<IRTreeEntry,bool> testEntry)
	    {
			funcTestBoundingBox = testBoundingBox;
			funcTestEntry = testEntry;
	    }

	    public bool TestBoundingBox(IBoundingBox b)
	    {
			return funcTestBoundingBox(b);
	    }

		public bool TestEntry(IRTreeEntry e)
		{
			return funcTestEntry(e);
		}
	}//*/

	public class EntryQuery : ISpatialQuery
	{
		public EntryQuery()
		{
			EqualityComparer = RTreeEntryExt.EntryDataEquals;
		}

		public EntryQuery(Func<IRTreeEntry,IRTreeEntry,bool> equalityComparer)
		{
			EqualityComparer = equalityComparer ?? RTreeEntryExt.EntryDataEquals;
		}
		
		private IRTreeEntry entry;
	    public IRTreeEntry Entry
		{
			get { return entry; }
			set {
				entry = value;
				BoundingBox = entry.Shape.GetBoundingBox();
			}
		}
	    public IBoundingBox BoundingBox { get; private set; }
	    
	    public Func<IRTreeEntry,IRTreeEntry,bool> EqualityComparer  { get; private set; }

	    public bool TestBoundingBox(IBoundingBox b)
	    {
			return b.Contains(BoundingBox);
	    }

		public bool TestEntry(IRTreeEntry e)
		{
			//return (EqualityComparer != null) ? EqualityComparer(this.entry, e) : this.entry.EntryEquals(e);
			return EqualityComparer(this.entry, e);
		}
	}

	public class BruteForceQuery : ISpatialQuery
	{
	    public IRTreeEntry Entry { get; private set; }
	    public IBoundingBox BoundingBox { get; private set; }
	    
	    public BruteForceQuery(IRTreeEntry entry)
	    {
	        Entry = entry;
	        BoundingBox = entry.Shape.GetBoundingBox();
	    }

	    public bool TestBoundingBox(IBoundingBox b)
	    {
			return true;
	    }

		public bool TestEntry(IRTreeEntry e)
		{
			//var p1 = e.Shape as Point3DInt;
			//var p2 = Entry.Shape as Point3DInt;
			//if (p1 != null && p2 != null) {
			//    var d = (p1 - p2).Length;
			//}
			return this.Entry.EntryDataEquals(e);//this.Entry.Data == e.Data;
		}
	}
}
