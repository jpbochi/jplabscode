﻿using System.Collections.Generic;
using System.Linq;
using JpLabs.Geometry;
using TNumeric = System.Double;

namespace JpLabs.RTree
{
	internal sealed class ExaustivePartition : ITreePartition
	{
		private ITree tree;
		
		public ExaustivePartition(ITree tree)
		{
			this.tree = tree;
		}
		
		public Pair<IEnumerable<ITreeNode>> Partition(IEnumerable<ITreeNode> source)
		{
			ITreeNode[] list = source.ToArray();
			TNumeric min;
			var pair = MinExaustive(
				out min,
				Enumerable.Empty<ITreeNode>(),
				Enumerable.Empty<ITreeNode>(),
				list, 0
			);
			
			return pair;
		}
		
		private Pair<IEnumerable<ITreeNode>> MinExaustive(out TNumeric outMin, IEnumerable<ITreeNode> l, IEnumerable<ITreeNode> r, ITreeNode[] list, int index)
		{
			if (index < list.Length) {
				TNumeric minL; var pairL = MinExaustive(out minL, l.Concat(list[index]), r, list, index + 1);
				TNumeric minR; var pairR = MinExaustive(out minR, l, r.Concat(list[index]), list, index + 1);
				if (minL < minR) { outMin = minL; return pairL; }
				else			 { outMin = minR; return pairR; }
			} else {
				//if (Math.Abs(l.Count() - r.Count()) > 2) { //TOREVIEW: differences bigger than 1 are experimental
				if (l.Count() < tree.Min ||  r.Count() < tree.Min) {
					outMin = int.MaxValue;
					return null;
				} else {
					var bbL = l.GetBoundingBox();
					var bbR = r.GetBoundingBox();
					outMin = bbL.Volume + bbR.Volume;
					return new Pair<IEnumerable<ITreeNode>>(l, r);
				}
			}
		}
	}
}
