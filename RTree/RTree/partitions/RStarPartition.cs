﻿using System.Collections.Generic;
using System.Linq;
using JpLabs.Geometry;
using TNumeric = System.Double;

namespace JpLabs.RTree
{
	internal sealed class RStarPartition : ITreePartition
	{
		ITree tree;
		int MinSplit;
		int MaxSplit;
		
		public RStarPartition(ITree tree)
		{
			this.tree = tree;
			MinSplit = tree.Min;
			MaxSplit = tree.Max + 1 - tree.Min;
		}

		public Pair<IEnumerable<ITreeNode>> Partition(IEnumerable<ITreeNode> source)
		{
			var sortedLists = ChooseSplitAxis(source);
			var splits = YieldSplits(sortedLists);
			return ChooseSplit(splits);
		}
		
		private IEnumerable<IEnumerable<ITreeNode>> ChooseSplitAxis(IEnumerable<ITreeNode> source)
		{
			//0. Pre calculating the bounding box of each node
			var select = source.Select(n => new { Node = n, Box = n.Shape.GetBoundingBox() }).ToArray();
			
			//1. For each axis, find the one with the minimum surface sum
			IEnumerable<IEnumerable<ITreeNode>> retLists = null;
			TNumeric minSurfaceSum = TNumeric.MaxValue;
			for (int axis = 0; axis < tree.Dimension; axis++) {
				//1.1. Get two sorted lists (one sorted by lower boundaries and other by upper boundaries)
				var sortedLower = select.OrderBy(s => s.Box.Lower[axis]).ToArray();
				var sortedUpper = select.OrderBy(s => s.Box.Upper[axis]).ToArray();
				
				//1.2. Use just one list if they are equal
				bool useSingleList = (sortedLower.SequenceEqual(sortedUpper));
				var lists
					= useSingleList
					? EnumerableExt.ToEnumerable(sortedLower)
					: EnumerableExt.YieldMany(sortedLower, sortedUpper);
				
				//1.3. For each list, find all splits
				var boxSorts = lists.Select(sort => sort.Select(s => (IBoundingBox)s.Box));
				TNumeric surfaceSum =
					Enumerable
					.Range(MinSplit, MaxSplit - MinSplit + 1)
					.SelectMany(
						(splitIndex) => boxSorts,
						(splitIndex, sorted) => Pair.Create(
							sorted.Take(splitIndex),
							sorted.Skip(splitIndex)
						)
					)
					//1.4. For each split, find the added surface of each side; then sum all added surfaces
					.Sum(
						(split) => split.One.GetBoundingBox().Surface
								 + split.Two.GetBoundingBox().Surface
					);
				if (useSingleList) surfaceSum *= 2;
				
				//1.5. Test if smaller surface sum was found
				if (surfaceSum < minSurfaceSum) {
					minSurfaceSum = surfaceSum;
					retLists = lists.Select(sort => sort.Select(s => s.Node));
				}
			}
			//2. Return the winner sorted lists (or list)
			return retLists;
		}

		private IEnumerable<Pair<IEnumerable<ITreeNode>>> YieldSplits(IEnumerable<IEnumerable<ITreeNode>> sortedLists)
		{
			return
				//Find all possible splits using the provided sorted lists 
				Enumerable
				.Range(MinSplit, MaxSplit - MinSplit + 1)
				.SelectMany(
				    (splitIndex) => sortedLists,
				    (splitIndex, sorted) => Pair.Create(
						sorted.Take(splitIndex),
						sorted.Skip(splitIndex)
					)
				);
		}

		private Pair<IEnumerable<ITreeNode>> ChooseSplit(IEnumerable<Pair<IEnumerable<ITreeNode>>> splits)
		{
			return
				//*
				splits.MinItem(
					(split) => {
						var boxOne = split.One.GetBoundingBox();
						var boxTwo = split.Two.GetBoundingBox();
						return new ComparableKeySet(
							boxOne.Intersection(boxTwo).Volume,
							boxOne.Volume + boxTwo.Volume
						);
					}
				);
				/*/
				//1. For each split, calculate the bounding boxes of each side of each split
				splits.Select(
					split => new {
						Split = split,
						BoxPair = Pair.New(
							split.One.GetBoundingBox(),
							split.Two.GetBoundingBox()
						)
					}
				)
				//2. Order splits according to the overlap of the boxes
				//TOREVIEW: it's not necessary to get a full order because we want just the first item
				.OrderBy(
				    s => s.BoxPair.One.Intersection(s.BoxPair.Two).Volume
				)
				//3. Then, in case of ties order by the added volume of the boxes
				.ThenBy(
				    s => s.BoxPair.One.Volume
					   + s.BoxPair.Two.Volume
				)
				//4. Finnaly, return the split that came first in the order
				.Select(s => s.Split)
				.First();
				//*/
			
			//var x = q.Select(
			//    s => new {
			//        Overlap = s.BoxPair.One.Intersection(s.BoxPair.Two).Volume,
			//        TotalVolume = s.BoxPair.One.Volume + s.BoxPair.Two.Volume
			//    });
			//var y = q.Select(s => s.BoxPair.One.Intersection(s.BoxPair.Two));
			//var z = q.Select(s => new {F = s.BoxPair.One, S = s.BoxPair.Two});
		}
	}
}
