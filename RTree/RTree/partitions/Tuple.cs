﻿
namespace JpLabs.RTree
{
	public class Pair<T>
	{
		public T One { get; set; }
		public T Two { get; set; }
		
		public Pair(T one, T two)
		{
			One = one;
			Two = two;
		}
	}

	public static class Pair
	{
		/// <summary>
		/// This function is useful to create Pair's with explicitly specify the type T
		/// </summary>
		static public Pair<T> Create<T>(T one, T two)
		{
			return new Pair<T>(one, two);
		}
	}

	public class Tuple<T1, T2>
	{
		public T1 One { get; set; }
		public T2 Two { get; set; }
		
		public Tuple(T1 one, T2 two)
		{
			One = one;
			Two = two;
		}
	}
}
