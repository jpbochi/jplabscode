﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using JpLabs.Geometry;

namespace JpLabs.RTree
{
	/// <summary>
	/// An IRTreeNode that contains only IRTreeNodes as children, i.e., children are not IRTreeEntry
	/// </summary>
	internal class RTreeBranch : BaseRTreeNode, IRTreeNode, IEnumerable<IRTreeNode>
	{
		public IEnumerable<IRTreeNode> Nodes { get { return Children.Cast<IRTreeNode>(); } }

		public RTreeBranch(RStarTree tree, int level) : base(tree)
		{
			this.Level = level;
		}

		public RTreeBranch(RStarTree tree, IEnumerable<IRTreeNode> initialNodes) : base(tree, initialNodes.Cast<ITreeNode>())
		{
			if (ChildrenCount <= 0) throw new ArgumentException("'initialNodes' can't be empty");
			
			this.Level = Nodes.First().Level + 1;
		}

		public override int TotalCount
		{
			get { return Nodes.Sum(n => n.TotalCount); }
		}
		
		public override IEnumerable<IRTreeEntry> Query(ISpatialQuery query)
		{
			foreach (var node in Nodes) {
				if (query.TestBoundingBox(node.Shape.GetBoundingBox())) {
					foreach (var entry in node.Query(query)) yield return entry;
				}
			}
		}

		//public override IEnumerable<ITreePath> QueryPath(ISpatialQuery query, IEnumerable<ITreeNode> initialPath)
		//{
		//    foreach (var node in Nodes) {
		//        if (query.TestBoundingBox(node.Shape.GetBoundingBox())) {
		//            foreach (var path in node.QueryPath(query)) yield return path;
		//        }
		//    }
		//}

		public override IRTreeNode Add(ITreeNode child)
		{
			// 0. Test if a subtree is being added
			var node = child as IRTreeNode;
			if (node != null && node.Level == this.Level - 1) {
				return this.AddChild(child);
			} else {
			
				// 1. Choose Subtree
				//int chosenIndex = ChooseChildToAdd(child.Shape);
				//IRTreeNode chosenChild = (IRTreeNode)ChildrenList[chosenIndex];
				IRTreeNode chosenChild = (IRTreeNode)ChooseChildToAdd(child.Shape);
				
				// 2. Insert Item (splitting or not)
				var splitNode = chosenChild.Add(child);
				
				/// 3. Adjust tree as needed
				/*
				//if (res.SourceNode != chosenChild) ChildrenList[chosenIndex] = res.SourceNode;
				if (!res.IsSplit) {
					UpdateSelfShapeFromChildren();
					
					return ResultSelf();
				} else {
					return this.AddChild(res.SplitNode);
				}
				/*/
				if (splitNode == null) {
					UpdateSelfShapeFromChildren();
					return null;
				} else {
					//Add a node that will be a sibling of the chosen child
					return this.AddChild(splitNode);
				}
				//*/
			}
		}

		ITreeNode ChooseChildToAdd(IShape shape)
		{
			if (Level == 1) {
			    //Last branch before the leaves uses LeastOverlapEnlargement
			    return LeastOverlapEnlargement(shape);
			} else {
				return LeastEnlargement(shape);
			}
		}
		
		ITreeNode LeastEnlargement(IShape shape)
		{
			var newBox = shape.GetBoundingBox();
			
			return Children.MinItem(
				(ITreeNode c) => {
				    var box = c.Shape.GetBoundingBox();
				    return new ComparableKeySet(
						box.Enlargement(newBox),
						box.Volume
					);
				}
			);
		}

		ITreeNode LeastOverlapEnlargement(IShape shape)
		{
			var newBox = shape.GetBoundingBox();
			
			/*		
			return Children.MinItem(
				(ITreeNode child) => {
				    var originalBox = child.Shape.GetBoundingBox();
				    var enlargedBox = originalBox.Merge(newBox);
				    
				    var siblingBoxes = (
						from otherChild in Children
						where otherChild != child
						select otherChild.Shape.GetBoundingBox()).ToArray();
				    var origOverlap	= siblingBoxes.Sum((siblingBox) => originalBox.Intersection(siblingBox).Volume);
				    var newOverlap	= siblingBoxes.Sum((siblingBox) => enlargedBox.Intersection(siblingBox).Volume);
				    var overlapEnlargement = newOverlap - origOverlap;
					
				    return new ComparableKeySet(
						overlapEnlargement,
						originalBox.Enlargement(newBox),
						originalBox.Volume
					);
				}
			);
			/*/
			var preCalculated = Children.Select(c => {
				var box = c.Shape.GetBoundingBox();
				return new {
					Node = c,
					Box = box,
					Enlargement = box.Enlargement(newBox)
				};
			});
			var testNum = rTree.LeastOverlapEnlargementTests;
			var filtered = (
					(testNum < ChildrenCount)
					? preCalculated.OrderBy(s => s.Enlargement).Take(testNum)
					: preCalculated
				).ToArray();

			return filtered.MinItem(
				(s) => {
				    var enlargedBox = s.Box.Merge(newBox);
				    
				    var siblingBoxes = (
						from other in filtered
						where other.Node != s.Node
						select other.Node.Shape.GetBoundingBox()
					).ToArray();
				    var origOverlap	= siblingBoxes.Sum((siblingBox) => s.Box.Intersection(siblingBox).Volume);
				    var newOverlap	= siblingBoxes.Sum((siblingBox) => enlargedBox.Intersection(siblingBox).Volume);
				    var overlapEnlargement = newOverlap - origOverlap;
					
				    return new ComparableKeySet(
						overlapEnlargement,
						s.Enlargement,
						s.Box.Volume
					);
				}
			).Node;
			//*/
		}

		IEnumerator<IRTreeNode> IEnumerable<IRTreeNode>.GetEnumerator()
		{ return Nodes.GetEnumerator(); }
		
		IEnumerator IEnumerable.GetEnumerator()
		{ return Nodes.GetEnumerator(); }
	}
}
