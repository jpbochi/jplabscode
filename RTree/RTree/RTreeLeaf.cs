﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace JpLabs.RTree
{
	/// <summary>
	/// An IRTreeNode that contains only IRTreeEntry (data-nodes) as children
	/// </summary>
	internal class RTreeLeaf : BaseRTreeNode, IRTreeNode, IEnumerable<IRTreeEntry>
	{
		public IEnumerable<IRTreeEntry> Entries { get { return Children.Cast<IRTreeEntry>(); } }
		
		public RTreeLeaf(RStarTree tree) : base(tree)
		{}

		public RTreeLeaf(RStarTree tree, IEnumerable<IRTreeEntry> initialEntries) : base(tree, initialEntries.Cast<ITreeNode>())
		{}		

		/// <summary>The level of an RTreeLeaf is Zero, by definition</summary>
		public override int Level { get { return 0; } }

		public override int TotalCount { get { return this.ChildrenCount; } }
		
		public override IEnumerable<IRTreeEntry> Query(ISpatialQuery query)
		{
			foreach (var entry in Entries) {
				if (query.TestEntry(entry)) yield return entry;
			}
		}
		
		public override IRTreeNode Add(ITreeNode child)
		{
			return this.AddChild(child);
		}
		
		IEnumerator<IRTreeEntry> IEnumerable<IRTreeEntry>.GetEnumerator()
		{ return Entries.GetEnumerator(); }
		
		IEnumerator IEnumerable.GetEnumerator()
		{ return Entries.GetEnumerator(); }
	}
}
