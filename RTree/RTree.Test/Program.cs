﻿//#define USE_INTEGER_SYSTEM

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JpLabs.Geometry;
using System.Diagnostics;

#if USE_INTEGER_SYSTEM
using TNumeric = System.Int32;
#else
using TNumeric = System.Double;
#endif

/// http://en.wikipedia.org/wiki/R-tree
/// http://en.wikipedia.org/wiki/R*_tree
/// http://www-db.deis.unibo.it/courses/SI-LS/papers/Gut84.pdf
/// 
/// http://www.rtreeportal.org/
/// 
/// R-tree = http://www.sai.msu.su/~megera/postgres/gist/papers/gutman-rtree.pdf
/// R*-tree = http://dbs.mathematik.uni-marburg.de/publications/myPapers/1990/BKSS90.pdf
/// 
/// TODO:
/// - Remove all Debug.Assert() calls [done]
/// - Implement a batch load algorithm in the R*-tree
/// - Make all shapes (Point, Vector, BoundingBox, etc.) immutable!
/// - Consider changing BoundingBox to a struct

namespace JpLabs.RTree.Test
{
	class Program
	{
		static Random rnd = new Random();

		//Tree configuration
		const int NODE_SIZE			= 7;
		const double REINSERT_RATIO = 0; //2d / NODE_SIZE; //0 = no reinsert
		
		//Data configuration
		const int DIMENSIONS		= 3;
		const int NUM_NODES			= 10000;
		const double RATIO_OF_BOXES = 0.4;
		const int DELTA_DATA		= 42;
		const int DELTA_DATA_BOX	= 3;
		
		//Test configuration
		const int NUM_NODES_TO_TEST	= 1000;
		const int NUM_REMOVE_NODES	= 1000;
		const int NUM_QUERIES		= 500;		
		const double QUERY_VOLUME	= 1 / 32d;
		static int DELTA_QUERY		= (int)Math.Pow(QUERY_VOLUME * Math.Pow(DELTA_DATA * 2, DIMENSIONS), 1d / DIMENSIONS);
		
		static void Main(string[] args)
		{
			IList<IRTreeEntry> list = new List<IRTreeEntry>();
			{
				Console.Write("Generating List...");
				var w = Stopwatch.StartNew();
				for (int i=0; i<NUM_NODES; i++) list.Add(new RTreeEntry(i, NewRandomShape()));
				Console.WriteLine("\tDone {0}", w.Formatted());
			}
			
			var tree = new RStarTree(DIMENSIONS, NODE_SIZE, REINSERT_RATIO);
			{
				Console.Write("Generating Tree...");
				var w = Stopwatch.StartNew();
				foreach(IRTreeEntry e in list) tree.Add(e);
				Console.WriteLine("\tDone {0}", w.Formatted());
			}
			
			{
				Console.Write("Testing Tree...");
				var w = Stopwatch.StartNew();
				var query = new EntryQuery();
				for (int i=0; i<NUM_NODES_TO_TEST; i++) {
					var e = list[rnd.Next(list.Count)];
					
					query.Entry = e;
					var result = tree.Query(query).ToArray();
					IRTreeEntry unique = result.SingleOrDefault();
					if (unique == null) throw new ApplicationException();
				}
				Console.WriteLine("\t\tDone {0}", w.Formatted());
			}
			
			{
				Console.Write("Removing some items...");
				var w = Stopwatch.StartNew();
				for (int i=0; i<NUM_REMOVE_NODES; i++) {
					int r = rnd.Next(list.Count);
					tree.Remove(list[r]);
					list.RemoveAt(r);
				}
				Console.WriteLine("\tDone {0}", w.Formatted());
				
				Console.WriteLine("Items left in list: {0}", list.Count);
				Console.WriteLine("Items left in tree: {0}", tree.Count);
				Console.WriteLine();
				if (list.Count != tree.Count) throw new ApplicationException();
			}
			
			{
				Console.Write("Performing queries...");
				int totalTreeTests = 0;
				int totalBruteTests = 0;
				int totalResults = 0;
				
				Stopwatch treeWatch = new Stopwatch();
				Stopwatch bruteWatch = new Stopwatch();
				
				for (int i=0; i<NUM_QUERIES; i++) {
					var query = NewRandomQuery();
					
					treeWatch.Start();
					var resultsT = tree.Query(query).ToArray().AsEnumerable();
					treeWatch.Stop();
					totalTreeTests += query.testCount;
					
					query.testCount = 0;					
					bruteWatch.Start();
					var resultsF = list.Where(query.TestEntry).ToArray().AsEnumerable();
					bruteWatch.Stop();
					totalBruteTests += query.testCount;
					
					if (resultsT.Count() != resultsF.Count()) throw new ApplicationException();
					totalResults += resultsT.Count();
				}
				Console.WriteLine("\tDone {0} queries", NUM_QUERIES);
				
				var totalTreeTime = treeWatch.Elapsed;
				var totalBruteTime = bruteWatch.Elapsed;
				var avgResults = totalResults / (double)NUM_QUERIES;
				Console.WriteLine("              Avg nodes found: {0}", avgResults);
				{
					var avgTests = totalTreeTests / (double)NUM_QUERIES;
					var avgTime = TimeSpan.FromTicks(totalTreeTime.Ticks / NUM_QUERIES);
					Console.WriteLine("Tree        - Avg tests: {0:0.0000}\tAvg Time: {1}", avgTests, avgTime.Formatted());
				}
				{
					var avgTests = totalBruteTests / (double)NUM_QUERIES;
					var avgTime = TimeSpan.FromTicks(totalBruteTime.Ticks / NUM_QUERIES);
					Console.WriteLine("Brute Force - Avg tests: {0}\t\tAvg Time: {1}", avgTests, avgTime.Formatted());
				}
				{
					var diffTests = totalBruteTests / (double)totalTreeTests;
					var diffTime = totalBruteTime.Ticks / (double)totalTreeTime.Ticks;
					Console.WriteLine("Difference  - Tests: {0:0.00} less tests\tTime: {1:0.00} faster", diffTests, diffTime);
				}
				
				// Beware that this comparison is made over a completely random set of data.
				// This is the worst-case scenario for a structure like an R-Tree
				
				Console.WriteLine();
			}
			
			#if DEBUG
				Console.Write("Press any key to continue . . .");
				Console.ReadKey(true);
				Console.WriteLine();
			#endif
		}

		static IShape NewRandomShape()
		{
			return (rnd.NextDouble() < RATIO_OF_BOXES) ? (IShape)NewRandomBox() : (IShape)NewRandomPoint();
		}
		
		#if USE_INTEGER_SYSTEM
		static TNumeric NewRandomNumber(TNumeric minValue, TNumeric maxValue)
		{
			return rnd.Next(minValue, maxValue);
		}
		#else
		static TNumeric NewRandomNumber(TNumeric minValue, TNumeric maxValue)
		{
			var diff = maxValue - minValue;
			return minValue + (rnd.NextDouble() * diff);
		}
		#endif
		
		static IPoint NewRandomPoint()
		{
			return PointExt.Create(
				Enumerable.Range(0, DIMENSIONS).Select(i => NewRandomNumber(-DELTA_DATA, DELTA_DATA)).ToArray()
			);
		}

		static IBoundingBox NewRandomBox()
		{
			var p1 = (IPoint<TNumeric>)NewRandomPoint();
			var p2 = (IPoint<TNumeric>)PointExt.Create(
				Enumerable.Range(0, DIMENSIONS).Select(i => p1[i] + NewRandomNumber(-DELTA_DATA_BOX, DELTA_DATA_BOX)).ToArray()
			);
			return BoundingBoxExt.Create(p1, p2);
		}

		static MyIntersectsShapeQuery NewRandomQuery()
		{
			var p1 = (IPoint<TNumeric>)NewRandomPoint();
			var p2 = (IPoint<TNumeric>)PointExt.Create(
				Enumerable.Range(0, DIMENSIONS).Select(i => p1[i] + NewRandomNumber(-DELTA_QUERY, DELTA_QUERY)).ToArray()
			);
			return new MyIntersectsShapeQuery(BoundingBoxExt.Create(p1, p2));
		}
	}

	public class MyIntersectsShapeQuery : ISpatialQuery
	{
	    public IShape Shape { get; private set; }
	    public IBoundingBox BoundingBox { get; private set; }
	    
	    public int testCount = 0; //DEBUG-ONLY
		
	    public MyIntersectsShapeQuery(IShape shape)
	    {
	        Shape = shape;
	        BoundingBox = shape.GetBoundingBox();
	    }
	    
	    public bool TestBoundingBox(IBoundingBox b)
	    {
			return b.Intersects(BoundingBox);
	    }

		public bool TestEntry(IRTreeEntry entry)
		{
			testCount ++;
			return Shape.Intersects(entry.Shape);
		}
	}
	
	static class StopwatchExt
	{
		static public string Formatted(this Stopwatch watch)
		{
			return watch.Elapsed.Formatted();
		}

		static public string Formatted(this TimeSpan ts)
		{
			//return string.Format("{0:00}:{1:00}:{2:000}", (int)ts.TotalMinutes, ts.Seconds, ts.Milliseconds);
			double msecs = ts.TotalMilliseconds;
			int secs = (int)Math.Floor(msecs / 1000);
			msecs -= secs * 1000;
			int mins = (int)Math.Floor(secs / 60.0);
			secs -= mins * 60;
			
			if (mins > 0) {
				return string.Format("{0:00}:{1:00}:{2:000.##}", mins, secs, msecs);
			} else {
				return string.Format("{0:00}s{1:000.##}ms", secs, msecs);
			}
		}
	}
}
