﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JpLabs.MQAL;

namespace MQAL.Demo
{
	class Program
	{
		static void Main(string[] args)
		{
			//MQ Reason Codes
			//http://publib.boulder.ibm.com/infocenter/wmqv7/v7r0/index.jsp?topic=/com.ibm.mq.csqsao.doc/fm12030_1.htm
			
			/// Possible connection string keys are:
			/// "hostname"
			/// "port"
			/// "channel"
			/// "userID"
			/// "password"
			/// "QueueManager"
			/// "CTF"
			/// "UseCTF"
			/// All other properties defined in MQSeries (IBM.WMQ.MQC) (this was not extensively tested)
			
			//System.Data.Common.DbConnection
			//System.Data.SqlClient.SqlConnection
			
			/// Not using a Channel Table File
			const string CONN_STRING = "hostname=localhost; port=1881; channel=MYCHANNEL; QueueManager = MYQMAN;";
			
			/// Or using a Channel Table File:
			//const string CONN_STRING = @"QueueManager = MYQMAN; UseCTF = true; CTF ='C:\Queues\myCTF.tab'";
			
			/// Queue name must be used separately
			const string QUEUE_NAME = "MYQUEUE";
			
			/// Testing connection
			{
				QConnection qConn = new QConnection();
				qConn.ConnectionString = CONN_STRING;
				
				Console.WriteLine("Testing connection...");
				qConn.Open();
				Console.WriteLine("Connection successful");
				qConn.Close();
				Console.WriteLine("Connection closed");
			}

			/// Sending items
			{
				Console.WriteLine("Sending one item...");
				using (QConnection conn = new QConnection(CONN_STRING)) {
					conn.Open();
					using (QWriter writer = conn.CreateWriter(QUEUE_NAME)) {
						writer.Put("MY MESSAGE");
					}
				}
			}
			
			/// Reading items
			{
				Console.WriteLine("Reading one item...");
				using (QConnection conn = new QConnection(CONN_STRING)) {
					conn.Open();
					using (QReader reader = conn.CreateReader(QUEUE_NAME)) {
						QMessage message = reader.Get();
						Console.WriteLine(message.ToString());
					}
				}
			}
			
			/// Using transactions
			{
				Console.WriteLine("Using transaction...");
				using (var conn = new QConnection(CONN_STRING)) {
					conn.Open();
					using (var writer = conn.CreateWriter(QUEUE_NAME)) {
						writer.Put("OTHER MESSAGE");
					}
					using (QTransaction tx = conn.BeginTransaction()) {
						using (var reader = conn.CreateReader(QUEUE_NAME)) {
							var msg = reader.Get();
							Console.WriteLine(msg.ToString());
						}
						tx.Commit();
					}
				}
			}
			
			/// Browsing
			{
				Console.WriteLine("Browsing...");
				using (var conn = new QConnection(CONN_STRING)) {
					conn.Open();

					using (QBrowser browser = conn.CreateBrowser(QUEUE_NAME)) {
						foreach (IQMessage msg in browser.Browse()) {
							Console.WriteLine(msg.ToString());
						}
					}
				}
			}
			
			
			/// Inquiring
			{
				Console.WriteLine("Inquiring...");
				using (var conn = new QConnection(CONN_STRING)) {
					conn.Open();

					using (QInquirer q = conn.CreateInquirer(QUEUE_NAME)) {
						Console.WriteLine("CurrentDepth = {0}", q.CurrentDepth);
						Console.WriteLine("OpenInputCount = {0}", q.OpenInputCount);
						Console.WriteLine("OpenOutputCount = {0}", q.OpenOutputCount);
						Console.WriteLine("MaximumDepth = {0}", q.MaximumDepth);
						Console.WriteLine("MaximumMessageLength = {0}", q.MaximumMessageLength);
						Console.WriteLine("InhibitGet = {0}", q.InhibitGet);
						Console.WriteLine("InhibitPut = {0}", q.InhibitPut);
						Console.WriteLine("QueueType = {0}", q.QueueType);
						Console.WriteLine("Shareable = {0}", q.Shareable);
					}
				}
			}
			
			Console.Write("Press any key to continue . . . ");
			Console.ReadKey(true);
		}
	}
}
