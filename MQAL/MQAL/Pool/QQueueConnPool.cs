using System;
using System.Collections.Generic;
using System.Text;

namespace JpLabs.MQAL
{
	/// <remarks>
	/// This connection pool keeps ALL connections (including the ones in use)
	/// Also, it's supposed to be controlled from outside (it has no timer to clean old connections).
	/// </remarks>
	internal abstract class QBaseUniqueConnPool<TKey,TConn> : IDisposable //: IConnectionPool, IDisposable
	where TConn : IPoolableConnection<TKey>
	{
		protected readonly object locker = new object();
		protected IDictionary<TKey,TConn> innerCache;
		//protected QConnectionPoolControl control;

		public QBaseUniqueConnPool()
		{
			//this.control = new QConnectionPoolControl(this);
			innerCache = new Dictionary<TKey,TConn>();
		}
		
		public virtual int Count
		{
			get { lock (locker) return innerCache.Count; }
		}
		
		public virtual void Add(TKey key, TConn conn)
		{
			lock (locker) {
				TConn pooledConn;
				if (!innerCache.TryGetValue(key, out pooledConn)) {
				    innerCache.Add(key, conn);
				    conn.LastUsedOn = DateTime.Now;
				} else if (!pooledConn.IsConnected) {
				    innerCache.Remove(key);
				    innerCache.Add(key, conn);
				    conn.LastUsedOn = DateTime.Now;
				} else {
				    pooledConn.LastUsedOn = DateTime.Now;
				}
			}
			
			//control.EnsureCleanupTimerRunning();
		}
		
		public virtual void Clear()
		{
			lock (locker) {
				foreach (TConn conn in innerCache.Values) conn.Disconnect();
				innerCache.Clear();
			}
		}
		
		public virtual bool Remove(TConn conn)
		{
			lock (locker) {
				bool removed = innerCache.Remove(conn.ConnectionKey);
				//if (innerCache.Count == 0) control.StopCleanupTimer();
				return removed;
			}
		}
		
		public virtual TConn GetOrCreate(TKey key)
		{
			TConn conn;
			lock (locker) {
				if (innerCache.TryGetValue(key, out conn)) {
					if (!conn.IsUnique) innerCache.Remove(key);
				}
			}
			
			if (conn == null) {
				conn = Create(key);
				if (conn.IsUnique) this.Add(key, conn);
			} else if (!conn.IsConnected) {
				conn.Connect();
			}
			conn.LastUsedOn = DateTime.Now;
			return conn;
		}
		
		public abstract TConn Create(TKey key);
		
		//public virtual void CleanUpOld(TimeSpan conectionTimeToLive)
		//{
		//    IEnumerable<TConn> list;
		//    lock (locker) list = Enumerable.ToList(innerCache.Values);
			
		//    DateTime now  = DateTime.Now;
		//    foreach (TConn conn in list) {
		//        if (!conn.LastUsedOn.HasValue) {
		//            conn.LastUsedOn = DateTime.Now;
		//        } else if (now - conn.LastUsedOn.Value > conectionTimeToLive) {
		//            lock (locker) {
		//                conn.Disconnect();
		//                Remove(conn);
		//            }
		//        }
		//    }
		//}

		public virtual void Dispose()
		{
			//control.StopCleanupTimer();
			Clear();
		}
	}
	
	internal class QQueueConnPool : QBaseUniqueConnPool<QQueueParams,QQueueConnection>
	{
		internal readonly QInternalConnection Connection;
		
		//readonly object locker = new object();
		//IDictionary<QQueueParams,QQueueConnection> innerList;
		//QConnectionPoolControl control;

		public QQueueConnPool(QInternalConnection connection)
		{
			this.Connection = connection;
			//control = new QConnectionPoolControl(this);
			//innerList = new Dictionary<QQueueParams,QQueueConnection>();
		}

		public override QQueueConnection Create(QQueueParams key)
		{
			//Console.WriteLine("creating queue connection"); //DEBUG ##########
			return QQueueConnection.Connect(key, this);
		}
		
		//public virtual int Count
		//{
		//    get { lock (locker) return innerList.Count; }
		//}
		
		//public virtual bool Add(QQueueParams key, QQueueConnection value)
		//{
		//    lock (locker) {
		//        control.EnsureCleanupTimerRunning();
				
		//        QQueueConnection oldValue;
		//        if (!innerList.TryGetValue(key, out oldValue)) {
		//            value.PooledOn = DateTime.Now;
		//            innerList.Add(key, value);
		//            return true;
		//        } else if (value != oldValue && !oldValue.IsConnected) {
		//            innerList.Remove(key);
		//            innerList.Add(key, value);
		//            return true;
		//        } else {
		//            oldValue.PooledOn = DateTime.Now;
		//            return false;
		//        }
		//    }
		//}
		
		//public virtual void Clear()
		//{
		//    lock (locker) innerList.Clear();
		//}
		
		//public virtual bool Remove(QQueueParams key)
		//{
		//    lock (locker) {
		//        bool removed = innerList.Remove(key);
		//        if (innerList.Count == 0) control.StopCleanupTimer();
		//        return removed;
		//    }
		//}
		
		//public QQueueConnection GetOrCreate(QQueueParams key)
		//{
		//    QQueueConnection conn;
		//    lock (locker) {
		//        if (innerList.TryGetValue(key, out conn)) {
		//            innerList.Remove(key);
		//        }
		//    }
			
		//    if (conn == null) {
		//        conn = QQueueConnection.Connect(key, this);
		//    } else if (!conn.IsConnected) {
		//        conn.Connect();
		//    }
		//    return conn;
		//}
		
		//static public IEnumerable<T> ToList<T>(ICollection<T> coll)
		//{
		//    T[] list = new T[coll.Count];
		//    coll.CopyTo(list, 0);
		//    return list;
		//}
		
		//public void CleanUpOld(TimeSpan conectionTimeToLive)
		//{
		//    IEnumerable<KeyValuePair<QQueueParams,QQueueConnection>> list;
		//    lock (locker) list = ToList(innerList);
			
		//    DateTime now  = DateTime.Now;
		//    foreach (KeyValuePair<QQueueParams,QQueueConnection> pair in list) {
		//        if (!pair.Value.PooledOn.HasValue) {
		//            pair.Value.Disconnect();
		//            Remove(pair.Key);
		//        } else if (now - pair.Value.PooledOn.Value > conectionTimeToLive) {
		//            pair.Value.Disconnect();
		//            Remove(pair.Key);
		//        } else {
		//        }
		//    }
		//}

		//void IDisposable.Dispose()
		//{
		//    control.StopCleanupTimer();
		//}
	}
}
