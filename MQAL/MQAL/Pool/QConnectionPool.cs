using System;
using System.Collections.Generic;
using System.Text;
using IBM.WMQ;
using System.Threading;

namespace JpLabs.MQAL
{
	/// TODO:
	/// - Pool shoul be able to hold more than one connection to the same location
	///		- In .NET 3.0, there is a LookUp class that does that
	
	///http://dotnet.sys-con.com/read/39040.htm
	internal class QConnectionPool : QBaseConnPool<QConnParams,QInternalConnection>//IConnectionPool
	{
		static public QConnectionPool Pool = new QConnectionPool();
		
		public QConnectionPool()
		{
			AppDomain.CurrentDomain.ProcessExit += new EventHandler(OnCurrentDomainUnload);
		}

        void OnCurrentDomainUnload(object sender, EventArgs e)
        {
            Pool.Dispose();
        }

		public override QInternalConnection Create(QConnParams key)
		{
			//Console.WriteLine("creating connection"); //DEBUG ##########
			return QInternalConnection.Connect(key, this);
		}
	}
}
