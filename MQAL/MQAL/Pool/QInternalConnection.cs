using System;
using System.Collections.Generic;
using System.Text;
using IBM.WMQ;

namespace JpLabs.MQAL
{
	internal class QInternalConnection : IPoolableConnection<QConnParams>
	{
		readonly QConnParams connKey;
		public readonly MQQueueManager MqSeriesManager;
		public readonly QConnectionPool Pool;
		DateTime? lastUsedOn;
		QQueueConnPool queuePool;
		
		public QInternalConnection(QConnectionPool pool, QConnParams connectionKey, MQQueueManager mqSeriesManager)
		{
			this.Pool = pool;
			this.connKey = connectionKey;
			this.MqSeriesManager = mqSeriesManager;
			queuePool =  new QQueueConnPool(this);
		}
		
		static public QInternalConnection Connect(QConnParams connParams, QConnectionPool pool)
		{
			MQQueueManager mqSeriesManager = connParams.Connect();
			QInternalConnection conn = new QInternalConnection(pool, connParams, mqSeriesManager);
			conn.Connect();
			return conn;
		}
		
		public bool IsConnected
		{
			get {
				return
					(MqSeriesManager == null) ? false :
					(!MqSeriesManager.IsConnected) ? false :
					true;
			}
		}

		public bool IsUnique
		{
			get { return false; } /// It means that several connections to the same key can exist
		}

		public QConnParams ConnectionKey
		{
			get { return connKey; }
		}
		
		public DateTime? LastUsedOn
		{
			get {return lastUsedOn;}
			set {lastUsedOn = value;}
		}
		
		public void MoveToPool()
		{
			Pool.Add(connKey, this);
		}
		
		public void Connect()
		{
			try {
				if (!MqSeriesManager.IsConnected) MqSeriesManager.Connect();
			} catch (MQException ex) {
				throw QException.Create(ex);
			}
		}
		
		internal MQQueue AccessQueue(string queueName, int openOptions)
		{
			try {
				return MqSeriesManager.AccessQueue(queueName, openOptions);
			} catch (MQException ex) {
				throw QException.Create(ex);
			}
		}

		internal QQueueConnection GetOpenQueue(string queueName, int openOptions)
		{
			return queuePool.GetOrCreate(new QQueueParams(queueName, openOptions));
		}
		
		public void Disconnect()
		{
			try {
				queuePool.Clear();
				if (MqSeriesManager.IsConnected) MqSeriesManager.Disconnect();
				MqSeriesManager.Close();
			} catch (MQException ex) {
				throw QException.Create(ex);
			}
		}

		public void Backout()
		{
			try {
				MqSeriesManager.Backout();
			} catch (MQException ex) {
				throw QException.Create(ex);
			}
		}
		
		public void Commit()
		{
			try {
				MqSeriesManager.Commit();
			} catch (MQException ex) {
				throw QException.Create(ex);
			}
		}

		void IDisposable.Dispose()
		{
			MoveToPool();
		}
		
		//~QInternalConnection() { Disconnect(); }
	}
}
