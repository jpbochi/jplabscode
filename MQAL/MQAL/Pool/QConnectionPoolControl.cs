using System;
using System.Collections.Generic;
using System.Text;
using IBM.WMQ;
using System.Threading;

namespace JpLabs.MQAL
{
	internal class QConnectionPoolControl : IDisposable
	{
		public static TimeSpan DefaultConectionTimeToLive = TimeSpan.FromMinutes(Math.PI);
		
		IConnectionPool pool;
		ITimeoutGenerator cleanUpTimeoutGen;
		TimeSpan connTimeToLive;
		Timer cleanUpTimer;
		bool isTimerRunning;
		
		public QConnectionPoolControl(IConnectionPool pool)
		: this (pool, DefaultConectionTimeToLive)
		{}

		public QConnectionPoolControl(IConnectionPool pool, TimeSpan conectionTimeToLive)
		{
			this.pool = pool;
			this.connTimeToLive = conectionTimeToLive;
			int ms = (int)connTimeToLive.TotalMilliseconds;
			cleanUpTimeoutGen = new RandomTimeoutGenerator(ms / 2, ms * 2);
			CreateCleanupTimer();
		}
		
		void CreateCleanupTimer()
		{
			cleanUpTimer = new Timer(CleanupCallback, null, Timeout.Infinite, Timeout.Infinite); 
			isTimerRunning = false;
		}
		
		public void EnsureCleanupTimerRunning()
		{
			if (!isTimerRunning) StartCleanupTimer();
		}

		public void StartCleanupTimer()
		{
			cleanUpTimer.Change(cleanUpTimeoutGen.GetNext(), Timeout.Infinite); 
			isTimerRunning = true;
		}

		public void StopCleanupTimer()
		{
			cleanUpTimer.Change(Timeout.Infinite, Timeout.Infinite); 
			isTimerRunning = false;
		}
		
		void CleanupCallback(object state)
		{
			try {
				pool.CleanUpOld(connTimeToLive);
			} catch (Exception ex) {
				QConnection.OnConnectionPoolControlError(this, ex);
			} finally {
				StartCleanupTimer();
			}
		}

		void IDisposable.Dispose()
		{
			StopCleanupTimer();
		}
	}
}
