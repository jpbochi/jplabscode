using System;
using System.Collections.Generic;
using System.Text;

namespace JpLabs.MQAL
{
	public class QQueueParams
	{
		public readonly string QueueName;
		public readonly int OpenOptions;
		int? hashCode;
		
		public QQueueParams(string queueName, int openOptions)
		{
			QueueName = queueName;
			OpenOptions = openOptions;
		}

		public override bool Equals(object obj)
		{
			QQueueParams other = obj as QQueueParams;
			if (other != null) {
				return this.GetHashCode() == other.GetHashCode();
			} else {
				return base.Equals(obj);
			}
		}
		
		public override int GetHashCode()
		{
			if (hashCode == null) {
				unchecked {hashCode = QueueName.GetHashCode() + OpenOptions.GetHashCode();}
			}
			return hashCode.Value;
		}
	}
}
