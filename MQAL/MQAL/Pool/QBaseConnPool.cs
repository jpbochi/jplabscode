using System;
using System.Collections.Generic;
using System.Text;

namespace JpLabs.MQAL
{
	public interface IPoolableConnection<TKey> : IDisposable
	{
		void Connect();
		void Disconnect();
		bool IsConnected	{ get; }
		
		bool IsUnique		{ get; }
		TKey ConnectionKey	{ get; }
		DateTime? LastUsedOn{ get; set; }
		void MoveToPool();
	}

	internal interface IConnectionPool
	{
		void CleanUpOld(TimeSpan conectionTimeToLive);
	}

	/// <remarks>This connection pool keeps only connections NOT in use</remarks>
	internal abstract class QBaseConnPool<TKey,TConn> : IConnectionPool, IDisposable
	where TConn : IPoolableConnection<TKey>
	{
		protected readonly object locker = new object();
		protected ILookup<TKey,TConn> innerCache;
		protected QConnectionPoolControl control;

		public QBaseConnPool()
		{
			this.control = new QConnectionPoolControl(this);
			innerCache = new Lookup<TKey,TConn>();
		}
		
		public virtual int Count
		{
			get { lock (locker) return innerCache.Count; }
		}
		
		public virtual void Add(TKey key, TConn conn)
		{
			lock (locker) innerCache.Add(key, conn);
			
			conn.LastUsedOn = DateTime.Now;
			control.EnsureCleanupTimerRunning();
		}
		
		public virtual void Clear()
		{
			lock (locker) {
				foreach (TConn conn in innerCache.Values) conn.Disconnect();
				innerCache.Clear();
			}
		}
		
		public virtual bool Remove(TConn conn)
		{
			lock (locker) {
				bool removed = innerCache.RemoveFromKey(conn.ConnectionKey, conn);
				if (innerCache.IsEmpty) control.StopCleanupTimer();
				return removed;
			}
		}
		
		public virtual TConn GetOrCreate(TKey key)
		{
			TConn conn;
			lock (locker) {
				if (innerCache.TryGetValue(key, out conn)) {
					if (!conn.IsUnique) innerCache.RemoveFromKey(key, conn);
				}
			}
			
			if (conn == null) {
				conn = Create(key);
			} else if (!conn.IsConnected) {
				conn.Connect();
			}
			conn.LastUsedOn = DateTime.Now;
			return conn;
		}
		
		public abstract TConn Create(TKey key);
		
		public virtual void CleanUpOld(TimeSpan conectionTimeToLive)
		{
			IEnumerable<TConn> list;
			lock (locker) list = Enumerable.ToList(innerCache.Values);
			
			DateTime now  = DateTime.Now;
			foreach (TConn conn in list) {
				if (!conn.LastUsedOn.HasValue) {
					conn.LastUsedOn = DateTime.Now;
				} else if (now - conn.LastUsedOn.Value > conectionTimeToLive) {
					conn.Disconnect();
					Remove(conn);
				}
			}
		}

		public virtual void Dispose()
		{
			control.StopCleanupTimer();
			Clear();
		}
	}
}
