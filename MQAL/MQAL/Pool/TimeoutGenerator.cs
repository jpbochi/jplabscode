using System;
using System.Collections.Generic;
using System.Text;

namespace JpLabs.MQAL
{
	public interface ITimeoutGenerator
	{
		int GetNext();
		void Reset();
	}

	sealed class RandomTimeoutGenerator : ITimeoutGenerator
	{
		Random rnd;
		int minValue;
		int maxValue;
		
		public RandomTimeoutGenerator(int minValue, int maxValue)
		{
			this.minValue = minValue;
			this.maxValue = maxValue;
			Reset();
		}
		
		public int GetNext()
		{
			return rnd.Next(minValue, maxValue);
		}

		public void Reset()
		{
			rnd = new Random();
		}
	}
}
