using System;
using System.Collections.Generic;
using System.Text;
using IBM.WMQ;

namespace JpLabs.MQAL
{
	internal class QQueueConnection : IPoolableConnection<QQueueParams>
	{
		public readonly QQueueParams ConnParams;
		internal readonly QQueueConnPool Pool;
		MQQueue mqSeriesQueue;
		DateTime? lastUsedOn;
		
		internal QQueueConnection(QQueueConnPool pool, QQueueParams connectionKey, MQQueue mqSeriesQueue)
		{
			this.Pool = pool;
			this.ConnParams = connectionKey;
			this.MqSeriesQueue = mqSeriesQueue;
		}

		static internal QQueueConnection Connect(QQueueParams connParams, QQueueConnPool pool)
		{
			MQQueue mqQueue = pool.Connection.AccessQueue(connParams.QueueName, connParams.OpenOptions);
			QQueueConnection conn = new QQueueConnection(pool, connParams, mqQueue);
			return conn;
		}
		
		public MQQueue MqSeriesQueue
		{
			get {
				//Connect();
				return mqSeriesQueue;
			}
			private set { mqSeriesQueue = value; }
		}

		public void Connect()
		{
			try {
				if (!IsConnected) MqSeriesQueue = Pool.Connection.AccessQueue(ConnParams.QueueName, ConnParams.OpenOptions);
				//LastUsedOn = DateTime.Now;
			} catch (MQException ex) {
				throw QException.Create(ex);
			}
		}

		public void Disconnect()
		{
			try {
				if (IsConnected) MqSeriesQueue.Close();
			} catch (MQException ex) {
				throw QException.Create(ex);
			}
		}

		public bool IsConnected
		{
			get {
				return
					(mqSeriesQueue == null) ? false :
					(!mqSeriesQueue.IsOpen) ? false :
					true;
			}
		}

		public bool IsUnique
		{
			get { return true; } /// It means that only one queue connection per key should exist
		}

		public QQueueParams ConnectionKey
		{
			get { return ConnParams; }
		}

		public DateTime? LastUsedOn
		{
			get {return lastUsedOn;}
			set {lastUsedOn = value;}
		}

		public void MoveToPool()
		{
			Pool.Add(ConnParams, this);
		}

		void IDisposable.Dispose()
		{
			MoveToPool();
		}
		
		//~QQueueConnection() { Disconnect(); }
	}
}
