using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace JpLabs.MQAL
{
	public interface IGroup<TKey,TValue> : ICollection<TValue>
	{
		TKey Key { get; }
		
		void AddRange(IEnumerable<TValue> values);
	}
	
	public interface ILookup<TKey,TValue> : ICollection<IGroup<TKey,TValue>>
	{
		bool IsEmpty { get; }
		IEnumerable<TValue> this[TKey key] { get; }

		IEnumerable<TKey> Keys { get; }
		IEnumerable<IGroup<TKey,TValue>> Groups { get; }
		IEnumerable<TValue> Values { get; }

		void Add(TKey key, TValue value);
		void AddRange(TKey key, IEnumerable<TValue> values);
		
		bool ContainsKey(TKey key);
		bool TryGetGroup(TKey key, out IGroup<TKey,TValue> group);
		bool TryGetValue(TKey key, out TValue value);

		bool ClearKey(TKey key);
		bool RemoveFromKey(TKey key, TValue value);
	}
}
