using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;

namespace JpLabs.MQAL
{
	public class Lookup<TKey,TValue> : ILookup<TKey,TValue>
	{
		protected class Group : List<TValue>, IGroup<TKey,TValue>
		{
			protected TKey key;

			public Group(TKey key) : base()
			{
				this.key = key;
			}
			
			public Group(TKey key, IEnumerable<TValue> values) : base(values)
			{
				this.key = key;
			}

			public TKey Key
			{
				get { return key; }
			}
			
			void ICollection<TValue>.Add(TValue item)
			{
				//don't put the same object two times
				if (!base.Exists(delegate (TValue v) {return object.ReferenceEquals(v, item);})) {
					base.Add(item);
				}
			}
		}

		IDictionary<TKey,IGroup<TKey,TValue>> innerDict;
		
		public Lookup()
		{
			innerDict = new Dictionary<TKey,IGroup<TKey,TValue>>();
		}
		
		public Lookup(IEqualityComparer<TKey> comparer)
		{
			innerDict = new Dictionary<TKey,IGroup<TKey,TValue>>(comparer);
		}
		
		public Lookup(int capacity)
		{
			innerDict = new Dictionary<TKey,IGroup<TKey,TValue>>(capacity);
		}
		
		public Lookup(int capacity, IEqualityComparer<TKey> comparer)
		{
			innerDict = new Dictionary<TKey,IGroup<TKey,TValue>>(capacity, comparer);
		}
		
		public void ClearEmptyGroups()
		{
			foreach (IGroup<TKey,TValue> g in Enumerable.ToList(innerDict.Values)) {
				if (g.Count == 0) innerDict.Remove(g.Key);
			}
		}

		//#region ILookup<TKey,TValue> Members
		
		public bool IsEmpty { get { return (innerDict.Count == 0); } }

		public IEnumerable<TValue> this[TKey key]
		{
			get {
				IGroup<TKey,TValue> group;
				if (innerDict.TryGetValue(key, out group)) {
					return group;
				} else {
					return Enumerable.Empty<TValue>();
				}
			}
		}

		public IEnumerable<TKey> Keys
		{
			get { return innerDict.Keys; }
		}

		public IEnumerable<IGroup<TKey, TValue>> Groups
		{
			get { return innerDict.Values; }
		}
		
		public IEnumerable<TValue> Values
		{
			get {
				foreach (IGroup<TKey,TValue> g in innerDict.Values) {
					foreach (TValue v in g) yield return v;
				}
			}
		}

		public void Add(TKey key, TValue value)
		{
			IGroup<TKey,TValue> g;
			if (innerDict.TryGetValue(key, out g)) {
				g.Add(value);
			} else {
				g = new Group(key);
				g.Add(value);
				innerDict.Add(key, g);
			}
		}

		public void AddRange(TKey key, IEnumerable<TValue> values)
		{
			IGroup<TKey,TValue> g;
			if (innerDict.TryGetValue(key, out g)) {
			    g.AddRange(values);
			} else {
			    innerDict.Add(key, new Group(key, values));
			}
		}

		public bool ContainsKey(TKey key)
		{
			return innerDict.ContainsKey(key);
		}

		public bool TryGetGroup(TKey key, out IGroup<TKey,TValue> group)
		{
			if (innerDict.TryGetValue(key, out group)) {
				if (group.Count == 0) {
				    innerDict.Remove(key);
				    return false;
				}
		        return true;
			}
		    return false;
		}

		public bool TryGetValue(TKey key, out TValue value)
		{
			IGroup<TKey,TValue> g;
			if (TryGetGroup(key, out g)) {
				value = Enumerable.FirstOrDefault(g);
				return !object.Equals(value, default(TValue));
			} else {
				value = default(TValue);
				return false;
			}
		}

		public bool ClearKey(TKey key)
		{
			return innerDict.Remove(key);
		}
		
		public bool RemoveFromKey(TKey key, TValue value)
		{
			IGroup<TKey,TValue> g;
			if (innerDict.TryGetValue(key, out g) && g.Remove(value)) {
				if (g.Count == 0) innerDict.Remove(key);
				return true;
			}
			return false;
		}

		//#region ICollection<IGroup<TKey,TValue>> Members

		public void Add(IGroup<TKey, TValue> group)
		{
			IGroup<TKey,TValue> g;
			if (innerDict.TryGetValue(group.Key, out g)) {
			    g.AddRange(group);
			} else {
			    innerDict.Add(group.Key, group);
			}
		}

		public void Clear()
		{
			innerDict.Clear();
		}

		public bool Contains(IGroup<TKey, TValue> group)
		{
			return innerDict.Contains(new KeyValuePair<TKey,IGroup<TKey,TValue>>(group.Key, group));
		}

		public void CopyTo(IGroup<TKey, TValue>[] array, int arrayIndex)
		{
			throw new NotImplementedException();
		}

		public int Count
		{
			/// TODO: return the count of values, not of groups
			get { return innerDict.Count; }
		}

		public bool IsReadOnly
		{
			get { return false; }
		}

		public bool Remove(IGroup<TKey,TValue> group)
		{
			return innerDict.Remove(new KeyValuePair<TKey,IGroup<TKey,TValue>>(group.Key, group));
		}

		public IEnumerator<IGroup<TKey, TValue>> GetEnumerator()
		{
			return innerDict.Values.GetEnumerator();
		}
		
		IEnumerator IEnumerable.GetEnumerator()
		{
			return innerDict.Values.GetEnumerator();
		}
	}
}
