using System;
using System.Collections.Generic;
using System.Text;
using IBM.WMQ;

namespace JpLabs.MQAL
{
	/// TODO List:
	/// - Pool connection to MQQueueManager's (done)
	///		- by the way, it seems that MQQueueManager already pools connections
	/// - Discover how to query the number of items in a Queue (done - QInquirer)
	/// - Encapsulate all MQException's to show a more descriptive message (done)
	/// - 
	
	/// Links
	/// MQ Help:
	/// http://publib.boulder.ibm.com/infocenter/wmqv6/v6r0/index.jsp?topic=/com.ibm.mq.branding.doc/help_home_wmq.htm
	/// http://inside.us.dell.com/it/MiddlewareServices/MQSeriesErrorCodes.asp
	/// 
	/// Other:
	/// http://bloggingabout.net/blogs/mark/archive/2007/03/09/using-ibm-webphere-mq-with-net.aspx
	
	
	/// <summary>Describes the current state of a <see cref="IQConnection"/></summary>
	[Flags]
	public enum QConnectionState
	{
		Closed = 0,
		Open = 1,
		//Connecting = 2, //reserved for the time when I get multi-threaded
	}
	
	/// <summary>Represents a connection to a queue server</summary>
	/// <remarks>The same IQConnection can be used to connect to different queues</remarks>
	public interface IQConnection : IDisposable
	{
		/// <summary>Gets/sets the connection string</summary>
		string ConnectionString { get; set; }
		
		//int ConnectionTimeout { get; }
		
		/// <summary>Timeout (in miliseconds) to be used by reader commands created by this connection</summary>
		int ReadWaitTimeout { get; set; }
		
		/// <summary>Gets the current state of the connection</summary>
		QConnectionState State { get; }
		
		/// <summary>Starts an queue transaction</summary>
		/// <returns>An object representing the new transaction</returns>
		IQTransaction BeginTransaction();
		
		/// <summary>Gets the current transaction for this connection or null if there's no connection being used</summary>
		IQTransaction CurrentTransaction { get; }
		
		/// <summary>Creates an asynchronous queue monitor associated with the current connection</summary>
		/// <param name="queueName">The name of the queue to be monitored</param>
		/// <returns>An object representing the new monitor</returns>
		IQMonitor CreateMonitor(string queueName);
		
		/// <summary>Creates an queue reader associated with the current connection</summary>
		/// <param name="queueName">The name of the queue to be read from</param>
		/// <returns>An object representing the new reader</returns>
		IQReader CreateReader(string queueName);

		/// <summary>Creates an queue writer associated with the current connection</summary>
		/// <param name="queueName">The name of the queue to be written to</param>
		/// <returns>An object representing the new writer</returns>
		IQWriter CreateWriter(string queueName);
		
		/// <summary>Opens a connection to the queue server, if it's not opened</summary>
		void Open();

		/// <summary>Closes the connection to the queue server, if it's opened</summary>
		void Close();
	}

	/// <summary>Represents a queue transaction</summary>
	public interface IQTransaction : IDisposable
	{
		IQConnection Connection { get; }

		void Commit();
		void Rollback();
	}
	
	/// <summary>Represents a queue reader</summary>
	public interface IQReader : IDisposable
	{
		IQConnection Connection { get; }
		int ReadWaitTimeout { get; set; }
		
		IQMessage Get();
		IQMessage Get(int readWaitTimeout);
		
		void Close();
	}
	
	/// <summary>Represents a queue writer</summary>
	public interface IQWriter : IDisposable
	{
		IQConnection Connection { get; }
		
		void Put(string msg);
		void Put(IQMessage msg);

		void Close();
	}
	
	/// <summary>Encapsulates a method that is meant to handle messages read by an <see cref="IQMonitor"/></summary>
	/// <param name="sender">The monitor that read the message</param>
	/// <param name="msg">The read message</param>
	public delegate void QMessageHandler(IQMonitor sender, IQMessage msg);

	/// <summary>Describes the state of a queue monitor</summary>
	/// <remarks>The values included in the enumaration are only a suggestion. No monitors have been implemented yet.</remarks>
	[Flags]
	public enum QMonitorState
	{
		NotConnected = 0,
		Connected = 1,
		Connecting = 2,
		Running = 4,
	}
	
	/// <summary>Represents a queue monitor</summary>
	/// <remarks>
	/// Queue monitors run asynchronously,
	/// trying to read messages from a queue,
	/// and sending them to be processed by means of the <see cref="MessageArrived"/> event
	/// </remarks>
	public interface IQMonitor : IDisposable
	{
		QMonitorState State { get; }
		IQConnection Connection { get; }
		IQTransaction CurrentTransaction { get; }
		
		void Start();
		void Stop();
		
		event QMessageHandler MessageArrived;
	}
	
	/// <summary>Encapsulates a queue message and its properties/attributes</summary>
	public interface IQMessage
	{
		object MessageId { get; }
		string PayLoad { get; }
		object Type { get; }
		DateTime SentOn { get; }
		DateTime ReadOn { get; }
		int RollbackCount { get; }
	}
}
