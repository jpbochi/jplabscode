using System;
using System.Collections.Generic;
using System.Text;
using IBM.WMQ;

namespace JpLabs.MQAL
{
	public class QTransaction : IQTransaction
	{
		QConnection mConnection;
		bool isCompleted;
		bool isDisposed;
		
		public QTransaction(QConnection mqConn)
		{
			isCompleted = false;
			isDisposed = false;
			mConnection = mqConn;
		}

		public QConnection Connection
		{
			get { return mConnection; }
		}
		IQConnection IQTransaction.Connection
		{
			get { return Connection; }
		}

		public void Commit()
		{
			mConnection.CommitTransaction();
			isCompleted = true;
		}

		public void Rollback()
		{
			mConnection.RollbackTransaction();
			isCompleted = true;
		}

		void IDisposable.Dispose()
		{
			if (!isCompleted && !isDisposed) {
				Rollback();
				mConnection = null;
				isDisposed = true;
			}
		}
		
		~QTransaction()
		{
			((IDisposable)this).Dispose();
		}
	}
}
