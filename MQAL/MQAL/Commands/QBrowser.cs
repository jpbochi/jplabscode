using System;
using System.Collections.Generic;
using System.Text;
using IBM.WMQ;

namespace JpLabs.MQAL
{
	public class QBrowser : QBaseReaderCommand
	{
		internal QBrowser(QConnection connection, QQueueConnection queue) : base(connection, queue)
		{}

		public IEnumerable<IQMessage> Browse()
		{
		    return Browse(base.ReadWaitTimeout);
		}
		
		public IEnumerable<IQMessage> Browse(int readWaitTimeout)
		{
			bool first = true;
			while (true) {
				MQMessage msg = MQSeriesUtil.Browse(MqQueue, first, readWaitTimeout);
				if (msg != null) {
					first = false;
					yield return QMessage.FromMQSeriesMessage(msg);
				} else {
					yield break;
				}
			}
		}
	}
}
