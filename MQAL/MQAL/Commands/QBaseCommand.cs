using System;
using System.Collections.Generic;
using System.Text;
using IBM.WMQ;

namespace JpLabs.MQAL
{
	public abstract class QBaseCommand : IDisposable
	{
		QConnection connection;
		QQueueConnection queueConn;
		bool isClosed;
		
		internal QBaseCommand(QConnection connection, QQueueConnection queue)
		{
			isClosed = false;
			this.connection = connection;
			this.queueConn = queue;
		}

		public QConnection Connection	{ get { return connection; } }
		protected MQQueue MqQueue		{ get { return queueConn.MqSeriesQueue; } }
		protected bool IsClosed			{ get { return isClosed; } }
		
		public void Close()
		{
			if (!isClosed) {
				((IDisposable)queueConn).Dispose();
				isClosed = true;
			}
		}

		void IDisposable.Dispose()
		{
			Close();
		}
	}
}
