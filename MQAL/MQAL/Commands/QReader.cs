using System;
using System.Collections.Generic;
using System.Text;
using IBM.WMQ;

namespace JpLabs.MQAL
{
	public class QReader : QBaseReaderCommand, IQReader
	{
		internal QReader(QConnection connection, QQueueConnection queue)
		: base(connection, queue)
		{}

		IQConnection IQReader.Connection { get { return Connection; } }
		
		IQMessage IQReader.Get()
		{
			return Get(this.ReadWaitTimeout);
		}
		IQMessage IQReader.Get(int readWaitTimeout)
		{
			return Get(readWaitTimeout);
		}

		public QMessage Get()
		{
			return Get(this.ReadWaitTimeout);
		}
		public QMessage Get(int readWaitTimeout)
		{
			try {
				MQMessage msg = MQSeriesUtil.Get(MqQueue, readWaitTimeout, Connection.HasTransaction());
				return QMessage.FromMQSeriesMessage(msg);
			} catch {
				Connection.RollbackTransaction();
				throw;
			}
		}
	}
}
