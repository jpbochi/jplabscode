using System;
using System.Collections.Generic;
using System.Text;
using IBM.WMQ;

namespace JpLabs.MQAL
{
	[Flags]
	public enum QueueTypeFlag : int
	{
		Alias = MQC.MQQT_ALIAS,
		Local = MQC.MQQT_LOCAL,
		Remote = MQC.MQQT_REMOTE,
		Cluster = MQC.MQQT_CLUSTER
   	}
	
	public class QInquirer : QBaseCommand
	{
		internal QInquirer(QConnection connection, QQueueConnection queue) : base(connection, queue)
		{}
		
		public int CurrentDepth
		{
			get {
				try { return MqQueue.CurrentDepth; }
				catch (MQException ex) { throw QException.Create(ex); }
			}
		}

		public int MaximumDepth
		{
			get {
				try { return MqQueue.MaximumDepth; }
				catch (MQException ex) { throw QException.Create(ex); }
			}
		}
		
		public bool InhibitGet
		{
			get {
				try { return MqQueue.InhibitGet == MQC.MQQA_GET_INHIBITED; }
				catch (MQException ex) { throw QException.Create(ex); }
			}
		}

		public bool InhibitPut
		{
			get {
				try { return MqQueue.InhibitGet == MQC.MQQA_PUT_INHIBITED; }
				catch (MQException ex) { throw QException.Create(ex); }
			}
		}
		
		public int OpenInputCount
		{
			get {
				try { return MqQueue.OpenInputCount; }
				catch (MQException ex) { throw QException.Create(ex); }
			}
		}
		
		public int OpenOutputCount
		{
			get {
				try { return MqQueue.OpenOutputCount; }
				catch (MQException ex) { throw QException.Create(ex); }
			}
		}
		
		public int MaximumMessageLength
		{
			get {
				try { return MqQueue.MaximumMessageLength; }
				catch (MQException ex) { throw QException.Create(ex); }
			}
		}
		
		public QueueTypeFlag QueueType
		{
			get {
				try { return (QueueTypeFlag)MqQueue.QueueType; }
				catch (MQException ex) { throw QException.Create(ex); }
			}
		}
		
		public bool Shareable
		{
			get {
				try { return MqQueue.Shareability == MQC.MQQA_SHAREABLE; }
				catch (MQException ex) { throw QException.Create(ex); }
			}
		}
		
		/// REVIEW: The following might be useful, but I don't know them [yet]
		//public int TriggerControl { get; set; }
		//public string TriggerData { get; set; }
		//public int TriggerDepth { get; set; }
		//public int TriggerMessagePriority { get; set; }
		//public int TriggerType { get; set; }
	}
}
