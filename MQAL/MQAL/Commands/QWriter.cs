using System;
using System.Collections.Generic;
using System.Text;
using IBM.WMQ;

namespace JpLabs.MQAL
{
	public class QWriter : QBaseCommand, IQWriter
	{
		internal QWriter(QConnection connection, QQueueConnection queue) : base(connection, queue)
		{}

		IQConnection IQWriter.Connection { get { return Connection; } }

		public void Put(string msg)
		{
		    Put((QMessage)msg);
		}

		void IQWriter.Put(IQMessage msg)
		{
			Put((QMessage)msg);
		}

		public void Put(QMessage msg)
		{
			try {
				MQMessage mqseriesMsg = MQSeriesUtil.Put(MqQueue, msg.PayLoad, Connection.HasTransaction());
				msg.UpdateFromMQSeriesMessage(mqseriesMsg);
			} catch {
				Connection.RollbackTransaction();
				throw;
			}
		}
	}
}
