using System;
using System.Collections.Generic;
using System.Text;
using IBM.WMQ;

namespace JpLabs.MQAL
{
	public abstract class QBaseReaderCommand : QBaseCommand
	{
		int readWaitTimeout;
		
		internal QBaseReaderCommand(QConnection connection, QQueueConnection queue)
		: base(connection, queue)
		{
			readWaitTimeout = connection.ReadWaitTimeout;
		}
		
		public int ReadWaitTimeout
		{
			get { return readWaitTimeout; }
			set { readWaitTimeout = value; }
		}
	}
}
