using System;
using System.Collections.Generic;
using System.Text;
using IBM.WMQ;

namespace JpLabs.MQAL
{
	[Serializable]
	public class QException : ApplicationException
	{
		static public QException Create(MQException ex)
		{
			/// TODO: Translate error codes
			return new QException(string.Format("MQException: {0}", ex.ToString()), ex);
		}
		
		public QException(string message, MQException ex) : base(message, ex)
		{}

		public override string StackTrace
		{
			get {
				return string.Concat(
					InnerException.StackTrace, Environment.NewLine,
					"---", Environment.NewLine,
					base.StackTrace
				);
			}
		}

		//public override string ToString()
		//{
		//    return string.Format("MQException:{0}\r\n{1}", InnerException.ToString(), InnerException.StackTrace);
		//}
	}
}
