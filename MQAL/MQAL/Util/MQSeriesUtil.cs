using System;
using System.Collections;
using IBM.WMQ;

namespace JpLabs.MQAL
{
	internal static class MQSeriesUtil
	{
		static public MQMessage Get(MQQueue _queue, int readWaitTimeMiliSec, bool withTransaction)
		{
			MQGetMessageOptions opt = new MQGetMessageOptions();
			if (readWaitTimeMiliSec == -1) {
				opt.Options = MQC.MQGMO_NO_WAIT;
				opt.WaitInterval = 0;
			} else {
				opt.Options = MQC.MQGMO_WAIT;
				opt.WaitInterval = readWaitTimeMiliSec;
			}
			
			if (withTransaction) opt.Options |= MQC.MQGMO_SYNCPOINT;
			//opt.Options |= MQC.MQGMO_ALL_MSGS_AVAILABLE;
			
			try
			{
				/// If no suitable message has arrived after WaitInterval has elapsed, the call
				/// completes with MQCC_FAILED and reason code MQRC_NO_MSG_AVAILABLE.
				MQMessage msg = new MQMessage();
				_queue.Get(msg, opt);
				return msg;
			} catch (MQException ex) {
				if (ex.CompletionCode == MQC.MQCC_FAILED && ex.ReasonCode == MQC.MQRC_NO_MSG_AVAILABLE) {
					return null;
				} else {
					throw QException.Create(ex);
				}
			}
		}
		
		static public MQMessage Put(MQQueue _queue, string message, bool withTransaction)
		{
			try
			{
				MQMessage mqMsg = new MQMessage();
				mqMsg.WriteString(message);
				mqMsg.Format = MQC.MQFMT_STRING;

				MQPutMessageOptions mqPutMsgOpts = new MQPutMessageOptions();
				if (withTransaction) {
					mqPutMsgOpts.Options = MQC.MQPMO_SYNCPOINT;
				} else {
					mqPutMsgOpts.Options = MQC.MQPMO_NONE;
				}
				_queue.Put(mqMsg, mqPutMsgOpts);
				
				return mqMsg;
			} catch (MQException ex) {
				throw QException.Create(ex);
			}
		}

		static public MQMessage Browse(MQQueue _queue, bool first, int readWaitTimeMiliSec)
		{
			MQGetMessageOptions opt = new MQGetMessageOptions();
			if (readWaitTimeMiliSec == -1) {
				opt.Options = MQC.MQGMO_NO_WAIT;
				opt.WaitInterval = 0;
			} else {
				opt.Options = MQC.MQGMO_WAIT;
				opt.WaitInterval = readWaitTimeMiliSec;
			}
			
			opt.Options |= (first) ? MQC.MQGMO_BROWSE_FIRST : MQC.MQGMO_BROWSE_NEXT;
			
			try
			{
				MQMessage msg = new MQMessage();
				_queue.Get(msg, opt);
				return msg;
			} catch (MQException ex) {
				if (ex.CompletionCode == MQC.MQCC_FAILED && ex.ReasonCode == MQC.MQRC_NO_MSG_AVAILABLE) {
					return null;
				} else {
					throw QException.Create(ex);
				}
			}
		}
		
		[Obsolete("NameValueParser.Parse() should be used")]
		static public Hashtable ParseConnectionString(string connectionString)
		{
			Hashtable dict = new Hashtable();

			string[] fields = connectionString.Split(';');
			for (int i = 0; i < fields.Length; i++) {
				string field = fields[i].Trim();
				int posEqualSign = field.IndexOf('=');
				if (posEqualSign != -1) {
					string fieldName = field.Substring(0, posEqualSign).Trim();
					string fieldValue = field.Substring(posEqualSign + 1).Trim();

					dict.Add(fieldName, fieldValue);
				}
			}
			
			return dict;
		}
	}
}