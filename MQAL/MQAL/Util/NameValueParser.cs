using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.Text.RegularExpressions;

namespace JpLabs.MQAL
{
	static public class NameValueParser
	{
		const string REGEX_PATTERN
			= @"\G *(?<key>[\w ]+?) *= *(?:"
				+ @"(?<value>[^ ;'""][^;\t\r\n]*[^ ;'""])|"
				+ @"(?:(?<quote>')(?<value>(?:[^'\t\r\n]|'')*)')|"
				+ @"(?:(?<quote>"")(?<value>(?:[^""\t\r\n]|"""")*)"")"
			+ @") *;? *";
		static Regex NameValueRegex = new Regex(REGEX_PATTERN);
		
		/// What about case sesitiveness?
		
		static public IDictionary Parse(string input, IDictionary dict)
		{
			MatchCollection matches = NameValueRegex.Matches(input);
			
			int totalLen = 0;
			foreach (Match match in matches) {
				string key = match.Groups["key"].Value;
				string value = match.Groups["value"].Value;
				string quote = match.Groups["quote"].Value;
				
				//undouble quotes
				if (!string.IsNullOrEmpty(quote)) value = value.Replace(quote + quote, quote);
				
				dict.Add(key, value);
				
				totalLen += match.Length;
			}
			
			if (totalLen < input.Length) throw new ArgumentException("Invalid connection string");
			
			return dict;
		}

		static public string UnParse(IDictionary<string, string> dict)
		{
			StringBuilder ret = new StringBuilder();
			foreach (KeyValuePair<string, string> pair in dict) {
				ret.AppendFormat("{0}=\"{1}\";", pair.Key, pair.Value.Replace("\"", "\"\""));
			}
			return ret.ToString();
		}
	}
}
