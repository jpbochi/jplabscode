using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;
using System.Security;
using System.Security.Permissions;

namespace JpLabs.MQAL
{
	public static class EnvironmentVariables
	{
		// Import the kernel32 dll.
		[DllImport("kernel32.dll",CharSet=CharSet.Auto, SetLastError=true)]

		// The declaration is similar to the SDK function
		[return:MarshalAs(UnmanagedType.Bool)]
		public static extern bool SetEnvironmentVariable(string lpName, string lpValue);

		public static bool Set(string environmentVariable, string variableValue)
		{
			//try {
				
				// Get the write permission to set the environment variable.
				EnvironmentPermission environmentPermission = new EnvironmentPermission(EnvironmentPermissionAccess.Write, environmentVariable);

				environmentPermission.Demand(); 

				return SetEnvironmentVariable(environmentVariable, variableValue);
			//} catch (SecurityException e) {
			//    //Console.WriteLine("Exception:" + e.Message);
			//    throw;
			//}
		}
		
		public static string Get(string environmentVariable)
		{
			return Environment.GetEnvironmentVariable(environmentVariable);
		}
	}
}
