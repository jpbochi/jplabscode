using System;
using System.Collections.Generic;
using System.Text;

namespace JpLabs.MQAL
{
	static public class Enumerable
	{
		static public IEnumerable<T> Empty<T>()
		{
			yield break;
		}

		static public T First<T>(IEnumerable<T> source)
		{
			using (IEnumerator<T> enumerator = source.GetEnumerator()) {
				if (enumerator.MoveNext()) {
					return enumerator.Current;
				} else {
					throw new InvalidOperationException("Source is empty");
				}
			}
		}

		static public T FirstOrDefault<T>(IEnumerable<T> source)
		{
			using (IEnumerator<T> enumerator = source.GetEnumerator()) {
				return (enumerator.MoveNext()) ? enumerator.Current : default(T);
			}
		}
		
		static public IEnumerable<T> ToList<T>(ICollection<T> coll)
		{
			T[] list = new T[coll.Count];
			coll.CopyTo(list, 0);
			return list;
		}

		static public IList<T> ToList<T>(IEnumerable<T> source)
		{
			return new List<T>(source);
		}
	}
}
