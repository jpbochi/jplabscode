using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using IBM.WMQ;
using System.Diagnostics;

namespace JpLabs.MQAL
{
	/// <summary>Simple EventArgs with an Exception propery</summary>
	public class ExceptionEventArgs : EventArgs
	{
		Exception exception;
		
		public ExceptionEventArgs(Exception exception)
		{
			this.exception = exception;
		}
		
		public Exception Exception
		{
			get { return exception; }
		}
	}
	
	/// <summary>Represents a connection to a MQSeries manager</summary>
	/// <remarks>The same QConnection can be used to connect to different queues</remarks>
	public class QConnection : IQConnection
	{
		/// <summary>Occurs when an exception happens in the connection pool control</summary>
		/// <remarks>
		/// Beware that the connection pool control runs on asynchronously from the rest of the application.
		/// It means that this event will run on a isolated thread.
		/// </remarks>
		static public event EventHandler<ExceptionEventArgs> ConnectionPoolControlError;
		
		/// <summary>Function internally used to invoke the <see cref="ConnectionPoolControlError"/> event</summary>
		static internal void OnConnectionPoolControlError(object sender, Exception exception)
		{
			if (ConnectionPoolControlError != null) ConnectionPoolControlError(sender, new ExceptionEventArgs(exception));
		}
		
		/// <summary>Default timeout (in miliseconds) for read operations</summary>
		public const int DEFAULT_READ_WAIT_TIMEOUT = 1618;
		bool isDisposed;
		QInternalConnection InternalConn;
		QConnParams mConnectionParams;
		int defaultReadWaitTimeout;
		//int mConnectionTimeout;
		
		/// <summary>Create an instance with given <paramref name="connParams"/></summary>
		/// <param name="connParams">Connection parameters encapsulated in a <see cref="QConnParams"/> instance</param>
		public QConnection(QConnParams connParams)
		{
			Debug.Assert(connParams != null);
			isDisposed = false;
			mConnectionParams = connParams;
			defaultReadWaitTimeout = DEFAULT_READ_WAIT_TIMEOUT;
			//mConnectionTimeout = -1;
		}

		/// <summary>Create an instance with an empty connection string set.</summary>
		public QConnection() : this(new QConnParams())
		{}

		/// <summary>Create an instance with given <paramref name="connString"/></summary>
		/// <param name="connString">A valid connection string</param>
		public QConnection(string connString) : this()
		{
			ConnectionString = connString;
		}
		
		/// <summary>Gets the connection parameters encapsulated in a <see cref="QConnParams"/> object</summary>
		public QConnParams ConnectionParams
		{
			get { return mConnectionParams; }
		}

		/// <summary>Gets/sets the connection string</summary>
		public string ConnectionString
		{
			get { return mConnectionParams.ToString(); }
			set	{ mConnectionParams = QConnParams.Parse(value); }
		}
		
		//public int ConnectionTimeout
		//{
		//    get { return mConnectionTimeout; }
		//}
		
		/// <summary>Timeout (in miliseconds) to be used by reader commands created by this connection</summary>
		public int ReadWaitTimeout
		{
			get { return defaultReadWaitTimeout; }
			set { defaultReadWaitTimeout = value; }
		}
		
		/// <summary>Gets the current state of the connection</summary>
		public QConnectionState State
		{
			get {
				return
					(InternalConn == null) ? QConnectionState.Closed :
					(!InternalConn.IsConnected) ? QConnectionState.Closed :
					QConnectionState.Open;
			}
		}
		
		QTransaction mTransaction;
		public QTransaction CurrentTransaction
		{
			get { return (HasTransaction()) ? mTransaction : null; }
		}

		public QTransaction BeginTransaction()
		{
			if (HasTransaction()) {
				throw new ApplicationException("Nested transactions are not supported");
			//} else if (State != QConnectionState.Open) {
			//	throw new ApplicationException("Connection is closed");
			} else {
				mTransaction = new QTransaction(this);
				return mTransaction;
			}
		}

		public bool HasTransaction()
		{
			return State == QConnectionState.Open
				&& mTransaction != null
				&& mTransaction.Connection == this;
		}

		public void CommitTransaction()
		{
			if (HasTransaction()) {
				InternalConn.Commit();
				mTransaction = null;
			}
		}

		public void RollbackTransaction()
		{
			if (HasTransaction()) {
				InternalConn.Backout();
				mTransaction = null;
			}
		}

		public IQMonitor CreateMonitor()
		{
			throw new NotImplementedException();
		}

		public QReader CreateReader(string queueName)
		{
			return CreateReader(queueName, false);
		}

		public QReader CreateReader(string queueName, bool exclusiveAccess)
		{
			if (State != QConnectionState.Open) {
				throw new ApplicationException("Connection is closed");
			} else {
				int openOptions = (exclusiveAccess) ? MQC.MQOO_INPUT_EXCLUSIVE : MQC.MQOO_INPUT_SHARED;
				openOptions |= MQC.MQOO_FAIL_IF_QUIESCING;
				
				QQueueConnection q = InternalConn.GetOpenQueue(queueName, openOptions);
				return new QReader(this, q);
			}
		}
		
		public QWriter CreateWriter(string queueName)
		{
			if (State != QConnectionState.Open) {
				throw new ApplicationException("Connection is closed");
			} else {
				QQueueConnection q = InternalConn.GetOpenQueue(queueName, MQC.MQOO_OUTPUT | MQC.MQOO_FAIL_IF_QUIESCING);
				return new QWriter(this, q);
			}
		}
		
		public QBrowser CreateBrowser(string queueName)
		{
			if (State != QConnectionState.Open) {
				throw new ApplicationException("Connection is closed");
			} else {
				QQueueConnection q = InternalConn.GetOpenQueue(queueName, MQC.MQOO_BROWSE | MQC.MQOO_FAIL_IF_QUIESCING);
				return new QBrowser(this, q);
			}
		}
		
		/// <remarks>If the queueName is an ALIAS then MQRC_SELECTOR_NOT_FOR_TYPE will be thrown</remarks>
		public QInquirer CreateInquirer(string queueNameNoAlias)
		{
			if (State != QConnectionState.Open) {
				throw new ApplicationException("Connection is closed");
			} else {
				QQueueConnection q = InternalConn.GetOpenQueue(queueNameNoAlias, MQC.MQOO_SET | MQC.MQOO_INQUIRE | MQC.MQOO_FAIL_IF_QUIESCING);
				return new QInquirer(this, q);
			}
		}
		
		/// <summary>Get an open connection from the pool or create a new one</summary>
		internal QInternalConnection GetOpenConnection()
		{
			return QConnectionPool.Pool.GetOrCreate(ConnectionParams);
		}

		/// <summary>Opens a MQSeries connection by means of a <see cref="IBM.WMQ.MQQueueManager"/>, if it's not opened</summary>
		/// <remarks>Under the covers, a pool of actual MQSeries connections is used.</remarks>
		public void Open()
		{
			if (State != QConnectionState.Open) {
				InternalConn = GetOpenConnection();
			}
		}

		/// <summary>Releases the MQSeries connection back to the pool, if it's opened</summary>
		public void Close()
		{
			if (State == QConnectionState.Open) {
				if (HasTransaction()) RollbackTransaction();
				InternalConn.MoveToPool();
				InternalConn = null;
			}
		}

		void IDisposable.Dispose()
		{
			if (!isDisposed) {
				Close();
				isDisposed = true;
			}
		}
		
		~QConnection()
		{
			((IDisposable)this).Dispose();
		}

		#region IQConnection Members

		IQTransaction IQConnection.BeginTransaction()
		{
			return BeginTransaction();
		}

		IQTransaction IQConnection.CurrentTransaction
		{
			get { return CurrentTransaction; }
		}

		IQMonitor IQConnection.CreateMonitor(string queueName)
		{
			return CreateMonitor();
		}

		IQReader IQConnection.CreateReader(string queueName)
		{
			return CreateReader(queueName);
		}

		IQWriter IQConnection.CreateWriter(string queueName)
		{
			return CreateWriter(queueName);
		}

		void IQConnection.Open()
		{
			Open();
		}

		void IQConnection.Close()
		{
			Close();
		}

		#endregion
	}
}
