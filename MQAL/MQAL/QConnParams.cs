using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.IO;
using IBM.WMQ;

namespace JpLabs.MQAL
{
	/// <summary>This class encapsulates connection parameters to a MQSeries channel</summary>
	public class QConnParams
	{
		public const string MQ_CTF_DIRECTORY_ENV_VARIABLE = "MQCHLLIB";
		public const string MQ_CTF_FILE_NAME_ENV_VARIABLE = "MQCHLTAB";

		public const string HOST_KEY				= MQC.HOST_NAME_PROPERTY; //"hostname"
		public const string PORT_KEY				= MQC.PORT_PROPERTY; //"port"
		public const string CHANNEL_KEY				= MQC.CHANNEL_PROPERTY; //"channel"
		public const string QUEUE_MANAGER_KEY		= "QueueManager";
		public const string CHANNEL_TABLE_FILE_KEY	= "CTF";
		public const string USE_CTF_KEY				= "UseCTF";
		public const bool USE_CTF_BY_DEFAULT		= false;
		
		IDictionary<string, string> Params;
		int? hashCode;
		
		public QConnParams()
		{
			//TOREVIEW: Why a SortedDictionary? I honestly don't remember
			Params = new SortedDictionary<string,string>();
		}
		
		public QConnParams(IDictionary<string, string> dictParams)
		{
			Params = new SortedDictionary<string,string>(dictParams);
		}
		
		private string GetParam(string key)
		{
			string value;
			return (Params.TryGetValue(key, out value)) ? value : null;
		}
		
		private void SetParam(string key, string value)
		{
			Params[key] = value;
		}

		private bool GetBoolParam(string key, bool defaultValue)
		{
			bool parsed;
			return (bool.TryParse(GetParam(key), out parsed)) ? parsed : defaultValue;
		}
		
		private void SetBoolParam(string key, bool value)
		{
			Params[key] = value.ToString();
		}
		
		public string HostName
		{
			get { return GetParam(HOST_KEY); }
			set { SetParam(HOST_KEY, value); }
		}
		
		public string Port
		{
			get { return GetParam(PORT_KEY); }
			set { SetParam(PORT_KEY, value); }
		}
		
		public string Channel
		{
			get { return GetParam(CHANNEL_KEY); }
			set { SetParam(CHANNEL_KEY, value); }
		}
		
		public string QueueManager
		{
			get { return GetParam(QUEUE_MANAGER_KEY); }
			set { SetParam(QUEUE_MANAGER_KEY, value); }
		}

		public string ChannelTableFile
		{
			get { return GetParam(CHANNEL_TABLE_FILE_KEY); }
			set { SetParam(CHANNEL_TABLE_FILE_KEY, value); }
		}

		public bool UseChannelTableFile
		{
			get { return GetBoolParam(USE_CTF_KEY, USE_CTF_BY_DEFAULT); }
			set { SetBoolParam(USE_CTF_KEY, value); }
		}
		
		public Hashtable GetMQProperties()
		{
			//TOREVIEW: only a subset of Params is needed to pass as MQ Properties (Host, Port and Channel are the know ones)
			return new Hashtable((IDictionary)Params);
		}
		
		static public void SetMQCTFEnvVariables(string ctfFilePath)
		{
			string ctfDir = Path.GetDirectoryName(ctfFilePath);
			string ctfFileName = Path.GetFileName(ctfFilePath);
			
			if (string.IsNullOrEmpty(ctfDir)) ctfDir = Environment.CurrentDirectory;
			
			EnvironmentVariables.Set(MQ_CTF_DIRECTORY_ENV_VARIABLE, ctfDir);
			EnvironmentVariables.Set(MQ_CTF_FILE_NAME_ENV_VARIABLE, ctfFileName);
		}
		
		public MQQueueManager Connect()
		{
			try {
				string ctf = this.ChannelTableFile;
				if (ctf != null) {
					SetMQCTFEnvVariables(ctf);
					return new MQQueueManager(QueueManager);
				} else if (UseChannelTableFile) {
					return new MQQueueManager(QueueManager);
				} else {
					return new MQQueueManager(QueueManager, GetMQProperties());
				}
			} catch (MQException ex) {
				throw QException.Create(ex);
			}
		}

		static public QConnParams Parse(string connString)
		{
			Dictionary<string, string> dictParams = new Dictionary<string,string>();
			NameValueParser.Parse(connString, dictParams);
			return new QConnParams(dictParams);
		}

		public override string ToString()
		{
			return NameValueParser.UnParse(Params);
		}

		public override bool Equals(object obj)
		{
			QConnParams other = obj as QConnParams;
			if (other != null) {
				return this.GetHashCode() == other.GetHashCode();
			} else {
				return base.Equals(obj);
			}
		}
		
		public override int GetHashCode()
		{
			if (hashCode == null) hashCode = this.ToString().GetHashCode();
			return hashCode.Value;
		}
	}
}
