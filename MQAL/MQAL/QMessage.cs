using System;
using System.Collections.Generic;
using System.Text;
using IBM.WMQ;

namespace JpLabs.MQAL
{
	public class QMessage : IQMessage
	{
		public static implicit operator QMessage(string msg)
		{
			return new QMessage(msg);
		}
		public static implicit operator string(QMessage msg)
		{
			return msg.ToString();
		}
		
	    public QMessage(string msg)
	    {
			mPayLoad = msg;
	    }

	    public QMessage(MQMessage msg)
	    {
			UpdateFromMQSeriesMessage(msg);
	    }
	    
	    static public QMessage FromMQSeriesMessage(MQMessage msg)
	    {
			return (msg == null) ? null : new QMessage(msg);
		}

	    public void UpdateFromMQSeriesMessage(MQMessage mqSeriesMsg)
	    {
			this.mMessageId = mqSeriesMsg.MessageId;
			mqSeriesMsg.Seek(0);
			this.mPayLoad = mqSeriesMsg.ReadString(mqSeriesMsg.MessageLength);
			this.mSentOn = mqSeriesMsg.PutDateTime.ToLocalTime();
			this.mPutApplicationName = mqSeriesMsg.PutApplicationName;
			this.mReadOn = DateTime.Now;
			this.mRollbackCount = mqSeriesMsg.BackoutCount;
			this.mType = mqSeriesMsg.MessageType;//null;
	    }
	    
		object mMessageId;
		public object MessageId
		{
			get { return mMessageId; }
		}

		string mPayLoad;
		public string PayLoad
		{
			get { return mPayLoad; }
		}

		object mType;
		public object Type
		{
			get { return mType; }
		}

		DateTime mSentOn;
		public DateTime SentOn
		{
			get { return mSentOn; }
		}

		DateTime mReadOn;
		public DateTime ReadOn
		{
			get { return mReadOn; }
		}
		
		string mPutApplicationName;
		public string PutApplicationName
		{
			get { return mPutApplicationName; }
		}

		int mRollbackCount;
		public int RollbackCount
		{
			get { return mRollbackCount; }
		}

		public override string ToString()
		{
			return PayLoad;
		}
	}
}
