﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.ComponentModel;
using System.Diagnostics;

//[assembly: JpLabs.Threading.PluginAttachPoint(typeof(int))]

//class TypeProvider
//{
//    Type[] RegisterTypes()
//    {
//        // code omitted...
//    }
//}

namespace JpLabs.Threading.Demo
{
	
	class Program
	{
		static void Main(string[] args)
		{
			//Simple Wait Test
			Console.WriteLine("Simple Wait Test");
			{
				IFuture<int> future = Future.Create(
					() => {
						Thread.Sleep(100);
						return 42;
					}
				);
				
				WaitAndOutput(future);
			}
			
			//Exception Test
			Console.WriteLine();
			Console.WriteLine("Exception Test");
			{
				IFuture<int> future = Future.Create<int>(
					() => {
						throw new TimeoutException();
					}
				);
				
				WaitAndOutput(future);
			}

			//CancelAndWait Test
			Console.WriteLine();
			Console.WriteLine("CancelAndWait Test");
			{
				var future = Future.Create(
					() => {
						Thread.Sleep(1000);
						return Math.PI;
					}
				);
				
				CancelAndWaitAndOutput(future);
			}

			//Completed Event Test
			Console.WriteLine();
			Console.WriteLine("Completed Event Test");
			{
				var future = Future.Create(
					() => {
						Thread.Sleep(42);
						return Math.E;
					}
				);
				
				const int NUMBER_OF_HANDLERS = 16;
				Random rnd = new Random();
				
				//Counter handlerCallCounter = new Counter();
				int count = 0;
				
				for (int i=0; i<NUMBER_OF_HANDLERS; i++)
				{
					Thread.Sleep(rnd.Next(42));
					Thread.Sleep(rnd.Next(42));
					Thread.Sleep(rnd.Next(42));
					Thread.Sleep(rnd.Next(42));
					future.Completed += CreateCompletedHandler(
						ref count, //handlerCallCounter,
						index => {
							Thread.Sleep(42);
							Console.WriteLine("Completed Handler {0} at thread {1}", index, Thread.CurrentThread.ManagedThreadId);
							//handlerCallCounter.Count++);
						}
					);
				}
				
				WaitAndOutput(future);

				future.Completed += CreateCompletedHandler(
					ref count, //handlerCallCounter,
					index => {
						Thread.Sleep(3);
						Console.WriteLine("Completed Handler {0} at thread {1} (added after Wait)", index, Thread.CurrentThread.ManagedThreadId);
						//handlerCallCounter.Count++);
					}
				);
				
				//Wait while handlers might be being called
				Thread.Sleep(500);
				
				//Debug.Assert(handlerCallCounter.Count == NUMBER_OF_HANDLERS + 1);
			}
			
			//Set Value Explicitly Test
			Console.WriteLine();
			Console.WriteLine("Set Value Explicitly Test");
			{
				var future = Future.Create<double>();
				
	            var setValueFunc = (WaitCallback)(
					state => {
						var localFuture = (IFuture<double>)state;
						
						Thread.Sleep(200);
						
						localFuture.Value = 2 * Math.PI;
					}
	            );
	            
	            ThreadPool.QueueUserWorkItem(setValueFunc, future);
				
				WaitAndOutput(future);
			}
			
			//TODO: make other tests

			if (System.Diagnostics.Debugger.IsAttached) {
				Console.Write("Press any key to continue . . . ");
				Console.ReadKey(true);
				Console.WriteLine();
			}
		}
		
		class Counter
		{
			public volatile int Count = 0;
		}
		
		static EventHandler<AsyncCompletedEventArgs> CreateCompletedHandler(ref int ix, Action<int> action)
		{
			int arg = ix++;
			return (EventHandler<AsyncCompletedEventArgs>)(
				(sender, e) => {
					action(arg);
				}
			);
		}
		
		static void WaitAndOutput<T>(IFuture<T> future)
		{
			try
			{
				future.Wait();
			} catch (Exception e) {
				Console.WriteLine("Exception thrown: {0} : {1}", e.GetType().Name, e.Message);
				return;
			}
			
			Console.WriteLine("Future.Value: {0}", future.Value);
		}

		static void CancelAndWaitAndOutput<T>(IFuture<T> future)
		{
			try
			{
				future.CancelAndWait();
			} catch (Exception e) {
				Console.WriteLine("Exception thrown: {0} : {1}", e.GetType().Name, e.Message);
				return;
			}

			Console.WriteLine("Future.Value: {0}", future.IsCancelled ? "{cancelled}" : future.Value.ToString());
		}
	}
}
