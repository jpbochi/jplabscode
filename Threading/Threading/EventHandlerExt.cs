﻿using System;

namespace JpLabs.Threading
{
	public static class EventHandlerExt
	{
		static public void RaiseEvent(this EventHandler handler, object sender, EventArgs e)
		{
			if (handler != null) handler(sender, e);
		}
		
		static public void RaiseEvent<T>(this EventHandler<T> handler, object sender, T e) where T : EventArgs
		{
			if (handler != null) handler(sender, e);
		}
	}
	
	//[AttributeUsage(AttributeTargets.Assembly)]
	//public class PluginAttachPointAttribute : Attribute
	//{
	//    public Type AttachPoint { get; private set; }
		
	//    public PluginAttachPointAttribute(Type attachPoint)
	//    {
	//        this.AttachPoint = attachPoint
	//    }
	//}
}
