﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using System.Security.Permissions;
using JpLabs.Extensions;

namespace JpLabs.Threading
{
	public static class AggregateExceptionExt
	{
		public static Exception Aggregate(this Exception oldEx, Exception newEx)
		{
			//No old exception => return the new one
			if (oldEx == null) return newEx;
			
			//No new exception => return the old one
			if (newEx == null) return oldEx;
			
			//Get old exceptions
			AggregateException oldAggrEx = oldEx as AggregateException;
			IEnumerable<Exception> exceptions
				= (oldAggrEx != null)
				? oldAggrEx.InnerExceptions
				: oldEx.ToEnumerable();
			
			//Concat with new exceptions
			AggregateException newAggrEx = newEx as AggregateException;
			exceptions
				= (newAggrEx != null)
				? exceptions.Concat(newAggrEx.InnerExceptions)
				: exceptions.Concat(newEx);
			
			//Get array of exceptions
			ICollection<Exception> exceptionList = exceptions.ToArray();
			
			//There's only one exception => return it
			if (exceptionList.Count == 1) return exceptionList.Single();
			
			//Multiple exceptions => return new AggregateException
			return new AggregateException(exceptionList);			
		}
	}

	[Serializable, System.Diagnostics.DebuggerDisplay("Count = {InnerExceptions.Count}")]
	public class AggregateException : Exception
	{
		private const string CTOR_DEFAULTMESSAGE = "Multiple exceptions have been thrown.";
		private const string CTOR_INNEREXCEPTIONNULL = "An element of innerExceptions was null.";
		private const string DESERIALIZATION_FAILURE_MESSAGE = "The serialization stream contains no inner exceptions.";
		private const string TOSTRING_FORMAT = "{0}{1}---> (Inner Exception #{2}) {3}{4}{5}";

		// Fields
		private readonly ReadOnlyCollection<Exception> m_innerExceptions;

		// Methods
		public AggregateException() : base(CTOR_DEFAULTMESSAGE)
		{
			this.m_innerExceptions = new ReadOnlyCollection<Exception>(new Exception[0]);
		}

		public AggregateException(IEnumerable<Exception> innerExceptions) : this(CTOR_DEFAULTMESSAGE, innerExceptions)
		{
		}

		public AggregateException(params Exception[] innerExceptions) : this(CTOR_DEFAULTMESSAGE, innerExceptions)
		{
		}

		public AggregateException(string message) : base(message)
		{
			this.m_innerExceptions = new ReadOnlyCollection<Exception>(new Exception[0]);
		}

		[SecurityPermission(SecurityAction.LinkDemand, Flags=SecurityPermissionFlag.SerializationFormatter)]
		protected AggregateException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			if (info == null) throw new ArgumentNullException("info");
			
			Exception[] list = info.GetValue("InnerExceptions", typeof(Exception[])) as Exception[];
			if (list == null) throw new SerializationException(DESERIALIZATION_FAILURE_MESSAGE);
			
			this.m_innerExceptions = new ReadOnlyCollection<Exception>(list);
		}

		public AggregateException(string message, IEnumerable<Exception> innerExceptions)
			: this(message, (innerExceptions == null) ? null : new List<Exception>(innerExceptions))
		{
		}

		public AggregateException(string message, Exception inner)
			: base(message, inner)
		{
			if (inner == null) throw new ArgumentNullException("inner");
			
			this.m_innerExceptions = new ReadOnlyCollection<Exception>(new [] { inner });
		}

		private AggregateException(string message, IList<Exception> innerExceptions)
			: base(message, ((innerExceptions != null) && (innerExceptions.Count > 0)) ? innerExceptions[0] : null)
		{
			if (innerExceptions == null)
			{
				throw new ArgumentNullException("innerExceptions");
			}
			Exception[] list = new Exception[innerExceptions.Count];
			for (int i = 0; i < list.Length; i++)
			{
				list[i] = innerExceptions[i];
				if (list[i] == null) throw new ArgumentException(CTOR_INNEREXCEPTIONNULL);
			}
			this.m_innerExceptions = new ReadOnlyCollection<Exception>(list);
		}

		public AggregateException(string message, params Exception[] innerExceptions)
			: this(message, (IList<Exception>)innerExceptions)
		{
		}

		public AggregateException Flatten()
		{
			List<Exception> list = new List<Exception>();
			Queue<AggregateException> queue = new Queue<AggregateException>();
			queue.Enqueue(this);
			while (queue.Count > 0)
			{
				IList<Exception> innerExceptions = queue.Dequeue().InnerExceptions;
				for (int i = 0; i < innerExceptions.Count; i++)
				{
					Exception item = innerExceptions[i];
					if (item == null) throw new ArgumentException(CTOR_INNEREXCEPTIONNULL);
					
					AggregateException exception2 = item as AggregateException;
					if (exception2 != null)
					{
						queue.Enqueue(exception2);
					}
					else
					{
						list.Add(item);
					}
				}
			}
			return new AggregateException(this.Message, list.ToArray());
		}

		[SecurityPermission(SecurityAction.LinkDemand, Flags=SecurityPermissionFlag.SerializationFormatter)]
		public override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			if (info == null)
			{
				throw new ArgumentNullException("info");
			}
			base.GetObjectData(info, context);
			Exception[] array = new Exception[this.m_innerExceptions.Count];
			this.m_innerExceptions.CopyTo(array, 0);
			info.AddValue("InnerExceptions", array, typeof(Exception[]));
		}

		public void Handle(Func<Exception, bool> handler)
		{
			if (handler == null) throw new ArgumentNullException("handler");
			
			List<Exception> list = null;
			for (int i = 0; i < this.m_innerExceptions.Count; i++)
			{
				if (!handler(this.m_innerExceptions[i]))
				{
					if (list == null)
					{
						list = new List<Exception>();
					}
					list.Add(this.m_innerExceptions[i]);
				}
			}
			
			if (list != null) throw new AggregateException(this.Message, list.ToArray());
		}

		public override string ToString()
		{
			string str = base.ToString();
			for (int i = 0; i < this.m_innerExceptions.Count; i++)
			{
				str = string.Format(System.Globalization.CultureInfo.InvariantCulture, TOSTRING_FORMAT, new object[] { str, Environment.NewLine, i, this.m_innerExceptions[i].ToString(), "<---", Environment.NewLine });
			}
			return str;
		}

		// Properties
		public ReadOnlyCollection<Exception> InnerExceptions
		{
			get
			{
				return this.m_innerExceptions;
			}
		}
	}
}
