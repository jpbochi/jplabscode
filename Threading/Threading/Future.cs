﻿using System;
using System.ComponentModel;
using System.Threading;

namespace JpLabs.Threading
{
	/// <summary>
	/// Static class for creating Futures
	/// </summary>
	public static class Future
	{
		/// <summary>
		/// Creates a new Future{T} that is considered complete when its Value property is explicitly set
		/// </summary>
		/// <typeparam name="T">The type of the value associated with the Future{T} to create.</typeparam>
		/// <returns>A new Future{T} object</returns>
		public static IFuture<T> Create<T>()
		{
			return new Future<T>(null, null);
		}

		/// <summary>
		/// Creates a new Future{T} associated with a Func{T} delegate.
		/// </summary>
		/// <typeparam name="T">The type of the value associated with the Future{T} to create.</typeparam>
		/// <param name="valueSelector">A Func{T} that yields the future value.</param>
		/// <returns>A new Future{T} object</returns>
		/// <remarks>
		/// The valueSelector delegate will be called asynchronously, and its returned value will be
		/// set to the future's Value property.
		/// </remarks>
		public static IFuture<T> Create<T>(Func<T> valueSelector)
		{
			return Create(valueSelector, null);
		}
		
		/// <summary>
		/// Creates a new Future{T} associated with a Func{T} delegate and an Action delegate.
		/// </summary>
		/// <typeparam name="T">The type of the value associated with the Future{T} to create.</typeparam>
		/// <param name="valueSelector">A Func{T} that yields the future value.</param>
		/// <param name="signalCancelTask">An Action that should signal the task to be cancelled</param>
		/// <returns>A new Future{T} object</returns>
		/// <remarks>
		/// The valueSelector delegate will be called asynchronously, and its returned value will be
		/// set to the future's Value property.
		/// It an request for the cancelllation of the created future happens, the signalCancelTask
		/// delegate is called. This delegate should run quickly and only be used to signal the task to be cancelled.
		/// If signalCancelTask throws an exception, it will be ignored.
		/// </remarks>
		public static IFuture<T> Create<T>(Func<T> valueSelector, Action signalCancelTask)
		{
			if (valueSelector == null) throw new ArgumentNullException("valueSelector");
			
			var future = new Future<T>(valueSelector, signalCancelTask);
			future.InternalStart();
			return future;
		}
	}
	
	/// <summary>
	/// Represents a value that will only be available after an asynchronous task runs
	/// </summary>
	/// <typeparam name="T">Type of the future value</typeparam>
	internal class Future<T> : IFuture<T>
	{
		private readonly object syncRoot = new object();
		
		private readonly Func<T> valueSelectorFunc;
		private readonly Action signalCancelTaskFunc;
		private EventWaitHandle completeEventWaitHandle;
		
		private volatile bool isStarted;
		private volatile bool isCancelled;
		private volatile bool isCompleted;
		private bool isDisposed;
		
		private T futureValue;
		private bool futureValueWasSet;

		//private CustomEvent<EventHandler<AsyncCompletedEventArgs>> _completedEvent = new WeakSyncedEvent<EventHandler<AsyncCompletedEventArgs>>();
		private EventHandler<AsyncCompletedEventArgs> completedEventDlg;
		
		internal Future(Func<T> valueSelector) : this(valueSelector, null)
		{}
		
		internal Future(Func<T> valueSelector, Action signalCancelTaskFunc)
		{
			this.valueSelectorFunc = valueSelector;
			this.signalCancelTaskFunc = signalCancelTaskFunc;
			this.completeEventWaitHandle = new ManualResetEvent(false);
		}
		
		internal void InternalStart()
		{
			this.ThrowIfDisposed();
			
			if (valueSelectorFunc == null) throw new InvalidOperationException("valueSelector is null");
			
			lock (syncRoot)
			{
				if (!isStarted)
				{
					isStarted = true;
					
					ThreadPool.QueueUserWorkItem(DoWork, null);
				}
			}
		}

		public event EventHandler<AsyncCompletedEventArgs> Completed
		{
			add
			{
				lock (syncRoot)
				{
					//Add handler only if future has not completed yet
					if (!this.isCompleted)
					{
						completedEventDlg += value;
						return;
					}
				}
				
				//If future has completed, call the new handler right now
				//Observe that the handler is not added
				var evArgs = new AsyncCompletedEventArgs(this.Exception, this.isCancelled, this.futureValue);
				value.RaiseEvent(this, evArgs);
			}
			remove
			{
				lock (syncRoot) completedEventDlg -= value;
			}
		}
		
		public T Value
		{
			get
			{
				if (!isCompleted) Wait();
				
				ThrowIfExceptional(false);
				
				return this.futureValue;
			}
			set
			{
				this.ThrowIfDisposed();
				lock (syncRoot)
				{
					if (valueSelectorFunc != null) throw new InvalidOperationException("This future has valueSelector assigned, so Value cannot be set externally");
					if (futureValueWasSet) throw new InvalidOperationException("Value was already set");
					this.futureValueWasSet = true;
				}
				this.futureValue = value;
				SetComplete();
			}
		}

		public bool IsCompleted
		{
			get { return isCompleted; }
		}

		public bool IsCancelled
		{
			get { return isCancelled; }
		}

		public Exception Exception
		{
			get; private set;
		}

		public void Cancel()
		{
			this.ThrowIfDisposed();
			
			lock (syncRoot)
			{
				//Can't cancel a completed future; Neither a cancelled one
				if (isCompleted || isCancelled) return;
				isCancelled = true;
			}
			
			//TODO: REVIEW: should I care about exceptions in _signalCancelTaskFunc?
			
			if (signalCancelTaskFunc != null) signalCancelTaskFunc.BeginInvoke(null, null);//SignalCancelTaskFuncAsyncCallback, null);
			
			this.SetComplete(new OperationCanceledException());
		}
		
		public void CancelAndWait()
		{
			CancelAndWait(Timeout.Infinite);
		}

		public bool CancelAndWait(int millisecondsTimeout)
		{
			//Cancel: Signal the task to be cancelled
			Cancel();
			
			//And Wait: Try to wait for it to complete, while ignoring cancellation exceptions
			return this.Wait(millisecondsTimeout, true);
		}

		public void Wait()
		{
			Wait(Timeout.Infinite);
		}

		public bool Wait(int millisecondsTimeout)
		{
			return Wait(millisecondsTimeout, false);
		}
		
		public bool Wait(int millisecondsTimeout, bool ignoreTaskCanceledException)
		{
			this.ThrowIfDisposed();
			
			//Wait for the requested time
			bool signalReceived = completeEventWaitHandle.WaitOne(millisecondsTimeout, false);
			
			//Throw exception if task was exceptional
			ThrowIfExceptional(ignoreTaskCanceledException);
			
			//Returns true if task has completed
			return signalReceived;
		}

		public void Dispose()
		{
			//If future has not completed, cancel it
			if (!this.isCompleted) CancelAndWait();
			
			//Mark this as disposed
			lock (syncRoot)
			{
				if (isDisposed) return;
				isDisposed = true;
			}
			
			//Release Complete EventWaitHandle
			if (completeEventWaitHandle != null)
			{
				completeEventWaitHandle.Close();
				completeEventWaitHandle = null;
			}
		}

		private void DoWork(object state)
		{
			//1. Start valueSelector fuction
			Func<T> valueSelector = this.valueSelectorFunc;
			IAsyncResult selectorAsyncResult = valueSelector.BeginInvoke(null, null);
			
			//2. Wait for valueSelector or for complete WaitHandle to be set
			WaitHandle[] handlesToWait = new [] {
				selectorAsyncResult.AsyncWaitHandle,
				this.completeEventWaitHandle
			};
			int handleIndex = WaitHandle.WaitAny(handlesToWait);
			
			//3. Only EndInvoke on valueSelector if it finished first
			if (handleIndex == 0)
			{
				try
				{
					this.futureValue = valueSelector.EndInvoke(selectorAsyncResult);
					SetComplete();
				}
				catch (Exception ex)
				{
					SetComplete(ex);
				}
			}
		}
		
		////This method would be useful to catch exceptions on the SignalCancelTaskFunc
		//private void SignalCancelTaskFuncAsyncCallback(IAsyncResult ar)
		//{
		//    //Ignores any exception if future has completed already
		//    if (this._isCompleted) return;
			
		//    try
		//    {
		//        this._signalCancelTaskFunc.EndInvoke(ar);
		//    }
		//    catch (Exception e)
		//    {
		//        this.SetComplete(e);
		//    }
		//}

		private void SetComplete()
		{
			SetComplete(null);
		}
		
		private void SetComplete(Exception ex)
		{
			//Can only comple once
			lock (syncRoot)
			{
				//Aggregate exceptions
				if (ex != null) this.Exception = this.Exception.Aggregate(ex);
				
				//Set complete only once
				if (isCompleted) return;
				isCompleted = true;
				
				//Set the eventWaitHandle in order to release any waiting threads
				completeEventWaitHandle.Set();
			}
			
			//Raise the Completed event
			var evArgs = new AsyncCompletedEventArgs(this.Exception, this.isCancelled, this.futureValue);
			completedEventDlg.RaiseEvent(this, evArgs);
		}

		private void ThrowIfDisposed()
		{
			if (this.isDisposed) throw new ObjectDisposedException("Future has been disposed");
		}
		
		private void ThrowIfExceptional(bool ignoreTaskCanceledException)
		{
			if (this.Exception != null)
			{
				if (ignoreTaskCanceledException && this.Exception is OperationCanceledException) return;
				throw this.Exception;
			}
		}
	}
}
