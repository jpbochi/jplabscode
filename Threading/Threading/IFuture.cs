﻿using System;
using System.ComponentModel;

namespace JpLabs.Threading
{
	public interface IFuture<T> : IDisposable //: IAsyncResult
	{
		/// <summary>
		/// Get or sets the value of this future.
		/// </summary>
		/// <remarks>
		/// When get is called, if the future has not completed yet, Wait() will be called.
		/// Set is only allowed once, and for futures that don't have any valueSelectorFunc assigned.
		/// A future is considered complete as soon as a value is set
		/// </remarks>
		T Value { get; set; }
		
		/// <summary>
		/// Gets whether this future has completed.
		/// </summary>
		bool IsCompleted { get; }
		
		/// <summary>
		/// Gets whether this future has been cancelled.
		/// </summary>
		bool IsCancelled { get; }
		
		/// <summary>
		/// Gets the Exception that caused this future to end prematurely.
		/// If it completed successfully or has not yet thrown any exceptions, this will return null. 
		/// </summary>
		Exception Exception { get; }
		
		/// <summary>
		/// CLR event that's raised asynchronously when the task completes or times out.
		/// </summary>
		/// <remarks>
		/// This event is guaranteed to execute once, and only once,
		/// even if a handler is attached after the future has completed.
		/// </remarks>
		event EventHandler<AsyncCompletedEventArgs> Completed;
		
		/// <summary>
		/// Request the cancellation of this future
		/// It will cause an OperationCanceledException if task is actually cancelled.
		/// </summary>
		void Cancel();
		
		/// <summary>
		/// Request the cancellation of this future, and waits for the task to finish. It ignores OperationCanceledException.
		/// </summary>
		void CancelAndWait();
		
		/// <summary>
		/// Request the cancellation of this task, and waits for the task to finish. It ignores OperationCanceledException.
		/// </summary>
		/// <returns>True if task completed before the timeout</returns>
		bool CancelAndWait(int millisecondsTimeout);
		
		/// <summary>
		/// Waits for task to complete, throwing any exceptions thrown at the task execution.
		/// </summary>
		void Wait();
		
		/// <summary>
		/// Waits for task to complete, throwing any exceptions thrown at the task execution.
		/// </summary>
		/// <returns>True if task completed before the timeout</returns>
		bool Wait(int millisecondsTimeout);
	}
}
