﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using JpLabs.TrackableValue;

namespace JpLabs.TrackableValue.Demo
{
	public partial class MainForm : Form
	{
		IBackEndService updaterService;
		
		public MainForm()
		{
			InitializeComponent();
		}

		private void MainForm_Load(object sender, EventArgs e)
		{
			updaterService = GetUpdaterService();
			UpdateUI();
			updaterService.StatusChanged += UpdaterService_StatusChanged;
			updaterService.ValueChanged += UpdaterService_ValueChanged;
		}
		
		void UpdaterService_StatusChanged(object sender, ValueChangedEventArgs<ServiceStatus> e)
		{
			//Ensure that update will run on UI thread
			this.Invoke(() => { UpdateServiceUI(); });
		}

		void UpdaterService_ValueChanged(object sender, ValueChangedEventArgs<double?> e)
		{
			//Ensure that update will run on UI thread
			this.Invoke(() => { FeedNewValueToUI(e.NewValue, e.OldValue); });
		}

		private void btnStart_Click(object sender, EventArgs e)
		{
			updaterService.Start();
		}

		private void btnPause_Click(object sender, EventArgs e)
		{
			updaterService.Pause();
		}

		private void btnStop_Click(object sender, EventArgs e)
		{
			updaterService.Stop();
		}

		static IBackEndService GetUpdaterService()
		{
			//This function could be implemented in a different way that would eliminate a dependency on the actual UpdaterService implementation
			return new BackEndService();
		}

		void UpdateUI()
		{
			UpdateValueUI();
			UpdateServiceUI();
		}

		void UpdateServiceUI()
		{
			lblUpdaterStatus.Text = updaterService.Status.ToString();
			btnStart.Enabled = updaterService.CanStart;
			btnPause.Enabled = updaterService.CanPause;
			btnStop.Enabled = updaterService.CanStop;
		}

		void UpdateValueUI()
		{
			lblTrackedValue.Text = FormatValue(updaterService.Value);
			lblTrackedValue.ForeColor = (updaterService.Value < 0) ? Color.Red : Color.Blue;
		}
		
		void FeedNewValueToUI(double? newValue, double? oldValue)
		{
			if (newValue != null) {
				if (oldValue != null) {
					double diff = newValue.Value - oldValue.Value;
					string log = string.Format("{0} [{1:+0.00}]", FormatValue(newValue), FormatValue(diff));
					lstHistory.Items.Insert(0, log);
				} else {
					lstHistory.Items.Insert(0, FormatValue(newValue));
				}
			}

			UpdateUI();
		}
		
		/// <summary>
		/// Interface updates should only be made by the UI thread.
		/// This function will forward a delegate to be executed by the UI thread.
		/// </summary>
		void Invoke(Action action)
		{
			this.Invoke((Delegate)action);
		}
		
		static string FormatValue(double? value)
		{
			return value.HasValue ? string.Format("{0:0.00}", value.Value) : "{null}";
		}
	}
}
