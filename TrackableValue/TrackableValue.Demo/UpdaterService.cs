﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.ComponentModel;

namespace JpLabs.TrackableValue.Demo
{
	public interface IBackEndService
	{
		ServiceStatus Status { get; }
		double? Value { get; }
		
		event EventHandler<ValueChangedEventArgs<ServiceStatus>> StatusChanged;
		event EventHandler<ValueChangedEventArgs<double?>> ValueChanged;

		bool CanStart { get; }
		bool CanPause { get; }
		bool CanStop { get; }

		bool Start();
		bool Pause();
		bool Stop();
	}

	public enum ServiceStatus
	{
		Stopped, Running, Paused
	}

	public class BackEndService : IBackEndService, INotifyPropertyChanged, IDisposable
	{
		private readonly TrackableValue<double?> innerValue;
		private readonly TrackableValue<ServiceStatus> status;
		private Timer timer;
		
		public BackEndService()
		{
			innerValue = new TrackableValue<double?>(this);
			status = new TrackableValue<ServiceStatus>(this);
			
			status.ValueChanged += Status_ValueChanged;
			innerValue.ValueChanged += innerValue_ValueChanged;
		}

		public ServiceStatus Status	{ get { return status.Value; } }
		
		public double? Value		{ get { return innerValue.Value; } }

		public event PropertyChangedEventHandler PropertyChanged;
		
		public event EventHandler<ValueChangedEventArgs<ServiceStatus>> StatusChanged
		{
			add		{ status.ValueChanged += value; }
			remove	{ status.ValueChanged -= value; }
		}
		
		public event EventHandler<ValueChangedEventArgs<double?>> ValueChanged
		{
			add		{ innerValue.ValueChanged += value; }
			remove	{ innerValue.ValueChanged -= value; }
		}
		
		public bool CanStart
		{
			get { return status.Value != ServiceStatus.Running; }
		}

		public bool CanPause
		{
			get { return status.Value == ServiceStatus.Running; }
		}

		public bool CanStop
		{
			get { return status.Value != ServiceStatus.Stopped; }
		}
		
		public bool Start()
		{
			if (CanStart) {
				status.Value = ServiceStatus.Running;
				return true;
			}
			return false;
		}

		public bool Pause()
		{
			if (CanPause) {
				status.Value = ServiceStatus.Paused;
				return true;
			}
			return false;
		}

		public bool Stop()
		{
			if (CanStop) {
				status.Value = ServiceStatus.Stopped;
				return true;
			}
			return false;
		}

		private void OnPropertyChanged(PropertyChangedEventArgs  e)
		{
			var handler = this.PropertyChanged;
			if (handler != null) handler(this, e);
		}

		private void Status_ValueChanged(object sender, ValueChangedEventArgs<ServiceStatus> e)
		{
			switch (e.NewValue)
			{
				case ServiceStatus.Running: {
					if (e.OldValue == ServiceStatus.Stopped || timer == null) {
						//restart timer
						DisposeTimer();
						timer = new Timer(TimerCallback, new Random(), 0, GetTimeout());
					} else {
						timer.Change(0, GetTimeout());
					}
				} break;
				case ServiceStatus.Paused: {
					//pause timer
					if (timer != null) timer.Change(Timeout.Infinite, Timeout.Infinite);
				} break;
				case ServiceStatus.Stopped: {
					DisposeTimer();
					innerValue.Value = null;
				} break;
			}
			
			this.OnPropertyChanged(new PropertyChangedEventArgs("Status"));
		}

		private void innerValue_ValueChanged(object sender, ValueChangedEventArgs<double?> e)
		{
			this.OnPropertyChanged(new PropertyChangedEventArgs("Value"));
		}

		private void TimerCallback(object state)
		{
			var rnd = (Random)state;
			innerValue.Value = rnd.NextDouble() * 200 - 100;
		}
		
		private static int GetTimeout()
		{
			return 2000; //updates every 2 seconds (could be random)
		}
		
		private void DisposeTimer()
		{
			if (timer != null) {
				timer.Dispose();
				timer = null;
			}
		}
		
		void IDisposable.Dispose()
		{
			DisposeTimer();
		}
	}
}
