﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using JpLabs.DynamicCode.Emit;
using System.Diagnostics;

namespace EmitDemo
{
	class Program
	{
		class Tuple
		{
			public string A { get; set; }
			public int B;

			public Tuple()
			{}
			
			public Tuple(string a, int b)
			{
				A = a;
				B = b;
			}
			
			public object DoWork(int num)
			{
				return num ^ B;
			}

			public override string ToString()
			{
				return string.Format("'{0}'; {1}", A, B);
			}
		}
		
		public static void DoSomething()
		{
			Console.WriteLine("Done!");
		}
		
		static void Main(string[] args)
		{
			{
				MethodInfo method = typeof(String).GetMethod("IsNullOrEmpty", new[] { typeof(string) });
				var callback = LateBoundMethodFactory.CreateStaticMethod(method);
				
				string arg = null;
				var result = (bool)callback(arg);//new object[] {null});
				Console.WriteLine(result);
			}
			{
				MethodInfo method = typeof(String).GetMethod("Replace", new[] { typeof(string), typeof(string) });
				var callback = LateBoundMethodFactory.CreateMethod(method);
				
				string foo = "this is a test";
				var result = (string)callback(foo, "this", "that");
				Console.WriteLine(result);
			}

			{
				MethodInfo method = typeof(Program).GetMethod("DoSomething", Type.EmptyTypes);
				var callback = LateBoundMethodFactory.CreateStaticMethod(method);
				
				var result = callback();
				Console.WriteLine(result);
			}

			Tuple tuple;
			{
				ConstructorInfo tupleConstructorInfo = typeof(Tuple).GetConstructor(new[] { typeof(string), typeof(int) });
				var tupleCreator = LateBoundMethodFactory.CreateConstructor(tupleConstructorInfo);
				
				tuple = (Tuple)tupleCreator("duh", 100);
				Console.WriteLine(tuple.ToString());
			}			
			{
				var prop = typeof(Tuple).GetProperty("A");
				var aSetter = LateBoundMethodFactory.CreateSet(prop);
				aSetter(tuple, "DUH!");
				Console.WriteLine(tuple.ToString());
			}
			{
				var field = typeof(Tuple).GetField("B");
				var bSetter = LateBoundMethodFactory.CreateSet(field);
				bSetter(tuple, 999);
				Console.WriteLine(tuple.ToString());
			}
			
			//### Performance test:

			{
				Type[] constructorTypeArgs = Type.EmptyTypes;
				object[] constructorArgs = new object[0];
				
				PerformanceTestConstructor<Tuple>(constructorTypeArgs, constructorArgs);
			}

			{
				Type[] constructorTypeArgs = new[] { typeof(string), typeof(int) };
				object[] constructorArgs = new object[] { "-1", -1 };
				
				PerformanceTestConstructor<Tuple>(constructorTypeArgs, constructorArgs);
			}

			{
				string methodName = "DoWork";
				Type[] methodTypeArgs = new[] { typeof(int) };
				
				Tuple target = new Tuple();
				object[] methodArgs = new object[] { -1 };
				
				PerformanceTestMethod<Tuple>(methodName, methodTypeArgs, target, methodArgs);
			}
			
			if (Debugger.IsAttached) {
				Console.WriteLine();
				Console.WriteLine("This results were determined with a debugger attached. Try the compiled version.");
				
				Console.ReadKey(true);
			}
		}
		
		private static void PerformanceTestConstructor<T>(Type[] constructorTypeArgs, object[] constructorArgs)
		{
			const int TEST_COUNT = 200000;

			Console.WriteLine();
			Console.WriteLine("Testing constructor with {0} arguments", constructorTypeArgs.Length);
			
			DynamicStaticMethod constructorFunc;
			{
				ConstructorInfo constructorInfo = typeof(T).GetConstructor(constructorTypeArgs);
				Stopwatch watch = Stopwatch.StartNew();
				constructorFunc = LateBoundMethodFactory.CreateConstructor(constructorInfo);
				watch.Stop();
				
				Console.WriteLine("DynamicMethod compiled in \t\t{0} ms", watch.ElapsedTicks / (double)TimeSpan.TicksPerMillisecond);
			}
			
			TimeSpan shortSpan;
			{
				T instance;
				Stopwatch watch = Stopwatch.StartNew();
				Console.Write("Calling dynamic method...");
				for (int i = 0; i < TEST_COUNT; i++) instance = (T)constructorFunc(constructorArgs);
				watch.Stop();
				shortSpan = watch.Elapsed;
				Console.WriteLine("\t\tFinished after {0}", shortSpan);
			}
			
			TimeSpan longSpan;
			{
				T instance;
				if (constructorTypeArgs.Length == 0) {
					Stopwatch watch = Stopwatch.StartNew();
					Console.Write("Calling Activator.CreateInstance<T>...");
					for (int i = 0; i < TEST_COUNT; i++) instance = Activator.CreateInstance<T>();
					watch.Stop();
					longSpan = watch.Elapsed;
				} else {
					Type type = typeof(T);
					Stopwatch watch = Stopwatch.StartNew();
					Console.Write("Calling Activator.CreateInstance...");
					for (int i = 0; i < TEST_COUNT; i++) instance = (T)Activator.CreateInstance(type, constructorArgs);
					watch.Stop();
					longSpan = watch.Elapsed;
				}
				Console.WriteLine("\tFinished after {0}", longSpan);
			}
			
			Console.WriteLine(
				"Difference: DynamicMethod was \t\t{0} times faster",
				longSpan.TotalMilliseconds / shortSpan.TotalMilliseconds
			);
		}

		private static void PerformanceTestMethod<T>(string methodName, Type[] methodTypeArgs, T target, object[] args)
		{
			const int TEST_COUNT = 200000;

			Console.WriteLine();
			Console.WriteLine("Testing method call with {0} arguments", methodTypeArgs.Length);
			
			DynamicInstanceMethod compiledMethod;
			MethodInfo methodInfo = typeof(T).GetMethod(methodName, methodTypeArgs);
			{
				
				Stopwatch watch = Stopwatch.StartNew();
				compiledMethod = LateBoundMethodFactory.CreateMethod(methodInfo);
				watch.Stop();
				
				Console.WriteLine("DynamicMethod compiled in \t\t{0} ms", watch.ElapsedTicks / (double)TimeSpan.TicksPerMillisecond);
			}
			
			TimeSpan shortSpan;
			{
				Stopwatch watch = Stopwatch.StartNew();
				Console.Write("Calling dynamic method...");
				for (int i = 0; i < TEST_COUNT; i++) compiledMethod(target, args);
				watch.Stop();
				shortSpan = watch.Elapsed;
				Console.WriteLine("\t\tFinished after {0}", shortSpan);
			}
			
			TimeSpan longSpan;
			{
				Type type = typeof(T);
				Stopwatch watch = Stopwatch.StartNew();
				Console.Write("Calling methodInfo.Invoke...");
				for (int i = 0; i < TEST_COUNT; i++) methodInfo.Invoke(target, args);
				watch.Stop();
				longSpan = watch.Elapsed;
				Console.WriteLine("\t\tFinished after {0}", longSpan);
			}
			
			Console.WriteLine(
				"Difference: DynamicMethod was \t\t{0} times faster",
				longSpan.TotalMilliseconds / shortSpan.TotalMilliseconds
			);
		}
	}
}
