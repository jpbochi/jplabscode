#define INTERACTIVE

using System;
using System.Linq;
using System.Linq.Expressions;
using System.Collections.Generic;
using JpLabs.DynamicCode;
using System.Diagnostics;

//TODO: Make something that really simplify polynomial expressions

namespace JpLabs.Simplify
{
	class Program
	{
		#if !INTERACTIVE
		
		static void Main(string[] args)
		{
			//double y = 0;
			
			Expression<Func<double,double>> func = //(x => x);
				//(x => -x);
				(x => x*x);// - 2*x);
				//(x => Math.Pow(x, 3));
				//(x => Math.Log(x));
				//(x => Math.Pow(Math.Log(x), 2));
				//(x => Math.Pow(Math.E, x));
				//(x => -Math.Cos(x));
				//(x => -Math.Sin(x));
				//(x => Math.Pow(x, x));
				//(x => Math.Pow(x * x, 2));
				//(x => x * Math.Pow(x, 2));
				//(x => 3 * x * Math.Pow(x, 2));
				//(x => x * y);
				//LambdaParser.Parse<Func<double,double>>("(x => Math.Pow(x, 4))");
			
			Expression<Func<double,double>> fdx = func.Differentiate();
			
			Console.WriteLine("f : {0}", func);
			Console.WriteLine("f': {0}", fdx);
			Console.WriteLine();
			
			Console.WriteLine("Press any key to finish");
			Console.ReadKey(true);
		}
		
		#else
		
		static void Main(string[] args)
		{
			Console.WriteLine("Complete the lambda expressions (or empty to exit)");
			Console.WriteLine();
			
			//var cx = new ParserContext();
			using (var compiler = new Compiler() {
				References = new [] {"System.dll", "System.Core.dll"},
				Usings = new [] {"System", "System.Linq", "System.Linq.Expressions", "System.Collections.Generic"}
			}) {
				while (true) {
					Console.Write("f : x => ");
					string exp = Console.ReadLine();
					if (string.IsNullOrEmpty(exp)) return;
					
					exp = "(x => " + exp + ")";
					try {
						var watch = Stopwatch.StartNew();
						//var func = JpLabs.Differentiation.Simple.LambdaParser.Parse<Func<double,double>>(exp);
						var func = compiler.ParseLambdaExpr<Func<double,double>>(exp);
						watch.Stop();
						
						Console.WriteLine("t : {0}ms", watch.ElapsedMilliseconds);
						Console.WriteLine("f : {0}", func.ToString());
						Console.WriteLine("f': {0}", func.Simplify());
					} catch (CompileException e) {
						Console.WriteLine(e.Message);
					} catch (NotImplementedException e) {
						Console.WriteLine("The differentiation of [ {0} ] was not implemented yet", e.Message);
					}
					Console.WriteLine();
					
					//Recycle AppDomains once in a while (this step is not a requirement)
					if (compiler.DynamicAssembliesCount >= 7) {
						compiler.RecycleAppDomain();
						GC.Collect();
					}
				}
			}
		}
		
		#endif
		
	}
}
