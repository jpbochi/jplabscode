﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;

namespace JpLabs.Simplify
{
	public interface IAppendable<T>
	{
	    void Append(T value);
	}

	//public interface IMyAppendable : IAppendable<double>, IAppendable<Expression>
	//{}
	
	public class PowerExpression
	{
		public readonly Expression BaseExp;
		public readonly AdditiveExpression Power;

		public PowerExpression(Expression baseExp)
		{
			this.BaseExp = baseExp;
			Power = new AdditiveExpression();
		}
		
		public PowerExpression(Expression baseExp, double pow)
		{
			this.BaseExp = baseExp;
			Power = new AdditiveExpression(pow);
		}

		public PowerExpression(Expression baseExp, Expression pow)
		{
			this.BaseExp = baseExp;
			Power = new AdditiveExpression(pow);
		}
		
		public static bool TryParse(Expression e, out PowerExpression pow)
		{
			pow = null;
			if (e is ParameterExpression) {
				pow = new PowerExpression(e, 1);
			} else if (e.NodeType == ExpressionType.Power) {
				BinaryExpression bin = (BinaryExpression)e;
				if (bin.Left is ParameterExpression) {
					pow = new PowerExpression(bin.Left, bin.Right);
				}
			}
			return pow != null;
		}

		public static implicit operator Expression(PowerExpression e)
		{
			return e.ToExpression();
		}
		
		public Expression ToExpression()
		{
			Expression power = Power.ToExpression();
		    return (power.IsConstant(1)) ? BaseExp : Expression.Power(BaseExp, power);
		}
	}
	
	//public class AssociativeExpression
	public class MultiplicativeExpression //: IAppendable<double>, IAppendable<Expression>
	{
		double accumValue;
		ICollection<Expression> accumExp;
		ICollection<PowerExpression> accumPowers;
		
		public MultiplicativeExpression() : this(1)
		{}

		public MultiplicativeExpression(Expression exp) : this(1)
		{
			Append(exp);
		}
		
		public MultiplicativeExpression(double value) //: base(ExpressionType.Multiply, typeof(double))
		{
			accumValue = value;
			accumExp = new List<Expression>();
			accumPowers = new List<PowerExpression>();
		}
		
		//public static MultiplyExpression operator *(MultiplyExpression m, double value) 
		//{
		//    m.Append(value);
		//    return m;
		//}
		
		public void Append(double value)
		{
			accumValue *= value;
			return;
		}

		public void Append(Expression e)
		{
			PowerExpression pow;
			if (e is ConstantExpression) {
				Append(e.GetValue());
			} else if (PowerExpression.TryParse(e, out pow)) {
				PowerExpression other = accumPowers.Where(p => p.BaseExp == pow.BaseExp).FirstOrDefault();
				if (other == null) {
					accumPowers.Add(pow);
				} else {
					other.Power.Add(pow.Power.ToExpression());
				}
			} else {
				accumExp.Add(e);
			}
		}
		
		public static implicit operator Expression(MultiplicativeExpression m)
		{
			return m.ToExpression();
		}
		
		public Expression ToExpressionWithoutCoefficient()
		{
			Expression exp = null;

			foreach (var p in accumPowers) {
			    Expression e = (Expression)p;
			    exp = (exp == null) ? e : Expression.Multiply(exp, e);
			}

			foreach (var e in accumExp) {
			    exp = (exp == null) ? e : Expression.Multiply(exp, e);
			}
			
			return exp;
		}
		
		public Expression ToExpression()
		{
			Expression exp = ToExpressionWithoutCoefficient();
			
			if (exp == null) {
				exp = Expression.Constant(accumValue);
			} else {
				exp  = (accumValue == 1) ? exp : Expression.Multiply(Expression.Constant(accumValue), exp);
			}
			return exp;
		}

		public override string ToString()
		{
			return ToExpression().ToString();
		}
	}
}
