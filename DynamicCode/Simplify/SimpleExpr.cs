﻿using System;
using System.Linq.Expressions;
using System.Reflection;

namespace JpLabs.Simplify
{
	static public class ExpressionExt
	{
		static public bool IsConstant(this Expression expr)
		{
			return expr.NodeType == ExpressionType.Constant;
		}

		static public bool IsConstant(this Expression expr, double value)
		{
			return expr.IsConstant() && (double)((ConstantExpression)expr).Value == value;
		}

		static public bool IsConstantZero(this Expression expr)
		{
			return expr.IsConstant(0d);
		}

		static public bool IsConstantOne(this Expression expr)
		{
			return expr.IsConstant(1d);
		}

		public static double GetValue(this Expression expr)
		{
			return (double)((ConstantExpression)expr).Value;
		}
	}

	static public class Funcs
	{
		public static MethodInfo Pow = typeof(Math).GetMethod("Pow", new Type[]{typeof(double), typeof(double)});
		
		public static MethodInfo Log = typeof(Math).GetMethod("Log", new Type[]{typeof(double)});
		public static MethodInfo LogBase = typeof(Math).GetMethod("Log", new Type[]{typeof(double), typeof(double)});
		public static MethodInfo Log10 = typeof(Math).GetMethod("Log10", new Type[]{typeof(double)});
		
		public static MethodInfo Sin = typeof(Math).GetMethod("Sin", new Type[]{typeof(double)});
		public static MethodInfo Cos = typeof(Math).GetMethod("Cos", new Type[]{typeof(double)});
	}
	
	static public class SimpleExpr
	{
		public static Expression Zero()
		{
			return Expression.Constant(0d);
		}
		
		public static Expression One()
		{
			return Expression.Constant(1d);
		}

		public static Expression MinusOne()
		{
			return Expression.Constant(-1d);
		}

		public static Expression Constant(double value)
		{
			return Expression.Constant(value);
		}

		public static Expression Negate(Expression exp)
		{
			if (exp.NodeType == ExpressionType.Negate) {
				return ((UnaryExpression)exp).Operand;
			} else if (exp.IsConstant()) {
				return Expression.Constant(-exp.GetValue());
			} else {
				return Expression.Negate(exp);
			}
		}

		public static Expression Add(Expression left, Expression right)
		{
			if (left.IsConstantZero())		return right;
			else if (right.IsConstantZero())return left;
			//else if (left == right)			return Expression.Multiply(Expression.Constant(2d), right);
			else if (left.IsConstant() && right.IsConstant()) {
				double value = left.GetValue() + right.GetValue();
				return Expression.Constant(value);
			} else {
				return Expression.Add(left, right);
			}
		}

		public static Expression Subtract(Expression left, Expression right)
		{
			if (left.IsConstantZero())			return Negate(right);
			else if (right.IsConstantZero())	return left;
			else if (left.IsConstant() && right.IsConstant()) {
				double value = left.GetValue() - right.GetValue();
				return Expression.Constant(value);
			} else {
				return Expression.Subtract(left, right);
			}
		}
		
		public static Expression Multiply(Expression left, Expression right)
		{
			bool leftIsConst = left.IsConstant();
			bool rightIsConst = right.IsConstant();
			if (leftIsConst) {
				if (left.IsConstantOne())		return right;
				else if (left.IsConstantZero())	return Zero();
				else if (left.IsConstant(-1d))	return Negate(right);
			}
			if (rightIsConst) {
				if (right.IsConstantOne())		return left;
				else if (right.IsConstantZero())return Zero();
				else if (right.IsConstant(-1d))	return Negate(left);
			}
			if (leftIsConst && rightIsConst) {
				return Expression.Constant(left.GetValue() * right.GetValue());
			}
			return Expression.Multiply(left, right);
		}
		
		public static Expression Divide(Expression left, Expression right)
		{
			if (right.IsConstantOne())		return left;
			else if (left.IsConstantZero())	return Zero();
			else if (left == right)			return One();
			else return Expression.Divide(left, right);
		}

		public static Expression Pow(Expression baseExp, Expression powerExp)
		{
			if (powerExp.IsConstantOne()) {
				return baseExp;
			} else if (powerExp.IsConstantZero()) {
				return baseExp;
			} else {
				//return Expression.Call(Funcs.Pow, baseExp, powerExp);
				return Expression.Power(baseExp, powerExp);
			}
		}

		public static Expression Log(Expression exp)
		{
			if (exp.IsConstant(Math.E)) {
				return One();
			} else {
				return Expression.Call(Funcs.Log, exp);
			}
		}

		public static Expression Sin(Expression exp)
		{
			return Expression.Call(Funcs.Sin, exp);
		}

		public static Expression Cos(Expression exp)
		{
			return Expression.Call(Funcs.Cos, exp);
		}
	}
}
