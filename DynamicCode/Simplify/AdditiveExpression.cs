﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;

namespace JpLabs.Simplify
{
	public class AdditiveExpression
	{
		double accumValue;
		List<Expression> accumAddExp;
		List<Expression> accumSubExp;

		public AdditiveExpression() : this(0)
		{}

		public AdditiveExpression(Expression exp) : this(0)
		{
			Add(exp);
		}
		
		public AdditiveExpression(double value) //: base(ExpressionType.Add, typeof(double))
		{
			accumValue = value;
			accumAddExp = new List<Expression>();
			accumSubExp = new List<Expression>();
		}
		
		public void Add(double value)
		{
			accumValue += value;
		}

		public void Subtract(double value)
		{
			accumValue -= value;
		}

		public void Add(Expression e)
		{
			if (e is ConstantExpression) {
				Add(e.GetValue());
			} else if (e.NodeType == ExpressionType.Negate) {
				UnaryExpression u = (UnaryExpression)e;
				accumSubExp.Add(u.Operand);
			} else {
				accumAddExp.Add(e);
			}
		}
		
		public void Subtract(Expression e)
		{
			if (e is ConstantExpression) {
				Subtract(e.GetValue());
			} else if (e.NodeType == ExpressionType.Negate) {
				UnaryExpression u = (UnaryExpression)e;
				accumAddExp.Add(u.Operand);
			} else {
				accumSubExp.Add(e);
			}
		}

		public static implicit operator Expression(AdditiveExpression e)
		{
			return e.ToExpression();
		}
		
		public Expression ToExpression()
		{
			Expression exp = (accumValue == 0) ? null : Expression.Constant(accumValue);
			
			accumAddExp.Sort();
			foreach (var e in accumAddExp) exp = (exp == null) ? e : SimpleExpr.Add(exp, e);

			accumSubExp.Sort();
			foreach (var e in accumSubExp) exp = (exp == null) ? e : SimpleExpr.Subtract(exp, e);
			
			return (exp == null) ? Expression.Constant(accumValue) : exp;
		}

		public override string ToString()
		{
			return ToExpression().ToString();
		}
	}
}
