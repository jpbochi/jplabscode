﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;

namespace JpLabs.Simplify
{
	static public class SimplifyExt
	{
		static public Expression<Func<double,double>> Simplify(this Expression<Func<double,double>> f)
		{
			Expression body = Simplify(f.Body);
			
			return Expression.Lambda<Func<double,double>>(body, f.Parameters);
		}

		static Expression Simplify(Expression exp)
		{
			return Simplify(exp, ExpressionType.Lambda);
		}
		
		static Expression Simplify(Expression exp, params ExpressionType[] ignoreTypes)
		{
			if (ignoreTypes.Contains(exp.NodeType)) return exp;
			switch (exp.NodeType) {
				//case ExpressionType.Parameter: {
				//    return (exp == dx) ? SimpleExpr.One() : exp;
				//}
				//case ExpressionType.Constant: {
				//    return SimpleExpr.Zero();
				//}
				//case ExpressionType.Negate: {
				//    UnaryExpression f = (UnaryExpression)exp;
				//    return SimpleExpr.Negate(Diff(f.Operand, dx));
				//}
				case ExpressionType.Subtract:
				case ExpressionType.Add: {
				    //BinaryExpression f = (BinaryExpression)exp;
				    //return SimpleExpr.Add(Simplify(f.Left), Simplify(f.Right));
				    return SimplifyAdd((BinaryExpression)exp);
				}
				//case ExpressionType.Subtract: {
				//    // [f(x) - g(x)]' == f'(x) - g'(x).
				//    BinaryExpression f = (BinaryExpression)exp;
				//    return SimpleExpr.Subtract(Diff(f.Left, dx), Diff(f.Right, dx));
				//}
				case ExpressionType.Multiply: {
					return SimplifyMultiply((BinaryExpression)exp);
				}
				//case ExpressionType.Divide: {
				//    return DiffDivide((BinaryExpression)exp, dx);
				//}
				case ExpressionType.Call: {
					MethodCallExpression f = (MethodCallExpression)exp;
					if (f.Method == Funcs.Pow) {
						return Expression.Power(f.Arguments[0], f.Arguments[1]);
					} else {
						return exp;
					}
				}
				default: return exp;
			}
		}

		static Expression SimplifyMultiply(BinaryExpression exp)
		{
			//var operands = FoldAssociative(exp, ExpressionType.Multiply);
			//MultiplyExpression multiply = new MultiplyExpression();
			//foreach (var e in operands) multiply.Insert(e);

			MultiplicativeExpression multiply = new MultiplicativeExpression();
			FoldAssociative(exp, e => multiply.Append(e), ExpressionType.Multiply);
			return multiply.ToExpression();
		}

		static Expression SimplifyAdd(BinaryExpression exp)
		{
			//var operands = UnfoldAdd(exp);
			
			//double value = 0;
			//foreach (var e in operands.Where(e => e.IsConstant())) {
			//    value += e.GetValue();
			//}
			//Expression simple = (value == 1) ? null : Expression.Constant(value);
			
			//var variables =
			//    from e in operands
			//    where !e.IsConstant()
			//    //orderby e
			//    select e;
				
			//foreach (var e in variables) {
			//    simple = (simple == null) ? e : Expression.Add(simple, e);
			//}
			
			//return simple;
			
			AdditiveExpression add = new AdditiveExpression();
			//ICollection<ThreadStaticAttribute>
			//var operands = FoldAssociative(exp, ExpressionType.Add);
			//foreach (var e in operands) add.Insert(e);
			FoldAssociative(exp, (e => add.Add(e)), ExpressionType.Add);//, ExpressionType.Subtract);
			return add.ToExpression();
		}

		//static IEnumerable<Expression> UnfoldMultiply(Expression exp)
		//{
		//    return UnfoldMultiply(exp, new List<Expression>());
		//}

		//static IEnumerable<Expression> UnfoldMultiply(Expression exp, ICollection<Expression> operands)
		//{
		//    if (exp.NodeType == ExpressionType.Multiply) {
		//        BinaryExpression bexp = (BinaryExpression)exp;
		//        UnfoldMultiply(bexp.Left, operands);
		//        UnfoldMultiply(bexp.Right, operands);
		//    } else {
		//        operands.Add(Simplify(exp, ExpressionType.Multiply));
		//    }
		//    return operands;
		//}

		//static IEnumerable<Expression> UnfoldAdd(Expression exp)
		//{
		//    return UnfoldAdd(exp, new List<Expression>());
		//}

		//static IEnumerable<Expression> UnfoldAdd(Expression exp, ICollection<Expression> operands)
		//{
		//    if (exp.NodeType == ExpressionType.Add) {
		//        BinaryExpression bexp = (BinaryExpression)exp;
		//        UnfoldAdd(bexp.Left, operands);
		//        UnfoldAdd(bexp.Right, operands);
		//    } else {
		//        operands.Add(Simplify(exp, ExpressionType.Add));
		//    }
		//    return operands;
		//}

		//static IEnumerable<Expression> UnfoldAssociative(Expression exp, ExpressionType type)
		//{
		//    if (exp.NodeType == type) {
		//        BinaryExpression bexp = (BinaryExpression)exp;
		//        return Enumerable.Concat(
		//            UnfoldAssociative(bexp.Left, type),
		//            UnfoldAssociative(bexp.Right, type)
		//        );
		//    } else {
		//        return Enumerable.Repeat(Simplify(exp, ExpressionType.Add), 1);
		//    }
		//}
		
		static IEnumerable<Expression> YieldAssociative(Expression exp, ExpressionType type)
		{
			if (exp.NodeType == type) {
				BinaryExpression bexp = (BinaryExpression)exp;
				foreach (var e in YieldAssociative(bexp.Left, type)) yield return e;
				foreach (var e in YieldAssociative(bexp.Right, type)) yield return e;
			} else {
				yield return Simplify(exp, type);
			}
		}

		static void FoldAssociative(Expression exp, Action<Expression> action, ExpressionType type)
		{
			if (type != exp.NodeType) exp = Simplify(exp, type);
			if (type == exp.NodeType) {
				BinaryExpression bexp = (BinaryExpression)exp;
				FoldAssociative(bexp.Left, action, type);
				FoldAssociative(bexp.Right, action, type);
			} else {
				action(exp);
			}
		}

		static void FoldAssociative(Expression exp, Action<Expression> action, params ExpressionType[] types)
		{
			if (!types.Contains(exp.NodeType)) exp = Simplify(exp, types);
			if (types.Contains(exp.NodeType)) {
				BinaryExpression bexp = (BinaryExpression)exp;
				FoldAssociative(bexp.Left, action, types);
				FoldAssociative(bexp.Right, action, types);
			} else {
				action(exp);
			}
		}
	}
}
