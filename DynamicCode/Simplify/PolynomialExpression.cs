﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;

namespace JpLabs.Simplify
{
	public class PolynomialTermExpression
	{
		string paramName;
		double exponent; //always integer and positive in canonical polynomials
		double coefficient;
		
		public PolynomialTermExpression(string paramName, double exponent, double coefficient)
		{
			this.paramName = paramName;
			this.exponent = exponent;
			this.coefficient = coefficient;
		}
	}
	
	public class PolynomialExpression
	{
		public readonly ICollection<PolynomialTermExpression> Terms;
		
		public PolynomialExpression()
		{
			Terms = new List<PolynomialTermExpression>();
		}
		
		//public static bool TryParse(Expression e, out PolynomialExpression p)
		//{
		//    switch (exp.NodeType) {
		//        case ExpressionType.Add:
		//        case ExpressionType.Subtract: {
					
		//        }
				
		//        case ExpressionType.Parameter: {
		//        //    return (exp == dx) ? SimpleExpr.One() : exp;
		//        //}
		//        //case ExpressionType.Constant: {
		//        //    return SimpleExpr.Zero();
		//        //}
		//        //case ExpressionType.Negate: {
		//        //    UnaryExpression f = (UnaryExpression)exp;
		//        //    return SimpleExpr.Negate(Diff(f.Operand, dx));
		//        //}
		//        : {
		//            //BinaryExpression f = (BinaryExpression)exp;
		//            //return SimpleExpr.Add(Simplify(f.Left), Simplify(f.Right));
		//            return SimplifyAdd((BinaryExpression)exp);
		//        }
		//        //case ExpressionType.Subtract: {
		//        //    // [f(x) - g(x)]' == f'(x) - g'(x).
		//        //    BinaryExpression f = (BinaryExpression)exp;
		//        //    return SimpleExpr.Subtract(Diff(f.Left, dx), Diff(f.Right, dx));
		//        //}
		//        case ExpressionType.Multiply: {
		//            return SimplifyMultiply((BinaryExpression)exp);
		//        }
		//        //case ExpressionType.Divide: {
		//        //    return DiffDivide((BinaryExpression)exp, dx);
		//        //}
		//        case ExpressionType.Call: {
		//            MethodCallExpression f = (MethodCallExpression)exp;
		//            if (f.Method == Funcs.Pow) {
		//                return Expression.Power(f.Arguments[0], f.Arguments[1]);
		//            } else {
		//                return exp;
		//            }
		//        }
		//        default: return exp;
		//    }
		//    return false;
		//}
		
		static void FoldAssociative(Expression exp, ExpressionType type, Action<Expression> action)
		{
			if (exp.NodeType == type) {
				BinaryExpression bexp = (BinaryExpression)exp;
				FoldAssociative(bexp.Left, type, action);
				FoldAssociative(bexp.Right, type, action);
			} else {
				action(exp);
			}
		}
	}
}
