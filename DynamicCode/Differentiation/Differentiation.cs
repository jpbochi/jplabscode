﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Collections.Generic;

namespace JpLabs.Differentiation
{
	/// <summary>
	/// http://mathworld.wolfram.com/Derivative.html
	/// </summary>
	static public class Differentiation
	{
		static public Expression<Func<double,double>> Differentiate(this Expression<Func<double,double>> f)
		{
			//I will find the derivative of f with respect to the variable x (it's the 1st and sole parameter)
			ParameterExpression x = f.Parameters.Single();
			
			//Get the body of the function
			Expression funcBody = f.Body;
			
			//Diff() is the function that actually does the differentiation
			Expression derivative = Diff(funcBody, x);
			
			//Build a new lambda expression with the derivative expression body
			return Expression.Lambda<Func<double,double>>(derivative, f.Parameters);
		}

		static Expression Diff(Expression exp, ParameterExpression px)
		{
			switch (exp.NodeType) {
				case ExpressionType.Parameter: {
					//[f(x) = x]' == 1
					//[f(x,y) = y]' == 0 (when deriving on x)
					return (exp == px) ? SimpleExpr.One() : exp;
				}
				case ExpressionType.Constant: {
					//[f(x) = c]' == 0
					return SimpleExpr.Zero();
				}
				case ExpressionType.MemberAccess: {
					//[f(x) = outerVariable]' == 0
					return SimpleExpr.Zero();
				}
				case ExpressionType.Negate: {
					//[-f(x)]' == -[f'(x)]
					UnaryExpression f = (UnaryExpression)exp;
					return SimpleExpr.Negate(Diff(f.Operand, px));
				}
				case ExpressionType.Add: {
					//Sum rule: [f(x) + h(x)]' == f'(x) + h'(x).
					BinaryExpression f = (BinaryExpression)exp;
					return SimpleExpr.Add(Diff(f.Left, px), Diff(f.Right, px));
				}
				case ExpressionType.Subtract: {
					//Subtract rule: [f(x) - g(x)]' == f'(x) - g'(x).
					BinaryExpression f = (BinaryExpression)exp;
					return SimpleExpr.Subtract(Diff(f.Left, px), Diff(f.Right, px));
				}
				case ExpressionType.Multiply: {
					return DiffMultiply((BinaryExpression)exp, px);
				}
				case ExpressionType.Divide: {
					return DiffDivide((BinaryExpression)exp, px);
				}
				case ExpressionType.Power: {
					return DiffPow((BinaryExpression)exp, px);
				}
				case ExpressionType.Call: {
					MethodCallExpression f = (MethodCallExpression)exp;
					if (f.Method == Funcs.Pow) {
						return DiffPow(f, px);
					} else if (f.Method == Funcs.Log) {
						Expression g = f.Arguments[0];
						return Chain(g, px, SimpleExpr.Divide(SimpleExpr.One(), g));
					} else if (f.Method == Funcs.Sin) {
						Expression g = f.Arguments[0];
						return Chain(g, px, SimpleExpr.Cos(g));
					} else if (f.Method == Funcs.Cos) {
						Expression g = f.Arguments[0];
						return Chain(g, px, SimpleExpr.Negate(SimpleExpr.Sin(g)));
					} else {
						throw new NotImplementedException(exp.ToString());
					}
				}
				default: throw new NotImplementedException(exp.ToString());
			}
		}

		static Expression DiffMultiply(BinaryExpression exp, ParameterExpression px)
		{
			//Product rule: [f(x)g(x)]' == f(x)g'(x) + f'(x)g(x)
			Expression f = exp.Left;
			Expression g = exp.Right;
			Expression fdx = Diff(f, px);
			Expression gdx = Diff(g, px);
			
			return SimpleExpr.Add(
				SimpleExpr.Multiply(f, gdx),
				SimpleExpr.Multiply(fdx, g)
			);
		}
		
		static Expression DiffDivide(BinaryExpression exp, ParameterExpression px)
		{
			//  d   f(x)      g(x)f'(x) - f(x)g'(x)
			// --- ------ == -----------------------
			//  dx  g(x)             g(x)^2
			Expression f = exp.Left;
			Expression g = exp.Right;
			Expression fdx = Diff(f, px);
			Expression gdx = Diff(g, px);
			
			return SimpleExpr.Divide(
				SimpleExpr.Subtract(
					SimpleExpr.Multiply(g, fdx),
					SimpleExpr.Multiply(f, gdx)
				),
				SimpleExpr.Pow(g, SimpleExpr.Constant(2))
			);
		}

		static Expression DiffPow(BinaryExpression exp, ParameterExpression px)
		{
			return DiffPow(exp.Left, exp.Right, px);
		}
		
		static Expression DiffPow(MethodCallExpression exp, ParameterExpression px)
		{
			Expression b = exp.Arguments[0];
			Expression p = exp.Arguments[1];
			return DiffPow(b, p, px);
		}
		
		static Expression DiffPow(Expression b, Expression p, ParameterExpression px)
		{
			if (p.IsConstant()) {
				//Power rule: d/(dx)[x^n] == n * x^(n-1).
				double n = (double)((ConstantExpression)p).Value;
				
				return Chain(b, px,
					SimpleExpr.Multiply(
						SimpleExpr.Constant(n),
						SimpleExpr.Pow(b, SimpleExpr.Constant(n - 1))
					)
				);
			} else if (b.IsConstant()) {
				// [a ^ x]' == ln(a) * a ^ x
				return Chain(p, px,
					SimpleExpr.Multiply(
						SimpleExpr.Log(b),
						SimpleExpr.Pow(b, p)
					)
				);
			} else {
				// if u e v are function of x so u ^ v = e ^ (v * ln(u))
				//  d               /  v   du            dv  \
				// --- u^v == u^v * | --- ---- +  ln(u) ---- |
				//  dx              \  u   dx            dx  /
				
				Expression bdx = Diff(b, px);
				Expression pdx = Diff(p, px);
								
				return SimpleExpr.Multiply(
					SimpleExpr.Pow(b, p),
					SimpleExpr.Add(
						SimpleExpr.Multiply(SimpleExpr.Divide(p, b), bdx),
						SimpleExpr.Multiply(SimpleExpr.Log(b), pdx)
					)
				);
			}
		}
		
		static Expression Chain(Expression g, ParameterExpression px, Expression fdx)
		{
			//http://mathworld.wolfram.com/ChainRule.html
			if (g == px) {
				return fdx;
			} else {
				return SimpleExpr.Multiply(Diff(g, px), fdx);
			}
		}
	}
}
