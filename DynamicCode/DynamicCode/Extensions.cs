﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JpLabs.DynamicCode
{
	internal static class Extensions
	{
		static public T Join<T>(this IEnumerable<T> source, T separator, Func<T,T,T> join)
		{
			return
				source
				.Select((s,i) => (i==0) ? s : join(separator, s))
				.Aggregate(default(T), (s1,s2) => join(s1,s2));
		}
		
		static public string Join(this IEnumerable<string> source, string separator)
		{
			return source.Join(separator, string.Concat);
		}

		static public IEnumerable<T> Concat<T>(this IEnumerable<T> source, params T[] other)
		{
			return source.Concat((IEnumerable<T>)other);
		}

		static public string ToCSharpCode(this Type type)
		{
			if (type.IsGenericType) {
				var strb = new StringBuilder();
				
				var p = type.Name.IndexOf('`');
				strb.Append(type.Name.Substring(0, p));
				
				strb.Append("<");
				bool first = true;
				foreach (var gType in type.GetGenericArguments()) {
					if (first) first = false; else strb.Append(",");
					strb.Append(gType.ToCSharpCode());
				}
				strb.Append(">");
				return strb.ToString();
			} else {
				return type.Name;
			}
		}
	}
}
