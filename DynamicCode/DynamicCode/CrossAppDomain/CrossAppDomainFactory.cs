﻿using System;
using System.Linq;

namespace JpLabs.DynamicCode
{
	internal class CrossAppDomainFactory : IDisposable
	{
		[Serializable]
		private class CrossAppDomainAssemblies : MarshalByRefObject
		{
			public int TotalAssemblyCount { get; private set; }
			public int DynamicAssembliesCount { get; private set; }
			
			public CrossAppDomainAssemblies()
			{
				Update();
			}
			
			public void Update()
			{
				var asms = AppDomain.CurrentDomain.GetAssemblies();
				TotalAssemblyCount = asms.Count();
				DynamicAssembliesCount = asms.Where(a => string.IsNullOrEmpty(a.Location)).Count();
			}
		}

		private AppDomain domain;
		
		private CrossAppDomainFactory(AppDomain domain)
		{
			this.domain = domain;
		}
		
		static public CrossAppDomainFactory Create(string domainName)
		{
            //create an AppDomain
            AppDomainSetup setup = new AppDomainSetup(){
				ApplicationBase = AppDomain.CurrentDomain.BaseDirectory
			};
            AppDomain domain = AppDomain.CreateDomain(domainName, null, setup);
            return new CrossAppDomainFactory(domain);
		}
		
		/// <summary>Use this if you do not want the object type to be loaded in your current domain</summary>
		public object CreateObj(string assemblyName, string typeName)
		{
			return domain.CreateInstanceFromAndUnwrap(assemblyName, typeName);
		}
		
		/// <summary>Use this if your current domain already references the required type</summary>
		public T CreateObj<T>() where T : MarshalByRefObject
		{
			string assemblyName = typeof(T).Assembly.Location;
			string typeName = typeof(T).FullName;
			return (T)domain.CreateInstanceFromAndUnwrap(assemblyName, typeName);
		}
		
		public int DynamicAssembliesCount()
		{
			return CreateObj<CrossAppDomainAssemblies>().DynamicAssembliesCount;
		}
		
		public void Dispose()
		{
			AppDomain.Unload(domain);
		}
	}
}
