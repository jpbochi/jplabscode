﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using System.Collections;
using System.Reflection;

namespace JpLabs.DynamicCode.CrossAppDomain.Queryable
{
	internal abstract class EnumerableQuery
	{
		// Methods
		protected EnumerableQuery()
		{
		}

		internal static IQueryable Create(Type elementType, IEnumerable sequence)
		{
			return (IQueryable) Activator.CreateInstance(typeof(EnumerableQuery<>).MakeGenericType(new Type[] { elementType }), BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance, null, new object[] { sequence }, null);
		}

		internal static IQueryable Create(Type elementType, Expression expression)
		{
			return (IQueryable) Activator.CreateInstance(typeof(EnumerableQuery<>).MakeGenericType(new Type[] { elementType }), BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance, null, new object[] { expression }, null);
		}

		// Properties
		internal abstract IEnumerable Enumerable { get; }

		internal abstract Expression Expression { get; }
	}

	internal class EnumerableQuery<T> : EnumerableQuery, IOrderedQueryable<T>, IQueryable<T>, IOrderedQueryable, IQueryable, IQueryProvider, IEnumerable<T>, IEnumerable
	{
		// Fields
		private IEnumerable<T> enumerable;
		private Expression expression;

		// Methods
		internal EnumerableQuery(IEnumerable<T> enumerable)
		{
			this.enumerable = enumerable;
			this.expression = Expression.Constant(this);
		}

		internal EnumerableQuery(Expression expression)
		{
			this.expression = expression;
		}

		private IEnumerator<T> GetEnumerator()
		{
			if (this.enumerable == null)
			{
				Expression body = new EnumerableRewriter().Visit(this.expression);
				ExpressionCompiler compiler = new ExpressionCompiler();
				Expression<Func<IEnumerable<T>>> lambda = Expression.Lambda<Func<IEnumerable<T>>>(body, (IEnumerable<ParameterExpression>) null);
				this.enumerable = compiler.Compile<Func<IEnumerable<T>>>(lambda)();
			}
			return this.enumerable.GetEnumerator();
		}

		IEnumerator<T> IEnumerable<T>.GetEnumerator()
		{
			return this.GetEnumerator();
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return this.GetEnumerator();
		}

		IQueryable IQueryProvider.CreateQuery(Expression expression)
		{
			if (expression == null)
			{
				throw Error.ArgumentNull("expression");
			}
			Type type = TypeHelper.FindGenericType(typeof(IQueryable<>), expression.Type);
			if (type == null) throw Error.ArgumentNotValid("expression");
			return EnumerableQuery.Create(type.GetGenericArguments()[0], expression);
		}

		IQueryable<S> IQueryProvider.CreateQuery<S>(Expression expression)
		{
			if (expression == null)
			{
				throw Error.ArgumentNull("expression");
			}
			if (!typeof(IQueryable<S>).IsAssignableFrom(expression.Type))
			{
				throw Error.ArgumentNotValid("expression");
			}
			return new EnumerableQuery<S>(expression);
		}

		S IQueryProvider.Execute<S>(Expression expression)
		{
			if (expression == null)
			{
				throw Error.ArgumentNull("expression");
			}
			if (!typeof(S).IsAssignableFrom(expression.Type))
			{
				throw Error.ArgumentNotValid("expression");
			}
			return new EnumerableExecutor<S>(expression).Execute();
		}

		object IQueryProvider.Execute(Expression expression)
		{
			if (expression == null)
			{
				throw Error.ArgumentNull("expression");
			}
			typeof(EnumerableExecutor<>).MakeGenericType(new Type[] { expression.Type });
			return EnumerableExecutor.Create(expression).ExecuteBoxed();
		}

		public override string ToString()
		{
			ConstantExpression expression = this.expression as ConstantExpression;
			if ((expression == null) || (expression.Value != this))
			{
				return this.expression.ToString();
			}
			if (this.enumerable != null)
			{
				return this.enumerable.ToString();
			}
			return "null";
		}

		// Properties
		internal override IEnumerable Enumerable
		{
			get
			{
				return this.enumerable;
			}
		}

		internal override Expression Expression
		{
			get
			{
				return this.expression;
			}
		}

		Type IQueryable.ElementType
		{
			get
			{
				return typeof(T);
			}
		}

		Expression IQueryable.Expression
		{
			get
			{
				return this.expression;
			}
		}

		IQueryProvider IQueryable.Provider
		{
			get
			{
				return this;
			}
		}
	}
}
