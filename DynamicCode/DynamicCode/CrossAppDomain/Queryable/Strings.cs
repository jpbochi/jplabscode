﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JpLabs.DynamicCode.CrossAppDomain.Queryable
{
	internal static class Strings
	{
		// Methods
		internal static string ArgumentMemberNotDeclOnType(object p0, object p1)
		{
			return SR.GetString("ArgumentMemberNotDeclOnType", new object[] { p0, p1 });
		}

		internal static string ArgumentTypeDoesNotMatchMember(object p0, object p1)
		{
			return SR.GetString("ArgumentTypeDoesNotMatchMember", new object[] { p0, p1 });
		}

		internal static string BinaryOperatorNotDefined(object p0, object p1, object p2)
		{
			return SR.GetString("BinaryOperatorNotDefined", new object[] { p0, p1, p2 });
		}

		internal static string CannotAutoInitializeValueTypeElementThroughProperty(object p0)
		{
			return SR.GetString("CannotAutoInitializeValueTypeElementThroughProperty", new object[] { p0 });
		}

		internal static string CannotAutoInitializeValueTypeMemberThroughProperty(object p0)
		{
			return SR.GetString("CannotAutoInitializeValueTypeMemberThroughProperty", new object[] { p0 });
		}

		internal static string CannotCastTypeToType(object p0, object p1)
		{
			return SR.GetString("CannotCastTypeToType", new object[] { p0, p1 });
		}

		internal static string CoercionOperatorNotDefined(object p0, object p1)
		{
			return SR.GetString("CoercionOperatorNotDefined", new object[] { p0, p1 });
		}

		internal static string ElementInitializerMethodNoRefOutParam(object p0, object p1)
		{
			return SR.GetString("ElementInitializerMethodNoRefOutParam", new object[] { p0, p1 });
		}

		internal static string ExpressionTypeCannotInitializeArrayType(object p0, object p1)
		{
			return SR.GetString("ExpressionTypeCannotInitializeArrayType", new object[] { p0, p1 });
		}

		internal static string ExpressionTypeCannotInitializeCollectionType(object p0, object p1)
		{
			return SR.GetString("ExpressionTypeCannotInitializeCollectionType", new object[] { p0, p1 });
		}

		internal static string ExpressionTypeDoesNotMatchArrayType(object p0, object p1)
		{
			return SR.GetString("ExpressionTypeDoesNotMatchArrayType", new object[] { p0, p1 });
		}

		internal static string ExpressionTypeDoesNotMatchConstructorParameter(object p0, object p1)
		{
			return SR.GetString("ExpressionTypeDoesNotMatchConstructorParameter", new object[] { p0, p1 });
		}

		internal static string ExpressionTypeDoesNotMatchMethodParameter(object p0, object p1, object p2)
		{
			return SR.GetString("ExpressionTypeDoesNotMatchMethodParameter", new object[] { p0, p1, p2 });
		}

		internal static string ExpressionTypeDoesNotMatchParameter(object p0, object p1)
		{
			return SR.GetString("ExpressionTypeDoesNotMatchParameter", new object[] { p0, p1 });
		}

		internal static string ExpressionTypeDoesNotMatchReturn(object p0, object p1)
		{
			return SR.GetString("ExpressionTypeDoesNotMatchReturn", new object[] { p0, p1 });
		}

		internal static string ExpressionTypeNotInvocable(object p0)
		{
			return SR.GetString("ExpressionTypeNotInvocable", new object[] { p0 });
		}

		internal static string FieldNotDefinedForType(object p0, object p1)
		{
			return SR.GetString("FieldNotDefinedForType", new object[] { p0, p1 });
		}

		internal static string IncorrectNumberOfMethodCallArguments(object p0)
		{
			return SR.GetString("IncorrectNumberOfMethodCallArguments", new object[] { p0 });
		}

		internal static string IncorrectTypeForTypeAs(object p0)
		{
			return SR.GetString("IncorrectTypeForTypeAs", new object[] { p0 });
		}

		internal static string InvalidCast(object p0, object p1)
		{
			return SR.GetString("InvalidCast", new object[] { p0, p1 });
		}

		internal static string LogicalOperatorMustHaveBooleanOperators(object p0, object p1)
		{
			return SR.GetString("LogicalOperatorMustHaveBooleanOperators", new object[] { p0, p1 });
		}

		internal static string LogicalOperatorMustHaveConsistentTypes(object p0, object p1)
		{
			return SR.GetString("LogicalOperatorMustHaveConsistentTypes", new object[] { p0, p1 });
		}

		internal static string MemberNotFieldOrProperty(object p0)
		{
			return SR.GetString("MemberNotFieldOrProperty", new object[] { p0 });
		}

		internal static string MethodContainsGenericParameters(object p0)
		{
			return SR.GetString("MethodContainsGenericParameters", new object[] { p0 });
		}

		internal static string MethodDoesNotExistOnType(object p0, object p1)
		{
			return SR.GetString("MethodDoesNotExistOnType", new object[] { p0, p1 });
		}

		internal static string MethodIsGeneric(object p0)
		{
			return SR.GetString("MethodIsGeneric", new object[] { p0 });
		}

		internal static string MethodNotDefinedForType(object p0, object p1)
		{
			return SR.GetString("MethodNotDefinedForType", new object[] { p0, p1 });
		}

		internal static string MethodNotPropertyAccessor(object p0, object p1)
		{
			return SR.GetString("MethodNotPropertyAccessor", new object[] { p0, p1 });
		}

		internal static string MethodWithArgsDoesNotExistOnType(object p0, object p1)
		{
			return SR.GetString("MethodWithArgsDoesNotExistOnType", new object[] { p0, p1 });
		}

		internal static string MethodWithMoreThanOneMatch(object p0, object p1)
		{
			return SR.GetString("MethodWithMoreThanOneMatch", new object[] { p0, p1 });
		}

		internal static string NotAMemberOfType(object p0, object p1)
		{
			return SR.GetString("NotAMemberOfType", new object[] { p0, p1 });
		}

		internal static string OperandTypesDoNotMatchParameters(object p0, object p1)
		{
			return SR.GetString("OperandTypesDoNotMatchParameters", new object[] { p0, p1 });
		}

		internal static string OperatorNotImplementedForType(object p0, object p1)
		{
			return SR.GetString("OperatorNotImplementedForType", new object[] { p0, p1 });
		}

		internal static string ParameterExpressionNotValidAsDelegate(object p0, object p1)
		{
			return SR.GetString("ParameterExpressionNotValidAsDelegate", new object[] { p0, p1 });
		}

		internal static string PropertyDoesNotHaveGetter(object p0)
		{
			return SR.GetString("PropertyDoesNotHaveGetter", new object[] { p0 });
		}

		internal static string PropertyDoesNotHaveSetter(object p0)
		{
			return SR.GetString("PropertyDoesNotHaveSetter", new object[] { p0 });
		}

		internal static string PropertyNotDefinedForType(object p0, object p1)
		{
			return SR.GetString("PropertyNotDefinedForType", new object[] { p0, p1 });
		}

		internal static string TypeContainsGenericParameters(object p0)
		{
			return SR.GetString("TypeContainsGenericParameters", new object[] { p0 });
		}

		internal static string TypeIsGeneric(object p0)
		{
			return SR.GetString("TypeIsGeneric", new object[] { p0 });
		}

		internal static string TypeMissingDefaultConstructor(object p0)
		{
			return SR.GetString("TypeMissingDefaultConstructor", new object[] { p0 });
		}

		internal static string TypeNotIEnumerable(object p0)
		{
			return SR.GetString("TypeNotIEnumerable", new object[] { p0 });
		}

		internal static string TypeParameterIsNotDelegate(object p0)
		{
			return SR.GetString("TypeParameterIsNotDelegate", new object[] { p0 });
		}

		internal static string UnaryOperatorNotDefined(object p0, object p1)
		{
			return SR.GetString("UnaryOperatorNotDefined", new object[] { p0, p1 });
		}

		internal static string UnhandledBinary(object p0)
		{
			return SR.GetString("UnhandledBinary", new object[] { p0 });
		}

		internal static string UnhandledBindingType(object p0)
		{
			return SR.GetString("UnhandledBindingType", new object[] { p0 });
		}

		internal static string UnhandledCall(object p0)
		{
			return SR.GetString("UnhandledCall", new object[] { p0 });
		}

		internal static string UnhandledConvert(object p0)
		{
			return SR.GetString("UnhandledConvert", new object[] { p0 });
		}

		internal static string UnhandledConvertFromDecimal(object p0)
		{
			return SR.GetString("UnhandledConvertFromDecimal", new object[] { p0 });
		}

		internal static string UnhandledConvertToDecimal(object p0)
		{
			return SR.GetString("UnhandledConvertToDecimal", new object[] { p0 });
		}

		internal static string UnhandledExpressionType(object p0)
		{
			return SR.GetString("UnhandledExpressionType", new object[] { p0 });
		}

		internal static string UnhandledMemberAccess(object p0)
		{
			return SR.GetString("UnhandledMemberAccess", new object[] { p0 });
		}

		internal static string UnhandledUnary(object p0)
		{
			return SR.GetString("UnhandledUnary", new object[] { p0 });
		}

		internal static string UserDefinedOperatorMustBeStatic(object p0)
		{
			return SR.GetString("UserDefinedOperatorMustBeStatic", new object[] { p0 });
		}

		internal static string UserDefinedOperatorMustNotBeVoid(object p0)
		{
			return SR.GetString("UserDefinedOperatorMustNotBeVoid", new object[] { p0 });
		}

		// Properties
		internal static string ArgumentCannotBeOfTypeVoid
		{
			get
			{
				return SR.GetString("ArgumentCannotBeOfTypeVoid");
			}
		}

		internal static string ArgumentMustBeArray
		{
			get
			{
				return SR.GetString("ArgumentMustBeArray");
			}
		}

		internal static string ArgumentMustBeArrayIndexType
		{
			get
			{
				return SR.GetString("ArgumentMustBeArrayIndexType");
			}
		}

		internal static string ArgumentMustBeBoolean
		{
			get
			{
				return SR.GetString("ArgumentMustBeBoolean");
			}
		}

		internal static string ArgumentMustBeCheckable
		{
			get
			{
				return SR.GetString("ArgumentMustBeCheckable");
			}
		}

		internal static string ArgumentMustBeComparable
		{
			get
			{
				return SR.GetString("ArgumentMustBeComparable");
			}
		}

		internal static string ArgumentMustBeConvertible
		{
			get
			{
				return SR.GetString("ArgumentMustBeConvertible");
			}
		}

		internal static string ArgumentMustBeFieldInfoOrPropertInfo
		{
			get
			{
				return SR.GetString("ArgumentMustBeFieldInfoOrPropertInfo");
			}
		}

		internal static string ArgumentMustBeFieldInfoOrPropertInfoOrMethod
		{
			get
			{
				return SR.GetString("ArgumentMustBeFieldInfoOrPropertInfoOrMethod");
			}
		}

		internal static string ArgumentMustBeInstanceMember
		{
			get
			{
				return SR.GetString("ArgumentMustBeInstanceMember");
			}
		}

		internal static string ArgumentMustBeInt32
		{
			get
			{
				return SR.GetString("ArgumentMustBeInt32");
			}
		}

		internal static string ArgumentMustBeInteger
		{
			get
			{
				return SR.GetString("ArgumentMustBeInteger");
			}
		}

		internal static string ArgumentMustBeIntegerOrBoolean
		{
			get
			{
				return SR.GetString("ArgumentMustBeIntegerOrBoolean");
			}
		}

		internal static string ArgumentMustBeNumeric
		{
			get
			{
				return SR.GetString("ArgumentMustBeNumeric");
			}
		}

		internal static string ArgumentMustBeSingleDimensionalArrayType
		{
			get
			{
				return SR.GetString("ArgumentMustBeSingleDimensionalArrayType");
			}
		}

		internal static string ArgumentTypesMustMatch
		{
			get
			{
				return SR.GetString("ArgumentTypesMustMatch");
			}
		}

		internal static string CoalesceUsedOnNonNullType
		{
			get
			{
				return SR.GetString("CoalesceUsedOnNonNullType");
			}
		}

		internal static string ElementInitializerMethodNotAdd
		{
			get
			{
				return SR.GetString("ElementInitializerMethodNotAdd");
			}
		}

		internal static string ElementInitializerMethodStatic
		{
			get
			{
				return SR.GetString("ElementInitializerMethodStatic");
			}
		}

		internal static string ElementInitializerMethodWithZeroArgs
		{
			get
			{
				return SR.GetString("ElementInitializerMethodWithZeroArgs");
			}
		}

		internal static string ExpressionMayNotContainByrefParameters
		{
			get
			{
				return SR.GetString("ExpressionMayNotContainByrefParameters");
			}
		}

		internal static string IncorrectNumberOfArgumentsForMembers
		{
			get
			{
				return SR.GetString("IncorrectNumberOfArgumentsForMembers");
			}
		}

		internal static string IncorrectNumberOfConstructorArguments
		{
			get
			{
				return SR.GetString("IncorrectNumberOfConstructorArguments");
			}
		}

		internal static string IncorrectNumberOfIndexes
		{
			get
			{
				return SR.GetString("IncorrectNumberOfIndexes");
			}
		}

		internal static string IncorrectNumberOfLambdaArguments
		{
			get
			{
				return SR.GetString("IncorrectNumberOfLambdaArguments");
			}
		}

		internal static string IncorrectNumberOfLambdaDeclarationParameters
		{
			get
			{
				return SR.GetString("IncorrectNumberOfLambdaDeclarationParameters");
			}
		}

		internal static string IncorrectNumberOfMembersForGivenConstructor
		{
			get
			{
				return SR.GetString("IncorrectNumberOfMembersForGivenConstructor");
			}
		}

		internal static string IncorrectNumberOfTypeArgsForAction
		{
			get
			{
				return SR.GetString("IncorrectNumberOfTypeArgsForAction");
			}
		}

		internal static string IncorrectNumberOfTypeArgsForFunc
		{
			get
			{
				return SR.GetString("IncorrectNumberOfTypeArgsForFunc");
			}
		}

		internal static string LambdaParameterNotInScope
		{
			get
			{
				return SR.GetString("LambdaParameterNotInScope");
			}
		}

		internal static string LambdaTypeMustBeDerivedFromSystemDelegate
		{
			get
			{
				return SR.GetString("LambdaTypeMustBeDerivedFromSystemDelegate");
			}
		}

		internal static string ListInitializerWithZeroMembers
		{
			get
			{
				return SR.GetString("ListInitializerWithZeroMembers");
			}
		}

		internal static string OwningTeam
		{
			get
			{
				return SR.GetString("OwningTeam");
			}
		}

		internal static string ParameterNotCaptured
		{
			get
			{
				return SR.GetString("ParameterNotCaptured");
			}
		}

		internal static string UnexpectedCoalesceOperator
		{
			get
			{
				return SR.GetString("UnexpectedCoalesceOperator");
			}
		}

		internal static string UnhandledBinding
		{
			get
			{
				return SR.GetString("UnhandledBinding");
			}
		}

		internal static string UnknownBindingType
		{
			get
			{
				return SR.GetString("UnknownBindingType");
			}
		}
	}
}
