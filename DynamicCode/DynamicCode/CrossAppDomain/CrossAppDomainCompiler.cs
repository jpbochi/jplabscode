﻿using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.CSharp;

namespace JpLabs.DynamicCode.CrossAppDomain
{
	internal class CrossAppDomainCompiler : MarshalByRefObject, ICompiler
	{
		public IEnumerable<string> References	{ get; set; }
		public IEnumerable<string> Usings		{ get; set; }
		
		public CrossAppDomainCompiler()
		{
			this.SetDefault();
		}

		public void AddReferences(params string[] asms)
		{
			this.References = this.References.Union(asms).ToArray();
		}

		public void AddUsings(params string[] usings)
		{
			this.Usings = this.Usings.Union(usings).ToArray();
		}

		public MethodIL Compile<TDelegate>(string functionCode)
		{
			Type funcType = typeof(TDelegate);
			if (!typeof(Delegate).IsAssignableFrom(funcType)) throw new ArgumentException();
			
			StringBuilder code = new StringBuilder();
			foreach (var use in Usings) code.AppendFormat("using {0};\n", use);
			
			code.AppendLine("namespace JpLabs.DynamicCode.DynamicAssembly {");
			code.AppendLine("public static class DynamicClass {");

			var invokeFuncInfo = funcType.GetMethod("Invoke");
			var returnType = invokeFuncInfo.ReturnType;
			var parameters = invokeFuncInfo.GetParameters();

			code.AppendFormat("static public {0} DynamicFunc({1})\n",
				returnType.ToCSharpCode(),
				parameters.Select(p => p.ParameterType.ToCSharpCode() + " " + p.Name).Join(", ")
			);
			code.AppendLine("{");
			code.AppendLine(functionCode);
			code.AppendLine("}}}");
			
			CompilerParameters cp = new CompilerParameters();
			foreach (var asm in References) cp.ReferencedAssemblies.Add(asm);
			cp.GenerateExecutable = false;
			cp.GenerateInMemory = true;
			
			var csc = CrossAppDomainCompiler.CodeProvider;
			
			CompilerResults output = csc.CompileAssemblyFromSource(cp, code.ToString());
			
			if (output.Errors.HasErrors) throw new CompileException(output.Errors);
			
			var dynClass = output.CompiledAssembly.GetType("JpLabs.DynamicCode.DynamicAssembly.DynamicClass");
			if (dynClass == null) throw new ApplicationException("Dynamic class not found in generated assembly");
			
			var methodInfo = dynClass.GetMethod("DynamicFunc");
			if (methodInfo == null) throw new ApplicationException("Dynamic function not found in generated assembly");
			
			return MethodIL.FromMethodInfo(methodInfo);
		}

		static CodeDomProvider _codeProvider;
		static public CodeDomProvider CodeProvider
		{
			get {
				if (_codeProvider == null) _codeProvider = GetCodeProvider();
				return _codeProvider;
			}
		}
		
		static public CodeDomProvider GetCodeProvider()
		{
			//http://blogs.msdn.com/lukeh/archive/2007/07/11/c-3-0-and-codedom.aspx
			IDictionary<string,string> codeProviderOptions = new Dictionary<string, string>() {{"CompilerVersion", "v3.5"}};
			//codeProviderOptions = new MyDictionary(codeProviderOptions);
			return new CSharpCodeProvider(codeProviderOptions);
		}
		
		/*
		class MyDictionary : IDictionary<string,string>
		{
			IDictionary<string,string> innerDict;
			
			public MyDictionary(IDictionary<string,string> dict)
			{
				innerDict = dict;
			}

			#region IDictionary<string,string> Members

			void IDictionary<string, string>.Add(string key, string value)
			{
				innerDict.Add(key, value);
			}

			bool IDictionary<string, string>.ContainsKey(string key)
			{
				return innerDict.ContainsKey(key);
			}

			ICollection<string> IDictionary<string, string>.Keys
			{
				get { return innerDict.Keys; }
			}

			bool IDictionary<string, string>.Remove(string key)
			{
				return innerDict.Remove(key);
			}

			bool IDictionary<string, string>.TryGetValue(string key, out string value)
			{
				return innerDict.TryGetValue(key, out value);
			}

			ICollection<string> IDictionary<string, string>.Values
			{
				get { return innerDict.Values; }
			}

			string IDictionary<string, string>.this[string key]
			{
				get
				{
					return innerDict[key];
				}
				set
				{
					innerDict[key] = value;
				}
			}

			#endregion

			#region ICollection<KeyValuePair<string,string>> Members

			void ICollection<KeyValuePair<string, string>>.Add(KeyValuePair<string, string> item)
			{
				innerDict.Add(item);
			}

			void ICollection<KeyValuePair<string, string>>.Clear()
			{
				innerDict.Clear();
			}

			bool ICollection<KeyValuePair<string, string>>.Contains(KeyValuePair<string, string> item)
			{
				return innerDict.Contains(item);
			}

			void ICollection<KeyValuePair<string, string>>.CopyTo(KeyValuePair<string, string>[] array, int arrayIndex)
			{
				innerDict.CopyTo(array, arrayIndex);
			}

			int ICollection<KeyValuePair<string, string>>.Count
			{
				get { return innerDict.Count; }
			}

			bool ICollection<KeyValuePair<string, string>>.IsReadOnly
			{
				get { return innerDict.IsReadOnly; }
			}

			bool ICollection<KeyValuePair<string, string>>.Remove(KeyValuePair<string, string> item)
			{
				return innerDict.Remove(item);
			}

			#endregion

			#region IEnumerable<KeyValuePair<string,string>> Members

			IEnumerator<KeyValuePair<string, string>> IEnumerable<KeyValuePair<string, string>>.GetEnumerator()
			{
				return innerDict.GetEnumerator();
			}

			#endregion

			#region IEnumerable Members

			System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
			{
				return ((System.Collections.IEnumerable)innerDict).GetEnumerator();
			}

			#endregion
		}
		//*/
	}
}
