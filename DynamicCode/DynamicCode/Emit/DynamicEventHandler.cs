using System;
using System.Reflection;
using System.Reflection.Emit;
using System.Collections.Generic;
using System.Text;

namespace JpLabs.DynamicCode.Emit
{
	internal class DynamicEventHandler
	{
		DynamicMethodDelegate dynamicMethod;
		
		internal DynamicEventHandler(MethodInfo method)
		{
			dynamicMethod = DynamicMethodCompiler.CreateMethod(method);
		}
		
		internal void DynamicInvoke(object sender, EventArgs e)
		{
			dynamicMethod(sender, new object[]{sender, e}); 
		}

		internal static EventHandler CreateDelegate(MethodInfo method)
		{
			if (method.IsStatic) {
			    ///http://www.devx.com/vb2themax/Tip/18827
			    return (EventHandler)Delegate.CreateDelegate(typeof(EventHandler), method);
			} else {
			    ///http://www.codeproject.com/csharp/dynamicmethoddelegates.asp
			    return new EventHandler(new DynamicEventHandler(method).DynamicInvoke);
			    
				//DynamicMethodDelegate dynamicMethod = DynamicMethodCompiler.CreateMethod(method);
				//EventHandler dynamicEventHandler = delegate(object sender, EventArgs e)
				//{
				//    dynamicMethod(sender, new object[]{sender, e}); 
				//}
			    //return (EventHandler)Delegate.CreateDelegate(typeof(EventHandler), dynamicMethod.Method);
			}
		}
	}

	#region LGPL License
	/*
	 *  DynamicMethod Delegates Demo
	 * 
	 *  Copyright (C) 2005 
	 *      Alessandro Febretti <mailto:febret@gmail.com>
	 *      SharpFactory
	 *
	 *  This program is free software; you can redistribute it and/or modify
	 *  it under the terms of the GNU General Public License as published by
	 *  the Free Software Foundation; either version 2 of the License, or
	 *  (at your option) any later version.
	 * 
	 *  This program is distributed in the hope that it will be useful,
	 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
	 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	 *  GNU General Public License for more details.
	 *
	 *  You should have received a copy of the GNU General Public License
	 *  along with this program; if not, write to the Free Software
	 *  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
	 */
	#endregion
}
