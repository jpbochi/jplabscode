using System;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace JpLabs.DynamicCode.Emit
{
	/// TODO:
	/// - Create functions for specific method signatures like EventHandler, Action, Func, Predicate, etc.
	/// - Is there a way to add custom attributes to a dynamically generated method? Unfortunately, no.
	
	using LateBoundGetMemberValue = Func<object,object>;
	using LateBoundSetMemberValue = Action<object,object>;

	public delegate object DynamicInstanceMethod(object target, params object[] args);
	public delegate object DynamicStaticMethod(params object[] args);

	/// <summary>
	/// Generates compiled delegates for these late-bound invocations:
	///		- Calling a constructor;
	///		- Calling a method;
	///		- Calling a property getter/setter;
	///		- Calling a field setter/setter;
	/// Inspired by: http://kohari.org/2009/03/06/fast-late-bound-invocation-with-expression-trees/
	/// Measured times show that calling It's about 24 times faster than calling MethodInfo.Invoke()
	/// </summary>
	public static class LateBoundMethodFactory
	{
		#region static cached reflection
		
		private static readonly MethodInfo ThrowArgumentNullExceptionInfo;
		private static readonly MethodInfo VoidThrowArgumentNullExceptionInfo;
		private static readonly MethodInfo ThrowTargetParameterCountExceptionInfo;
		private static readonly MethodInfo FieldInfo_SetValue_Info;
		
		static LateBoundMethodFactory()
		{
			BindingFlags staticNonPublic = BindingFlags.Static | BindingFlags.NonPublic;
			ThrowArgumentNullExceptionInfo = typeof(LateBoundMethodFactory).GetMethod("ThrowArgumentNullException", staticNonPublic);
			VoidThrowArgumentNullExceptionInfo = typeof(LateBoundMethodFactory).GetMethod("VoidThrowArgumentNullException", staticNonPublic);
			ThrowTargetParameterCountExceptionInfo = typeof(LateBoundMethodFactory).GetMethod("ThrowTargetParameterCountException", staticNonPublic);
			FieldInfo_SetValue_Info = typeof(FieldInfo).GetMethod("SetValue", new[] {typeof(object), typeof(object)});
		}
		
		#endregion

		/// <summary>
		///	Creates and compiles an Expression like this:
		/// (object[] args) =>
		/// (object)(
		///     (args == null) ? (object)ThrowArgumentNullException("args") :
		///     (args.Length != {method.ArgumentCount}) ? (object)ThrowTargetParameterCountException() :
		///     method(args[0], args[1], ...)
		/// )
		/// </summary>
		static public DynamicStaticMethod CreateStaticMethod(MethodInfo staticMethod)
		{
			if (staticMethod == null) throw new ArgumentNullException("method");
			if (!staticMethod.IsStatic) throw new ArgumentException("method parameter has to be static");
			
			//Create 'args' parameter expression
			ParameterExpression argsParameter = Expression.Parameter(typeof(object[]), "args");
			
			//Create body expression
			ParameterInfo[] methodParams = staticMethod.GetParameters();
			Expression body = Expression.Call(
				staticMethod,
				CreateParameterExpressions(methodParams, argsParameter)
			);
			
			//If the provided method returns use (method() is object), which always evaluates to false
			if (body.Type == typeof(void)) {
				body = Expression.TypeIs(body, typeof(void));
			}
			
			//Prepend parameter validation
			body = EnsureArgumentCount(argsParameter, methodParams.Length, body);
			body = EnsureParameterNotNull(argsParameter, body);
			
			//Create and compile lambda
			var lambda = Expression.Lambda<DynamicStaticMethod>(
				Expression.Convert(body, typeof(object)),
				argsParameter
			);
			return lambda.Compile();
		}
		
		/// <summary>
		///	Creates and compiles an Expression like this:
		/// (target, args) =>
		/// (object)(
		///     (target == null) ? (object)ThrowArgumentNullException("target") : //line NOT generated if 'method' is static
		///     (args == null) ? (object)ThrowArgumentNullException("args") :
		///     (args.Length != {method.ArgumentCount}) ? (object)ThrowTargetParameterCountException() :
		///     target.method(args[0], args[1], ...)
		/// )
		/// </summary>
		static public DynamicInstanceMethod CreateMethod(MethodInfo method)
		{
			if (method == null) throw new ArgumentNullException("method");
			
			//Create parameter expressions
			ParameterExpression targetParameter = Expression.Parameter(typeof(object), "target");
			ParameterExpression argsParameter = Expression.Parameter(typeof(object[]), "args");
			
			//Create body expression
			ParameterInfo[] methodParams = method.GetParameters();
			Expression body = Expression.Call(
				Expression.Convert(targetParameter, method.DeclaringType),
				method,
				CreateParameterExpressions(methodParams, argsParameter)
			);
			
			//If the provided method returns use (method() is object), which always evaluates to false
			if (body.Type == typeof(void)) {
				body = Expression.TypeIs(body, typeof(void));
			}
			
			//Prepend parameter validation
			body = EnsureArgumentCount(argsParameter, methodParams.Length, body);
			body = EnsureParameterNotNull(argsParameter, body);
			if (!method.IsStatic) body = EnsureParameterNotNull(targetParameter, body);
			
			//Create and compile lambda
			var lambda = Expression.Lambda<DynamicInstanceMethod>(
				Expression.Convert(body, typeof(object)),
				targetParameter,
				argsParameter
			);
			return lambda.Compile();
		}
		
		/// <summary>
		///	Creates and compiles an Expression like this:
		/// (object[] args) =>
		/// (object)(
		///     (args == null) ? (object)ThrowArgumentNullException("args") :
		///     (args.Length != {constructor.ArgumentCount}) ? (object)ThrowTargetParameterCountException() :
		///     new {ConstructedType}(args[0], args[1], ...)
		/// )
		/// </summary>
		static public DynamicStaticMethod CreateConstructor(ConstructorInfo constructor)
		{
			if (constructor == null) throw new ArgumentNullException("constructor");
			
			//Create 'args' parameter expression
			ParameterExpression argsParameter = Expression.Parameter(typeof(object[]), "args");
			
			//Create body expression
			ParameterInfo[] constructorParams = constructor.GetParameters();
			Expression body = Expression.New(
				constructor,
				CreateParameterExpressions(constructorParams, argsParameter)
			);
			
			//Prepend parameter validation
			body = EnsureArgumentCount(argsParameter, constructorParams.Length, body);
			body = EnsureParameterNotNull(argsParameter, body);
			
			//Create and compile lambda
			var lambda = Expression.Lambda<DynamicStaticMethod>(
				Expression.Convert(body, typeof(object)),
				argsParameter
			);
			return lambda.Compile();
		}

		/// <summary>
		///	Creates and compiles an Expression like this:
		///	(target, value) =>
		/// (object)(
		///     (target == null) ? (object)ThrowArgumentNullException("target") : //line NOT generated if 'member' is static
		///		target.get_Member(value)
		/// )
		/// </summary>
		static public LateBoundGetMemberValue CreateGet(MemberInfo member)
		{
			if (member == null) throw new ArgumentNullException("member");
			
			//Create 'target' parameter expression
			ParameterExpression targetParameter = Expression.Parameter(typeof(object), "target");
			
			//Create body expression
			Expression body = Expression.MakeMemberAccess(targetParameter, member);
			
			//Prepend parameter validation
			if (!IsStatic(member)) body = EnsureParameterNotNull(targetParameter, body);
			
			//Create and compile lambda
			var lambda = Expression.Lambda<LateBoundGetMemberValue>(
				Expression.Convert(body, typeof(object)),
				targetParameter
			);
			return lambda.Compile();
		}

		/// <summary>
		///	Creates and compiles an Expression like this:
		///	(target, value) =>
		/// (void)(
		///     (target == null) ? (object)ThrowArgumentNullException("target") : //line NOT generated if 'property' is static
		///		target.set_Property(value)
		/// )
		/// </summary>
		/// <remarks>
		/// Beware that lambda expressions are meant to represent functions without side-effects.
		/// That means that generating a Get is simpler than generating a Set.
		/// To generate a Set, a call to the setter method has to be called.
		/// </remarks>
		static public LateBoundSetMemberValue CreateSet(PropertyInfo property)
		{
			if (property == null) throw new ArgumentNullException("property");
			if (!property.CanWrite) throw new ArgumentException("Property has no set accessor");
			
			//Create parameter expressions
			ParameterExpression targetParameter = Expression.Parameter(typeof(object), "target");
			ParameterExpression valueParameter = Expression.Parameter(typeof(object), "value");
			
			//Create body expression
			MethodInfo setterMethod = property.GetSetMethod(true);
			Expression body = Expression.Call(
				Expression.Convert(targetParameter, setterMethod.DeclaringType),
				setterMethod,
				Expression.Convert(valueParameter, property.PropertyType)
			);
			
			//Prepend parameter validation
			if (!IsStatic(property)) body = EnsureParameterNotNull(targetParameter, body);
			
			//Create and compile lambda
			var lambda = Expression.Lambda<LateBoundSetMemberValue>(
				body,
				targetParameter,
				valueParameter
			);
			return lambda.Compile();
		}

		/// <summary>
		///	Creates and compiles an Expression like this:
		///	(target, value) =>
		/// (void)(
		///     (target == null) ? (object)ThrowArgumentNullException("target") : //line NOT generated if 'field' is static
		///		fieldInfo.SetValue(target, value)
		/// )
		/// </summary>
		/// <remarks>
		/// Fields don't have accessor methods, so the FieldInfo's SetValue is called
		/// </remarks>
		static public LateBoundSetMemberValue CreateSet(FieldInfo field)
		{
			if (field == null) throw new ArgumentNullException("field");
			
			//Create parameter expressions
			ParameterExpression targetParameter = Expression.Parameter(typeof(object), "target");
			ParameterExpression valueParameter = Expression.Parameter(typeof(object), "value");
			
			//Create body expression
			Expression body = Expression.Call(
				Expression.Constant(field),
				FieldInfo_SetValue_Info,
				targetParameter,
				valueParameter
			);
			
			//Prepend parameter validation
			if (!field.IsStatic) body = EnsureParameterNotNull(targetParameter, body);
			
			//Create and compile lambda
			var lambda = Expression.Lambda<LateBoundSetMemberValue>(
				body,
				targetParameter,
				valueParameter
			);
			return lambda.Compile();
		}
		
		#region private helper methods

		private static Expression[] CreateParameterExpressions(ParameterInfo[] methodParams, ParameterExpression argsParameter)
		{
			return methodParams.Select(
				(parameter, index) =>
				Expression.Convert(
					Expression.ArrayIndex(argsParameter, Expression.Constant(index)),
					parameter.ParameterType
				)
			).ToArray();
		}

		private static ConditionalExpression EnsureParameterNotNull(ParameterExpression parameter, Expression continuation)
		{
			//Choose how to throw exception according to continuation type
			Type continuationType = continuation.Type;
			Expression throwArgNullExpression;
			if (continuationType == typeof(void)) {
				throwArgNullExpression = Expression.Call(VoidThrowArgumentNullExceptionInfo, Expression.Constant(parameter.Name));
			} else {
				throwArgNullExpression =  Expression.Convert(
					Expression.Call(ThrowArgumentNullExceptionInfo, Expression.Constant(parameter.Name)),
					continuationType
				);
			}
			
			//Build conditional expression
			return Expression.Condition(
				Expression.Equal(parameter, Expression.Constant(null)),
				throwArgNullExpression,
				continuation
			);
		}

		private static ConditionalExpression EnsureArgumentCount(ParameterExpression argsParameter, int expectedArgCount, Expression continuation)
		{
			//Build conditional expression
			return Expression.Condition(
				Expression.NotEqual(Expression.ArrayLength(argsParameter), Expression.Constant(expectedArgCount)),
				Expression.Convert(
					Expression.Call(ThrowTargetParameterCountExceptionInfo),
					continuation.Type
				),
				continuation
			);
		}
		
		private static bool IsStatic(MemberInfo member)
		{
			if (member is PropertyInfo) {
				return IsStatic((PropertyInfo)member);
			} else if (member is FieldInfo) {
				return ((FieldInfo)member).IsStatic;
			} else {
				throw new ArgumentException("member has to be either PropertyInfo or FieldInfo");
			}
		}

		private static bool IsStatic(PropertyInfo property)
		{
			MethodInfo accessor = property.GetAccessors().FirstOrDefault();
			if (accessor == null) throw new ArgumentException("property has no accessors!?");
			return accessor.IsStatic;
		}

		[System.Diagnostics.DebuggerHidden]
		private static object ThrowArgumentNullException(string paramName)
		{
			throw new ArgumentNullException(paramName);
		}

		[System.Diagnostics.DebuggerHidden]
		private static void VoidThrowArgumentNullException(string paramName)
		{
			throw new ArgumentNullException(paramName);
		}

		[System.Diagnostics.DebuggerHidden]
		private static object ThrowTargetParameterCountException()
		{
			throw new TargetParameterCountException();
		}
		
		#endregion
	}
}