using System;
using System.CodeDom.Compiler;
using System.Linq;
using System.Runtime.Serialization;

namespace JpLabs.DynamicCode
{
    /// <summary>
    /// A custom exception that will be thrown when there is a compile exception when using CodeDom
    /// </summary>
    [Serializable]
    public class CompileException : ApplicationException, ISerializable
    {
        private CompilerErrorCollection m_errors;

        protected CompileException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
            m_errors = (CompilerErrorCollection)info.GetValue("Errors", typeof(CompilerErrorCollection));
        }

        public CompileException(string message) : base(message)
        {
        }

        public CompileException(CompilerErrorCollection errors) : base()
        {
            m_errors = errors;
        }

        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
            
            info.AddValue("Errors", m_errors);
        }

        public CompilerErrorCollection CompileErrors
        {
            get { return m_errors; }
        }

		public override string Message
		{
			get {
				return
					m_errors
					.Cast<CompilerError>()
					.Select((Func<CompilerError,string>)CompilerErrorToString)
					.Join("\n");
			}
		}
		
		static public string CompilerErrorToString(CompilerError error)
		{
			return string.Format("{0} {1}: {2}", error.IsWarning ? "Warning" : "Error" , error.ErrorNumber, error.ErrorText);
		}
    }
}
