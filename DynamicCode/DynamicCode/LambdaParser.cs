﻿using System;
using System.Linq;
using System.Text.RegularExpressions;

namespace JpLabs.DynamicCode
{
	public struct ParamInfo
	{
		public string Type;
		public string ParamName;
	}
	
	public static class LambdaParser
	{
		private static Regex LambdaParamsRegex = new Regex(
				@"(?:\A\s*(?<beginparen>\(?)\s*|(?:(?<!\A\s*)\G\s*,\s*))" +
				@"(?:(?<type>(?:[@\w-[\d]][\w]*.)*[@\w-[\d]][\w]*)\s+)?" +
				@"(?<arg>[@\w-[\d]][\w]*)" +
				@"(?:(?<endparen>\)?)\s*(?<endargs>\=\>))?"
			);

		static public bool TryParseLambdaArgs(string expression, out ParamInfo[] outParams)
		{
			outParams = null;
			var matches = LambdaParamsRegex.Matches(expression).Cast<Match>().ToArray();

			bool ended = matches.Last().Groups["endargs"].Value.Length > 0;
			if (!ended) return false;

			bool usedOpenParen	= matches.First().Groups["beginparen"].Value.Length > 0;
			bool usedCloseParen	= matches.Last().Groups["endparen"].Value.Length > 0;
			if (usedOpenParen != usedCloseParen) return false;
			if (matches.Length > 1 && !usedOpenParen) return false;

			outParams = matches.Select(
				m => new ParamInfo(){
					Type = m.Groups["type"].Value,
					ParamName = m.Groups["arg"].Value
				}
			).ToArray();

			if (matches.Length == 1 && !usedOpenParen && !string.IsNullOrEmpty(outParams[0].Type)) return false;
			return true;
		}

		static public ParamInfo[] ParseLambdaArgs(string expression)
		{
			ParamInfo[] retParams;
			if (TryParseLambdaArgs(expression, out retParams)) {
				return retParams;
			} else {
				throw FailParseLambdaParams();
			}
		}
		
		private static Exception FailParseLambdaParams()
		{
			return new CompileException("Failed to parse Lambda Expression parameters");
		}
	}
}
