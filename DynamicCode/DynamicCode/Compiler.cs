﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using JpLabs.DynamicCode.CrossAppDomain;

namespace JpLabs.DynamicCode
{
	public interface ICompiler
	{
		IEnumerable<string> References	{ get; set; }
		IEnumerable<string> Usings		{ get; set; }
	}

	public static class CompilerExt
	{
		static public void SetDefault(this ICompiler compiler)
		{
			compiler.References = new[] {
				"System.dll",
				"System.Core.dll"
			};
			compiler.Usings = new[] {
				"System"
			};
		}
		
		static public void AddReferences(this ICompiler compiler, params string[] asms)
		{
			AddReferences(compiler, (IEnumerable<string>)asms);
		}
		
		static public void AddUsings(this ICompiler compiler, params string[] usings)
		{
			AddUsings(compiler, (IEnumerable<string>)usings);
		}

		static public void AddReferences(this ICompiler compiler, IEnumerable<string> asms)
		{
			compiler.References = compiler.References.Union(asms);
		}
		
		static public void AddUsings(this ICompiler compiler, IEnumerable<string> usings)
		{
			compiler.Usings = compiler.Usings.Union(usings);
		}
		
		static public void AddTypeReference(this ICompiler compiler, Type type)
		{
			compiler.AddReferences(type.Assembly.ManifestModule.Name);
			compiler.AddUsings(type.Namespace);
		}
	}

	public class CompilerParams : ICompiler
	{
		public IEnumerable<string> References	{ get; set; }
		public IEnumerable<string> Usings		{ get; set; }
		
		public CompilerParams() { this.SetDefault(); }
	}

	public class Compiler: ICompiler, IDisposable
	{
		private CrossAppDomainFactory _crossDomainFactory;
		CrossAppDomainFactory CrossDomainFactory
		{
			get {
				EnsureCrossDomainFactory();
				return _crossDomainFactory;
			}
		}
		
		private void EnsureCrossDomainFactory()
		{
			lock (this) if (_crossDomainFactory == null) _crossDomainFactory = CrossAppDomainFactory.Create(Guid.NewGuid().ToString());
		}
		
		public Compiler()
		{
			this.SetDefault();
		}

		public Compiler(ICompiler copy)
		{
			References = copy.References.ToArray();
			Usings = copy.Usings.ToArray();
		}

		public IEnumerable<string> References	{ get; set; }
		public IEnumerable<string> Usings		{ get; set; }
		
		static private void ValidateTDelegate(Type typeofTDelegate)
		{
			//This function is needed becase the following syntax is invalid (fi you don't believe me, just try it):
			//... where TDelegate : System.Delegate
			if (!typeof(Delegate).IsAssignableFrom(typeofTDelegate)) throw new ArgumentException();
		}
		
		static public Expression<TDelegate> ParseLambdaExprDefault<TDelegate>(string expression)
		{
			ValidateTDelegate(typeof(TDelegate));
			
			using (var parser = new Compiler()) return parser.ParseLambdaExpr<TDelegate>(expression);
		}

		public Expression<TDelegate> ParseLambdaExpr<TDelegate>(string expression)
		{
			ValidateTDelegate(typeof(TDelegate));
			return (Expression<TDelegate>)ParseLambdaExpr(expression, typeof(TDelegate).ToCSharpCode());
		}
		
		public LambdaExpression ParseLambdaExpr(string expression, string encodedFuncType)
		{
			var compiler = GetNewCompiler();
			
			compiler.AddUsings(
				"System.Linq",
				"System.Linq.Expressions",
				"System.Collections.Generic"
			);
			
			var funcSource = string.Format("return (Expression<{0}>)({1});", encodedFuncType, expression);
			
			var methodIL = compiler.Compile<Func<LambdaExpression>>(funcSource);
			
			var getLambdaFunc = methodIL.ToDelegate<Func<LambdaExpression>>();
			
			return getLambdaFunc();
		}

		public TDelegate ParseLambdaFunc<TDelegate>(string expression)
		{
			//It's impossible to serialize a Delegate so that it can cross AppDomains
			//So we need to get a lambda expression from the other AppDomain and compile it
			return ParseLambdaExpr<TDelegate>(expression).Compile();
		}

		public LambdaExpression ParseLambdaExpr(string expression)
		{
			var args = LambdaParser.ParseLambdaArgs(expression);
			string encodedFuncType = string.Concat(
				"Func<", args.Select(a => a.Type).Concat("object").Join(","), ">"
			);
			return ParseLambdaExpr(expression, encodedFuncType);
		}

		public Delegate ParseLambdaFunc(string expression)
		{
			return ParseLambdaExpr(expression).Compile();
		}

		internal CrossAppDomainCompiler GetNewCompiler()
		{
			this.References = this.References.ToArray();
			this.Usings = this.Usings.ToArray();

			var compiler = CrossDomainFactory.CreateObj<CrossAppDomainCompiler>();
			compiler.References = this.References;
			compiler.Usings = this.Usings;
			return compiler;
		}
		
		public int DynamicAssembliesCount
		{
			get {
				var count = CrossDomainFactory.DynamicAssembliesCount();
				return count;
			}
		}

		public void RecycleAppDomain()
		{
			lock (this) {
				Dispose();
				EnsureCrossDomainFactory();
			}
		}
		
		public void Dispose()
		{
			lock (this) {
				if (_crossDomainFactory != null) {
					_crossDomainFactory.Dispose();
					_crossDomainFactory = null;
				}
			}
		}
	}
}
