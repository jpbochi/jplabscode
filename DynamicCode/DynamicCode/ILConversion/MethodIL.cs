﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using JpLabs.DynamicCode.ILConversion;

namespace JpLabs.DynamicCode
{
	//Original source: http://www.codeproject.com/KB/cs/ExpressionEval.aspx
	
    /// <summary>
    /// A serialized version of a method.  Holds everything to create a DynamicMethod.
    /// </summary>
    [Serializable]
    internal sealed class MethodIL
    {
        /// <summary>The IL bytes from compilation</summary>
        public byte[] CodeBytes;

        /// <summary>Whether or not local variables are initialized</summary>
        public bool InitLocals;

        /// <summary>The maximum size of the stack required for this method</summary>
        public int MaxStackSize;

        /// <summary>Local variables defined for the method</summary>
        //public IDictionary<int,LocalVariable> LocalVariables;
        public LocalVariable[] LocalVariables;

        /// <summary>Definition of tokens that need to be resolved</summary>
        public IList<ILToken> Tokens;
        
        private MethodIL() {}

        static public MethodIL FromMethodInfo(MethodInfo dynamicMethod)
        {
            //IL info from method
            MethodBody sourceIL = dynamicMethod.GetMethodBody();

            //get code bytes and other method properties
            MethodIL methodIL = new MethodIL();
            methodIL.CodeBytes = sourceIL.GetILAsByteArray();
            methodIL.InitLocals = sourceIL.InitLocals;
            methodIL.MaxStackSize = sourceIL.MaxStackSize;

            //get any local variable information
			methodIL.LocalVariables
				= sourceIL.LocalVariables
				.OrderBy(lv => lv.LocalIndex)
				.Select(lv => new LocalVariable(lv.IsPinned, lv.LocalType.TypeHandle))
				.ToArray();

            //get metadata token offsets
            methodIL.Tokens = ILReader.ReadCode(methodIL.CodeBytes, dynamicMethod.Module);

            return methodIL;
        }

        public TDelegate ToDelegate<TDelegate>() //where TDelegate : System.Delegate
        {
			if (!typeof(Delegate).IsAssignableFrom(typeof(TDelegate))) throw new ArgumentException();

			var invokeFuncInfo = typeof(TDelegate).GetMethod("Invoke");
			var returnType = invokeFuncInfo.ReturnType;
			var parameterTypes = invokeFuncInfo.GetParameters().Select(pinfo => pinfo.ParameterType).ToArray();

			//create a dynamic method
			DynamicMethod dynamicMethod = new DynamicMethod(
				"_" + Guid.NewGuid().ToString("N"),
				returnType,
				parameterTypes, typeof(object)
			);

			//get the IL writer for it
			DynamicILInfo dynamicInfo = dynamicMethod.GetDynamicILInfo();

			//set the properties gathered from the compiled expression
			dynamicMethod.InitLocals = this.InitLocals;

			//set local variables
			SignatureHelper locals = SignatureHelper.GetLocalVarSigHelper();
			foreach (var lv in this.LocalVariables) locals.AddArgument(Type.GetTypeFromHandle(lv.LocalType), lv.IsPinned);

			dynamicInfo.SetLocalSignature(locals.GetSignature());

			//resolve any metadata tokens
			this.CodeBytes = ILTokenResolver.ResolveCodeTokens(this.Tokens, this.CodeBytes, dynamicInfo);

			//set the IL code for the dynamic method
			dynamicInfo.SetCode(this.CodeBytes, this.MaxStackSize);

			//create a delegate for fast execution
			return (TDelegate)(object)dynamicMethod.CreateDelegate(typeof(TDelegate));
        }
    }

    /// <summary>
    /// Defines a local variable for a method
    /// </summary>
    [Serializable]
    internal sealed class LocalVariable
    {
        /// <summary>
        /// Whether or not the variable is pinned in memory, used for working with unmanaged memory
        /// </summary>
        public bool IsPinned;

        /// <summary>
        /// Type of the variable
        /// </summary>
        public RuntimeTypeHandle LocalType;

        public LocalVariable(bool isPinned, RuntimeTypeHandle localType)
        {
            IsPinned = isPinned;
            LocalType = localType;
        }
	}

	[Serializable]
	internal sealed class ILToken
	{
		public int Offset { get; private set; }
		public object Handle { get; private set; }
		
		public ILToken(int offset, object handle)
		{
			Offset = offset;
			Handle = handle;
		}
	}
}
