using System;
using System.Collections.Generic;
using System.Reflection;
using System.Reflection.Emit;

namespace JpLabs.DynamicCode.ILConversion
{
    /// <summary>
    /// Resolves tokens for given code bytes and a DynamicILInfo
    /// </summary>
    internal static class ILTokenResolver
    {
        /// <summary>
        /// Resolves the tokens given the code bytes and DynamicILInfo class
        /// </summary>
        /// <param name="code"></param>
        /// <param name="dynamicInfo"></param>
        /// <returns>byte[] - code bytes with resolved tokens</returns>
        static public byte[] ResolveCodeTokens(IEnumerable<ILToken> tokens, byte[] code, DynamicILInfo dynamicInfo)
        {
            byte[] resolvedCode = new byte[code.Length];

            //copy code bytes
            Array.Copy(code, resolvedCode, code.Length);
            
            foreach (var token in tokens) {
				if (token.Handle is RuntimeFieldHandle) {
					var handle = (RuntimeFieldHandle)token.Handle;
					
					int newMetadataToken = dynamicInfo.GetTokenFor(handle);
					OverwriteInt32(resolvedCode, token.Offset, newMetadataToken);
				} else if (token.Handle is MethodBase) {
					var handle = (MethodBase)token.Handle;

					int newMetadataToken;
	                
					//generic types require the declaring type when resolving
					if (handle.DeclaringType != null && handle.DeclaringType.IsGenericType) {
						newMetadataToken = dynamicInfo.GetTokenFor(handle.MethodHandle, handle.DeclaringType.TypeHandle);
					} else {
						newMetadataToken = dynamicInfo.GetTokenFor(handle.MethodHandle);
					}

					OverwriteInt32(resolvedCode, token.Offset, newMetadataToken);
				} else if (token.Handle is RuntimeTypeHandle) {
					var handle = (RuntimeTypeHandle)token.Handle;
					
					int newMetadataToken = dynamicInfo.GetTokenFor(handle);
					OverwriteInt32(resolvedCode, token.Offset, newMetadataToken);
				} else if (token.Handle is string) {
					var handle = (string)token.Handle;
					
					int newMetadataToken = dynamicInfo.GetTokenFor(handle);
					OverwriteInt32(resolvedCode, token.Offset, newMetadataToken);
				}
			}
			
            return resolvedCode;
        }

        /// <summary>
        /// Method to overwrite an int value with another within code bytes.
        /// </summary>
        /// <param name="code">code bytes</param>
        /// <param name="offset">byte index</param>
        /// <param name="tokenValue">value to write</param>
        private static void OverwriteInt32(byte[] code, int offset, int tokenValue)
        {
            code[offset++] = (byte)tokenValue;
            code[offset++] = (byte)(tokenValue >> 8);
            code[offset++] = (byte)(tokenValue >> 16);
            code[offset] = (byte)(tokenValue >> 24);
        }
    }
}
