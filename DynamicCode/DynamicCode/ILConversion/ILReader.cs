using System;
using System.Collections.Generic;
using System.Reflection;
using System.Reflection.Emit;

namespace JpLabs.DynamicCode.ILConversion
{
    /// <summary>
    /// Reads the Intermediate Language from a byte array and the module the method belongs to.
    /// Parses out the Field, Method, Type, and Literal String tokens.
    /// </summary>
    internal static class ILReader
    {
        private static OpCode[] m_multiByteOpCodes;
        private static OpCode[] m_singleByteOpCodes;

        static ILReader()
        {
            LoadOpCodes();
        }

        //Original Author
        //'Parsing the IL of a Method Body' - by Sorin Serban    
        //http://www.codeproject.com/csharp/sdilreader.asp
        //Drew Wilson refactored here a little
        //JP Bochi refactord more

        private static void LoadOpCodes()
        {
            m_singleByteOpCodes = new OpCode[0x100];
            m_multiByteOpCodes = new OpCode[0x100];
            FieldInfo[] opCodeFields = typeof(OpCodes).GetFields();
            for (int fieldIndex = 0; fieldIndex < opCodeFields.Length; fieldIndex++) {
                FieldInfo opCodeField = opCodeFields[fieldIndex];
                
                if (opCodeField.FieldType == typeof(OpCode)) {
                    OpCode opCode = (OpCode)opCodeField.GetValue(null);
                    ushort opCodeValue = (ushort)opCode.Value;
                    if (opCodeValue < 0x100) {
                        m_singleByteOpCodes[(int)opCodeValue] = opCode;
                    } else {
                        m_multiByteOpCodes[opCodeValue & 0xff] = opCode;
                    }
                }
            }
        }

        static public IList<ILToken> ReadCode(byte[] code, Module module)
        {
			IList<ILToken> tokens = new List<ILToken>();
			
            int byteIndex = 0;
            while (byteIndex < code.Length)
            {
                OpCode opCode;

                ushort opValue = code[byteIndex++];

                if (opValue != 0xfe) {
                    opCode = m_singleByteOpCodes[(int)opValue];
                } else {
                    opValue = code[byteIndex++];
                    opCode = m_multiByteOpCodes[(int)opValue];
                }

                int metadataToken;
                int startingIndex = byteIndex;

                switch (opCode.OperandType)
                {
                    case OperandType.InlineField:
						metadataToken = ReadInt32(code, ref byteIndex);
						tokens.Add(new ILToken(startingIndex, module.ResolveField(metadataToken).FieldHandle));
						break;
                    case OperandType.InlineMethod:
                        metadataToken = ReadInt32(code, ref byteIndex);
                        tokens.Add(new ILToken(startingIndex, module.ResolveMethod(metadataToken)));
                        break;
                    case OperandType.InlineType:
                        metadataToken = ReadInt32(code, ref byteIndex);
                        tokens.Add(new ILToken(startingIndex, module.ResolveType(metadataToken).TypeHandle));
                        break;
                    case OperandType.InlineString:
                        metadataToken = ReadInt32(code, ref byteIndex);
                        tokens.Add(new ILToken(startingIndex, module.ResolveString(metadataToken)));
                        break;
                    case OperandType.InlineBrTarget:
                        byteIndex = byteIndex + 4;
                        break;
                    case OperandType.InlineSig:
                        byteIndex = byteIndex + 4;
                        break;
                    case OperandType.InlineTok:
                        metadataToken = ReadInt32(code, ref byteIndex);

                        MemberInfo memberInfo = module.ResolveMember(metadataToken);

                        if (memberInfo.MemberType == MemberTypes.TypeInfo || memberInfo.MemberType == MemberTypes.NestedType) {
                            Type type = memberInfo as Type;
                            tokens.Add(new ILToken(startingIndex, type.TypeHandle));
                        } else if (memberInfo.MemberType == MemberTypes.Method || memberInfo.MemberType == MemberTypes.Constructor) {
                            MethodBase m = memberInfo as MethodBase;
                            tokens.Add(new ILToken(startingIndex, m));
                        } else if (memberInfo.MemberType == MemberTypes.Field) {
                            FieldInfo f = memberInfo as FieldInfo;
                            tokens.Add(new ILToken(startingIndex, f.FieldHandle));
                        }

                        break;
                    case OperandType.InlineI:
                        byteIndex = byteIndex + 4;
                        break;
                    case OperandType.InlineI8:
                        byteIndex = byteIndex + 8;
                        break;
                    case OperandType.InlineR:
                        byteIndex = byteIndex + 8;
                        break;
                    case OperandType.InlineSwitch:
                        int count = ReadInt32(code, ref byteIndex);
                        byteIndex = byteIndex + (count * 4);
                        break;
                    case OperandType.InlineVar:
                        byteIndex = byteIndex + 2;
                        break;
                    case OperandType.ShortInlineBrTarget:
                        byteIndex = byteIndex + 1;
                        break;
                    case OperandType.ShortInlineI:
                        byteIndex = byteIndex + 1;
                        break;
                    case OperandType.ShortInlineR:
                        byteIndex = byteIndex + 4;
                        break;
                    case OperandType.ShortInlineVar:
                        byteIndex = byteIndex + 1;
                        break;
                }
            }
            
            return tokens;
        }

		static private int ReadInt32(byte[] code, ref int byteIndex)
		{
			return (((code[byteIndex++] | (code[byteIndex++] << 8)) | (code[byteIndex++] << 0x10)) | (code[byteIndex++] << 0x18));
		}
    }
}
