﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace JpLabs.Linq.LinqToObj
{
	public partial class Lookup<TKey, TElement> : ILookup<TKey, TElement>
	{
		internal class Grouping : IGrouping<TKey, TElement>, IList<TElement>
		{
			// Fields
			internal int count;
			internal TElement[] elements;
			internal int hashCode;
			internal Lookup<TKey, TElement>.Grouping hashNext;
			internal TKey key;
			internal Lookup<TKey, TElement>.Grouping next;

			// Methods
			internal void Add(TElement element)
			{
				if (this.elements.Length == this.count) Array.Resize<TElement>(ref this.elements, this.count * 2);
				
				this.elements[this.count] = element;
				this.count++;
			}

			public IEnumerator<TElement> GetEnumerator()
			{
				for (int i=0; i<count; i++) yield return elements[i];
			}

			void ICollection<TElement>.Add(TElement item)
			{
				throw Error.NotSupported();
			}

			void ICollection<TElement>.Clear()
			{
				throw Error.NotSupported();
			}

			bool ICollection<TElement>.Contains(TElement item)
			{
				return (Array.IndexOf<TElement>(this.elements, item, 0, this.count) >= 0);
			}

			void ICollection<TElement>.CopyTo(TElement[] array, int arrayIndex)
			{
				Array.Copy(this.elements, 0, array, arrayIndex, this.count);
			}

			bool ICollection<TElement>.Remove(TElement item)
			{
				throw Error.NotSupported();
			}

			int IList<TElement>.IndexOf(TElement item)
			{
				return Array.IndexOf<TElement>(this.elements, item, 0, this.count);
			}

			void IList<TElement>.Insert(int index, TElement item)
			{
				throw Error.NotSupported();
			}

			void IList<TElement>.RemoveAt(int index)
			{
				throw Error.NotSupported();
			}

			IEnumerator IEnumerable.GetEnumerator()
			{
				return this.GetEnumerator();
			}

			// Properties
			public TKey Key
			{
				get { return this.key; }
			}

			int ICollection<TElement>.Count
			{
				get { return this.count; }
			}

			bool ICollection<TElement>.IsReadOnly
			{
				get { return true; }
			}

			TElement IList<TElement>.this[int index]
			{
				get {
					if ((index < 0) || (index >= this.count)) throw Error.ArgumentOutOfRange("index");
					
					return this.elements[index];
				}
				
				set { throw Error.NotSupported(); }
			}
		}
	}
}
