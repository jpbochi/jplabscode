﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace JpLabs.Linq.LinqToObj
{
	public partial class Lookup<TKey, TElement> : ILookup<TKey, TElement>
	{
		const int INITIAL_ARRAY_SIZE = 7;

		// Fields
		private IEqualityComparer<TKey> comparer;
		private int count;
		private Grouping[] groupings;
		private Grouping lastGrouping;

		// Methods
		private Lookup(IEqualityComparer<TKey> comparer)
		{
			if (comparer == null)
			{
				comparer = EqualityComparer<TKey>.Default;
			}
			this.comparer = comparer;
			this.groupings = new Grouping[INITIAL_ARRAY_SIZE];
		}

		public IEnumerable<TResult> ApplyResultSelector<TResult>(Func<TKey, IEnumerable<TElement>, TResult> resultSelector)
		{
			var g = lastGrouping;
			if (g != null) {
				do {
					g = g.next;
					if (g.count != g.elements.Length) Array.Resize(ref g.elements, count);
					yield return resultSelector(g.key, g.elements);
				} while (g != lastGrouping);
			}
		}

		public bool Contains(TKey key)
		{
			return (this.GetGrouping(key, false) != null);
		}

		internal static Lookup<TKey, TElement> Create<TSource>(IEnumerable<TSource> source, Func<TSource, TKey> keySelector, Func<TSource, TElement> elementSelector, IEqualityComparer<TKey> comparer)
		{
			if (source == null) throw Error.ArgumentNull("source");
			if (keySelector == null) throw Error.ArgumentNull("keySelector");
			if (elementSelector == null) throw Error.ArgumentNull("elementSelector");

			Lookup<TKey, TElement> lookup = new Lookup<TKey, TElement>(comparer);
			foreach (TSource local in source) {
				lookup.GetGrouping(keySelector(local), true).Add(elementSelector(local));
			}
			return lookup;
		}

		internal static Lookup<TKey, TElement> CreateForJoin(IEnumerable<TElement> source, Func<TElement, TKey> keySelector, IEqualityComparer<TKey> comparer)
		{
			Lookup<TKey, TElement> lookup = new Lookup<TKey, TElement>(comparer);
			foreach (TElement local in source) {
				TKey key = keySelector(local);
				if (key != null) lookup.GetGrouping(key, true).Add(local);
			}
			return lookup;
		}

		public IEnumerator<IGrouping<TKey, TElement>> GetEnumerator()
		{
			var g = lastGrouping;
			if (g != null) {
				do {
					g = g.next;
					yield return g;
				} while (g != lastGrouping);
			}
		}

		internal Grouping GetGrouping(TKey key, bool create)
		{
			int hashCode = this.InternalGetHashCode(key);
			for (Grouping g = this.groupings[hashCode % this.groupings.Length]; g != null; g = g.hashNext) {
				if ((g.hashCode == hashCode) && this.comparer.Equals(g.key, key)) {
					return g;
				}
			}
			
			if (!create) return null;

			if (this.count == this.groupings.Length) this.Resize();
			
			int index = hashCode % this.groupings.Length;
			Grouping newG = new Grouping();
			newG.key = key;
			newG.hashCode = hashCode;
			newG.elements = new TElement[1];
			newG.hashNext = this.groupings[index];
			this.groupings[index] = newG;
			if (this.lastGrouping == null) {
				newG.next = newG;
			} else {
				newG.next = this.lastGrouping.next;
				this.lastGrouping.next = newG;
			}
			this.lastGrouping = newG;
			this.count++;
			return newG;
		}

		internal int InternalGetHashCode(TKey key)
		{
			if (key != null) return (this.comparer.GetHashCode(key) & 0x7fffffff);
			return 0;
		}

		private void Resize()
		{
			int num = (this.count * 2) + 1;
			Grouping[] groupingArray = new Grouping[num];
			Grouping lastGrouping = this.lastGrouping;
			do {
				lastGrouping = lastGrouping.next;
				int index = lastGrouping.hashCode % num;
				lastGrouping.hashNext = groupingArray[index];
				groupingArray[index] = lastGrouping;
			} while (lastGrouping != this.lastGrouping);
			this.groupings = groupingArray;
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return this.GetEnumerator();
		}

		// Properties
		public int Count
		{
			get { return this.count; }
		}

		public IEnumerable<TElement> this[TKey key]
		{
			get
			{
				Grouping grouping = this.GetGrouping(key, false);
				if (grouping != null) return grouping;
				return EmptyEnumerable<TElement>.Instance;
			}
		}
	}
}
