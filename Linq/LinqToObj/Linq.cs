﻿using System;
using System.Collections.Generic;
using System.Text;

namespace System.Runtime.CompilerServices
{
	sealed class ExtensionAttribute : Attribute {}
}

namespace JpLabs.Linq.LinqToObj
{
	public delegate TResult Func<TResult>();
	public delegate TResult Func<T,TResult>(T arg);
	public delegate TResult Func<T1,T2,TResult>(T1 arg1, T2 arg2);
	public delegate TResult Func<T1,T2,T3,TResult>(T1 arg1, T2 arg2, T3 arg3);
	public delegate TResult Func<T1,T2,T3,T4,TResult>(T1 arg1, T2 arg2, T3 arg3, T4 arg4);

	public interface IGrouping<TKey, TElement> : IEnumerable<TElement>
	{
		TKey Key { get; }
	}

	public interface ILookup<TKey, TElement> : IEnumerable<IGrouping<TKey, TElement>>
	{
		bool Contains(TKey key);

		int Count { get; }
		IEnumerable<TElement> this[TKey key] { get; }
	}

	public interface IOrderedEnumerable<TElement> : IEnumerable<TElement>
	{
		IOrderedEnumerable<TElement> CreateOrderedEnumerable<TKey>(Func<TElement, TKey> keySelector, IComparer<TKey> comparer, bool descending);
	}
}
