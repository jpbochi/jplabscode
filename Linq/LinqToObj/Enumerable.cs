﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace JpLabs.Linq.LinqToObj
{
	public static partial class Enumerable
	{
		public static TSource Aggregate<TSource>(this IEnumerable<TSource> source, Func<TSource, TSource, TSource> func)
		{
			if (source == null) throw Error.ArgumentNull("source");
			if (func == null) throw Error.ArgumentNull("func");
			
			using (IEnumerator<TSource> enumerator = source.GetEnumerator())
			{
				if (!enumerator.MoveNext()) throw Error.NoElements();
				TSource current = enumerator.Current;
				while (enumerator.MoveNext()) current = func(current, enumerator.Current);
				return current;
			}
		}

		public static TAccumulate Aggregate<TSource, TAccumulate>(this IEnumerable<TSource> source, TAccumulate seed, Func<TAccumulate, TSource, TAccumulate> func)
		{
			if (source == null) throw Error.ArgumentNull("source");
			if (func == null) throw Error.ArgumentNull("func");
			
			TAccumulate local = seed;
			foreach (TSource local2 in source) local = func(local, local2);
			return local;
		}

		public static TResult Aggregate<TSource, TAccumulate, TResult>(this IEnumerable<TSource> source, TAccumulate seed, Func<TAccumulate, TSource, TAccumulate> func, Func<TAccumulate, TResult> resultSelector)
		{
			if (source == null) throw Error.ArgumentNull("source");
			if (func == null) throw Error.ArgumentNull("func");
			if (resultSelector == null) throw Error.ArgumentNull("resultSelector");
			
			TAccumulate local = seed;
			foreach (TSource local2 in source) local = func(local, local2);
			return resultSelector(local);
		}

		public static bool All<TSource>(this IEnumerable<TSource> source, Func<TSource, bool> predicate)
		{
			if (source == null) throw Error.ArgumentNull("source");
			if (predicate == null) throw Error.ArgumentNull("predicate");
			
			foreach (TSource local in source) if (!predicate(local)) return false;
			return true;
		}

		public static bool Any<TSource>(this IEnumerable<TSource> source)
		{
			if (source == null) throw Error.ArgumentNull("source");
			
			using (IEnumerator<TSource> enumerator = source.GetEnumerator())
			{
				if (enumerator.MoveNext()) return true;
			}
			return false;
		}

		public static bool Any<TSource>(this IEnumerable<TSource> source, Func<TSource, bool> predicate)
		{
			if (source == null) throw Error.ArgumentNull("source");
			if (predicate == null) throw Error.ArgumentNull("predicate");
			
			foreach (TSource local in source) if (predicate(local)) return true;
			return false;
		}

		public static IEnumerable<TSource> AsEnumerable<TSource>(this IEnumerable<TSource> source)
		{
			return source;
		}

		public static decimal? Average(this IEnumerable<decimal?> source)
		{
			if (source == null) throw Error.ArgumentNull("source");
			
			decimal num = 0M;
			long count = 0L;
			foreach (decimal? nullable in source) {
				if (nullable.HasValue) {
					num += nullable.GetValueOrDefault();
					count++;
				}
			}
			if (count > 0L) return new decimal?(num / count);
			return null;
		}

		public static decimal Average(this IEnumerable<decimal> source)
		{
			if (source == null) throw Error.ArgumentNull("source");
			
			decimal num = 0M;
			long count = 0L;
			foreach (decimal value in source) {
				num += value;
				count++;
			}
			if (count <= 0L) throw Error.NoElements();
			return (num / count);
		}

		public static double Average(this IEnumerable<double> source)
		{
			if (source == null) throw Error.ArgumentNull("source");
			
			double num = 0.0;
			long count = 0L;
			foreach (double value in source) {
				num += value;
				count++;
			}
			if (count <= 0L) throw Error.NoElements();
			return (num / ((double) count));
		}

		public static double Average(this IEnumerable<int> source)
		{
			if (source == null) throw Error.ArgumentNull("source");
			
			long num = 0L;
			long count = 0L;
			foreach (int value in source) {
				num += value;
				count++;
			}
			if (count <= 0L) throw Error.NoElements();
			return (((double) num) / ((double) count));
		}

		public static double? Average(this IEnumerable<double?> source)
		{
			if (source == null) throw Error.ArgumentNull("source");
			
			double num = 0.0;
			long count = 0L;
			foreach (double? nullable in source) {
				if (nullable.HasValue) {
					num += nullable.GetValueOrDefault();
					count++;
				}
			}
			if (count > 0L) return new double?(num / ((double) count));
			return null;
		}

		public static double? Average(this IEnumerable<int?> source)
		{
			if (source == null) throw Error.ArgumentNull("source");
			
			long num = 0L;
			long count = 0L;
			foreach (int? nullable in source) {
				if (nullable.HasValue) {
					num += (long) nullable.GetValueOrDefault();
					count++;
				}
			}
			if (count > 0L) return new double?(((double) num) / ((double) count));
			return null;
		}

		public static double? Average(this IEnumerable<long?> source)
		{
			if (source == null) throw Error.ArgumentNull("source");
			
			long num = 0L;
			long count = 0L;
			foreach (long? nullable in source) {
				if (nullable.HasValue) {
					num += nullable.GetValueOrDefault();
					count++;
				}
			}
			if (count > 0L) return new double?(((double) num) / ((double) count));
			return null;
		}

		public static double Average(this IEnumerable<long> source)
		{
			if (source == null) throw Error.ArgumentNull("source");
			
			long num = 0L;
			long count = 0L;
			foreach (long value in source) {
				num += value;
				count++;
			}
			if (count <= 0L) throw Error.NoElements();
			return (((double) num) / ((double) count));
		}

		public static float? Average(this IEnumerable<float?> source)
		{
			if (source == null) throw Error.ArgumentNull("source");
			
			double num = 0.0;
			long count = 0L;
			foreach (float? nullable in source) {
				if (nullable.HasValue) {
					num += (double) nullable.GetValueOrDefault();
					count++;
				}
			}
			if (count > 0L) return new float?((float) (num / ((double) count)));
			return null;
		}

		public static float Average(this IEnumerable<float> source)
		{
			if (source == null) throw Error.ArgumentNull("source");
			
			double num = 0.0;
			long count = 0L;
			foreach (float value in source) {
				num += value;
				count += 1L;
			}
			if (count <= 0L) throw Error.NoElements();
			return (float) (num / ((double) count));
		}

		public static decimal? Average<TSource>(this IEnumerable<TSource> source, Func<TSource, decimal?> selector)
		{
			return source.Select<TSource, decimal?>(selector).Average();
		}

		public static decimal Average<TSource>(this IEnumerable<TSource> source, Func<TSource, decimal> selector)
		{
			return source.Select<TSource, decimal>(selector).Average();
		}

		public static double Average<TSource>(this IEnumerable<TSource> source, Func<TSource, double> selector)
		{
			return source.Select<TSource, double>(selector).Average();
		}

		public static double Average<TSource>(this IEnumerable<TSource> source, Func<TSource, int> selector)
		{
			return source.Select<TSource, int>(selector).Average();
		}

		public static double Average<TSource>(this IEnumerable<TSource> source, Func<TSource, long> selector)
		{
			return source.Select<TSource, long>(selector).Average();
		}

		public static double? Average<TSource>(this IEnumerable<TSource> source, Func<TSource, double?> selector)
		{
			return source.Select<TSource, double?>(selector).Average();
		}

		public static double? Average<TSource>(this IEnumerable<TSource> source, Func<TSource, int?> selector)
		{
			return source.Select<TSource, int?>(selector).Average();
		}

		public static double? Average<TSource>(this IEnumerable<TSource> source, Func<TSource, long?> selector)
		{
			return source.Select<TSource, long?>(selector).Average();
		}

		public static float? Average<TSource>(this IEnumerable<TSource> source, Func<TSource, float?> selector)
		{
			return source.Select<TSource, float?>(selector).Average();
		}

		public static float Average<TSource>(this IEnumerable<TSource> source, Func<TSource, float> selector)
		{
			return source.Select<TSource, float>(selector).Average();
		}

		public static IEnumerable<TResult> Cast<TResult>(this IEnumerable source)
		{
			IEnumerable<TResult> enumerable = source as IEnumerable<TResult>;
			if (enumerable != null) return enumerable;
			if (source == null) throw Error.ArgumentNull("source");
			
			return CastIterator<TResult>(source);
		}

		private static IEnumerable<TResult> CastIterator<TResult>(IEnumerable source)
		{
			foreach (var o in source) yield return (TResult)o;
		}

		private static Func<TSource, bool> CombinePredicates<TSource>(Func<TSource, bool> predicate1, Func<TSource, bool> predicate2)
		{
			return (x => (predicate1(x)) ? predicate2(x) : false);
		}

		private static Func<TSource, TResult> CombineSelectors<TSource, TMiddle, TResult>(Func<TSource, TMiddle> selector1, Func<TMiddle, TResult> selector2)
		{
			return (x => selector2(selector1(x)));
		}

		public static IEnumerable<TSource> Concat<TSource>(this IEnumerable<TSource> first, IEnumerable<TSource> second)
		{
			if (first == null) throw Error.ArgumentNull("first");
			if (second == null) throw Error.ArgumentNull("second");
			
			return ConcatIterator<TSource>(first, second);
		}

		private static IEnumerable<TSource> ConcatIterator<TSource>(IEnumerable<TSource> first, IEnumerable<TSource> second)
		{
			foreach (var o in first) yield return o;
			foreach (var o in second) yield return o;
		}

		public static bool Contains<TSource>(this IEnumerable<TSource> source, TSource value)
		{
			ICollection<TSource> is2 = source as ICollection<TSource>;
			if (is2 != null) return is2.Contains(value);
			
			return source.Contains<TSource>(value, null);
		}

		public static bool Contains<TSource>(this IEnumerable<TSource> source, TSource value, IEqualityComparer<TSource> comparer)
		{
			if (comparer == null) comparer = EqualityComparer<TSource>.Default;
			if (source == null) throw Error.ArgumentNull("source");
			
			foreach (TSource local in source) if (comparer.Equals(local, value)) return true;
			return false;
		}

		public static int Count<TSource>(this IEnumerable<TSource> source)
		{
			if (source == null) throw Error.ArgumentNull("source");
			
			ICollection<TSource> is2 = source as ICollection<TSource>;
			if (is2 != null) return is2.Count;
			
			int num = 0;
			using (IEnumerator<TSource> enumerator = source.GetEnumerator()) {
				while (enumerator.MoveNext()) num++;
			}
			return num;
		}

		public static int Count<TSource>(this IEnumerable<TSource> source, Func<TSource, bool> predicate)
		{
			if (source == null) throw Error.ArgumentNull("source");
			if (predicate == null) throw Error.ArgumentNull("predicate");
			
			int num = 0;
			foreach (TSource local in source) if (predicate(local)) num++;
			return num;
		}

		public static IEnumerable<TSource> DefaultIfEmpty<TSource>(this IEnumerable<TSource> source)
		{
			return source.DefaultIfEmpty<TSource>(default(TSource));
		}

		public static IEnumerable<TSource> DefaultIfEmpty<TSource>(this IEnumerable<TSource> source, TSource defaultValue)
		{
			if (source == null) throw Error.ArgumentNull("source");
			return DefaultIfEmptyIterator<TSource>(source, defaultValue);
		}

		private static IEnumerable<TSource> DefaultIfEmptyIterator<TSource>(IEnumerable<TSource> source, TSource defaultValue)
		{
			using (var e = source.GetEnumerator()) {
				yield return (e.MoveNext()) ? e.Current : defaultValue;
			}
		}

		public static IEnumerable<TSource> Distinct<TSource>(this IEnumerable<TSource> source)
		{
			if (source == null) throw Error.ArgumentNull("source");
			return DistinctIterator<TSource>(source, null);
		}

		public static IEnumerable<TSource> Distinct<TSource>(this IEnumerable<TSource> source, IEqualityComparer<TSource> comparer)
		{
			if (source == null) throw Error.ArgumentNull("source");
			return DistinctIterator<TSource>(source, comparer);
		}

		private static IEnumerable<TSource> DistinctIterator<TSource>(IEnumerable<TSource> source, IEqualityComparer<TSource> comparer)
		{
			var set = new Set<TSource>(comparer);
			foreach (var o in source) if (set.Add(o)) yield return o;
		}

		public static TSource ElementAt<TSource>(this IEnumerable<TSource> source, int index)
		{
			if (source == null) throw Error.ArgumentNull("source");
			
			IList<TSource> list = source as IList<TSource>;
			if (list != null) return list[index];
			
			if (index < 0) throw Error.ArgumentOutOfRange("index");
			
			using (IEnumerator<TSource> enumerator = source.GetEnumerator()) {
				for (; index>=0; index--) {
					if (!enumerator.MoveNext()) throw Error.ArgumentOutOfRange("index");
				}
				return enumerator.Current;
			}
		}

		public static TSource ElementAtOrDefault<TSource>(this IEnumerable<TSource> source, int index)
		{
			if (source == null) throw Error.ArgumentNull("source");
			
			if (index >= 0) {
				IList<TSource> list = source as IList<TSource>;
				if (list != null) {
					if (index < list.Count) return list[index];
				} else {
					using (IEnumerator<TSource> enumerator = source.GetEnumerator()) {
						while (enumerator.MoveNext()) {
							if (index == 0) return enumerator.Current;
							index--;
						}
					}
				}
			}
			return default(TSource);
		}

		public static IEnumerable<TResult> Empty<TResult>()
		{
			return EmptyEnumerable<TResult>.Instance;
		}

		public static IEnumerable<TSource> Except<TSource>(this IEnumerable<TSource> first, IEnumerable<TSource> second)
		{
			if (first == null) throw Error.ArgumentNull("first");
			if (second == null) throw Error.ArgumentNull("second");
			
			return ExceptIterator<TSource>(first, second, null);
		}

		public static IEnumerable<TSource> Except<TSource>(this IEnumerable<TSource> first, IEnumerable<TSource> second, IEqualityComparer<TSource> comparer)
		{
			if (first == null) throw Error.ArgumentNull("first");
			if (second == null) throw Error.ArgumentNull("second");
			
			return ExceptIterator<TSource>(first, second, comparer);
		}

		private static IEnumerable<TSource> ExceptIterator<TSource>(IEnumerable<TSource> first, IEnumerable<TSource> second, IEqualityComparer<TSource> comparer)
		{
			var set = new Set<TSource>(comparer);
			foreach (var o in second) set.Add(o);
			foreach (var o in first) if (set.Add(o)) yield return o;
		}

		public static TSource First<TSource>(this IEnumerable<TSource> source)
		{
			if (source == null) throw Error.ArgumentNull("source");
			
			IList<TSource> list = source as IList<TSource>;
			if (list != null) {
				if (list.Count > 0) return list[0];
			} else {
				using (IEnumerator<TSource> enumerator = source.GetEnumerator()) {
					if (enumerator.MoveNext()) return enumerator.Current;
				}
			}
			throw Error.NoElements();
		}

		public static TSource First<TSource>(this IEnumerable<TSource> source, Func<TSource, bool> predicate)
		{
			if (source == null) throw Error.ArgumentNull("source");
			if (predicate == null) throw Error.ArgumentNull("predicate");
			
			foreach (TSource local in source) if (predicate(local)) return local;
			throw Error.NoMatch();
		}

		public static TSource FirstOrDefault<TSource>(this IEnumerable<TSource> source)
		{
			if (source == null) throw Error.ArgumentNull("source");
			
			IList<TSource> list = source as IList<TSource>;
			if (list != null) {
				if (list.Count > 0) return list[0];
			} else {
				using (IEnumerator<TSource> enumerator = source.GetEnumerator()) {
					if (enumerator.MoveNext()) return enumerator.Current;
				}
			}
			return default(TSource);
		}

		public static TSource FirstOrDefault<TSource>(this IEnumerable<TSource> source, Func<TSource, bool> predicate)
		{
			if (source == null) throw Error.ArgumentNull("source");
			if (predicate == null) throw Error.ArgumentNull("predicate");
			
			foreach (TSource local in source) if (predicate(local)) return local;
			return default(TSource);
		}

		public static IEnumerable<IGrouping<TKey, TSource>> GroupBy<TSource, TKey>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector)
		{
			return new GroupedEnumerable<TSource, TKey, TSource>(source, keySelector, IdentityFunction<TSource>.Instance, null);
		}

		public static IEnumerable<IGrouping<TKey, TSource>> GroupBy<TSource, TKey>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector, IEqualityComparer<TKey> comparer)
		{
			return new GroupedEnumerable<TSource, TKey, TSource>(source, keySelector, IdentityFunction<TSource>.Instance, comparer);
		}

		public static IEnumerable<IGrouping<TKey, TElement>> GroupBy<TSource, TKey, TElement>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector, Func<TSource, TElement> elementSelector)
		{
			return new GroupedEnumerable<TSource, TKey, TElement>(source, keySelector, elementSelector, null);
		}

		public static IEnumerable<TResult> GroupBy<TSource, TKey, TResult>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector, Func<TKey, IEnumerable<TSource>, TResult> resultSelector)
		{
			return new GroupedEnumerable<TSource, TKey, TSource, TResult>(source, keySelector, IdentityFunction<TSource>.Instance, resultSelector, null);
		}

		public static IEnumerable<IGrouping<TKey, TElement>> GroupBy<TSource, TKey, TElement>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector, Func<TSource, TElement> elementSelector, IEqualityComparer<TKey> comparer)
		{
			return new GroupedEnumerable<TSource, TKey, TElement>(source, keySelector, elementSelector, comparer);
		}

		public static IEnumerable<TResult> GroupBy<TSource, TKey, TElement, TResult>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector, Func<TSource, TElement> elementSelector, Func<TKey, IEnumerable<TElement>, TResult> resultSelector)
		{
			return new GroupedEnumerable<TSource, TKey, TElement, TResult>(source, keySelector, elementSelector, resultSelector, null);
		}

		public static IEnumerable<TResult> GroupBy<TSource, TKey, TResult>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector, Func<TKey, IEnumerable<TSource>, TResult> resultSelector, IEqualityComparer<TKey> comparer)
		{
			return new GroupedEnumerable<TSource, TKey, TSource, TResult>(source, keySelector, IdentityFunction<TSource>.Instance, resultSelector, comparer);
		}

		public static IEnumerable<TResult> GroupBy<TSource, TKey, TElement, TResult>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector, Func<TSource, TElement> elementSelector, Func<TKey, IEnumerable<TElement>, TResult> resultSelector, IEqualityComparer<TKey> comparer)
		{
			return new GroupedEnumerable<TSource, TKey, TElement, TResult>(source, keySelector, elementSelector, resultSelector, comparer);
		}

		public static IEnumerable<TResult> GroupJoin<TOuter, TInner, TKey, TResult>(this IEnumerable<TOuter> outer, IEnumerable<TInner> inner, Func<TOuter, TKey> outerKeySelector, Func<TInner, TKey> innerKeySelector, Func<TOuter, IEnumerable<TInner>, TResult> resultSelector)
		{
			if (outer == null) throw Error.ArgumentNull("outer");
			if (inner == null) throw Error.ArgumentNull("inner");
			if (outerKeySelector == null) throw Error.ArgumentNull("outerKeySelector");
			if (innerKeySelector == null) throw Error.ArgumentNull("innerKeySelector");
			if (resultSelector == null) throw Error.ArgumentNull("resultSelector");
			
			return GroupJoinIterator<TOuter, TInner, TKey, TResult>(outer, inner, outerKeySelector, innerKeySelector, resultSelector, null);
		}

		public static IEnumerable<TResult> GroupJoin<TOuter, TInner, TKey, TResult>(this IEnumerable<TOuter> outer, IEnumerable<TInner> inner, Func<TOuter, TKey> outerKeySelector, Func<TInner, TKey> innerKeySelector, Func<TOuter, IEnumerable<TInner>, TResult> resultSelector, IEqualityComparer<TKey> comparer)
		{
			if (outer == null) throw Error.ArgumentNull("outer");
			if (inner == null) throw Error.ArgumentNull("inner");
			if (outerKeySelector == null) throw Error.ArgumentNull("outerKeySelector");
			if (innerKeySelector == null) throw Error.ArgumentNull("innerKeySelector");
			if (resultSelector == null) throw Error.ArgumentNull("resultSelector");
			
			return GroupJoinIterator<TOuter, TInner, TKey, TResult>(outer, inner, outerKeySelector, innerKeySelector, resultSelector, comparer);
		}

		private static IEnumerable<TResult> GroupJoinIterator<TOuter, TInner, TKey, TResult>(IEnumerable<TOuter> outer, IEnumerable<TInner> inner, Func<TOuter, TKey> outerKeySelector, Func<TInner, TKey> innerKeySelector, Func<TOuter, IEnumerable<TInner>, TResult> resultSelector, IEqualityComparer<TKey> comparer)
		{
			var lookup = Lookup<TKey,TInner>.CreateForJoin(inner, innerKeySelector, comparer);
			foreach (var o in outer) yield return resultSelector(o, lookup[outerKeySelector(o)]);
		}

		public static IEnumerable<TSource> Intersect<TSource>(this IEnumerable<TSource> first, IEnumerable<TSource> second)
		{
			if (first == null) throw Error.ArgumentNull("first");
			if (second == null) throw Error.ArgumentNull("second");
			
			return IntersectIterator<TSource>(first, second, null);
		}

		public static IEnumerable<TSource> Intersect<TSource>(this IEnumerable<TSource> first, IEnumerable<TSource> second, IEqualityComparer<TSource> comparer)
		{
			if (first == null) throw Error.ArgumentNull("first");
			if (second == null) throw Error.ArgumentNull("second");
			
			return IntersectIterator<TSource>(first, second, comparer);
		}

		private static IEnumerable<TSource> IntersectIterator<TSource>(IEnumerable<TSource> first, IEnumerable<TSource> second, IEqualityComparer<TSource> comparer)
		{
			var set = new Set<TSource>(comparer);
			foreach (var o in second) set.Add(o);
			foreach (var o in first) if (set.Remove(o)) yield return o;
		}

		public static IEnumerable<TResult> Join<TOuter, TInner, TKey, TResult>(this IEnumerable<TOuter> outer, IEnumerable<TInner> inner, Func<TOuter, TKey> outerKeySelector, Func<TInner, TKey> innerKeySelector, Func<TOuter, TInner, TResult> resultSelector)
		{
			if (outer == null) throw Error.ArgumentNull("outer");
			if (inner == null) throw Error.ArgumentNull("inner");
			if (outerKeySelector == null) throw Error.ArgumentNull("outerKeySelector");
			if (innerKeySelector == null) throw Error.ArgumentNull("innerKeySelector");
			if (resultSelector == null) throw Error.ArgumentNull("resultSelector");
			
			return JoinIterator<TOuter, TInner, TKey, TResult>(outer, inner, outerKeySelector, innerKeySelector, resultSelector, null);
		}

		public static IEnumerable<TResult> Join<TOuter, TInner, TKey, TResult>(this IEnumerable<TOuter> outer, IEnumerable<TInner> inner, Func<TOuter, TKey> outerKeySelector, Func<TInner, TKey> innerKeySelector, Func<TOuter, TInner, TResult> resultSelector, IEqualityComparer<TKey> comparer)
		{
			if (outer == null) throw Error.ArgumentNull("outer");
			if (inner == null) throw Error.ArgumentNull("inner");
			if (outerKeySelector == null) throw Error.ArgumentNull("outerKeySelector");
			if (innerKeySelector == null) throw Error.ArgumentNull("innerKeySelector");
			if (resultSelector == null) throw Error.ArgumentNull("resultSelector");
			
			return JoinIterator<TOuter, TInner, TKey, TResult>(outer, inner, outerKeySelector, innerKeySelector, resultSelector, comparer);
		}

		private static IEnumerable<TResult> JoinIterator<TOuter, TInner, TKey, TResult>(IEnumerable<TOuter> outer, IEnumerable<TInner> inner, Func<TOuter, TKey> outerKeySelector, Func<TInner, TKey> innerKeySelector, Func<TOuter, TInner, TResult> resultSelector, IEqualityComparer<TKey> comparer)
		{
			var lookup = Lookup<TKey,TInner>.CreateForJoin(inner, innerKeySelector, comparer);
			foreach (var o in outer) {
				var g = lookup.GetGrouping(outerKeySelector(o), false);
				if (g != null) {
					for (int i=0; i<g.count; i++) yield return resultSelector(o, g.elements[i]);
				}
			}
		}

		public static TSource Last<TSource>(this IEnumerable<TSource> source)
		{
			if (source == null) throw Error.ArgumentNull("source");
			
			IList<TSource> list = source as IList<TSource>;
			if (list != null) {
				int count = list.Count;
				if (count > 0) return list[count - 1];
			} else {
				using (IEnumerator<TSource> enumerator = source.GetEnumerator()) {
					if (enumerator.MoveNext()) {
						TSource current;
						do current = enumerator.Current; while (enumerator.MoveNext());
						return current;
					}
				}
			}
			throw Error.NoElements();
		}

		public static TSource Last<TSource>(this IEnumerable<TSource> source, Func<TSource, bool> predicate)
		{
			if (source == null) throw Error.ArgumentNull("source");
			if (predicate == null) throw Error.ArgumentNull("predicate");
			
			TSource last = default(TSource);
			bool foundOne = false;
			foreach (TSource value in source) {
				if (predicate(value)) {
					last = value;
					foundOne = true;
				}
			}
			if (!foundOne) throw Error.NoMatch();
			return last;
		}

		public static TSource LastOrDefault<TSource>(this IEnumerable<TSource> source)
		{
			if (source == null) throw Error.ArgumentNull("source");
			
			IList<TSource> list = source as IList<TSource>;
			if (list != null) {
				int count = list.Count;
				if (count > 0) return list[count - 1];
			} else {
				using (IEnumerator<TSource> enumerator = source.GetEnumerator()) {
					if (enumerator.MoveNext()) {
						TSource current;
						do {
							current = enumerator.Current;
						} while (enumerator.MoveNext());
						return current;
					}
				}
			}
			return default(TSource);
		}

		public static TSource LastOrDefault<TSource>(this IEnumerable<TSource> source, Func<TSource, bool> predicate)
		{
			if (source == null) throw Error.ArgumentNull("source");
			if (predicate == null) throw Error.ArgumentNull("predicate");
			
			TSource last = default(TSource);
			foreach (TSource value in source) if (predicate(value)) last = value;
			return last;
		}

		public static long LongCount<TSource>(this IEnumerable<TSource> source)
		{
			if (source == null) throw Error.ArgumentNull("source");
			
			long num = 0L;
			using (IEnumerator<TSource> enumerator = source.GetEnumerator()) {
				while (enumerator.MoveNext()) num++;
			}
			return num;
		}

		public static long LongCount<TSource>(this IEnumerable<TSource> source, Func<TSource, bool> predicate)
		{
			if (source == null) throw Error.ArgumentNull("source");
			if (predicate == null) throw Error.ArgumentNull("predicate");
			
			long count = 0L;
			foreach (TSource local in source) if (predicate(local)) count++;
			return count;
		}

		public static decimal Max(this IEnumerable<decimal> source)
		{
			if (source == null) throw Error.ArgumentNull("source");
			
			decimal max = 0M;
			bool foundOne = false;
			foreach (decimal value in source) {
				if (foundOne) {
					if (value > max) max = value;
				} else {
					max = value;
					foundOne = true;
				}
			}
			if (!foundOne) throw Error.NoElements();
			return max;
		}

		public static decimal? Max(this IEnumerable<decimal?> source)
		{
			if (source == null) throw Error.ArgumentNull("source");
			
			decimal? max = null;
			foreach (decimal? nullable in source) {
				if (nullable.HasValue) {
					if (!max.HasValue || nullable.GetValueOrDefault() > max.GetValueOrDefault()) {
						max = nullable;
					}
				}
			}
			return max;
		}

		public static int Max(this IEnumerable<int> source)
		{
			if (source == null) throw Error.ArgumentNull("source");
			
			int max = 0;
			bool foundOne = false;
			foreach (int value in source) {
				if (foundOne) {
					if (value > max) max = value;
				} else {
					max = value;
					foundOne = true;
				}
			}
			if (!foundOne) throw Error.NoElements();
			return max;
		}

		public static double? Max(this IEnumerable<double?> source)
		{
			if (source == null) throw Error.ArgumentNull("source");
			
			double? max = null;
			foreach (double? nullable in source) {
				if (nullable.HasValue && !double.IsNaN(nullable.GetValueOrDefault())) {
					if (!max.HasValue || nullable.GetValueOrDefault() > max.GetValueOrDefault()) {
						max = nullable;
					}
				}
			}
			return max;
		}

		public static long? Max(this IEnumerable<long?> source)
		{
			if (source == null) throw Error.ArgumentNull("source");
			
			long? max = null;
			foreach (long? nullable in source) {
				if (nullable.HasValue) {
					if (!max.HasValue || nullable.GetValueOrDefault() > max.GetValueOrDefault()) {
						max = nullable;
					}
				}
			}
			return max;
		}

		public static double Max(this IEnumerable<double> source)
		{
			if (source == null) throw Error.ArgumentNull("source");
			
			double max = 0.0;
			bool foundOne = false;
			foreach (double value in source) {
				if (foundOne) {
					if ((value > max) || double.IsNaN(max)) max = value;
				} else {
					max = value;
					foundOne = true;
				}
			}
			if (!foundOne) throw Error.NoElements();
			return max;
		}

		public static int? Max(this IEnumerable<int?> source)
		{
			if (source == null) throw Error.ArgumentNull("source");
			
			int? max = null;
			foreach (int? nullable in source) {
				if (nullable.HasValue) {
					if (!max.HasValue || nullable.GetValueOrDefault() > max.GetValueOrDefault()) {
						max = nullable;
					}
				}
			}
			return max;
		}

		public static long Max(this IEnumerable<long> source)
		{
			if (source == null) throw Error.ArgumentNull("source");
			
			long max = 0L;
			bool foundOne = false;
			foreach (long value in source) {
				if (foundOne) {
					if (value > max) max = value;
				} else {
					max = value;
					foundOne = true;
				}
			}
			if (!foundOne) throw Error.NoElements();
			return max;
		}

		public static float? Max(this IEnumerable<float?> source)
		{
			if (source == null) throw Error.ArgumentNull("source");
			
			float? max = null;
			foreach (float? nullable in source) {
				if (nullable.HasValue && !float.IsNaN(nullable.GetValueOrDefault())) {
					if (!max.HasValue || nullable.GetValueOrDefault() > max.GetValueOrDefault()) {
						max = nullable;
					}
				}
			}
			return max;
		}

		public static float Max(this IEnumerable<float> source)
		{
			if (source == null) throw Error.ArgumentNull("source");
			
			float max = 0f;
			bool foundOne = false;
			foreach (float value in source) {
				if (foundOne) {
					if ((value > max) || float.IsNaN(max)) max = value;
				} else {
					max = value;
					foundOne = true;
				}
			}
			if (!foundOne) throw Error.NoElements();
			return max;
		}

		public static TSource Max<TSource>(this IEnumerable<TSource> source)
		{
			if (source == null) throw Error.ArgumentNull("source");
			
			Comparer<TSource> comparer = Comparer<TSource>.Default;
			TSource max = default(TSource);
			if (max == null) {
				foreach (TSource obj in source) {
					if ((obj != null) && ((max == null) || (comparer.Compare(obj, max) > 0))) {
						max = obj;
					}
				}
				return max;
			} else {
				bool foundOne = false;
				foreach (TSource value in source) {
					if (foundOne) {
						if (comparer.Compare(value, max) > 0) max = value;
					} else {
						max = value;
						foundOne = true;
					}
				}
				if (!foundOne) throw Error.NoElements();
				return max;
			}
		}

		public static decimal Max<TSource>(this IEnumerable<TSource> source, Func<TSource, decimal> selector)
		{
			return source.Select<TSource, decimal>(selector).Max();
		}

		public static double Max<TSource>(this IEnumerable<TSource> source, Func<TSource, double> selector)
		{
			return source.Select<TSource, double>(selector).Max();
		}

		public static int Max<TSource>(this IEnumerable<TSource> source, Func<TSource, int> selector)
		{
			return source.Select<TSource, int>(selector).Max();
		}

		public static long Max<TSource>(this IEnumerable<TSource> source, Func<TSource, long> selector)
		{
			return source.Select<TSource, long>(selector).Max();
		}

		public static long? Max<TSource>(this IEnumerable<TSource> source, Func<TSource, long?> selector)
		{
			return source.Select<TSource, long?>(selector).Max();
		}

		public static decimal? Max<TSource>(this IEnumerable<TSource> source, Func<TSource, decimal?> selector)
		{
			return source.Select<TSource, decimal?>(selector).Max();
		}

		public static double? Max<TSource>(this IEnumerable<TSource> source, Func<TSource, double?> selector)
		{
			return source.Select<TSource, double?>(selector).Max();
		}

		public static int? Max<TSource>(this IEnumerable<TSource> source, Func<TSource, int?> selector)
		{
			return source.Select<TSource, int?>(selector).Max();
		}

		public static TResult Max<TSource, TResult>(this IEnumerable<TSource> source, Func<TSource, TResult> selector)
		{
			return source.Select<TSource, TResult>(selector).Max<TResult>();
		}

		public static float? Max<TSource>(this IEnumerable<TSource> source, Func<TSource, float?> selector)
		{
			return source.Select<TSource, float?>(selector).Max();
		}

		public static float Max<TSource>(this IEnumerable<TSource> source, Func<TSource, float> selector)
		{
			return source.Select<TSource, float>(selector).Max();
		}

		public static decimal Min(this IEnumerable<decimal> source)
		{
			if (source == null) throw Error.ArgumentNull("source");
			
			decimal min = 0M;
			bool foundOne = false;
			foreach (decimal value in source) {
				if (foundOne) {
					if (value < min) min = value;
				} else {
					min = value;
					foundOne = true;
				}
			}
			if (!foundOne) throw Error.NoElements();
			return min;
		}

		public static double Min(this IEnumerable<double> source)
		{
			if (source == null) throw Error.ArgumentNull("source");
			
			double min = 0.0;
			bool foundOne = false;
			foreach (double value in source) {
				if (foundOne) {
					if ((value < min) || double.IsNaN(value)) min = value;
				} else {
					min = value;
					foundOne = true;
				}
			}
			if (!foundOne) throw Error.NoElements();
			return min;
		}

		public static int Min(this IEnumerable<int> source)
		{
			if (source == null) throw Error.ArgumentNull("source");
			
			int min = 0;
			bool foundOne = false;
			foreach (int value in source) {
				if (foundOne) {
					if (value < min) min = value;
				} else {
					min = value;
					foundOne = true;
				}
			}
			if (!foundOne) throw Error.NoElements();
			return min;
		}

		public static decimal? Min(this IEnumerable<decimal?> source)
		{
			if (source == null)throw Error.ArgumentNull("source");
			
			decimal? min = null;
			foreach (decimal? nullable in source) {
				if (nullable.HasValue) {
					if (!min.HasValue || nullable.GetValueOrDefault() < min.GetValueOrDefault()) {
						min = nullable;
					}
				}
			}
			return min;
		}

		public static TSource Min<TSource>(this IEnumerable<TSource> source)
		{
			if (source == null) throw Error.ArgumentNull("source");
			
			Comparer<TSource> comparer = Comparer<TSource>.Default;
			TSource min = default(TSource);
			if (min == null) {
				foreach (TSource value in source) {
					if ((value != null) && ((min == null) || (comparer.Compare(value, min) < 0))) {
						min = value;
					}
				}
				return min;
			} else {
				bool foundOne = false;
				foreach (TSource value in source) {
					if (foundOne) {
						if (comparer.Compare(value, min) < 0) min = value;
					} else {
						min = value;
						foundOne = true;
					}
				}
				if (!foundOne) throw Error.NoElements();
				return min;
			}
		}

		public static double? Min(this IEnumerable<double?> source)
		{
			if (source == null) throw Error.ArgumentNull("source");
			
			double? min = null;
			foreach (double? nullable in source) {
				if (nullable.HasValue && !double.IsNaN(nullable.Value)) {
					if (!min.HasValue || nullable.GetValueOrDefault() < min.GetValueOrDefault()) {
						min = nullable;
					}
				}
			}
			return min;
		}

		public static int? Min(this IEnumerable<int?> source)
		{
			if (source == null) throw Error.ArgumentNull("source");
			
			int? min = null;
			foreach (int? nullable in source) {
				if (nullable.HasValue) {
					if (!min.HasValue || nullable.GetValueOrDefault() < min.GetValueOrDefault()) {
						min = nullable;
					}
				}
			}
			return min;
		}

		public static long Min(this IEnumerable<long> source)
		{
			if (source == null) throw Error.ArgumentNull("source");
			
			long min = 0L;
			bool foundOne = false;
			foreach (long value in source) {
				if (foundOne) {
					if (value < min) min = value;
				} else {
					min = value;
					foundOne = true;
				}
			}
			if (!foundOne) throw Error.NoElements();
			return min;
		}

		public static long? Min(this IEnumerable<long?> source)
		{
			if (source == null) throw Error.ArgumentNull("source");
			
			long? min = null;
			foreach (long? nullable in source) {
				if (nullable.HasValue) {
					if (!min.HasValue || nullable.GetValueOrDefault() < min.GetValueOrDefault()) {
						min = nullable;
					}
				}
			}
			return min;
		}

		public static float? Min(this IEnumerable<float?> source)
		{
			if (source == null) throw Error.ArgumentNull("source");
			
			float? min = null;
			foreach (float? nullable in source) {
				if (nullable.HasValue && !float.IsNaN(nullable.Value)) {
					if (!min.HasValue || nullable.GetValueOrDefault() < min.GetValueOrDefault()) {
						min = nullable;
					}
				}
			}
			return min;
		}

		public static float Min(this IEnumerable<float> source)
		{
			if (source == null) throw Error.ArgumentNull("source");
			
			float min = 0f;
			bool foundOne = false;
			foreach (float value in source) {
                if (foundOne)
                {
                    if ((value < min) || float.IsNaN(value)) min = value;
                } else {
                    min = value;
                    foundOne = true;
                }
			}
			if (!foundOne) throw Error.NoElements();
			return min;
		}

		public static double? Min<TSource>(this IEnumerable<TSource> source, Func<TSource, double?> selector)
		{
			return source.Select<TSource, double?>(selector).Min();
		}

		public static int? Min<TSource>(this IEnumerable<TSource> source, Func<TSource, int?> selector)
		{
			return source.Select<TSource, int?>(selector).Min();
		}

		public static decimal Min<TSource>(this IEnumerable<TSource> source, Func<TSource, decimal> selector)
		{
			return source.Select<TSource, decimal>(selector).Min();
		}

		public static decimal? Min<TSource>(this IEnumerable<TSource> source, Func<TSource, decimal?> selector)
		{
			return source.Select<TSource, decimal?>(selector).Min();
		}

		public static double Min<TSource>(this IEnumerable<TSource> source, Func<TSource, double> selector)
		{
			return source.Select<TSource, double>(selector).Min();
		}

		public static TResult Min<TSource, TResult>(this IEnumerable<TSource> source, Func<TSource, TResult> selector)
		{
			return source.Select<TSource, TResult>(selector).Min<TResult>();
		}

		public static long? Min<TSource>(this IEnumerable<TSource> source, Func<TSource, long?> selector)
		{
			return source.Select<TSource, long?>(selector).Min();
		}

		public static int Min<TSource>(this IEnumerable<TSource> source, Func<TSource, int> selector)
		{
			return source.Select<TSource, int>(selector).Min();
		}

		public static float? Min<TSource>(this IEnumerable<TSource> source, Func<TSource, float?> selector)
		{
			return source.Select<TSource, float?>(selector).Min();
		}

		public static long Min<TSource>(this IEnumerable<TSource> source, Func<TSource, long> selector)
		{
			return source.Select<TSource, long>(selector).Min();
		}

		public static float Min<TSource>(this IEnumerable<TSource> source, Func<TSource, float> selector)
		{
			return source.Select<TSource, float>(selector).Min();
		}

		public static IEnumerable<TResult> OfType<TResult>(this IEnumerable source)
		{
			if (source == null) throw Error.ArgumentNull("source");

			return OfTypeIterator<TResult>(source);
		}

		private static IEnumerable<TResult> OfTypeIterator<TResult>(IEnumerable source)
		{
			foreach (var o in source) if (o is TResult) yield return (TResult)o;
		}

		public static IOrderedEnumerable<TSource> OrderBy<TSource, TKey>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector)
		{
			return new OrderedEnumerable<TSource, TKey>(source, keySelector, null, false);
		}

		public static IOrderedEnumerable<TSource> OrderBy<TSource, TKey>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector, IComparer<TKey> comparer)
		{
			return new OrderedEnumerable<TSource, TKey>(source, keySelector, comparer, false);
		}

		public static IOrderedEnumerable<TSource> OrderByDescending<TSource, TKey>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector)
		{
			return new OrderedEnumerable<TSource, TKey>(source, keySelector, null, true);
		}

		public static IOrderedEnumerable<TSource> OrderByDescending<TSource, TKey>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector, IComparer<TKey> comparer)
		{
			return new OrderedEnumerable<TSource, TKey>(source, keySelector, comparer, true);
		}

		public static IEnumerable<int> Range(int start, int count)
		{
			long num = (start + count) - 1L;
			if ((count < 0) || (num > 0x7fffffffL)) throw Error.ArgumentOutOfRange("count");

			return RangeIterator(start, count);
		}

		private static IEnumerable<int> RangeIterator(int start, int count)
		{
			int end = start + count;
			for (int i=start; i<end; i++) yield return i;
		}

		public static IEnumerable<TResult> Repeat<TResult>(TResult element, int count)
		{
			if (count < 0) throw Error.ArgumentOutOfRange("count");

			return RepeatIterator<TResult>(element, count);
		}

		private static IEnumerable<TResult> RepeatIterator<TResult>(TResult element, int count)
		{
			for (int i=0; i<count; i++) yield return element;
		}

		public static IEnumerable<TSource> Reverse<TSource>(this IEnumerable<TSource> source)
		{
			if (source == null) throw Error.ArgumentNull("source");

			return ReverseIterator<TSource>(source);
		}

		private static IEnumerable<TSource> ReverseIterator<TSource>(IEnumerable<TSource> source)
		{
			var buffer = new Buffer<TSource>();
			for (int i = buffer.count - 1; i>=0; i--) yield return buffer.items[i];
		}

		public static IEnumerable<TResult> Select<TSource, TResult>(this IEnumerable<TSource> source, Func<TSource, TResult> selector)
		{
			if (source == null) throw Error.ArgumentNull("source");
			if (selector == null) throw Error.ArgumentNull("selector");

			if (source is Iterator<TSource>) return ((Iterator<TSource>) source).Select<TResult>(selector);
			if (source is TSource[]) return new WhereSelectArrayIterator<TSource, TResult>((TSource[]) source, null, selector);
			if (source is List<TSource>) return new WhereSelectListIterator<TSource, TResult>((List<TSource>) source, null, selector);

			return new WhereSelectEnumerableIterator<TSource, TResult>(source, null, selector);
		}

		public static IEnumerable<TResult> Select<TSource, TResult>(this IEnumerable<TSource> source, Func<TSource, int, TResult> selector)
		{
			if (source == null) throw Error.ArgumentNull("source");
			if (selector == null) throw Error.ArgumentNull("selector");

			return SelectIterator<TSource, TResult>(source, selector);
		}

		private static IEnumerable<TResult> SelectIterator<TSource, TResult>(IEnumerable<TSource> source, Func<TSource, int, TResult> selector)
		{
			int i = 0;
			foreach (var o in source) yield return selector(o, i++);
		}

		public static IEnumerable<TResult> SelectMany<TSource, TResult>(this IEnumerable<TSource> source, Func<TSource, IEnumerable<TResult>> selector)
		{
			if (source == null) throw Error.ArgumentNull("source");
			if (selector == null) throw Error.ArgumentNull("selector");

			return SelectManyIterator<TSource, TResult>(source, selector);
		}

		public static IEnumerable<TResult> SelectMany<TSource, TResult>(this IEnumerable<TSource> source, Func<TSource, int, IEnumerable<TResult>> selector)
		{
			if (source == null) throw Error.ArgumentNull("source");
			if (selector == null) throw Error.ArgumentNull("selector");

			return SelectManyIterator<TSource, TResult>(source, selector);
		}

		public static IEnumerable<TResult> SelectMany<TSource, TCollection, TResult>(this IEnumerable<TSource> source, Func<TSource, IEnumerable<TCollection>> collectionSelector, Func<TSource, TCollection, TResult> resultSelector)
		{
			if (source == null) throw Error.ArgumentNull("source");
			if (collectionSelector == null) throw Error.ArgumentNull("collectionSelector");
			if (resultSelector == null) throw Error.ArgumentNull("resultSelector");

			return SelectManyIterator<TSource, TCollection, TResult>(source, collectionSelector, resultSelector);
		}

		public static IEnumerable<TResult> SelectMany<TSource, TCollection, TResult>(this IEnumerable<TSource> source, Func<TSource, int, IEnumerable<TCollection>> collectionSelector, Func<TSource, TCollection, TResult> resultSelector)
		{
			if (source == null) throw Error.ArgumentNull("source");
			if (collectionSelector == null) throw Error.ArgumentNull("collectionSelector");
			if (resultSelector == null) throw Error.ArgumentNull("resultSelector");

			return SelectManyIterator<TSource, TCollection, TResult>(source, collectionSelector, resultSelector);
		}

		private static IEnumerable<TResult> SelectManyIterator<TSource, TResult>(IEnumerable<TSource> source, Func<TSource, IEnumerable<TResult>> selector)
		{
			foreach (var s in source)
			foreach (var o in selector(s)) yield return o;
		}

		private static IEnumerable<TResult> SelectManyIterator<TSource, TResult>(IEnumerable<TSource> source, Func<TSource, int, IEnumerable<TResult>> selector)
		{
			int i = 0;
			foreach (var s in source)
			foreach (var o in selector(s, i++)) yield return o;
		}

		private static IEnumerable<TResult> SelectManyIterator<TSource, TCollection, TResult>(IEnumerable<TSource> source, Func<TSource, IEnumerable<TCollection>> collectionSelector, Func<TSource, TCollection, TResult> resultSelector)
		{
			foreach (var s in source)
			foreach (var o in collectionSelector(s)) yield return resultSelector(s, o);
		}

		private static IEnumerable<TResult> SelectManyIterator<TSource, TCollection, TResult>(IEnumerable<TSource> source, Func<TSource, int, IEnumerable<TCollection>> collectionSelector, Func<TSource, TCollection, TResult> resultSelector)
		{
			int i = 0;
			foreach (var s in source)
			foreach (var o in collectionSelector(s, i++)) yield return resultSelector(s, o);
		}

		public static bool SequenceEqual<TSource>(this IEnumerable<TSource> first, IEnumerable<TSource> second)
		{
			return first.SequenceEqual<TSource>(second, null);
		}

		public static bool SequenceEqual<TSource>(this IEnumerable<TSource> first, IEnumerable<TSource> second, IEqualityComparer<TSource> comparer)
		{
			if (comparer == null) comparer = EqualityComparer<TSource>.Default;

			if (first == null) throw Error.ArgumentNull("first");
			if (second == null) throw Error.ArgumentNull("second");

			using (IEnumerator<TSource> eFirst = first.GetEnumerator()) {
				using (IEnumerator<TSource> eSecond = second.GetEnumerator()) {
					while (eFirst.MoveNext()) {
						if (!eSecond.MoveNext() || !comparer.Equals(eFirst.Current, eSecond.Current)) {
							return false;
						}
					}
					if (eSecond.MoveNext()) return false;
				}
			}
			return true;
		}

		public static TSource Single<TSource>(this IEnumerable<TSource> source)
		{
			if (source == null) throw Error.ArgumentNull("source");

			IList<TSource> list = source as IList<TSource>;
			if (list != null) {
				switch (list.Count) {
					case 0: throw Error.NoElements();
					case 1: return list[0];
				}
			} else {
				using (IEnumerator<TSource> enumerator = source.GetEnumerator()) {
					if (!enumerator.MoveNext()) throw Error.NoElements();

					TSource first = enumerator.Current;
					if (!enumerator.MoveNext()) return first;
				}
			}
			throw Error.MoreThanOneElement();
		}

		public static TSource Single<TSource>(this IEnumerable<TSource> source, Func<TSource, bool> predicate)
		{
			if (source == null) throw Error.ArgumentNull("source");
			if (predicate == null) throw Error.ArgumentNull("predicate");

			TSource single = default(TSource);
			long count = 0L;
			foreach (TSource value in source) {
				if (predicate(value)) {
					single = value;
					count++;
				}
			}
			switch (count) {
				case 0: throw Error.NoMatch();
			    case 1: return single;
                default: throw Error.MoreThanOneMatch();
			}
		}

		public static TSource SingleOrDefault<TSource>(this IEnumerable<TSource> source)
		{
			if (source == null) throw Error.ArgumentNull("source");

			IList<TSource> list = source as IList<TSource>;
			if (list != null) {
				switch (list.Count) {
					case 0: return default(TSource);
					case 1: return list[0];
				}
			} else {
				using (IEnumerator<TSource> enumerator = source.GetEnumerator()) {
					if (!enumerator.MoveNext()) return default(TSource);

					TSource first = enumerator.Current;
					if (!enumerator.MoveNext()) return first;
				}
			}
			throw Error.MoreThanOneElement();
		}

		public static TSource SingleOrDefault<TSource>(this IEnumerable<TSource> source, Func<TSource, bool> predicate)
		{
			if (source == null) throw Error.ArgumentNull("source");
			if (predicate == null) throw Error.ArgumentNull("predicate");

			TSource single = default(TSource);
			long count = 0L;
			foreach (TSource local2 in source) {
				if (predicate(local2)) {
					single = local2;
					count++;
				}
			}
			switch (count) {
				case 0: return default(TSource);
				case 1: return single;
                default: throw Error.MoreThanOneMatch();
			}
		}

		public static IEnumerable<TSource> Skip<TSource>(this IEnumerable<TSource> source, int count)
		{
			if (source == null) throw Error.ArgumentNull("source");

			return SkipIterator<TSource>(source, count);
		}

		private static IEnumerable<TSource> SkipIterator<TSource>(IEnumerable<TSource> source, int count)
		{
			var e = source.GetEnumerator();
			while (count > 0 && e.MoveNext()) count--;
			if (count <= 0) while (e.MoveNext()) yield return e.Current;
		}

		public static IEnumerable<TSource> SkipWhile<TSource>(this IEnumerable<TSource> source, Func<TSource, bool> predicate)
		{
			if (source == null) throw Error.ArgumentNull("source");
			if (predicate == null) throw Error.ArgumentNull("predicate");

			return SkipWhileIterator<TSource>(source, predicate);
		}

		public static IEnumerable<TSource> SkipWhile<TSource>(this IEnumerable<TSource> source, Func<TSource, int, bool> predicate)
		{
			if (source == null) throw Error.ArgumentNull("source");
			if (predicate == null) throw Error.ArgumentNull("predicate");

			return SkipWhileIterator<TSource>(source, predicate);
		}

		private static IEnumerable<TSource> SkipWhileIterator<TSource>(IEnumerable<TSource> source, Func<TSource, bool> predicate)
		{
			bool yielding = false;
			foreach (var o in source) {
				if (predicate(o)) yielding = true;
				if (yielding) yield return o;
			}
		}

		private static IEnumerable<TSource> SkipWhileIterator<TSource>(IEnumerable<TSource> source, Func<TSource, int, bool> predicate)
		{
			int i = 0;
			bool yielding = false;
			foreach (var o in source) {
				if (predicate(o, i++)) yielding = true;
				if (yielding) yield return o;
			}
		}

		public static decimal Sum(this IEnumerable<decimal> source)
		{
			if (source == null) throw Error.ArgumentNull("source");

			decimal sum = 0M;
			foreach (decimal value in source) sum += value;
			return sum;
		}

		public static double Sum(this IEnumerable<double> source)
		{
			if (source == null) throw Error.ArgumentNull("source");

			double sum = 0.0;
			foreach (double value in source) sum += value;
			return sum;
		}

		public static int Sum(this IEnumerable<int> source)
		{
			if (source == null) throw Error.ArgumentNull("source");

			int sum = 0;
			foreach (int value in source) sum += value;
			return sum;
		}

		public static long Sum(this IEnumerable<long> source)
		{
			if (source == null) throw Error.ArgumentNull("source");

			long sum = 0L;
			foreach (long value in source) sum += value;
			return sum;
		}

		public static decimal? Sum(this IEnumerable<decimal?> source)
		{
			if (source == null) throw Error.ArgumentNull("source");

			decimal sum = 0M;
			foreach (decimal? nullable in source) {
				if (nullable.HasValue) sum += nullable.GetValueOrDefault();
			}
			return new decimal?(sum);
		}

		public static double? Sum(this IEnumerable<double?> source)
		{
			if (source == null) throw Error.ArgumentNull("source");

			double sum = 0.0;
			foreach (double? nullable in source) {
				if (nullable.HasValue) sum += nullable.GetValueOrDefault();
			}
			return new double?(sum);
		}

		public static int? Sum(this IEnumerable<int?> source)
		{
			if (source == null) throw Error.ArgumentNull("source");

			int sum = 0;
			foreach (int? nullable in source) {
				if (nullable.HasValue) sum += nullable.GetValueOrDefault();
			}
			return new int?(sum);
		}

		public static long? Sum(this IEnumerable<long?> source)
		{
			if (source == null) throw Error.ArgumentNull("source");

			long sum = 0L;
			foreach (long? nullable in source) {
				if (nullable.HasValue) sum += nullable.GetValueOrDefault();
			}
			return new long?(sum);
		}

		public static float? Sum(this IEnumerable<float?> source)
		{
			if (source == null) throw Error.ArgumentNull("source");

			double num = 0.0;
			foreach (float? nullable in source) {
				if (nullable.HasValue) num += (double) nullable.GetValueOrDefault();
			}
			return new float?((float) num);
		}

		public static float Sum(this IEnumerable<float> source)
		{
			if (source == null) throw Error.ArgumentNull("source");

			double sum = 0.0;
			foreach (float value in source) sum += value;
			return (float) sum;
		}

		public static decimal? Sum<TSource>(this IEnumerable<TSource> source, Func<TSource, decimal?> selector)
		{
			return source.Select<TSource, decimal?>(selector).Sum();
		}

		public static decimal Sum<TSource>(this IEnumerable<TSource> source, Func<TSource, decimal> selector)
		{
			return source.Select<TSource, decimal>(selector).Sum();
		}

		public static double? Sum<TSource>(this IEnumerable<TSource> source, Func<TSource, double?> selector)
		{
			return source.Select<TSource, double?>(selector).Sum();
		}

		public static double Sum<TSource>(this IEnumerable<TSource> source, Func<TSource, double> selector)
		{
			return source.Select<TSource, double>(selector).Sum();
		}

		public static int? Sum<TSource>(this IEnumerable<TSource> source, Func<TSource, int?> selector)
		{
			return source.Select<TSource, int?>(selector).Sum();
		}

		public static int Sum<TSource>(this IEnumerable<TSource> source, Func<TSource, int> selector)
		{
			return source.Select<TSource, int>(selector).Sum();
		}

		public static long Sum<TSource>(this IEnumerable<TSource> source, Func<TSource, long> selector)
		{
			return source.Select<TSource, long>(selector).Sum();
		}

		public static long? Sum<TSource>(this IEnumerable<TSource> source, Func<TSource, long?> selector)
		{
			return source.Select<TSource, long?>(selector).Sum();
		}

		public static float? Sum<TSource>(this IEnumerable<TSource> source, Func<TSource, float?> selector)
		{
			return source.Select<TSource, float?>(selector).Sum();
		}

		public static float Sum<TSource>(this IEnumerable<TSource> source, Func<TSource, float> selector)
		{
			return source.Select<TSource, float>(selector).Sum();
		}

		public static IEnumerable<TSource> Take<TSource>(this IEnumerable<TSource> source, int count)
		{
			if (source == null) throw Error.ArgumentNull("source");

			return TakeIterator<TSource>(source, count);
		}

		private static IEnumerable<TSource> TakeIterator<TSource>(IEnumerable<TSource> source, int count)
		{
			if (count > 0) {
				foreach (var o in source) {
					yield return o;
					if (--count == 0) break;
				}
			}
		}

		public static IEnumerable<TSource> TakeWhile<TSource>(this IEnumerable<TSource> source, Func<TSource, bool> predicate)
		{
			if (source == null) throw Error.ArgumentNull("source");
			if (predicate == null) throw Error.ArgumentNull("predicate");

			return TakeWhileIterator<TSource>(source, predicate);
		}

		public static IEnumerable<TSource> TakeWhile<TSource>(this IEnumerable<TSource> source, Func<TSource, int, bool> predicate)
		{
			if (source == null) throw Error.ArgumentNull("source");
			if (predicate == null) throw Error.ArgumentNull("predicate");

			return TakeWhileIterator<TSource>(source, predicate);
		}

		private static IEnumerable<TSource> TakeWhileIterator<TSource>(IEnumerable<TSource> source, Func<TSource, bool> predicate)
		{
			foreach (var o in source) {
				if (!predicate(o)) break;
				yield return o;
			}
		}

		private static IEnumerable<TSource> TakeWhileIterator<TSource>(IEnumerable<TSource> source, Func<TSource, int, bool> predicate)
		{
			int i = 0;
			foreach (var o in source) {
				if (!predicate(o, i++)) break;
				yield return o;
			}
		}

		public static IOrderedEnumerable<TSource> ThenBy<TSource, TKey>(this IOrderedEnumerable<TSource> source, Func<TSource, TKey> keySelector)
		{
			if (source == null) throw Error.ArgumentNull("source");
			return source.CreateOrderedEnumerable<TKey>(keySelector, null, false);
		}

		public static IOrderedEnumerable<TSource> ThenBy<TSource, TKey>(this IOrderedEnumerable<TSource> source, Func<TSource, TKey> keySelector, IComparer<TKey> comparer)
		{
			if (source == null) throw Error.ArgumentNull("source");

			return source.CreateOrderedEnumerable<TKey>(keySelector, comparer, false);
		}

		public static IOrderedEnumerable<TSource> ThenByDescending<TSource, TKey>(this IOrderedEnumerable<TSource> source, Func<TSource, TKey> keySelector)
		{
			if (source == null) throw Error.ArgumentNull("source");

			return source.CreateOrderedEnumerable<TKey>(keySelector, null, true);
		}

		public static IOrderedEnumerable<TSource> ThenByDescending<TSource, TKey>(this IOrderedEnumerable<TSource> source, Func<TSource, TKey> keySelector, IComparer<TKey> comparer)
		{
			if (source == null) throw Error.ArgumentNull("source");

			return source.CreateOrderedEnumerable<TKey>(keySelector, comparer, true);
		}

		public static TSource[] ToArray<TSource>(this IEnumerable<TSource> source)
		{
			if (source == null) throw Error.ArgumentNull("source");

			Buffer<TSource> buffer = new Buffer<TSource>(source);
			return buffer.ToArray();
		}

		public static Dictionary<TKey, TSource> ToDictionary<TSource, TKey>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector)
		{
			return source.ToDictionary<TSource, TKey, TSource>(keySelector, IdentityFunction<TSource>.Instance, null);
		}

		public static Dictionary<TKey, TSource> ToDictionary<TSource, TKey>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector, IEqualityComparer<TKey> comparer)
		{
			return source.ToDictionary<TSource, TKey, TSource>(keySelector, IdentityFunction<TSource>.Instance, comparer);
		}

		public static Dictionary<TKey, TElement> ToDictionary<TSource, TKey, TElement>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector, Func<TSource, TElement> elementSelector)
		{
			return source.ToDictionary<TSource, TKey, TElement>(keySelector, elementSelector, null);
		}

		public static Dictionary<TKey, TElement> ToDictionary<TSource, TKey, TElement>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector, Func<TSource, TElement> elementSelector, IEqualityComparer<TKey> comparer)
		{
			if (source == null) throw Error.ArgumentNull("source");
			if (keySelector == null) throw Error.ArgumentNull("keySelector");
			if (elementSelector == null) throw Error.ArgumentNull("elementSelector");

			Dictionary<TKey, TElement> dictionary = new Dictionary<TKey, TElement>(comparer);
			foreach (TSource local in source) dictionary.Add(keySelector(local), elementSelector(local));
			return dictionary;
		}

		public static List<TSource> ToList<TSource>(this IEnumerable<TSource> source)
		{
			if (source == null) throw Error.ArgumentNull("source");

			return new List<TSource>(source);
		}

		public static ILookup<TKey, TSource> ToLookup<TSource, TKey>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector)
		{
			return (ILookup<TKey, TSource>) Lookup<TKey, TSource>.Create<TSource>(source, keySelector, IdentityFunction<TSource>.Instance, null);
		}

		public static ILookup<TKey, TSource> ToLookup<TSource, TKey>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector, IEqualityComparer<TKey> comparer)
		{
			return (ILookup<TKey, TSource>) Lookup<TKey, TSource>.Create<TSource>(source, keySelector, IdentityFunction<TSource>.Instance, comparer);
		}

		public static ILookup<TKey, TElement> ToLookup<TSource, TKey, TElement>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector, Func<TSource, TElement> elementSelector)
		{
			return (ILookup<TKey, TElement>) Lookup<TKey, TElement>.Create<TSource>(source, keySelector, elementSelector, null);
		}

		public static ILookup<TKey, TElement> ToLookup<TSource, TKey, TElement>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector, Func<TSource, TElement> elementSelector, IEqualityComparer<TKey> comparer)
		{
			return (ILookup<TKey, TElement>) Lookup<TKey, TElement>.Create<TSource>(source, keySelector, elementSelector, comparer);
		}

		public static IEnumerable<TSource> Union<TSource>(this IEnumerable<TSource> first, IEnumerable<TSource> second)
		{
			if (first == null) throw Error.ArgumentNull("first");
			if (second == null) throw Error.ArgumentNull("second");

			return UnionIterator<TSource>(first, second, null);
		}

		public static IEnumerable<TSource> Union<TSource>(this IEnumerable<TSource> first, IEnumerable<TSource> second, IEqualityComparer<TSource> comparer)
		{
			if (first == null) throw Error.ArgumentNull("first");
			if (second == null) throw Error.ArgumentNull("second");

			return UnionIterator<TSource>(first, second, comparer);
		}

		private static IEnumerable<TSource> UnionIterator<TSource>(IEnumerable<TSource> first, IEnumerable<TSource> second, IEqualityComparer<TSource> comparer)
		{
			var set = new Set<TSource>(comparer);
			foreach (var o in first) if (set.Add(o)) yield return o;
			foreach (var o in second) if (set.Add(o)) yield return o;
		}

		public static IEnumerable<TSource> Where<TSource>(this IEnumerable<TSource> source, Func<TSource, bool> predicate)
		{
			if (source == null) throw Error.ArgumentNull("source");
			if (predicate == null) throw Error.ArgumentNull("predicate");

			if (source is Iterator<TSource>) return ((Iterator<TSource>) source).Where(predicate);
			if (source is TSource[]) return new WhereArrayIterator<TSource>((TSource[]) source, predicate);
			if (source is List<TSource>) return new WhereListIterator<TSource>((List<TSource>) source, predicate);

			return new WhereEnumerableIterator<TSource>(source, predicate);
		}

		public static IEnumerable<TSource> Where<TSource>(this IEnumerable<TSource> source, Func<TSource, int, bool> predicate)
		{
			if (source == null) throw Error.ArgumentNull("source");
			if (predicate == null) throw Error.ArgumentNull("predicate");

			return WhereIterator<TSource>(source, predicate);
		}

		private static IEnumerable<TSource> WhereIterator<TSource>(IEnumerable<TSource> source, Func<TSource, int, bool> predicate)
		{
			int i = 0;
			foreach (var o in source) if (predicate(o, i++)) yield return o;
		}
	}
}
