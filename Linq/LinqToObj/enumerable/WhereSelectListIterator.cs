﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JpLabs.Linq.LinqToObj
{
	public static partial class Enumerable
	{
		private class WhereSelectListIterator<TSource, TResult> : Enumerable.Iterator<TResult>
		{
			// Fields
			private List<TSource>.Enumerator enumerator;
			private Func<TSource, bool> predicate;
			private Func<TSource, TResult> selector;
			private List<TSource> source;

			// Methods
			public WhereSelectListIterator(List<TSource> source, Func<TSource, bool> predicate, Func<TSource, TResult> selector)
			{
				this.source = source;
				this.predicate = predicate;
				this.selector = selector;
			}

			public override Enumerable.Iterator<TResult> Clone()
			{
				return new Enumerable.WhereSelectListIterator<TSource, TResult>(this.source, this.predicate, this.selector);
			}

			public override bool MoveNext()
			{
				switch (base.state)
				{
					case 1:
						this.enumerator = this.source.GetEnumerator();
						base.state = 2;
						break;

					case 2: break;
					default: return false;
				}
				while (this.enumerator.MoveNext())
				{
					TSource current = this.enumerator.Current;
					if ((this.predicate == null) || this.predicate(current))
					{
						base.current = this.selector(current);
						return true;
					}
				}
				this.Dispose();
				return false;
			}

			public override IEnumerable<TResult2> Select<TResult2>(Func<TResult, TResult2> selector)
			{
				return new Enumerable.WhereSelectListIterator<TSource, TResult2>(this.source, this.predicate, Enumerable.CombineSelectors<TSource, TResult, TResult2>(this.selector, selector));
			}

			public override IEnumerable<TResult> Where(Func<TResult, bool> predicate)
			{
				return (IEnumerable<TResult>) new Enumerable.WhereEnumerableIterator<TResult>(this, predicate);
			}
		}
	}
}
