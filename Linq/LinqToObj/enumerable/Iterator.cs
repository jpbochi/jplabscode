﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Collections;

namespace JpLabs.Linq.LinqToObj
{
	public static partial class Enumerable
	{
		public abstract class Iterator<TSource> : IEnumerable<TSource>, IEnumerator<TSource>
		{
			// Fields
			internal TSource current;
			internal int state;
			private int threadId;

			// Methods
			public Iterator()
			{
				this.threadId = Thread.CurrentThread.ManagedThreadId;
			}

			public abstract Enumerable.Iterator<TSource> Clone();
			public abstract bool MoveNext();
			public abstract IEnumerable<TResult> Select<TResult>(Func<TSource, TResult> selector);
			public abstract IEnumerable<TSource> Where(Func<TSource, bool> predicate);

			public virtual void Dispose()
			{
				this.current = default(TSource);
				this.state = -1;
			}

			public IEnumerator<TSource> GetEnumerator()
			{
				if ((this.threadId == Thread.CurrentThread.ManagedThreadId) && (this.state == 0))
				{
					this.state = 1;
					return this;
				}
				Enumerable.Iterator<TSource> iterator = this.Clone();
				iterator.state = 1;
				return iterator;
			}

			IEnumerator IEnumerable.GetEnumerator()
			{
				return this.GetEnumerator();
			}

			void IEnumerator.Reset()
			{
				throw new NotImplementedException();
			}

			// Properties
			public TSource Current
			{
				get
				{
					return this.current;
				}
			}

			object IEnumerator.Current
			{
				get
				{
					return this.Current;
				}
			}
		}
	}
}