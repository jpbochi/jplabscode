﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JpLabs.Linq.LinqToObj
{
	public static partial class Enumerable
	{
		private class WhereListIterator<TSource> : Enumerable.Iterator<TSource>
		{
			// Fields
			private List<TSource>.Enumerator enumerator;
			private Func<TSource, bool> predicate;
			private List<TSource> source;

			// Methods
			public WhereListIterator(List<TSource> source, Func<TSource, bool> predicate)
			{
				this.source = source;
				this.predicate = predicate;
			}

			public override Enumerable.Iterator<TSource> Clone()
			{
				return new Enumerable.WhereListIterator<TSource>(this.source, this.predicate);
			}

			public override bool MoveNext()
			{
				switch (base.state)
				{
					case 1:
						this.enumerator = this.source.GetEnumerator();
						base.state = 2;
						break;

					case 2: break;
					default: return false;
				}
				while (this.enumerator.MoveNext())
				{
					TSource current = this.enumerator.Current;
					if (this.predicate(current))
					{
						base.current = current;
						return true;
					}
				}
				this.Dispose();
				return false;
			}

			public override IEnumerable<TResult> Select<TResult>(Func<TSource, TResult> selector)
			{
				return new Enumerable.WhereSelectListIterator<TSource, TResult>(this.source, this.predicate, selector);
			}

			public override IEnumerable<TSource> Where(Func<TSource, bool> predicate)
			{
				return new Enumerable.WhereListIterator<TSource>(this.source, Enumerable.CombinePredicates<TSource>(this.predicate, predicate));
			}
		}
	}
}
