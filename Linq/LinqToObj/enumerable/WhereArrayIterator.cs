﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JpLabs.Linq.LinqToObj
{
	public static partial class Enumerable
	{
		private class WhereArrayIterator<TSource> : Enumerable.Iterator<TSource>
		{
			// Fields
			private int index;
			private Func<TSource, bool> predicate;
			private TSource[] source;

			// Methods
			public WhereArrayIterator(TSource[] source, Func<TSource, bool> predicate)
			{
				this.source = source;
				this.predicate = predicate;
			}

			public override Enumerable.Iterator<TSource> Clone()
			{
				return new Enumerable.WhereArrayIterator<TSource>(this.source, this.predicate);
			}

			public override bool MoveNext()
			{
				if (base.state == 1)
				{
					while (this.index < this.source.Length)
					{
						TSource arg = this.source[this.index];
						this.index++;
						if (this.predicate(arg))
						{
							base.current = arg;
							return true;
						}
					}
					this.Dispose();
				}
				return false;
			}

			public override IEnumerable<TResult> Select<TResult>(Func<TSource, TResult> selector)
			{
				return new Enumerable.WhereSelectArrayIterator<TSource, TResult>(this.source, this.predicate, selector);
			}

			public override IEnumerable<TSource> Where(Func<TSource, bool> predicate)
			{
				return new Enumerable.WhereArrayIterator<TSource>(this.source, Enumerable.CombinePredicates<TSource>(this.predicate, predicate));
			}
		}
	}
}
