﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JpLabs.Linq.LinqToObj
{
	public static class ImmutableArrayExt
	{
	    public static T[] Add<T>(this T[] source, T item)
	    {
			int currLen = source.Length;
			var newArray = new T[currLen + 1];

			source.CopyTo(newArray, 0);
			newArray[currLen] = item;

			return newArray;
	    }

		public static T[] RemoveAt<T>(this T[] source, int index)
		{
			return source.Where( (ev, i) => (i != index) ).ToArray();
		}
		
		public static T[] Insert<T>(this T[] source, int index, T item)
		{
			//TODO: code this
			throw new NotImplementedException();
		}
	}
}
