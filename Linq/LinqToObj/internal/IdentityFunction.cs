﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JpLabs.Linq.LinqToObj
{
	internal class IdentityFunction<TElement>
	{
		// Properties
		public static Func<TElement, TElement> Instance
		{
			get { return (x => x); }
		}
	}
}
