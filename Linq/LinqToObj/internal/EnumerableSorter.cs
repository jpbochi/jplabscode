﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JpLabs.Linq.LinqToObj
{
	internal abstract class EnumerableSorter<TElement>
	{
		// Methods
		protected EnumerableSorter() {}

		internal abstract int CompareKeys(int index1, int index2);
		internal abstract void ComputeKeys(TElement[] elements, int count);
		
		private void QuickSort(int[] map, int left, int right)
		{
			do {
				int l = left;
				int r = right;
				int center = map[l + ((r - l) >> 1)];
				do {
					while ((l < map.Length) && (this.CompareKeys(center, map[l]) > 0)) {
						l++;
					}
					
					while ((r >= 0) && (this.CompareKeys(center, map[r]) < 0)) {
						r--;
					}
					
					if (l > r) break;
					
					if (l < r) {
						int swap = map[l];
						map[l] = map[r];
						map[r] = swap;
					}
					l++;
					r--;
				} while (l <= r);
				
				if ((r - left) <= (right - l)) {
					if (left < r) this.QuickSort(map, left, r);
					left = l;
				} else {
					if (l < right) this.QuickSort(map, l, right);
					right = r;
				}
			} while (left < right);
		}

		internal int[] Sort(TElement[] elements, int count)
		{
			this.ComputeKeys(elements, count);
			int[] map = new int[count];
			for (int i = 0; i < count; i++) map[i] = i;
			this.QuickSort(map, 0, count - 1);
			return map;
		}
	}

	internal class EnumerableSorter<TElement, TKey> : EnumerableSorter<TElement>
	{
		// Fields
		internal IComparer<TKey> comparer;
		internal bool descending;
		internal TKey[] keys;
		internal Func<TElement, TKey> keySelector;
		internal EnumerableSorter<TElement> next;

		// Methods
		internal EnumerableSorter(Func<TElement, TKey> keySelector, IComparer<TKey> comparer, bool descending, EnumerableSorter<TElement> next)
		{
			this.keySelector = keySelector;
			this.comparer = comparer;
			this.descending = descending;
			this.next = next;
		}

		internal override int CompareKeys(int index1, int index2)
		{
			int num = this.comparer.Compare(this.keys[index1], this.keys[index2]);
			if (num == 0) {
				if (this.next == null) return (index1 - index2);
				return this.next.CompareKeys(index1, index2);
			}
			if (!this.descending) return num;
			return -num;
		}

		internal override void ComputeKeys(TElement[] elements, int count)
		{
			this.keys = new TKey[count];
			for (int i = 0; i < count; i++) this.keys[i] = this.keySelector(elements[i]);
			
			if (this.next != null) this.next.ComputeKeys(elements, count);
		}
	}
}
