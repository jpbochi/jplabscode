﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace JpLabs.Linq.LinqToObj
{
	internal abstract class OrderedEnumerable<TElement> : IOrderedEnumerable<TElement>
	{
		// Fields
		internal IEnumerable<TElement> source;

		// Methods
		protected OrderedEnumerable() {}

		internal abstract EnumerableSorter<TElement> GetEnumerableSorter(EnumerableSorter<TElement> next);
		
		public IEnumerator<TElement> GetEnumerator()
		{
			var buffer = new Buffer<TElement>(source);
			if (buffer.count > 0) {
				int[] map = GetEnumerableSorter(null).Sort(buffer.items, buffer.count);
				for (int i=0; i < buffer.count; i++) yield return buffer.items[map[i]];
			}
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return this.GetEnumerator();
		}

		IOrderedEnumerable<TElement> IOrderedEnumerable<TElement>.CreateOrderedEnumerable<TKey>(Func<TElement, TKey> keySelector, IComparer<TKey> comparer, bool descending)
		{
			OrderedEnumerable<TElement, TKey> enumerable = new OrderedEnumerable<TElement, TKey>(this.source, keySelector, comparer, descending);
			enumerable.parent = (OrderedEnumerable<TElement>) this;
			return enumerable;
		}
	}

	internal class OrderedEnumerable<TElement, TKey> : OrderedEnumerable<TElement>
	{
		// Fields
		internal IComparer<TKey> comparer;
		internal bool descending;
		internal Func<TElement, TKey> keySelector;
		internal OrderedEnumerable<TElement> parent;

		// Methods
		internal OrderedEnumerable(IEnumerable<TElement> source, Func<TElement, TKey> keySelector, IComparer<TKey> comparer, bool descending)
		{
			if (source == null) throw Error.ArgumentNull("source");
			if (keySelector == null) throw Error.ArgumentNull("keySelector");
			
			base.source = source;
			this.parent = null;
			this.keySelector = keySelector;
			this.comparer = (comparer != null) ? comparer : ((IComparer<TKey>) Comparer<TKey>.Default);
			this.descending = descending;
		}

		internal override EnumerableSorter<TElement> GetEnumerableSorter(EnumerableSorter<TElement> next)
		{
			EnumerableSorter<TElement> enumerableSorter = new EnumerableSorter<TElement, TKey>(this.keySelector, this.comparer, this.descending, next);
			
			if (this.parent != null) enumerableSorter = this.parent.GetEnumerableSorter(enumerableSorter);
			
			return enumerableSorter;
		}
	}
}
