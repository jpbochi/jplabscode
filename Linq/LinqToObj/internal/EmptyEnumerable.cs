﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JpLabs.Linq.LinqToObj
{
	internal class EmptyEnumerable<TElement>
	{
		// Fields
		private static TElement[] instance;

		// Properties
		public static IEnumerable<TElement> Instance
		{
			get
			{
				if (EmptyEnumerable<TElement>.instance == null) {
					EmptyEnumerable<TElement>.instance = new TElement[0];
				}
				return EmptyEnumerable<TElement>.instance;
			}
		}
	}
}
