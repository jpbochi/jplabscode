﻿using System;
using System.Linq;
using System.Data.Odbc;
using System.Collections.Generic;
using JpLabs.Extensions;

namespace JpLabs.TestLinq
{
	class Program
	{
		static void Main(string[] args)
		{
			var array0 = new string[]{"Alice", "Bob", "Carol"};
			var array1 = new string[]{"Bob", "Carol", "Ted", "Alice"};
			var array2 = new string[]{"Alice", "Bob", "Charlie", "Dave", "Eve"};
			var array3 = new string[]{
				"Alice", "Bob", "Charlie", "Dave", "Eve", "Isaac", "Ivan", "Merlin",
				"Mallory", "Matilda", "Oscar", "Pat", "Peggy", "Victor", "Vanna",
				"Plod", "Steve", "Trent", "Trudy", "Walter", "Zoe"};
			
			//var q = array0.Select(s => s.StartsWith("A"));
			//var q = array0.Concat(array1);
			//var q = array0.Union(array1);
			//var q = array0.Concat(array1).OrderBy(o => o);
			//List<string> q = new List<string>(Enumerable.Except(array2, array0));
			var q = array3.Shuffle();
			
			foreach (var o in q) Console.WriteLine(o);
			
			if (System.Diagnostics.Debugger.IsAttached) {
				Console.Write("Press any key to continue . . . ");
				Console.ReadKey(true);
				Console.WriteLine();
			}
		}
	}
}
