﻿using System;
using System.Collections.Generic;
using System.Text;
using JpLabs.Linq.LinqToObj;

namespace JpLabs.Linq.Test
{
	//static class MyEnumerable
	//{
	//    public static TSource FirstOrDefault<TSource>(this IEnumerable<TSource> source, TSource defaultValue)
	//    {
	//        if (source == null) throw Error.ArgumentNull("source");
			
	//        IList<TSource> list = source as IList<TSource>;
	//        if (list != null) {
	//            if (list.Count > 0) return list[0];
	//        } else {
	//            using (IEnumerator<TSource> enumerator = source.GetEnumerator()) {
	//                if (enumerator.MoveNext()) return enumerator.Current;
	//            }
	//        }
	//        return defaultValue;
	//    }
	//}

	class Program
	{
		static void Main(string[] args)
		{
			var array0 = new string[]{"Alice", "Bob", "Carol"};
			var array1 = new string[]{"Bob", "Carol", "Ted", "Alice"};
			var array2 = new string[]{"Alice", "Bob", "Charlie", "Dave", "Eve"};
			var array3 = new string[]{
				"Alice", "Bob", "Charlie", "Dave", "Eve", "Isaac", "Ivan", "Merlin",
				"Mallory", "Matilda", "Oscar", "Pat", "Peggy", "Victor", "Vanna",
				"Plod", "Steve", "Trent", "Trudy", "Walter", "Zoe"};
			
			//var q = array0.Select(s => s.StartsWith("A"));
			//var q = array0.Concat(array1);
			//var q = array0.Union(array1);
			//var q = array0.Concat(array1).OrderBy(o => o);
			var q = from o in array1.Union(array2) orderby o select o;
			
			foreach (var o in q) Console.WriteLine(o);
			
			//Enumerable.Range(0, q.Count()).Select(i => q.ElementAt(i)).ForEach(Console.WriteLine);
			
			if (System.Diagnostics.Debugger.IsAttached) {
				Console.Write("Press any key to continue . . . ");
				Console.ReadKey(true);
				Console.WriteLine();
			}
		}
	}
}
