﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NotLinq
{
	class Program
	{
		static void Main(string[] args)
		{
			string[] array0 = new string[]{"Alice", "Bob", "Carol"};
			string[] array1 = new string[]{"Bob", "Carol", "Ted", "Alice"};
			string[] array2 = new string[]{"Alice", "Bob", "Charlie", "Dave", "Eve"};
			string[] array3 = new string[]{
				"Alice", "Bob", "Charlie", "Dave", "Eve", "Isaac", "Ivan", "Merlin",
				"Mallory", "Matilda", "Oscar", "Pat", "Peggy", "Victor", "Vanna",
				"Plod", "Steve", "Trent", "Trudy", "Walter", "Zoe"};
			
			//var q = array0.Select(s => s.StartsWith("A"));
			//var q = array0.Concat(array1);
			//var q = array0.Union(array1);
			//var q = array0.Concat(array1).OrderBy(o => o);
			
			List<string> q = new List<string>(Enumerable.Except(array2, array0));
			
			foreach (string o in q) Console.WriteLine(o);
			
			Console.ReadKey(true);
		}
	}
}
