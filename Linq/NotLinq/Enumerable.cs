﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace NotLinq
{
	public static partial class Enumerable
	{
		public static IEnumerable<TSource> Except<TSource>(IEnumerable<TSource> first, IEnumerable<TSource> second)
		{
			if (first == null) throw new ArgumentNullException("first");
			if (second == null) throw new ArgumentNullException("second");
			
			return ExceptIterator<TSource>(first, second, null);
		}

		public static IEnumerable<TSource> Except<TSource>(IEnumerable<TSource> first, IEnumerable<TSource> second, IEqualityComparer<TSource> comparer)
		{
			if (first == null) throw new ArgumentNullException("first");
			if (second == null) throw new ArgumentNullException("second");
			
			return ExceptIterator<TSource>(first, second, comparer);
		}

		private static IEnumerable<TSource> ExceptIterator<TSource>(IEnumerable<TSource> first, IEnumerable<TSource> second, IEqualityComparer<TSource> comparer)
		{
			Set<TSource> set = new Set<TSource>(comparer);
			foreach (TSource o in second) set.Add(o);
			foreach (TSource o in first) if (set.Add(o)) yield return o;
		}
	}
}
