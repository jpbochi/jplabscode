﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;
using Xunit.Extensions;

namespace JpLabs.Extensions.Tests
{
	public class MiscExtensionsTests
	{
		[Theory]
		[InlineData(null, null)]
		[InlineData("", "")]
		[InlineData("foo", "foo")]
		[InlineData("Bar", "bar")]
		[InlineData("JRRTolkien", "jRRTolkien")]
		[InlineData("Will not care about spaces!", "will not care about spaces!")]
		void should_convert_to_camel_case(string input, string expected)
		{
			Assert.Equal(expected, input.ToCamelCase());
		}
	}
}
