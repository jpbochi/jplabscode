﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Reflection;

namespace JpLabs.Extensions
{
	public class TypeComparer : IComparer<Type>
	{
		public static IComparer<Type> Instance = new TypeComparer();
		
		private TypeComparer() {}
		
		public int Compare(Type x, Type y)
		{
			return TypeExt.Compare(x, y);
		}
	}
	
	public static class TypeExt
	{
		public static int Compare(this Type x, Type y)
		{
			//if (x == null) {
			//    if (y == null) return 0; //null == null
			//    return -1; // null < y
			//}
			//if (y == null) return +1; // x > null
			if (x == null) throw new ArgumentNullException("x");
			if (y == null) throw new ArgumentNullException("y");
			
			if (x == y) return 0; //x == y
			if (x.IsAssignableFrom(y)) return +1; //x > y
			if (y.IsAssignableFrom(x)) return -1; //x < y
			return 0; //x and y are not directly related
		}
		
		public static bool IsBiggerOrEqual(this Type x, Type y)
		{
			return x.IsAssignableFrom(y);
		}

		public static bool IsLittlerOrEqual(this Type x, Type y)
		{
			return y.IsAssignableFrom(x);
		}
		
		public static object GetDefaultValue(this Type type)
		{
		    return type.IsValueType ? Activator.CreateInstance(type) : null;
		}
		
		public static bool IsAssignableFromValue(this Type type, object value)
		{
			if (value == null) return IsAssignableFromNull(type);
			
			return type.IsAssignableFrom(value.GetType());
		}

		public static bool IsAssignableFromNull(this Type type)
		{
			return !type.IsValueType || (type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Nullable<>));
		}

		/// <summary>
		/// Returns the contents of the <see cref="DescriptionAttribute"/> applied to the type.
		/// </summary>
		/// <param name="type">The type whose description will be returned.</param>
		/// <returns>The description of the typpe.</returns>
		public static string GetDescription(this Type type)
		{
			if (type == null) return null;

			var descAttrib = type.GetSingleAttrOrNull<DescriptionAttribute>(false);
			return (descAttrib != null) ? descAttrib.Description : type.FullName;
		}

		public static Type ResolveTypeFromFullName(string typeFullName, bool throwIfTypeNotFound)
		{
			return ResolveTypeFromFullName(typeFullName, throwIfTypeNotFound, AppDomain.CurrentDomain.GetAssemblies());
		}

		public static Type ResolveTypeFromFullName(string typeFullName, bool throwIfTypeNotFound, IEnumerable<Assembly> assemblies)
		{
			return Type.GetType(
				typeFullName,
				null,
				(asm, name, caseSensitive) =>
					assemblies.Select( subAsm => subAsm.GetType(name) ).Where(t => t != null).FirstOrDefault()
				,
				throwIfTypeNotFound
			);
		}
	}
}
