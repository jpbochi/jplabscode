﻿using System;

namespace JpLabs.Extensions
{
	internal static class WeakReferenceExt
	{
		public static bool WeakRefEquals(this WeakReference x, WeakReference y)
		{
			return (x == null) ? (y == null) : (y != null && x.Target == y.Target);
		}
		
		public static T GetValueOrNull<T>(this WeakReference weakRef) where T : class
		{
			return (weakRef == null) ? null : weakRef.Target as T;
		}
	}
}
