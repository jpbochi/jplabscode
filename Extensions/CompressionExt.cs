﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.IO.Compression;

namespace JpLabs.Extensions
{
	public static class CompressionExt
	{
		public static string ToBase64String(this byte[] bytes)
		{
			return Convert.ToBase64String(bytes);
		}

		/// <summary>
		/// Follows this pattern: http://en.wikipedia.org/wiki/Base64#URL_applications
		/// </summary>
		public static string ToBase64Url(this byte[] bytes)
		{
			return Convert.ToBase64String(bytes).TrimEnd('=').Replace('+', '-').Replace('/', '_');
		}

		public static byte[] Compress(this byte[] data)
		{
			using (var output = new MemoryStream()) {
				using (var compress = new GZipStream(output, CompressionMode.Compress)) {
					compress.Write(data, 0, data.Length);
				}
				return output.ToArray();
			}
		}

		public static byte[] Decompress(this byte[] data)
		{
			using (var decompress = new GZipStream(new MemoryStream(data), CompressionMode.Decompress)) {
				using (var output = new MemoryStream()) {
					decompress.CopyTo(output);
					return output.ToArray();
				}
			}
		}

		public static byte[] Compress(this string text)
		{
			return Compress(text, Encoding.Default);
		}

		public static byte[] Compress(this string text, Encoding encoding)
		{
			return encoding.GetBytes(text).Compress();
		}

		public static string DecompressToString(this byte[] data)
		{
			return DecompressToString(data, Encoding.Default);
		}

		public static string DecompressToString(this byte[] data, Encoding encoding)
		{
			return encoding.GetString(data.Decompress());
		}
	}
}
