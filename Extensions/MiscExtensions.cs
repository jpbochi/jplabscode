﻿using System;
using System.Linq;
using System.Diagnostics;
using System.Reflection;
using System.Collections.Generic;
using System.ComponentModel;

namespace JpLabs.Extensions
{
	public static class AssemblyExt
	{
		public static bool IsBuiltByMicrosoft(this Assembly asm)
		{
			return asm.IsDefined(typeof(AssemblyCompanyAttribute), false) && asm.GetSingleAttr<AssemblyCompanyAttribute>(false).Company == "Microsoft Corporation";
		}
	}

	public static class StringExt
	{
		public static string ToCamelCase(this string word)
		{
			return string.IsNullOrEmpty(word) ? word : string.Concat(word.Substring(0, 1).ToLowerInvariant(), word.Substring(1));
		}
	}

	public static class UriTemplateExt
	{
		public static Uri BindBy(this UriTemplate template, Uri baseAddress, object uriArgs)
		{
			return template.BindByName(baseAddress, AnonymousObjectToUriVariables(uriArgs));
		}

		private static IDictionary<string,string> AnonymousObjectToUriVariables(object uriArgs)
		{
			return uriArgs.ToObjectDictionary(value => (value ?? "").ToString());
		}
	}

	public static class ObjectExt
	{
		public static object TryGetProperty(this object obj, string propertyName, bool ignoreCase = true)
		{
			var prop = TypeDescriptor.GetProperties(obj).Find(propertyName, ignoreCase);
			return (prop == null) ? null : prop.GetValue(obj);
		}

		public static IDictionary<string,object> ToObjectDictionary(this object obj)
		{
			return obj.ToObjectDictionary(value => value);
		}

		public static IDictionary<string,T> ToObjectDictionary<T>(this object obj, Func<object,T> valueSelect)
		{
			return TypeDescriptor.GetProperties(obj)
				.OfType<PropertyDescriptor>()
				.ToDictionary<PropertyDescriptor,string,T>(
					prop => prop.Name,
					prop => valueSelect(prop.GetValue(obj))
				);
		}
	}

	public static class ComparableExt
	{
		public static bool Between<T>(this T actual, T lower, T upper) where T : IComparable<T>
		{
			return actual.CompareTo(lower) >= 0 && actual.CompareTo(upper) < 0;
		}

		/*public static R Pipe<T, R>(this T obj, Func<T, R> func)
		{
			if (func == null) throw new ArgumentNullException("func");
			
			return func(obj);
		}
		
		public static T Pipe<T>(this T obj, Action<T> action)
		{
			if (action == null) throw new ArgumentNullException("action");
			
			action(obj);
			return obj;
		}//*/
	}

	public static class StopwatchExt
	{
		public static long Time(this Stopwatch sw, Action action, int iterations)
		{
			sw.Reset();
			sw.Start();
			for (int i = 0; i < iterations; i++) { action(); }
			sw.Stop();
			return sw.ElapsedMilliseconds;
		}
	}
}
