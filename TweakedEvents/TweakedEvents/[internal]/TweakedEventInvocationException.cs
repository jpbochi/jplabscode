﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;

namespace JpLabs.TweakedEvents
{
	public class TweakedEventInvocationException : ApplicationException
	{
		public TweakedEventInvocationException(Exception exception) : base(exception.Message)
		{
			this.InvocationException = exception;
		}

		public TweakedEventInvocationException(Exception innerException, MethodInfo methodInfo) : base(innerException.Message)
		{
			this.InvocationException = innerException;
			this.InvokedDelegateName = GetMethodFullName(methodInfo);
		}

		public Exception InvocationException { get; private set; }
		public string InvokedDelegateName { get; private set; }

		public static TweakedEventInvocationException From(Exception ex)
		{
			if (ex.InnerException != null) {
			    ex = ex.InnerException;
			}

			return new TweakedEventInvocationException(ex);
		}

		public static TweakedEventInvocationException From(Exception ex, MethodInfo invokedMethodInfo)
		{
			if (ex.InnerException != null) {
			    ex = ex.InnerException;
			}
			return new TweakedEventInvocationException(ex, invokedMethodInfo);
		}

		public static TweakedEventInvocationException From(Exception ex, Delegate invokedDelegate)
		{
			return From(ex, invokedDelegate.Method);
		}

		private static string GetMethodFullName(System.Reflection.MethodInfo method)
		{
			string name = method.ToString();
			
			string returnParam = method.ReturnParameter.ToString();
			if (name.StartsWith(returnParam)) name = name.Remove(0, returnParam.Length);
			
			return string.Concat(method.DeclaringType.FullName, ".", name);
		}

		public override string ToString()
		{
			string className = this.GetType().FullName;
			string message = this.Message;

			string text = string.IsNullOrEmpty(message) ? className : string.Concat(className, ": ", message);

			return string.Concat(text, Environment.NewLine, this.ToShortString());
		}

		private string ToShortString()
		{
			var text = new StringBuilder();

			if (this.InvocationException is TweakedEventInvocationException) {
				text.Append(
					string.Concat(
						"[-",
						((TweakedEventInvocationException)InvocationException).ToShortString(),
						Environment.NewLine,
						"-]"
					)
				);
			} else {
				if (this.InvokedDelegateName != null)
				{
					text.Append(
						string.Concat(
							"[-- ",
							this.InvokedDelegateName,
							" --]"
						)
					);
				}
				
				if (this.InvocationException != null) {
					if (text.Length > 0) text.AppendLine();

					text.Append(
						string.Concat(
							"[- ",
							InvocationException,
							Environment.NewLine,
							"-]"
						)
					);
				}
			}

			if (this.StackTrace != null)
			{
				if (text.Length > 0) text.AppendLine();

				text.Append(this.StackTrace);
			}
			return text.ToString();
		}
	}
}
