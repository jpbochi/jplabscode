﻿using System;
using System.Reflection;

namespace JpLabs.TweakedEvents
{
	internal static class GenericArgumentValidator
	{
		/// <summary>Validate generic argument <typeparamref name="TEventHandler"/></summary>
		/// <exception cref="ArgumentException"></exception>
		public static void ValidateEventHandler<TEventHandler>() where TEventHandler : class
		{
			Type eventArgsType;
			ValidateEventHandler<TEventHandler>(out eventArgsType);
		}
		
		public static void ValidateEventHandler<TEventHandler>(out Type outEventArgsType) where TEventHandler : class
		{
			if (!typeof(TEventHandler).IsSubclassOf(typeof(Delegate)))
				throw new ArgumentException("T must be a delegate type");
			
			MethodInfo invoke = typeof(TEventHandler).GetMethod("Invoke");
			if (invoke == null || invoke.GetParameters().Length != 2)
				throw new ArgumentException("T must be a delegate type taking 2 parameters");
			
			ParameterInfo[] methodParams = invoke.GetParameters();
			ParameterInfo senderParameter = methodParams[0];
			//if (senderParameter.ParameterType != typeof(object))
			//	throw new ArgumentException("The first delegate parameter type must be 'object'");
			if (senderParameter.ParameterType.IsValueType)
				throw new ArgumentException("The first delegate parameter type must not be a value type");
			
			ParameterInfo eventArgsParameter = methodParams[1];
			outEventArgsType = eventArgsParameter.ParameterType;
			if (!(typeof(EventArgs).IsAssignableFrom(outEventArgsType)))
				throw new ArgumentException("The second delegate parameter must be derived from type 'EventArgs'");
			
			if (invoke.ReturnType != typeof(void))
				throw new ArgumentException("The delegate return type must be void.");
		}
	}
}
