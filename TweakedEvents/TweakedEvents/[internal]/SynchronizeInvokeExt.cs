﻿using System;
using System.ComponentModel;

namespace JpLabs.TweakedEvents
{
	internal static class SynchronizeInvokeExt
	{
		private static object[] EmptyArgs = new object[0];
		
		public static void Invoke(this ISynchronizeInvoke syncInvoke, Action method)
		{
			syncInvoke.Invoke(method, EmptyArgs);
		}

		public static void Invoke<T>(this ISynchronizeInvoke syncInvoke, Action<T> method, T arg)
		{
			syncInvoke.Invoke(method, new object[]{ arg });
		}

		public static void Invoke<T1,T2>(this ISynchronizeInvoke syncInvoke, Action<T1,T2> method, T1 arg1, T2 arg2)
		{
			syncInvoke.Invoke(method, new object[]{ arg1, arg2 });
		}

		public static void Invoke<T1,T2,T3>(this ISynchronizeInvoke syncInvoke, Action<T1,T2,T3> method, T1 arg1, T2 arg2, T3 arg3)
		{
			syncInvoke.Invoke(method, new object[]{ arg1, arg2, arg3 });
		}

		public static void Invoke<T1,T2,T3,T4>(this ISynchronizeInvoke syncInvoke, Action<T1,T2,T3,T4> method, T1 arg1, T2 arg2, T3 arg3, T4 arg4)
		{
			syncInvoke.Invoke(method, new object[]{ arg1, arg2, arg3, arg4 });
		}

		public static TResult Invoke<TResult>(this ISynchronizeInvoke syncInvoke, Func<TResult> method)
		{
			return (TResult)syncInvoke.Invoke(method, EmptyArgs);
		}

		public static TResult Invoke<T,TResult>(this ISynchronizeInvoke syncInvoke, Func<T,TResult> method, T arg)
		{
			return (TResult)syncInvoke.Invoke(method, new object[]{ arg });
		}

		public static TResult Invoke<T1,T2,TResult>(this ISynchronizeInvoke syncInvoke, Func<T1,T2,TResult> method, T1 arg1, T2 arg2)
		{
			return (TResult)syncInvoke.Invoke(method, new object[]{ arg1, arg2 });
		}

		public static TResult Invoke<T1,T2,T3,TResult>(this ISynchronizeInvoke syncInvoke, Func<T1,T2,T3,TResult> method, T1 arg1, T2 arg2, T3 arg3)
		{
			return (TResult)syncInvoke.Invoke(method, new object[]{ arg1, arg2, arg3 });
		}

		public static TResult Invoke<T1,T2,T3,T4,TResult>(this ISynchronizeInvoke syncInvoke, Func<T1,T2,T3,T4,TResult> method, T1 arg1, T2 arg2, T3 arg3, T4 arg4)
		{
			return (TResult)syncInvoke.Invoke(method, new object[]{ arg1, arg2, arg3, arg4 });
		}
	}
}
