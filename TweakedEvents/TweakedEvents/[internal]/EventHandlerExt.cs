﻿using System;

namespace JpLabs.TweakedEvents
{
	public static class EventHandlerExt
	{
		public static void RaiseEvent(this EventHandler handler, object sender, EventArgs e)
		{
			if (handler != null) handler(sender, e);
		}
		
		public static void RaiseEvent<T>(this EventHandler<T> handler, object sender, T e) where T : EventArgs
		{
			if (handler != null) handler(sender, e);
		}
		
		/*
		// PROBLEM: Detaching the event is apparently not possible!
		
		public static EventHandler ToSynced(this EventHandler handler, ISynchronizeInvoke syncInvoke)
		{
			return new SyncedEventEntry<EventHandler>(handler, syncInvoke).ToDelegate();
		}

		public static EventHandler<T> ToSynced<T>(this EventHandler<T> handler, ISynchronizeInvoke syncInvoke) where T : EventArgs
		{
			return new SyncedEventEntry<EventHandler<T>>(handler, syncInvoke).ToDelegate<T>();
		}

		public static EventHandler ToWeak(this EventHandler handler)
		{
			return new WeakEventEntry<EventHandler>(handler).ToDelegate();
		}

		public static EventHandler<T> ToWeak<T>(this EventHandler<T> handler) where T : EventArgs
		{
			return new WeakEventEntry<EventHandler<T>>(handler).ToDelegate<T>();
		}

		public static EventHandler ToWeakSynced(this EventHandler handler, ISynchronizeInvoke syncInvoke)
		{
			return new WeakSyncedEventEntry<EventHandler>(handler, syncInvoke).ToDelegate();
		}

		public static EventHandler<T> ToWeakSynced<T>(this EventHandler<T> handler, ISynchronizeInvoke syncInvoke) where T : EventArgs
		{
			return new WeakSyncedEventEntry<EventHandler<T>>(handler, syncInvoke).ToDelegate<T>();
		}

		public static EventHandler ToDelegate(this IEventEntry eventEntry)
		{
			return (
				(object sender, EventArgs e) => eventEntry.Invoke(sender, e)
			);
		}

		public static EventHandler<T> ToDelegate<T>(this IEventEntry eventEntry) where T : EventArgs
		{
			return (
				(object sender, T e) => eventEntry.Invoke(sender, e)
			);
		}
		//*/
	}
}
