﻿using System;
using System.Reflection;

namespace JpLabs.TweakedEvents
{
	public abstract class BaseEventEntry<TEH> : IEventEntry<TEH> where TEH : class
	{
		private static readonly MethodInfo DynamicInvoke_Info;
		
		static BaseEventEntry()
		{
			// Make sure TEH is an acceptable EventHandler
			Type eventArgsType;
			GenericArgumentValidator.ValidateEventHandler<TEH>(out eventArgsType);
			
			//Get the MethodInfo for DynamicInvoke<TEventArgs>
			MethodInfo genericDynamicInvoke = typeof(BaseEventEntry<TEH>).GetMethod("DynamicInvoke", BindingFlags.Instance | BindingFlags.NonPublic);
			DynamicInvoke_Info = genericDynamicInvoke.MakeGenericMethod(eventArgsType);
		}
		
		// Abstract Methods / IEventEntry<TEH> members		
		abstract public bool Equals(IEventEntry other);
		abstract public bool EqualsHandler(TEH handler);
		abstract public bool Invoke(object sender, EventArgs e);
		abstract public void Dispose();
		abstract public bool IsDisposed { get; }
		
		// Conversion to a TEventHandler delegate
		public static implicit operator TEH(BaseEventEntry<TEH> entry)
		{
			if (entry == null) return null;
			
			return entry.ToEventHandler();
		}

		public TEH ToEventHandler()
		{
			return (TEH)(object)Delegate.CreateDelegate(typeof(TEH), this, DynamicInvoke_Info);
		}

		private void DynamicInvoke<TEventArgs>(object sender, TEventArgs e) where TEventArgs : EventArgs
		{
		    ((IEventEntry)this).Invoke(sender, e);
		}
	}
}
