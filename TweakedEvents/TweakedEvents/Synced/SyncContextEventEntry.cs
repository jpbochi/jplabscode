﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Reflection;

namespace JpLabs.TweakedEvents.Synced
{
	internal sealed class SyncContextEventEntry<TEH> : SyncedEventEntry<TEH>, IEventEntry<TEH> where TEH : class
	{
		private SynchronizationContext syncContext;
		
		public SyncContextEventEntry(IEventEntry<TEH> entry) : base(entry)
		{
			this.syncContext = SynchronizationContext.Current;
		}

		public SyncContextEventEntry(TEH handler) : base(handler)
		{
			this.syncContext = SynchronizationContext.Current;
		}

		public SyncContextEventEntry(IEventEntry<TEH> entry, SynchronizationContext syncContext) : base(entry)
		{
			this.syncContext = syncContext ?? SynchronizationContext.Current;
		}
		
		public SyncContextEventEntry(TEH handler, SynchronizationContext syncContext) : base(handler)
		{
			this.syncContext = syncContext ?? SynchronizationContext.Current;
		}

		public override bool Equals(IEventEntry other)
		{
			var objOther = other as SyncContextEventEntry<TEH>;
			return (objOther == null) ? false : Equals(objOther);
		}
		
		public bool Equals(SyncContextEventEntry<TEH> other)
		{
			//1. Make base comparison
			if (!base.Equals(other)) return false;
			
			//2. Compare SyncContexts
			return this.syncContext == other.syncContext;
		}

		protected override void SyncedInvoke(object sender, EventArgs e)
		{
		    //EndSetSyncContextFromSyncInvoke();
			var syncContext = this.syncContext;
			if (syncContext == null) {
				base.SyncedInvoke(sender, e);
			} else {
				//Invoke handler using SynchronizationContext
				var args = new object[] { sender, e };
				syncContext.Send(SyncContextSendOrPostCallback, args);
			}
		}

		private void SyncContextSendOrPostCallback(object arg)
		{
			var dlg = this.HandlerDelegate;
			if (dlg == null) return;
			var args = (object[])arg;
			try
			{
				dlg.DynamicInvoke(args);
			} catch (Exception ex) { //(TargetInvocationException ex) {
				throw TweakedEventInvocationException.From(ex);
			}
		}

		public override void Dispose()
		{
			base.Dispose();
			syncContext = null;
		}
	}
}
