﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Threading;
using System.Reflection;

namespace JpLabs.TweakedEvents.Synced
{
	[Obsolete("DispatcherEventEntry are not reliable. Use SyncContextEventEntry preferrably.")]
	internal sealed class DispatcherEventEntry<TEH> : SyncedEventEntry<TEH>, IEventEntry<TEH> where TEH : class
	{
		private Dispatcher dispatcher;

		public DispatcherEventEntry(IEventEntry<TEH> entry, Dispatcher dispatcher) : base(entry)
		{
			this.dispatcher = dispatcher;
		}
		
		public DispatcherEventEntry(TEH handler, Dispatcher dispatcher) : base(handler)
		{
			this.dispatcher = dispatcher;
		}

		public override bool Equals(IEventEntry other)
		{
			var objOther = other as DispatcherEventEntry<TEH>;
			return (objOther == null) ? false : Equals(objOther);
		}
		
		public bool Equals(DispatcherEventEntry<TEH> other)
		{
			//1. Make base comparison
			if (!base.Equals(other)) return false;
			
			//2. Compare dispatchers
			return this.dispatcher == other.dispatcher;
		}

		protected override void SyncedInvoke(object sender, EventArgs e)
		{
			var dlg = this.HandlerDelegate;
			if (dlg == null) return;

			var dispatcher = this.dispatcher;

		    if (dispatcher != null && !dispatcher.CheckAccess()) {
				try
				{
					dispatcher.Invoke(dlg, sender, e);
				} catch (Exception ex) {
					//TODO: test if this will throw a meaningfull exception
					throw TweakedEventInvocationException.From(ex, dlg);
				}
		    } else {
				base.SyncedInvoke(sender, e);
			}
		}

		public override void Dispose()
		{
			base.Dispose();
			dispatcher = null;
		}
	}
}
