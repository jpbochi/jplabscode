﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Threading;
using System.Reflection;

namespace JpLabs.TweakedEvents.Synced
{
	[Obsolete("DispatcherObjEventEntry are not reliable. Use SyncContextEventEntry preferrably.")]
	internal sealed class DispatcherObjEventEntry<TEH> : SyncedEventEntry<TEH>, IEventEntry<TEH> where TEH : class
	{
		private DispatcherObject dispatcherObj;

		public DispatcherObjEventEntry(IEventEntry<TEH> entry, DispatcherObject dispatcherObj) : base(entry)
		{
			this.dispatcherObj = dispatcherObj;
		}
		
		public DispatcherObjEventEntry(TEH handler, DispatcherObject dispatcherObj) : base(handler)
		{
			this.dispatcherObj = dispatcherObj;
		}

		public override bool Equals(IEventEntry other)
		{
			var objOther = other as DispatcherEventEntry<TEH>;
			return (objOther == null) ? false : Equals(objOther);
		}
		
		public bool Equals(DispatcherObjEventEntry<TEH> other)
		{
			//1. Make base comparison
			if (!base.Equals(other)) return false;
			
			//2. Compare dispatcher objects
			return this.dispatcherObj == other.dispatcherObj;
		}

		protected override void SyncedInvoke(object sender, EventArgs e)
		{
			var dlg = this.HandlerDelegate;
			if (dlg == null) return;

			var dispatcherObj = this.dispatcherObj;

		    if (dispatcherObj != null && !dispatcherObj.CheckAccess()) {
				try
				{
					dispatcherObj.Dispatcher.Invoke(dlg, sender, e);
				} catch (Exception ex) {
					//TODO: test if this will throw a meaningfull exception
					throw TweakedEventInvocationException.From(ex, dlg);
				}
		    } else {
				base.SyncedInvoke(sender, e);
			}
		}

		public override void Dispose()
		{
			base.Dispose();
			dispatcherObj = null;
		}
	}
}
