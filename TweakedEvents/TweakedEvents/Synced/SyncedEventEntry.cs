﻿using System;
using System.ComponentModel;
using System.Threading;
using TargetInvocationException = System.Reflection.TargetInvocationException;
using JpLabs.TweakedEvents.Synced;
using System.Windows.Threading;

namespace JpLabs.TweakedEvents.Synced
{
	/// TODO: The following class in being broken into subclasses. Missing tasks are:
	/// - WinForms support;
	/// - Better factory (must not propagate dependency on WPF of WinForms unless needed)
	/// - Clean up;

	/// <summary>
	/// This class in partially inspired in Microsoft.Win32.SystemEvents+SystemEventInvokeInfo
	/// <seealso cref="http://msdn.microsoft.com/en-us/magazine/cc163533.aspx"/>
	/// <seealso cref="http://msdn.microsoft.com/en-us/library/system.componentmodel.asyncoperationmanager.aspx"/>
	/// <seealso cref="http://msdn.microsoft.com/en-us/library/system.threading.synchronizationcontext.aspx"/>
	/// <seealso cref="http://www.codeproject.com/KB/threads/SynchronizationContext.aspx"/>
	/// </summary>
	public abstract class SyncedEventEntry<TEH> : BaseEventEntry<TEH>, IEventEntry<TEH> where TEH : class
	{
		private Delegate handlerDlg;
		private IEventEntry<TEH> eventEntry;
		//private volatile bool isDisposed;

		protected Delegate HandlerDelegate
		{
			get { return handlerDlg; }
		}

		protected SyncedEventEntry(IEventEntry<TEH> entry)
		{
		    if (entry == null) throw new ArgumentNullException("entry");

			//TODO: implement decoration!
			// - Problem: isDisposed state has to be propagated from decorated entry
			// - Solution1: always use an decorated entry (use an StrongEventEntry as default) (performance and debugging would be compromised)
			// - Solution2: compromise

			this.eventEntry = entry;
		    this.handlerDlg = (Delegate)(object)(EventHandler)EventHandlerProxy;
		}
		
		private void EventHandlerProxy(object sender, EventArgs e)
		{
			var entry = this.eventEntry;
			if (entry == null) return;
			
			bool shouldDispose;
			try
			{
				shouldDispose = entry.Invoke(sender, e);
			} catch (Exception ex) { //(TargetInvocationException ex) {
				throw TweakedEventInvocationException.From(ex);
			}

			if (shouldDispose) Dispose();
		}
		
		protected SyncedEventEntry(TEH handler)//, ISynchronizeInvoke syncInvoke)
		{
			this.handlerDlg = (Delegate)(object)handler;
			this.eventEntry = null;
			
			//if (syncInvoke == null) {
			//    //An ISynchronizeInvoke won't be used in this event entry
			//    this._syncInvoke = null;
				
			//    //Gets a current SynchronizationContext
			//    this._syncContext = SynchronizationContext.Current; //AsyncOperationManager.SynchronizationContext;
			//} else {
			//    this._syncInvoke = syncInvoke;
				
			//    //*
			//    this._syncContext = null;
			//    /*/
			//    //0. Temporarily, use the current SynchronizationContext, if any
			//    this._syncContext = SynchronizationContext.Current;

			//    //REMARK: if the syncInvoke thread is waiting for this call to end, a DEADLOCK could happen
			//    //In order to avoid this, a SynchronizationContext is get asynchronously
				
			//    #if !IGNORE_WIN_FORMS
			//        Control control = syncInvoke as Control;
			//        if (control != null) {
			//            //REMARK: If syncInvoke is a Windows.Forms.Control, the following exception must be avoided:
			//            //"Invoke or BeginInvoke cannot be called on a control until the window handle has been created"

			//            //So, we need to ensure that the Control handle was created before getting a SynchronizationContext
			//            //Since there is no way to lock while we test if it has a handle, the following code might appear weird

			//            //1. Sign up to the control's HandleCreated event
			//            control.HandleCreated += control_HandleCreated;

			//            //2. If the control has a handle => 
			//            if (control.IsHandleCreated) {
			//                //2.1. Remove the event handler from the HandleCreated event
			//                control.HandleCreated -= control_HandleCreated;

			//                //2.2. Use the control to get the SynchronizationContext on its own thread
			//                BeginSetSyncContextFromSyncInvoke(syncInvoke);
			//            }
			//            return;
			//        }
			//    #endif
				
			//    //Use syncInvoke to get the SynchronizationContext on its own thread
			//    BeginSetSyncContextFromSyncInvoke(syncInvoke);
			//    //*/
			//}
		}

		//public static SyncedEventEntry<TEH> CreateEntry(TEH handler, ISynchronizeInvoke syncInvoke)
		//{
		//    //if (syncInvoke == null) return CreateEntry(handler);
		//    return new SyncInvokeEventEntry<TEH>(handler, syncInvoke);
		//}

		//public static SyncedEventEntry<TEH> CreateEntry(TEH handler, SynchronizationContext syncContext)
		//{
		//    return new SyncContextEventEntry<TEH>(handler, syncContext);
		//}

		//public static SyncedEventEntry<TEH> CreateEntry(TEH handler, Dispatcher dispatcher)
		//{
		//    return new DispatcherEventEntry<TEH>(handler, dispatcher);
		//}

		//public static SyncedEventEntry<TEH> CreateEntry(TEH handler, DispatcherObject dispatcherObj)
		//{
		//    return new DispatcherObjEventEntry<TEH>(handler, dispatcherObj);
		//}

		//private void BeginSetSyncContextFromSyncInvoke(ISynchronizeInvoke syncInvoke)
		//{
		//    var setSyncContextFunc = (Func<ISynchronizeInvoke,SynchronizationContext>)GetSyncContextFromSyncInvoke;
		//    this._syncContextAsyncResult = setSyncContextFunc.BeginInvoke(syncInvoke, null, null);
		//}

		//private void EndSetSyncContextFromSyncInvoke()
		//{
		//    if (_syncContextAsyncResult != null) {
		//        //In order to avoid any possibility of deadlocks, only call EndInvoke after we know the call IsCompleted
		//        if (_syncContextAsyncResult.IsCompleted) {
		//            //Retrieve the delegate and call EndInvoke (see http://msdn.microsoft.com/en-us/library/2e08f6yc.aspx)
		//            var caller = (Func<ISynchronizeInvoke,SynchronizationContext>)((AsyncResult)_syncContextAsyncResult).AsyncDelegate;
		//            try {
		//                _syncContext = caller.EndInvoke(_syncContextAsyncResult);
		//            } finally {
		//                _syncContextAsyncResult = null;
		//            }
		//        }
		//    }
		//}

		//private static SynchronizationContext GetSyncContextFromSyncInvoke(ISynchronizeInvoke syncInvoke)
		//{
		//    return syncInvoke.Invoke<SynchronizationContext>(GetSynchronizationContext);
		//}

		//private static SynchronizationContext GetSynchronizationContext()
		//{
		//    //return AsyncOperationManager.SynchronizationContext;
		//    return SynchronizationContext.Current;
		//}

		//private void control_HandleCreated(object sender, EventArgs e)
		//{
		//    var control = (Control)sender;

		//    //Unregister event in order to loose reference
		//    control.HandleCreated -= control_HandleCreated;
			
		//    //Gets (or creates) a current SynchronizationContext
		//    this._syncContext = GetSynchronizationContext();
		//}

		//public override bool Equals(IEventEntry other)
		//{
		//    var objOther = other as SyncedEventEntry<TEH>;
		//    return (objOther == null) ? false : Equals(objOther);
		//}
		
		//public bool Equals(SyncedEventEntry<TEH> other)
		//{
		//    //1. Compare delegates
		//    if (this._delegate != other._delegate) return false;
			
		//    /*
		//    //2. Try to ensure that the SyncContext is present on both event entries
		//    this.EndSetSyncContextFromSyncInvoke();
		//    other.EndSetSyncContextFromSyncInvoke();
			
		//    //3. if available, compare SyncContexts. Else, compare SyncInvoke weak references
		//    if (this._syncContextAsyncResult == null || other._syncContextAsyncResult == null) { // 
		//        //SyncContexts are available, so use them for comparison
		//        return this._syncContext == other._syncContext;
		//    } else {
		//        //SyncContexts are not available yet, so use SyncInvoke objects for comparison
		//        return this._syncInvoke == other._syncInvoke;
		//    }
		//    /*/
			
		//    //2. Compare SyncContexts
		//    if (this._syncContext != other._syncContext) return false;
			
		//    //3. Compare SyncInvoke objects
		//    return this._syncInvoke == other._syncInvoke;
		//    //*/
		//}

		public override bool Equals(IEventEntry other)
		{
			var objOther = other as SyncedEventEntry<TEH>;
			return (objOther == null) ? false : Equals(objOther);
		}
		
		protected bool Equals(SyncedEventEntry<TEH> other)
		{
			var entry = eventEntry;
			if (entry != null) return other.Equals(entry);

			return (this.handlerDlg == other.handlerDlg);
		}

		public override bool EqualsHandler(TEH handler)
		{
			var entry = eventEntry;
			if (entry != null) return entry.EqualsHandler(handler);

			return handlerDlg == (Delegate)(object)handler;
		}
		
		public sealed override bool Invoke(object sender, EventArgs e)
		{
			SyncedInvoke(sender, e);
			
			return this.IsDisposed;
		}

		protected virtual void SyncedInvoke(object sender, EventArgs e)
		{
			var entry = eventEntry;
			if (entry != null) {
				if (entry.Invoke(sender, e)) Dispose();
			} else {
				var dlg = this.handlerDlg;
				if (dlg == null) return;

				try {
					dlg.DynamicInvoke(sender, e);
				} catch (TargetInvocationException ex) {
					throw TweakedEventInvocationException.From(ex, dlg);
				}
			}
		}

		//private void Invoke(params object[] args)
		//{
		//    try
		//    {
		//        //EndSetSyncContextFromSyncInvoke();
		//        if (_syncContext != null) {
		//            //Invoke handler using SynchronizationContext
		//            _syncContext.Send(SyncContextSendOrPostCallback, args);
		//            return;
		//        }
				
		//        if (_syncInvoke != null && _syncInvoke.InvokeRequired) {
		//            //Invoke handler using ISynchronizeInvoke
		//            _syncInvoke.Invoke(_delegate, args);
		//            return;
		//        }
		//    } catch (Exception ex) {
		//        throw CreateInvocationException(ex, _delegate);
		//    }
			
		//    try
		//    {
		//        //Invoke handler directly and synchronously
		//        _delegate.DynamicInvoke(args);
		//    } catch (TargetInvocationException ex) {
		//        throw CreateInvocationException(ex.InnerException, _delegate);
		//    }
		//}

		////TODO: REVIEW: This function is completely unnecessary! InvokeRequired property already does that!
		//private static bool CanUseInvokeOnFormsControl(ISynchronizeInvoke syncInvoke)
		//{
		//    Control control = syncInvoke as Control;
		//    return (control == null || control.IsHandleCreated);
		//}
		
		//private void SyncContextSendOrPostCallback(object arg)
		//{
		//    var args = (object[])arg;
		//    this._delegate.DynamicInvoke((object[]) arg);
		//}

		//protected Exception CreateInvocationException(Exception ex)
		//{
		//    return TweakedEvent.CreateInvocationException(ex, this.handlerDlg);
		//}
		
		public override void Dispose()
		{
			var entry = eventEntry;
			if (entry != null) entry.Dispose();
			
			//release any strong references
			handlerDlg = null;
			eventEntry = null;
		}

		public override bool IsDisposed
		{
			get {
				//TODO: verify if the next two lines are necessary
				//var entry = eventEntry;
				//if (entry != null) return entry.IsDisposed;
				
				return handlerDlg == null;
			}
		}
	}
}
