﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Reflection;

namespace JpLabs.TweakedEvents.Synced
{
	internal sealed class SyncInvokeEventEntry<TEH> : SyncedEventEntry<TEH>, IEventEntry<TEH> where TEH : class
	{
		private ISynchronizeInvoke syncInvoke;

		public SyncInvokeEventEntry(IEventEntry<TEH> entry, ISynchronizeInvoke syncInvoke) : base(entry)
		{
			this.syncInvoke = syncInvoke;
		}
		
		public SyncInvokeEventEntry(TEH handler, ISynchronizeInvoke syncInvoke) : base(handler)
		{
			this.syncInvoke = syncInvoke;
		}

		public override bool Equals(IEventEntry other)
		{
			var objOther = other as SyncInvokeEventEntry<TEH>;
			return (objOther == null) ? false : Equals(objOther);
		}
		
		public bool Equals(SyncInvokeEventEntry<TEH> other)
		{
			//1. Make base comparison
			if (!base.Equals(other)) return false;
			
			//2. Compare SyncInvoke objects
			return this.syncInvoke == other.syncInvoke;
		}

		protected override void SyncedInvoke(object sender, EventArgs e)
		{
			var dlg = this.HandlerDelegate;
			if (dlg == null) return;

			var syncInvoke = this.syncInvoke;

		    if (syncInvoke != null && syncInvoke.InvokeRequired) {
				try
				{
					syncInvoke.Invoke(dlg, new [] {sender, e});
					//syncInvoke.Invoke((Action<Delegate,object,EventArgs>)InvokeCallback, new [] {dlg, sender, e});
				} catch (Exception ex) {
					throw TweakedEventInvocationException.From(ex, dlg);
				}
		    } else {
				base.SyncedInvoke(sender, e);
			}
		}

		//private void InvokeCallback(Delegate handlerFunc, object sender, EventArgs e)
		//{
		//    try
		//    {
		//        handlerFunc.DynamicInvoke(sender, e);
		//    } catch (Exception ex) {
		//        throw ex.WrapAsTweakedEventInvocationException(handlerFunc);
		//    }
		//}

		public override void Dispose()
		{
			base.Dispose();
			syncInvoke = null;
		}
	}
}
