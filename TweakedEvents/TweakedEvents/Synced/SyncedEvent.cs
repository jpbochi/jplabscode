﻿using System;
using System.ComponentModel;
using JpLabs.TweakedEvents.Synced;
using System.Threading;
using System.Windows.Threading;

namespace JpLabs.TweakedEvents
{
	public sealed class SyncedEvent<TEH> : TweakedEvent<TEH> where TEH : class
	{
		// Members

		/// <summary>
		/// Supported types are <code>ISynchronizeInvoke</code> and <code>SynchronizationContext</code>
		/// </summary>
		public object SyncronizerObject { get; set; }

		public bool RestrictEventEntryType { get; set; }
		
		// Contructors
		public SyncedEvent() : base ()
		{}
		
		private SyncedEvent(SyncedEvent<TEH> other, IEventEntry<TEH>[] entries) : base(entries) //IList<IEventEntry<TEH>> entries) : base(entries)
		{
			this.SyncronizerObject = other.SyncronizerObject;
			this.RestrictEventEntryType = other.RestrictEventEntryType;
		}
		
		// Overridden Methods
		//protected override ITweakedEvent<TEH> CreateNew(IList<IEventEntry<TEH>> entries)
		protected override ITweakedEvent<TEH> CreateNew(IEventEntry<TEH>[] entries)
		{
			return new SyncedEvent<TEH>(this, entries);
		}
		
		public override IEventEntry<TEH> CreateEntry(TEH handler)
		{
			//return new SyncedEventEntry<TEH>(handler, this.SyncInvoke);
			return CreateEntry(handler, this.SyncronizerObject);
		}

		/// <summary>
		/// Supported syncObj types are: <code>ISynchronizeInvoke</code>, <code>SynchronizationContext</code>, <code>Dispatcher</code>, and <code>DispatcherObject</code>
		/// A null syncObj is treated as SynchronizationContext.Current
		/// </summary>
		public static SyncedEventEntry<TEH> CreateEntry(IEventEntry<TEH> entry, object syncObj)
		{
			if (entry == null) throw new ArgumentNullException("entry");

			if (syncObj == null) return new SyncContextEventEntry<TEH>(entry);//throw new ArgumentNullException("syncObj");

			if (syncObj is ISynchronizeInvoke)		return new SyncInvokeEventEntry<TEH>(entry, (ISynchronizeInvoke)syncObj);
			if (syncObj is SynchronizationContext)	return new SyncContextEventEntry<TEH>(entry, (SynchronizationContext)syncObj);
			//if (syncObj is Dispatcher)				return new DispatcherEventEntry<TEH>(entry, (Dispatcher)syncObj);
			//if (syncObj is DispatcherObject)		return new DispatcherObjEventEntry<TEH>(entry, (DispatcherObject)syncObj);

			throw new ArgumentException("syncObj argument is not of a supported type", "syncObj");
		}

		/// <summary>
		/// Supported syncObj types are: <code>ISynchronizeInvoke</code>, <code>SynchronizationContext</code>, <code>Dispatcher</code>, and <code>DispatcherObject</code>
		/// A null syncObj is treated as SynchronizationContext.Current
		/// </summary>
		public static SyncedEventEntry<TEH> CreateEntry(TEH handler, object syncObj)
		{
			if (handler == null) throw new ArgumentNullException("handler");

			if (syncObj == null) return new SyncContextEventEntry<TEH>(handler);//throw new ArgumentNullException("syncObj");

			if (syncObj is ISynchronizeInvoke)		return new SyncInvokeEventEntry<TEH>(handler, (ISynchronizeInvoke)syncObj);
			if (syncObj is SynchronizationContext)	return new SyncContextEventEntry<TEH>(handler, (SynchronizationContext)syncObj);
			//if (syncObj is Dispatcher)				return new DispatcherEventEntry<TEH>(handler, (Dispatcher)syncObj);
			//if (syncObj is DispatcherObject)		return new DispatcherObjEventEntry<TEH>(handler, (DispatcherObject)syncObj);

			throw new ArgumentException("syncObj argument is not of a supported type", "syncObj");
		}

		public override ITweakedEvent<TEH> Combine(IEventEntry<TEH> entry)
		{
			if (RestrictEventEntryType && !(entry is SyncedEventEntry<TEH>)) {
				throw new NotSupportedException("Only entries of type SyncedEventEntry<> are supported by this custom event");
			}
			return base.Combine(entry);
		}
	}
}
