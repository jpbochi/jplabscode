﻿using System;
using System.ComponentModel;
using System.Threading;

namespace JpLabs.TweakedEvents
{
	/// <summary>
	/// It's similar to SyncedEventEntry, but it doesn't keep strong references.
	/// Be aware that this custom event can NOT be used with non-static anonymous delegates. A runtime exception will be thrown if you do it.
	/// </summary>
	public abstract class EventEntryDecorator<TEH> : BaseEventEntry<TEH> where TEH : class
	{
		private BaseEventEntry<TEH> decoratedEntry;

		public EventEntryDecorator(BaseEventEntry<TEH> eventEntry)
		{
			if (eventEntry == null) throw new ArgumentNullException("eventEntry");

			this.decoratedEntry = eventEntry;
		}

		protected BaseEventEntry<TEH> DecoratedEntry
		{
			get { return decoratedEntry; }
		}

		public override bool Equals(IEventEntry other)
		{
			var objOtherAsDecorator = other as EventEntryDecorator<TEH>;

			if (objOtherAsDecorator != null) return decoratedEntry.Equals(objOtherAsDecorator.decoratedEntry);
			
			return decoratedEntry.Equals(other);
		}

		public override bool EqualsHandler(TEH handler)
		{
			return decoratedEntry.EqualsHandler(handler);
		}

		public override bool Invoke(object sender, EventArgs e)
		{
			return decoratedEntry.Invoke(sender, e);
		}

		public override void Dispose()
		{
			decoratedEntry.Dispose();
		}

		public override bool IsDisposed
		{
			get {
				return decoratedEntry.IsDisposed;
			}
		}
	}
}
