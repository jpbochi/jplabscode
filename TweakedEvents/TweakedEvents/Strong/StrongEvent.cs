﻿
namespace JpLabs.TweakedEvents
{
	internal sealed class StrongEvent<TEH> : TweakedEvent<TEH> where TEH : class
	{
		// Static methods / members
		public static readonly TweakedEvent<TEH> Empty = new StrongEvent<TEH>();
		
		// Constructors
		private StrongEvent()
		{}

		internal StrongEvent(IEventEntry<TEH>[] entries) : base(entries)
		{}
		
		// Overridden methods
		protected override ITweakedEvent<TEH> CreateNew(IEventEntry<TEH>[] entries)
		{
			//Empty StrongEvents are converted to {null}
			if (entries == null || entries.Length == 0) return null;
			
			return new StrongEvent<TEH>(entries);
		}
		
		public override IEventEntry<TEH> CreateEntry(TEH handler)
		{
			return new StrongEventEntry<TEH>(handler);
		}
	}
}
