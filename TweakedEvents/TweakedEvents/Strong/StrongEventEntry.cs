﻿using System;
using System.Reflection;

namespace JpLabs.TweakedEvents
{
	internal sealed class StrongEventEntry<TEH> : BaseEventEntry<TEH> where TEH : class
	{
		private Delegate handlerDlg;

		public StrongEventEntry(TEH handler)
		{
			this.handlerDlg = (Delegate)(object)handler;
		}
		
		public override bool Invoke(object sender, EventArgs e)
		{
			DynamicInvoke(sender, e);
			
			return IsDisposed;
		}

		private void DynamicInvoke(object sender, EventArgs e)
		{
			var dlg = this.handlerDlg;
			if (dlg == null) return;

			try {
				dlg.DynamicInvoke(sender, e);
			} catch (TargetInvocationException ex) {
			    throw TweakedEventInvocationException.From(ex, dlg);
			}
		}

		public override bool Equals(IEventEntry other)
		{
			var objOther = other as StrongEventEntry<TEH>;
			return (objOther == null) ? false : Equals(objOther);
		}
		
		public bool Equals(StrongEventEntry<TEH> other)
		{
			return handlerDlg == other.handlerDlg;
		}
		
		public override bool EqualsHandler(TEH handler)
		{
			//Cast handler to a Delegate
			Delegate dlg = (Delegate)(object)handler;
			
			return handlerDlg == dlg;
		}

		public override void Dispose()
		{
			handlerDlg = null;
		}

		public override bool IsDisposed
		{
			get { return handlerDlg == null; }
		}
	}
}
