﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Threading;
using System.Reflection;
using System.Text;
using JpLabs.TweakedEvents.Synced;
using JpLabs.TweakedEvents.Weak;

namespace JpLabs.TweakedEvents
{
	public static class TweakedEvent
	{
		public static BaseEventEntry<EventHandler> ToSynced(object syncObj, EventHandler handler)
		{
			return SyncedEvent<EventHandler>.CreateEntry(handler, syncObj);
		}

		public static BaseEventEntry<EventHandler<T>> ToSynced<T>(object syncObj, EventHandler<T> handler) where T : EventArgs
		{
			return SyncedEvent<EventHandler<T>>.CreateEntry(handler, syncObj);
		}

		public static BaseEventEntry<TEH> ToSynced<TEH>(object syncObj, TEH handler) where TEH : class
		{
			return SyncedEvent<TEH>.CreateEntry(handler, syncObj);
		}


		public static BaseEventEntry<EventHandler> ToWeak(EventHandler handler)
		{
		    return new WeakEventEntry<EventHandler>(handler);
		}

		public static BaseEventEntry<EventHandler<T>> ToWeak<T>(EventHandler<T> handler) where T : EventArgs
		{
			return new WeakEventEntry<EventHandler<T>>(handler);
		}

		public static BaseEventEntry<TEH> ToWeak<TEH>(TEH handler) where TEH : class
		{
			return new WeakEventEntry<TEH>(handler);
		}


		public static BaseEventEntry<EventHandler> ToWeakSynced(object syncObj, EventHandler handler)
		{
			//return new WeakSyncedEventEntry<EventHandler>(handler, syncObj);
			IEventEntry<EventHandler> weakEvent = new WeakEventEntry<EventHandler>(handler);
			return SyncedEvent<EventHandler>.CreateEntry(weakEvent, syncObj);
		}

		public static BaseEventEntry<EventHandler<T>> ToWeakSynced<T>(object syncObj, EventHandler<T> handler) where T : EventArgs
		{
			//return new WeakSyncedEventEntry<EventHandler<T>>(handler, syncObj);
			IEventEntry<EventHandler<T>> weakEvent = new WeakEventEntry<EventHandler<T>>(handler);
			return SyncedEvent<EventHandler<T>>.CreateEntry(weakEvent, syncObj);
		}

		public static BaseEventEntry<TEH> ToWeakSynced<TEH>(object syncObj, TEH handler) where TEH : class
		{
			//return new WeakSyncedEventEntry<TEH>(handler, syncObj);
			IEventEntry<TEH> weakEvent = new WeakEventEntry<TEH>(handler);
			return SyncedEvent<TEH>.CreateEntry(weakEvent, syncObj);
		}


		public static BaseEventEntry<EventHandler> ToDisposable(EventHandler handler)
		{
			return new StrongEventEntry<EventHandler>(handler);
		}

		public static BaseEventEntry<EventHandler<T>> ToDisposable<T>(EventHandler<T> handler) where T : EventArgs
		{
			return new StrongEventEntry<EventHandler<T>>(handler);
		}

		public static BaseEventEntry<TEH> ToDisposable<TEH>(TEH handler) where TEH : class
		{
			return new StrongEventEntry<TEH>(handler);
		}

		
		public static void Raise(this ITweakedEvent twEvent, object sender, EventArgs e)
		{
			#if xDEBUG
			try {
			#endif
				if (twEvent != null) twEvent.Raise(sender, e);
			#if xDEBUG
			} catch (Exception ex) {
				System.Diagnostics.Debug.WriteLine("-- // --");
				System.Diagnostics.Debug.WriteLine(ex);
				System.Diagnostics.Debug.WriteLine("-- // --");
				throw;
			}
			#endif
		}

		static internal TweakedEvent<TEH> Add<TEH>(this TweakedEvent<TEH> twEvent, TEH handler) where TEH : class
		{
			//Default to a StrongEvent
			if (twEvent == null) twEvent = StrongEvent<TEH>.Empty;
			
			//Combine with handler
		    return (TweakedEvent<TEH>)twEvent.Combine(handler);
		}

		static internal TweakedEvent<TEH> Remove<TEH>(this TweakedEvent<TEH> twEvent, TEH handler) where TEH : class
		{
			//Default to a StrongEvent
			if (twEvent == null) twEvent = StrongEvent<TEH>.Empty;
			
			//Take handler off
		    return (TweakedEvent<TEH>)twEvent.Subtract(handler);
		}

		static internal TweakedEvent<TEH> Add<TEH>(this TweakedEvent<TEH> twEvent, IEventEntry<TEH> entry) where TEH : class
		{
			//Default to a StrongEvent
			if (twEvent == null) twEvent = StrongEvent<TEH>.Empty;
			
			//Combine with entry
		    return (TweakedEvent<TEH>)twEvent.Combine(entry);
		}

		static internal TweakedEvent<TEH> Remove<TEH>(this TweakedEvent<TEH> twEvent, IEventEntry<TEH> entry) where TEH : class
		{
			//Default to a StrongEvent
			if (twEvent == null) twEvent = StrongEvent<TEH>.Empty;
			
			//Take entry off
		    return (TweakedEvent<TEH>)twEvent.Subtract(entry);
		}

		public static TweakedEvent<TEH> Add<TEH>(ref TweakedEvent<TEH> refTweakedEvent, TEH handler) where TEH : class
		{
			TweakedEvent<TEH> valueBeforeCombine;
			TweakedEvent<TEH> valueBeforeAttribution = refTweakedEvent;
			do
			{
				valueBeforeCombine = valueBeforeAttribution;
				var newValue = valueBeforeCombine.Add(handler);
				valueBeforeAttribution = Interlocked.CompareExchange(ref refTweakedEvent, newValue, valueBeforeCombine);
			}
			while (valueBeforeAttribution != valueBeforeCombine);
			
			return refTweakedEvent;
		}

		public static TweakedEvent<TEH> Remove<TEH>(ref TweakedEvent<TEH> refTweakedEvent, TEH handler) where TEH : class
		{
			TweakedEvent<TEH> valueBeforeRemove;
			TweakedEvent<TEH> valueBeforeAttribution = refTweakedEvent;
			do
			{
				valueBeforeRemove = valueBeforeAttribution;
				var newValue = valueBeforeRemove.Remove(handler);
				valueBeforeAttribution = Interlocked.CompareExchange(ref refTweakedEvent, newValue, valueBeforeRemove);
			}
			while (valueBeforeAttribution != valueBeforeRemove);
			
			return refTweakedEvent;
		}

		public static TweakedEvent<TEH> Add<TEH>(ref TweakedEvent<TEH> refTweakedEvent, IEventEntry<TEH> entry) where TEH : class
		{
			TweakedEvent<TEH> valueBeforeCombine;
			TweakedEvent<TEH> valueBeforeAttribution = refTweakedEvent;
			do
			{
				valueBeforeCombine = valueBeforeAttribution;
				var newValue = valueBeforeCombine.Add(entry);
				valueBeforeAttribution = Interlocked.CompareExchange(ref refTweakedEvent, newValue, valueBeforeCombine);
			}
			while (valueBeforeAttribution != valueBeforeCombine);
			
			return refTweakedEvent;
		}

		public static TweakedEvent<TEH> Remove<TEH>(ref TweakedEvent<TEH> refTweakedEvent, IEventEntry<TEH> entry) where TEH : class
		{
			TweakedEvent<TEH> valueBeforeRemove;
			TweakedEvent<TEH> valueBeforeAttribution = refTweakedEvent;
			do
			{
				valueBeforeRemove = valueBeforeAttribution;
				var newValue = valueBeforeRemove.Remove(entry);
				valueBeforeAttribution = Interlocked.CompareExchange(ref refTweakedEvent, newValue, valueBeforeRemove);
			}
			while (valueBeforeAttribution != valueBeforeRemove);
			
			return refTweakedEvent;
		}

		public static TweakedEvent<TEH> Mutate<TEH>(ref TweakedEvent<TEH> refTweakedEvent, Func<TweakedEvent<TEH>,TweakedEvent<TEH>> mutateFunc) where TEH : class
		{
			TweakedEvent<TEH> valueBeforeMutation;
			TweakedEvent<TEH> valueBeforeAttribution = refTweakedEvent;
			do
			{
				valueBeforeMutation = valueBeforeAttribution;
				var newValue = mutateFunc(valueBeforeMutation);
				valueBeforeAttribution = Interlocked.CompareExchange(ref refTweakedEvent, newValue, valueBeforeMutation);
			}
			while (valueBeforeAttribution != valueBeforeMutation);
			
			return refTweakedEvent;
		}
	}
}
