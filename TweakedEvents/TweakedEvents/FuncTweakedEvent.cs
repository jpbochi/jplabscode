﻿using System;

namespace JpLabs.TweakedEvents
{
	/// <summary>
	/// This is a generic event holder
	/// It has a default factory to generate event entries, but can hold any kind of event entries
	/// </summary>
	/// <typeparam name="TEH">An Delegate similar to an System.EventHandler</typeparam>
	public sealed class FuncTweakedEvent<TEH> : TweakedEvent<TEH> where TEH : class
	{
		// Members
		private Func<TEH,IEventEntry<TEH>> _funcCreateEntry;
		
		// Constructors
		public FuncTweakedEvent(Func<TEH,IEventEntry<TEH>> funcCreateEntry)
		{
			if (funcCreateEntry == null) throw new ArgumentNullException("funcCreateEntry");
			
			this._funcCreateEntry = funcCreateEntry;
		}

		private FuncTweakedEvent(FuncTweakedEvent<TEH> other, IEventEntry<TEH>[] entries) : base(entries) //IList<IEventEntry<TEH>> entries) : base(entries)
		{
			this._funcCreateEntry = other._funcCreateEntry;
		}
		
		// Overridden methods
		//protected override ITweakedEvent<TEH> CreateNew(IList<IEventEntry<TEH>> entries)
		protected override ITweakedEvent<TEH> CreateNew(IEventEntry<TEH>[] entries)
		{
			return new FuncTweakedEvent<TEH>(this, entries);
		}
		
		public override IEventEntry<TEH> CreateEntry(TEH handler)
		{
			return _funcCreateEntry(handler);
		}
	}
}
