using System;
using System.Reflection;
using System.Reflection.Emit;
using System.Collections.Generic;
using System.Text;
using System.Runtime.CompilerServices;

namespace JpLabs.DynamicMethods
{
	public delegate void EventHandlerForwarder(object target, object sender, EventArgs e);
	
	public class DynamicEventHandler
	{	
		private DynamicMethodDelegate dynamicMethod;
		
		private DynamicEventHandler(MethodInfo method)
		{
			dynamicMethod = DynamicMethodCompiler.CreateMethod(method);
		}
		
		private void DynamicInvoke(object target, object sender, EventArgs e)
		{
			dynamicMethod(target, new object[]{sender, e}); 
		}

		public static EventHandlerForwarder CreateDelegate(MethodInfo method)
		{
			//if (method.IsStatic) {
			//    ///http://www.devx.com/vb2themax/Tip/18827
			//    return (EventHandler)Delegate.CreateDelegate(typeof(EventHandler), method);
			//} else {
			    ///http://www.codeproject.com/csharp/dynamicmethoddelegates.asp
			    return (EventHandlerForwarder)(new DynamicEventHandler(method).DynamicInvoke);
			//}
		}
	}

	public static class DynamicEventHandlerProvider
	{
		static readonly Type[] eventHandlerParameters = { typeof(object), typeof(EventArgs) };
		static readonly Dictionary<MethodInfo, EventHandlerForwarder> dynHandlers = new Dictionary<MethodInfo, EventHandlerForwarder>();
		
		internal static EventHandlerForwarder GetHandler(MethodInfo method)
		{
			lock (forwarders) {
				EventHandlerForwarder d;
				if (dynHandlers.TryGetValue(method, out d)) return d;
			}
			
			//if (method.DeclaringType.GetCustomAttributes(typeof(CompilerGeneratedAttribute), false).Length != 0) {
			//    throw new ArgumentException("Cannot create weak event to anonymous method with closure.");
			//}
			
			DynamicMethod dm = new DynamicMethod("EventHandler", typeof(bool), eventHandlerParameters, method.DeclaringType);
			
			EventHandlerForwarder dynamicHandler = DynamicEventHandler.CreateDelegate(method);
			
			lock (forwarders) {
				forwarders[method] = fd;
			}
			return fd;
		}
	}
}
