﻿using System;
using System.ComponentModel;
using System.Threading;
using JpLabs.TweakedEvents.Weak;

namespace JpLabs.TweakedEvents
{
	/// <summary>
	/// It's similar to SyncedEventEntry, but it doesn't keep strong references.
	/// Be aware that this custom event can NOT be used with non-static anonymous delegates. A runtime exception will be thrown if you do it.
	/// [BUG] The syncedEntry will keep an strong reference to the handler. This event entry does not work!
	/// </summary>
	[Obsolete("Use an SyncedEventEntry with an decorated WeakEventEntry instead", true)]
	public sealed class WeakSyncedEventEntry<TEH> : BaseEventEntry<TEH> where TEH : class
	{
		private WeakEventForwarderProvider.ForwarderDelegate _forwarder;
		private SinglecastWeakDelegate _delegateRef;
		private WeakReference _syncInvokeRef;
		private SynchronizationContext _syncContext;

		private volatile bool _isDisposed;

		public WeakSyncedEventEntry(TEH handler)
			: this(handler, null)
		{ }

		public WeakSyncedEventEntry(TEH handler, ISynchronizeInvoke syncInvoke)
		{
			if (handler == null) throw new ArgumentNullException("handler");
			
			Delegate dlg = (Delegate)(object)handler;
			this._delegateRef = new SinglecastWeakDelegate(dlg);			
			this._forwarder = WeakEventForwarderProvider.GetForwarder(dlg.Method);

			if (syncInvoke == null) {
				//An ISynchronizeInvoke won't be used in this event entry
				this._syncInvokeRef = null;

				//Gets the current SynchronizationContext, if any
				this._syncContext = SynchronizationContext.Current;
			} else {
				this._syncInvokeRef = new WeakReference(syncInvoke);

				this._syncContext = null;
			}
		}

		public override bool Equals(IEventEntry other)
		{
			var objOther = other as WeakSyncedEventEntry<TEH>;
			return (objOther == null) ? false : Equals(objOther);
		}

		public bool Equals(WeakSyncedEventEntry<TEH> other)
		{
			//1. Compare delegates
			if (this._delegateRef != other._delegateRef) return false;

			//2. Compare SyncContexts
			if (this._syncContext != other._syncContext) return false;
			
			//3. Compare SyncInvoke weak references
			return this._syncInvokeRef.WeakRefEquals(other._syncInvokeRef);
		}

		public override bool EqualsHandler(TEH handler)
		{
			//Cast handler to a Delegate
			Delegate dlg = (Delegate)(object)handler;
			
			//Compare delegates (this._syncInvokeRef is ignored here)
			return this._delegateRef.Equals(dlg);
		}

		public override bool Invoke(object sender, EventArgs e)
		{
			if (!_isDisposed) if (InvokeInternal(sender, e)) Dispose();

			return _isDisposed;
		}

		private bool InvokeInternal(object sender, EventArgs e)
		{
			if (_syncContext != null) {
				//Invoke handler using SynchronizationContext
			    _syncContext.Send(SyncContextSendOrPostCallback, new object[] { sender, e });
			    return _isDisposed;//ignore value returned from forwarder
			} else {
				ISynchronizeInvoke syncInvoke = _syncInvokeRef.GetValueOrNull<ISynchronizeInvoke>();

				if (syncInvoke != null && syncInvoke.InvokeRequired) {
					//Invoke forwarder using ISynchronizeInvoke
					var invokeForwarder = (Func<object,EventArgs,bool>)InvokeForwarder;//(Func<object[],bool>)InvokeForwarder;
					return (bool)syncInvoke.Invoke(invokeForwarder, new object[] { sender, e });
				} else {
					//Invoke forwarder directly
					return CallForwarder(sender, e);
				}
			}
		}

		private bool CallForwarder(object sender, EventArgs e)
		{
			return _forwarder(_delegateRef.TargetReference, sender, e);
		}

		private bool InvokeForwarder(object sender, EventArgs e)//object[] args)
		{
			return _forwarder(_delegateRef.TargetReference, sender, e);//args[0], (EventArgs)args[1]);
		}

		private void SyncContextSendOrPostCallback(object state)
		{
			var args = (object[])state;
			if (_forwarder(_delegateRef.TargetReference, args[0], (EventArgs)args[1])) Dispose();
		}

		public override void Dispose()
		{
			_isDisposed = true;

			//release any strong references
			_syncContext = null;
			_syncInvokeRef = null;
			_delegateRef = default(SinglecastWeakDelegate);
			_forwarder = null;
		}

		public override bool IsDisposed
		{
			get {
				if (!_isDisposed && _delegateRef.IsDead()) Dispose();
				return _isDisposed;
			}
		}
	}
}
