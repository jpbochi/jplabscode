﻿using System;
using System.ComponentModel;
using JpLabs.TweakedEvents.Synced;
using JpLabs.TweakedEvents.Weak;

namespace JpLabs.TweakedEvents
{
	public sealed class WeakSyncedEvent<TEH> : TweakedEvent<TEH> where TEH : class
	{
		//Members
		public object SyncronizerObject { get; set; }

		public bool RestrictEventEntryType { get; set; }
		
		//Contructors
		public WeakSyncedEvent() : base ()
		{}
		
		private WeakSyncedEvent(WeakSyncedEvent<TEH> other, IEventEntry<TEH>[] entries) : base(entries)
		{
			this.SyncronizerObject = other.SyncronizerObject;
			this.RestrictEventEntryType = other.RestrictEventEntryType;
		}
		
		// Overridden Methods
		protected override ITweakedEvent<TEH> CreateNew(IEventEntry<TEH>[] entries)
		{
			return new WeakSyncedEvent<TEH>(this, entries);
		}
		
		public override IEventEntry<TEH> CreateEntry(TEH handler)
		{
			IEventEntry<TEH> weakEvent = new WeakEventEntry<TEH>(handler);
			return SyncedEvent<TEH>.CreateEntry(weakEvent, this.SyncronizerObject);
		}

		// Other Methods
		public override ITweakedEvent<TEH> Combine(IEventEntry<TEH> entry)
		{
			if (RestrictEventEntryType && !(entry is SyncedEventEntry<TEH>)) {
				throw new NotSupportedException("Only entries of type SyncedEventEntry<> are supported by this custom event");
			}
			return base.Combine(entry);
		}
	}
}
