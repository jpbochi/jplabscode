﻿using System;

namespace JpLabs.TweakedEvents.Weak
{
	public sealed class WeakEventEntry<TEH> : BaseEventEntry<TEH> where TEH : class
	{
		private WeakEventForwarderProvider.ForwarderDelegate forwarder;
		private SinglecastWeakDelegate delegateRef;

		public WeakEventEntry(TEH handler)
		{
			if (handler == null) {
				this.delegateRef = null;
				this.forwarder = null;
			} else {
				Delegate dlg = (Delegate)(object)handler;
				this.delegateRef = new SinglecastWeakDelegate(dlg);
				this.forwarder = WeakEventForwarderProvider.GetForwarder(dlg.Method);
			}
		}
		
		public override bool Invoke(object sender, EventArgs e)
		{
			var forwarder = this.forwarder;
			var delegateRef = this.delegateRef;

			if (forwarder != null && delegateRef != null) InnerInvoke(forwarder, delegateRef, sender, e);				
			
			return IsDisposed;
		}

		private void InnerInvoke(WeakEventForwarderProvider.ForwarderDelegate forwarder, SinglecastWeakDelegate delegateRef, object sender, EventArgs e)
		{
			bool shouldDispose;
			try
			{
				shouldDispose = forwarder(delegateRef.TargetReference, sender, e);
			} catch (Exception ex) {
				throw TweakedEventInvocationException.From(ex, delegateRef.TargetMethod);
			}

			if (shouldDispose) Dispose();
		}

		public override bool Equals(IEventEntry other)
		{
			var objOther = other as WeakEventEntry<TEH>;
			return (objOther == null) ? false : Equals(objOther);
		}
		
		public bool Equals(WeakEventEntry<TEH> other)
		{
			return delegateRef == other.delegateRef;
		}
		
		public override bool EqualsHandler(TEH handler)
		{
			//Cast handler to a Delegate
			Delegate dlg = (Delegate)(object)handler;
			
			return delegateRef.Equals(dlg);
		}

		public override void Dispose()
		{
			forwarder = null;
			delegateRef = null;
		}

		public override bool IsDisposed
		{
			get {
				var dlgRef = delegateRef;
				if (dlgRef != null && dlgRef.IsDead()) {
					Dispose();
					return true;
				}

				return dlgRef == null;
			}
		}
	}
}
