﻿//#define USE_REFLECTION_EMIT

using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Reflection;

#if (USE_REFLECTION_EMIT)
using System.Reflection.Emit;
using System.Runtime.CompilerServices;
#endif

namespace JpLabs.TweakedEvents.Weak
{
	/// <summary>
	/// The forwarder-generating code is in a separate class because it does not depend on type T.
	/// </summary>
	internal static class WeakEventForwarderProvider
	{
		internal delegate bool ForwarderDelegate(WeakReference wr, object sender, EventArgs e);
		
		private static class CachedExpressions
		{
			/// <summary>"WeakReference wr"</summary>
			public static readonly ParameterExpression WRParameter		= Expression.Parameter(typeof(WeakReference), "wr");
			/// <summary>"object sender"</summary>
			public static readonly ParameterExpression SenderParameter	= Expression.Parameter(typeof(object), "sender");
			/// <summary>"EventArgs e"</summary>
			public static readonly ParameterExpression EvArgsParameter	= Expression.Parameter(typeof(EventArgs), "e");
			
			/// <summary>"wr.Target"</summary>
			public static readonly MemberExpression WR_Target			= Expression.Property(WRParameter, typeof(WeakReference).GetMethod("get_Target"));
			/// <summary>"wr.Target == null"</summary>
			public static readonly BinaryExpression WR_Target_Equal_Null= Expression.Equal(WR_Target, Expression.Constant(null));
		}

		//private static readonly MethodInfo WeakReference_GetTarget = typeof(WeakReference).GetMethod("get_Target");
		private static readonly MethodInfo ThrowArgumentNullException_Info;
		private static readonly MethodInfo VoidThrowArgumentNullException_Info;
		
		private static readonly Dictionary<MethodInfo, ForwarderDelegate> Forwarders = new Dictionary<MethodInfo, ForwarderDelegate>();
					
		#if (USE_REFLECTION_EMIT)
		private static readonly Type[] ForwarderDlg_ParameterTypes = { typeof(WeakReference), typeof(object), typeof(EventArgs) };
		#endif
		
		static WeakEventForwarderProvider()
		{
			BindingFlags staticNonPublic = BindingFlags.Static | BindingFlags.NonPublic;
			ThrowArgumentNullException_Info = typeof(WeakEventForwarderProvider).GetMethod("ThrowArgumentNullException", staticNonPublic);
			VoidThrowArgumentNullException_Info = typeof(WeakEventForwarderProvider).GetMethod("VoidThrowArgumentNullException", staticNonPublic);
		}

		internal static ForwarderDelegate GetForwarder(MethodInfo method)
		{
			lock (Forwarders) {
				ForwarderDelegate d;
				if (Forwarders.TryGetValue(method, out d)) return d;
				
				d = CompileForwarder(method);
				Forwarders[method] = d;
				return d;
			}
		}
		
		/// <summary>
		/// This function dinamically creates a function that forwards calls to a specified method
		/// </summary>
		/// <param name="eventHandlerMethod">A method to which the calls should be forwarded to</param>
		/// <remarks>
		/// This functions assumes that the provided MethodInfo is from a method like: void Handler(object sender, EventArgs e)
		/// </remarks>
		private static ForwarderDelegate CompileForwarder(MethodInfo eventHandlerMethod)
		{
			if (eventHandlerMethod.DeclaringType.IsDefined(typeof(System.Runtime.CompilerServices.CompilerGeneratedAttribute), false)) {
				throw new ArgumentException("Cannot create weak event to anonymous method with closure.");
			}
			
			#if (!USE_REFLECTION_EMIT)
			/// Lamba Expression style
			/// 
			///	The created expression looks like this:
			///	- Static version:
			///		(WeakReference wr, object sender, EventArgs e) =>
			///		(
			///			OwnerClass.StaticHandlerMethod(sender, (TEventHandler)e) is Object
			///		)
			///	- Non-static version:
			///		(WeakReference wr, object sender, EventArgs e) =>
			///		(
			///			//(wr == null) ? VoidThrowArgumentNullException("wr") :
			///			(wr.Target == null) ? true :
			///			((OwnerClass)wr.Target).HandlerMethod(sender, (TEventHandler)e) is Object
			///		)
			///	
			///	Performance results:
			///	- Execution: about 15% faster to execute than DynamicMethod + IL Generator (~10% faster with null-parameter validation)
			///	- Compilation: no significant difference was perceived
			
			//Create parameter expressions: WeakReference wr, object sender, EventArgs e
			ParameterExpression wrParameter = CachedExpressions.WRParameter;
			ParameterExpression senderParameter = CachedExpressions.SenderParameter;
			ParameterExpression evArgsParameter = CachedExpressions.EvArgsParameter;
			
			var parameters = eventHandlerMethod.GetParameters();
			
			//Cast the 'sender' parameter to the expected sender type (usually, it's Sytem.Object)
			Type senderArgType = parameters[0].ParameterType;
			Expression convertedSender = ConvertIfNeeded(senderParameter, senderArgType);
			
			//Cast the 'e' parameter to the expected EventArgs type
			Type eventArgsType = parameters[1].ParameterType;
			Expression convertedEventArgs = ConvertIfNeeded(evArgsParameter, eventArgsType);
				
			//Create body expression
			Expression body;
			if (eventHandlerMethod.IsStatic) {
				body = Expression.Call(
					eventHandlerMethod,
					convertedSender, convertedEventArgs
				);
				body = ConvertToBoolean(body);
			} else {
				MemberExpression wr_Target = CachedExpressions.WR_Target;
				body = ConvertIfNeeded(wr_Target, eventHandlerMethod.DeclaringType);
				body = Expression.Call(
					body,
					eventHandlerMethod, 
					convertedSender, convertedEventArgs
				);
				body = ConvertToBoolean(body);
				body = Expression.Condition(
					CachedExpressions.WR_Target_Equal_Null,
					Expression.Constant(true),
					body
				);
				//body = EnsureParameterNotNull(wrParameter, body); //I'll assume wr (weakReference) is never null
			}

			//Create and compile lambda
			var lambda = Expression.Lambda<ForwarderDelegate>(
				body,
				wrParameter, senderParameter, evArgsParameter
			);
			return lambda.Compile();
			
			#else
			
			/// DynamicMethod + ILGenerator style
			/// Author: Daniel Grunwald (2009)

			///	If the method is static, the generated code will be equivalent to this:
			///	<code>
			///		bool Forwarder(WeakReference wr, object sender, EventArgs e)
			///		{
			///			Method(sender, (TEventArgs)e);
			///			return false;
			///		}
			///	</code>
			///	If the method is not static, the generated code will be equivalent to this:
			///	<code>
			///		bool Forwarder(WeakReference wr, object sender, EventArgs e)
			///		{
			///			if (wr.Target == null) return true;
			///			wr.Target.Method(sender, (TEventArgs)e);
			///			return false;
			///		}
			///	</code>
			
			DynamicMethod dm = new DynamicMethod("FastSmartWeakEvent", typeof(bool), ForwarderDlg_ParameterTypes, eventHandlerMethod.DeclaringType);
			ILGenerator il = dm.GetILGenerator();
			
			if (!eventHandlerMethod.IsStatic) {
				il.Emit(OpCodes.Ldarg_0);
				il.EmitCall(OpCodes.Callvirt, WeakReference_GetTarget, null);
				il.Emit(OpCodes.Dup);
				Label label = il.DefineLabel();
				il.Emit(OpCodes.Brtrue, label);
				il.Emit(OpCodes.Pop);
				il.Emit(OpCodes.Ldc_I4_1);
				il.Emit(OpCodes.Ret);					//if (wr.Target == null) return true;
				il.MarkLabel(label);
				// The castclass here is required for the generated code to be verifiable.
				// We can leave it out because we know this cast will always succeed
				// (the instance/method pair was taken from a delegate).
				// Unverifiable code is fine because private reflection is only allowed under FullTrust
				// anyways.
				//il.Emit(OpCodes.Castclass, method.DeclaringType);
			}
			il.Emit(OpCodes.Ldarg_1);
			il.Emit(OpCodes.Ldarg_2);
			
			// This castclass here is required to prevent creating a hole in the .NET type system.
			// See Program.TypeSafetyProblem in the 'SmartWeakEventBenchmark' to see the effect when
			// this cast is not used.
			// You can remove this cast if you trust add FastSmartWeakEvent.Raise callers to do
			// the right thing, but the small performance increase (about 5%) usually isn't worth the risk.
			il.Emit(OpCodes.Castclass, eventHandlerMethod.GetParameters()[1].ParameterType);

			il.EmitCall(OpCodes.Call, eventHandlerMethod, null);	//Method(sender, e);
			il.Emit(OpCodes.Ldc_I4_0);
			il.Emit(OpCodes.Ret);						//return false;
			
			return (ForwarderDelegate)dm.CreateDelegate(typeof(ForwarderDelegate));
			#endif
		}
		
		private static Expression ConvertToBoolean(Expression expr)
		{
			//Expression is void-typed => tranlate it to a type test ({expression} is object), which always evaluates to false
			if (expr.Type == typeof(void)) return Expression.TypeIs(expr, typeof(object));
			
			//Else => Convert it
			return ConvertIfNeeded(expr, typeof(bool));
		}

		private static Expression ConvertIfNeeded(Expression expr, Type type)
		{
			//Expression already is of expected type => no converttion needed
			if (expr.Type == type) return expr;
			
			//Else => Convert it
			return Expression.Convert(expr, type);
		}

		/* //This code is never used
		private static ConditionalExpression EnsureParameterNotNull(ParameterExpression parameter, Expression continuation)
		{
		    //Choose how to throw exception according to continuation type
		    Type continuationType = continuation.Type;
		    Expression throwArgNullExpression;
		    if (continuationType == typeof(void)) {
		        throwArgNullExpression = Expression.Call(VoidThrowArgumentNullException_Info, Expression.Constant(parameter.Name));
		    } else {
		        throwArgNullExpression =  Expression.Convert(
		            Expression.Call(ThrowArgumentNullException_Info, Expression.Constant(parameter.Name)),
		            continuationType
		        );
		    }
			
		    //Build conditional expression
		    return Expression.Condition(
		        Expression.Equal(parameter, Expression.Constant(null)),
		        throwArgNullExpression,
		        continuation
		    );
		}

		[System.Diagnostics.DebuggerHidden]
		private static void VoidThrowArgumentNullException(string paramName)
		{
		    throw new ArgumentNullException(paramName);
		}
		
		[System.Diagnostics.DebuggerHidden]
		private static object ThrowArgumentNullException(string paramName)
		{
		    throw new ArgumentNullException(paramName);
		}//*/
	}
}
