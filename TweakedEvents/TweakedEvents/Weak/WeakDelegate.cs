﻿using System;
using System.Reflection;

namespace JpLabs.TweakedEvents.Weak
{
	public class SinglecastWeakDelegate : IEquatable<SinglecastWeakDelegate>, IEquatable<Delegate>
	{
		public readonly MethodInfo TargetMethod;
		public readonly WeakReference TargetReference;
		
		internal SinglecastWeakDelegate(Delegate dlg)
		{
			this.TargetReference = dlg.Target != null ? new WeakReference(dlg.Target) : null;
			this.TargetMethod = dlg.Method;
		}

		public bool IsDead()
		{
			return TargetReference != null && !TargetReference.IsAlive;
		}
		
		public static bool operator ==(SinglecastWeakDelegate wd1, SinglecastWeakDelegate wd2) 
		{
			if (((object)wd2) == null) return ((object)wd1) == null;
			if (((object)wd1) == null) return ((object)wd2) == null;
			return wd1.Equals(wd2);
		}

		public static bool operator !=(SinglecastWeakDelegate wd1, SinglecastWeakDelegate wd2) 
		{
			if (((object)wd2) == null) return ((object)wd1) != null;
			if (((object)wd1) == null) return ((object)wd2) != null;
			return !wd1.Equals(wd2);
		}

		public override int GetHashCode()
		{
			int hashCode = TargetMethod.GetHashCode();
			if (TargetReference != null) hashCode ^= TargetReference.GetHashCode(); //bitwise XOR
			return hashCode;
		}

		public override bool Equals(object obj)
		{
			if (obj is SinglecastWeakDelegate) return Equals((SinglecastWeakDelegate)obj);
			return false;
		}

		public bool Equals(SinglecastWeakDelegate other)
		{
			return (
				((object)other) != null
			) && (
				TargetReference.WeakRefEquals(other.TargetReference) //Compare target object
			) && (
				this.TargetMethod == other.TargetMethod //Compare method info
			);
		}

		public bool Equals(Delegate dlg)
		{
			return (
				//Compare target object
				(this.TargetReference == null)
				? (dlg.Target == null) //method is static
				: (dlg.Target == this.TargetReference.Target) //method is not static
			) && (
				//Compare method info
				this.TargetMethod == dlg.Method
			);
		}
	}
}
