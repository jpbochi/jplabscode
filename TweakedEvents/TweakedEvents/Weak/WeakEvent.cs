﻿using System;
using JpLabs.TweakedEvents.Weak;

namespace JpLabs.TweakedEvents
{
	public sealed class WeakEvent<TEH> : TweakedEvent<TEH> where TEH : class
	{
		// Members
		public bool RestrictEventEntryType { get; set; }

		// Contructors
		public WeakEvent() : base ()
		{}
		
		//private WeakEvent(WeakEvent<TEH> other, IList<IEventEntry<TEH>> entries) : base(entries)
		private WeakEvent(WeakEvent<TEH> other, IEventEntry<TEH>[] entries) : base(entries)
		{
			this.RestrictEventEntryType = other.RestrictEventEntryType;
		}
		
		// Overridden Methods
		//protected override ITweakedEvent<TEH> CreateNew(IList<IEventEntry<TEH>> entries)
		protected override ITweakedEvent<TEH> CreateNew(IEventEntry<TEH>[] entries)
		{
			return new WeakEvent<TEH>(this, entries);
		}
		
		public override IEventEntry<TEH> CreateEntry(TEH handler)
		{
			return new WeakEventEntry<TEH>(handler);
		}
		
		public override ITweakedEvent<TEH> Combine(IEventEntry<TEH> entry)
		{
			if (RestrictEventEntryType && !(entry is WeakEventEntry<TEH>)) {
				throw new NotSupportedException("Only entries of type WeakEventEntry<> are supported by this custom event");
			}
			return base.Combine(entry);
		}
	}
}