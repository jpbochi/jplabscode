﻿#define TWE

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using JpLabs.TweakedEvents;

namespace JpLabs.WeakEventDemo
{
	public partial class MyForm : Form, ICounter
	{
		public MyForm()
		{
			InitializeComponent();
		}
		
		#if !TWE
		
		public event EventHandler<CountEventArgs> IssueCount;
		
		#else
		
		private TweakedEvent<EventHandler<CountEventArgs>> issueCountEvent = new WeakEvent<EventHandler<CountEventArgs>>();
		
		public event EventHandler<CountEventArgs> IssueCount
		{
			//add    { issueCountEvent = issueCountEvent + value; }
			//remove { issueCountEvent -= value; }
			add { TweakedEvent.Add(ref issueCountEvent, value); }
			remove { TweakedEvent.Remove(ref issueCountEvent, value); }
		}
		
		#endif

		private void btnAdd_Click(object sender, EventArgs e)
		{
			lstItems.Items.Add(new IncrementerComponent(Guid.NewGuid().ToString(), this));
		}

		private void btnRemove_Click(object sender, EventArgs e)
		{
			var selected = lstItems.SelectedItem;
			
			if (selected != null) {
				lstItems.Items.Remove(selected);
			} else if (lstItems.Items.Count > 0) {
				lstItems.Items.RemoveAt(0);
			}
			
			GC.Collect();
		}

		private void btnIssueCount_Click(object sender, EventArgs e)
		{
			var evArgs = new CountEventArgs();
			
			OnIssueCount(evArgs);
			
			lblLastCount.Text = string.Format("{0} components", evArgs.Count);
		}

		private void OnIssueCount(CountEventArgs e)
		{
			#if !TWE
			
			var dlg = this.IssueCount;
			if (dlg != null) dlg(this, e);
			
			#else
			
			issueCountEvent.Raise(this, e);
			
			#endif
		}
	}
}
