﻿namespace JpLabs.WeakEventDemo
{
	partial class MyForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.lstItems = new System.Windows.Forms.ListBox();
			this.btnAdd = new System.Windows.Forms.Button();
			this.btnRemove = new System.Windows.Forms.Button();
			this.btnIssueCount = new System.Windows.Forms.Button();
			this.lblLastCount = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// lstItems
			// 
			this.lstItems.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lstItems.FormattingEnabled = true;
			this.lstItems.ItemHeight = 16;
			this.lstItems.Location = new System.Drawing.Point(12, 12);
			this.lstItems.Name = "lstItems";
			this.lstItems.Size = new System.Drawing.Size(303, 260);
			this.lstItems.TabIndex = 0;
			// 
			// btnAdd
			// 
			this.btnAdd.Location = new System.Drawing.Point(321, 12);
			this.btnAdd.Name = "btnAdd";
			this.btnAdd.Size = new System.Drawing.Size(133, 23);
			this.btnAdd.TabIndex = 1;
			this.btnAdd.Text = "Add Component";
			this.btnAdd.UseVisualStyleBackColor = true;
			this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
			// 
			// btnRemove
			// 
			this.btnRemove.Location = new System.Drawing.Point(321, 41);
			this.btnRemove.Name = "btnRemove";
			this.btnRemove.Size = new System.Drawing.Size(133, 23);
			this.btnRemove.TabIndex = 2;
			this.btnRemove.Text = "Remove Component";
			this.btnRemove.UseVisualStyleBackColor = true;
			this.btnRemove.Click += new System.EventHandler(this.btnRemove_Click);
			// 
			// btnIssueCount
			// 
			this.btnIssueCount.Location = new System.Drawing.Point(321, 86);
			this.btnIssueCount.Name = "btnIssueCount";
			this.btnIssueCount.Size = new System.Drawing.Size(133, 23);
			this.btnIssueCount.TabIndex = 3;
			this.btnIssueCount.Text = "Issue Count";
			this.btnIssueCount.UseVisualStyleBackColor = true;
			this.btnIssueCount.Click += new System.EventHandler(this.btnIssueCount_Click);
			// 
			// lblLastCount
			// 
			this.lblLastCount.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblLastCount.ForeColor = System.Drawing.SystemColors.ActiveCaption;
			this.lblLastCount.Location = new System.Drawing.Point(322, 116);
			this.lblLastCount.Name = "lblLastCount";
			this.lblLastCount.Size = new System.Drawing.Size(132, 21);
			this.lblLastCount.TabIndex = 4;
			this.lblLastCount.Text = "No Counts Made";
			this.lblLastCount.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// MyForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(466, 285);
			this.Controls.Add(this.lblLastCount);
			this.Controls.Add(this.btnIssueCount);
			this.Controls.Add(this.btnRemove);
			this.Controls.Add(this.btnAdd);
			this.Controls.Add(this.lstItems);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.Name = "MyForm";
			this.Text = "JP Labs - WeakEvents Demo";
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.ListBox lstItems;
		private System.Windows.Forms.Button btnAdd;
		private System.Windows.Forms.Button btnRemove;
		private System.Windows.Forms.Button btnIssueCount;
		private System.Windows.Forms.Label lblLastCount;
	}
}

