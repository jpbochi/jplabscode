﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JpLabs.TweakedEvents;

namespace JpLabs.WeakEventDemo
{
	public interface ICounter //implemented by MyForm
	{
		event EventHandler<CountEventArgs> IssueCount;
	}

	public class CountEventArgs : EventArgs
	{
		public int Count { get; set; }
	}

	public class IncrementerComponent
	{
		private string name;
		
		public IncrementerComponent(string name, ICounter counter)
		{
			this.name = name;

			counter.IssueCount += TweakedEvent.ToWeak<CountEventArgs>(counter_ShouldNotBeCalled);

			counter.IssueCount += counter_IssueCount;

			
			//counter.IssueCount += (object sender, CountEventArgs e) => {
			//    name = "";
			//    e.Count++;
			//};
			
			//Proves that custom events created by subscriber can be removed
			counter.IssueCount -= TweakedEvent.ToWeak<CountEventArgs>(counter_ShouldNotBeCalled);
		}
		
		private void counter_IssueCount(object sender, CountEventArgs e)
		{
			e.Count++;
		}

		private void counter_ShouldNotBeCalled(object sender, CountEventArgs e)
		{
			e.Count++;
		}

		public override string ToString()
		{
			return name;
		}
	}
}
