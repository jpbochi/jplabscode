﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using JpLabs.TweakedEvents;
using System.Threading;
using System.Diagnostics;

namespace JpLabs.SynchronizedEvent.Demo
{
	public partial class Foo : Form
	{
		private TweakedEvent<EventHandler<AsyncCompletedEventArgs>> TaskCompletedEvent;

		public Foo()
		{
			InitializeComponent();
			
			var funcCreateEntry = (Func<EventHandler<AsyncCompletedEventArgs>,IEventEntry<EventHandler<AsyncCompletedEventArgs>>>) (
				//(handler) => new SyncedEventEntry<EventHandler<AsyncCompletedEventArgs>>(handler, this)
				//(handler) => SyncedEventEntry<EventHandler<AsyncCompletedEventArgs>>.CreateEntry(handler, this)
				(handler) => TweakedEvent.ToWeakSynced(this, handler)
				//(handler) => new WeakEventEntry<EventHandler<AsyncCompletedEventArgs>>(handler)
			);
			TaskCompletedEvent = new FuncTweakedEvent<EventHandler<AsyncCompletedEventArgs>>(funcCreateEntry);

			//TaskCompleted += TweakedEvent.ToSynced<AsyncCompletedEventArgs>(Pop_TaskCompleted, this);
			TaskCompleted += Pop_TaskCompleted;
			TaskCompleted += Pop_TaskCompleted_Two;
		}
		
		private event EventHandler<AsyncCompletedEventArgs> TaskCompleted
		{
			add { TweakedEvent.Add(ref TaskCompletedEvent, value); }
			remove { TweakedEvent.Remove(ref TaskCompletedEvent, value); }
		}

		void Pop_TaskCompleted_Two(object sender, AsyncCompletedEventArgs e)
		{
			//this.Dispose();
			this.SuspendLayout();
			this.ResumeLayout(true);
			
			//var lbl = new Label() { Text = "Arrrgh" };
			//lbl.Dispose();
			//this.Controls.Add(lbl);
			//lbl.BringToFront();

			this.SuspendLayout();
			this.ResumeLayout();
			
			this.Focus();

			this.Show();
		}

		void Pop_TaskCompleted(object sender, AsyncCompletedEventArgs e)
		{
			ShowMessage(e.UserState.ToString());
		}
		
		void ShowMessage(string message)
		{
			message = message + " [" + Thread.CurrentThread.ManagedThreadId + "]";

			lstLog.Items.Add(message);
			
			if (this.IsDisposed) {
				System.Diagnostics.Debug.Print(message);
				
				//MessageBox.Show(message);
			}
		}
		
		
		//private event EventHandler<AsyncCompletedEventArgs> 
		
        //public event EventHandler<AsyncCompletedEventArgs> TaskCompleted;
		//{
		//    add { DoorOpenedEvent += value; }
		//    remove { DoorOpenedEvent -= value; }
		//}

		private void btnRun_Click(object sender, EventArgs e)
		{
            var f = (Action<int>)(
				(timeout) => {
					Thread.Sleep(timeout);
					
					try
					{
						TaskCompletedEvent.Raise(this, new AsyncCompletedEventArgs(null, false, "Task completed"));
					}
					catch (Exception ex)
					{
					    System.Diagnostics.Debug.Print(ex.ToString());
					    
						if (Debugger.IsAttached) Debugger.Break();

					    MessageBox.Show(ex.Message);
					}
				}
            );
            
            {
				int timeout = new Random().Next(500, 3000);
	            
				ShowMessage("Task started (" + timeout.ToString() + ")");
	            
				var asyncResult = f.BeginInvoke(timeout, null, null);
				//f.EndInvoke(asyncResult);
			}
		}
	}
}
