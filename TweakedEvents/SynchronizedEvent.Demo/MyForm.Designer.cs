﻿namespace JpLabs.SynchronizedEvent.Demo
{
    partial class MyForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose ( bool disposing )
        {
            if ( disposing && ( components != null ) )
            {
                components.Dispose ();
            }
            base.Dispose ( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent ()
        {
			this.btnOpenDoor = new System.Windows.Forms.Button();
			this.label1 = new System.Windows.Forms.Label();
			this.btnCloseDoor = new System.Windows.Forms.Button();
			this.lblDoorStatus = new System.Windows.Forms.Label();
			this.btnAttachDetach = new System.Windows.Forms.Button();
			this.pnlInvoke = new System.Windows.Forms.Panel();
			this.lblInvoke = new System.Windows.Forms.Label();
			this.btnPopup = new System.Windows.Forms.Button();
			this.wpfElementHost = new System.Windows.Forms.Integration.ElementHost();
			this.pnlInvoke.SuspendLayout();
			this.SuspendLayout();
			// 
			// btnOpenDoor
			// 
			this.btnOpenDoor.BackColor = System.Drawing.Color.LightGoldenrodYellow;
			this.btnOpenDoor.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnOpenDoor.Location = new System.Drawing.Point(12, 12);
			this.btnOpenDoor.Name = "btnOpenDoor";
			this.btnOpenDoor.Size = new System.Drawing.Size(110, 31);
			this.btnOpenDoor.TabIndex = 0;
			this.btnOpenDoor.Text = "Open Door";
			this.btnOpenDoor.UseVisualStyleBackColor = false;
			this.btnOpenDoor.Click += new System.EventHandler(this.btnOpenDoor_Click);
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label1.ForeColor = System.Drawing.Color.Black;
			this.label1.Location = new System.Drawing.Point(47, 71);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(116, 18);
			this.label1.TabIndex = 1;
			this.label1.Text = "Door status:";
			// 
			// btnCloseDoor
			// 
			this.btnCloseDoor.BackColor = System.Drawing.Color.LightGoldenrodYellow;
			this.btnCloseDoor.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnCloseDoor.Location = new System.Drawing.Point(128, 12);
			this.btnCloseDoor.Name = "btnCloseDoor";
			this.btnCloseDoor.Size = new System.Drawing.Size(110, 31);
			this.btnCloseDoor.TabIndex = 2;
			this.btnCloseDoor.Text = "Close Door";
			this.btnCloseDoor.UseVisualStyleBackColor = false;
			this.btnCloseDoor.Click += new System.EventHandler(this.btnCloseDoor_Click);
			// 
			// lblDoorStatus
			// 
			this.lblDoorStatus.AutoSize = true;
			this.lblDoorStatus.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblDoorStatus.ForeColor = System.Drawing.Color.Black;
			this.lblDoorStatus.Location = new System.Drawing.Point(169, 71);
			this.lblDoorStatus.Name = "lblDoorStatus";
			this.lblDoorStatus.Size = new System.Drawing.Size(66, 18);
			this.lblDoorStatus.TabIndex = 3;
			this.lblDoorStatus.Text = "Closed";
			// 
			// btnAttachDetach
			// 
			this.btnAttachDetach.BackColor = System.Drawing.Color.PeachPuff;
			this.btnAttachDetach.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnAttachDetach.Location = new System.Drawing.Point(245, 12);
			this.btnAttachDetach.Name = "btnAttachDetach";
			this.btnAttachDetach.Size = new System.Drawing.Size(110, 31);
			this.btnAttachDetach.TabIndex = 4;
			this.btnAttachDetach.Text = "{Detach Events}";
			this.btnAttachDetach.UseVisualStyleBackColor = false;
			this.btnAttachDetach.Click += new System.EventHandler(this.btnAttachDetach_Click);
			// 
			// pnlInvoke
			// 
			this.pnlInvoke.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.pnlInvoke.Controls.Add(this.lblInvoke);
			this.pnlInvoke.Location = new System.Drawing.Point(63, 168);
			this.pnlInvoke.Name = "pnlInvoke";
			this.pnlInvoke.Size = new System.Drawing.Size(222, 45);
			this.pnlInvoke.TabIndex = 5;
			// 
			// lblInvoke
			// 
			this.lblInvoke.AutoSize = true;
			this.lblInvoke.Location = new System.Drawing.Point(20, 14);
			this.lblInvoke.Name = "lblInvoke";
			this.lblInvoke.Size = new System.Drawing.Size(58, 13);
			this.lblInvoke.TabIndex = 0;
			this.lblInvoke.Text = "{lblInvoke}";
			// 
			// btnPopup
			// 
			this.btnPopup.BackColor = System.Drawing.Color.LightGoldenrodYellow;
			this.btnPopup.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnPopup.Location = new System.Drawing.Point(12, 105);
			this.btnPopup.Name = "btnPopup";
			this.btnPopup.Size = new System.Drawing.Size(110, 31);
			this.btnPopup.TabIndex = 6;
			this.btnPopup.Text = "Pop up";
			this.btnPopup.UseVisualStyleBackColor = false;
			this.btnPopup.Click += new System.EventHandler(this.btnPopup_Click);
			// 
			// wpfElementHost
			// 
			this.wpfElementHost.Location = new System.Drawing.Point(158, 105);
			this.wpfElementHost.Name = "wpfElementHost";
			this.wpfElementHost.Size = new System.Drawing.Size(197, 31);
			this.wpfElementHost.TabIndex = 7;
			this.wpfElementHost.Text = "wpfElementHost";
			this.wpfElementHost.Child = null;
			// 
			// MyForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.White;
			this.ClientSize = new System.Drawing.Size(367, 148);
			this.Controls.Add(this.wpfElementHost);
			this.Controls.Add(this.btnPopup);
			this.Controls.Add(this.pnlInvoke);
			this.Controls.Add(this.btnAttachDetach);
			this.Controls.Add(this.lblDoorStatus);
			this.Controls.Add(this.btnCloseDoor);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.btnOpenDoor);
			this.Name = "MyForm";
			this.Text = "JP Labs SynchronizedEvent Demo";
			this.pnlInvoke.ResumeLayout(false);
			this.pnlInvoke.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnOpenDoor;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnCloseDoor;
		private System.Windows.Forms.Label lblDoorStatus;
		private System.Windows.Forms.Button btnAttachDetach;
		private System.Windows.Forms.Panel pnlInvoke;
		private System.Windows.Forms.Label lblInvoke;
		private System.Windows.Forms.Button btnPopup;
		private System.Windows.Forms.Integration.ElementHost wpfElementHost;
    }
}

