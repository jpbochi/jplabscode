﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.ComponentModel;
using JpLabs.TweakedEvents;

namespace JpLabs.SynchronizedEvent.Demo
{
	public class AsyncDoor
	{
		public enum DoorStatus
		{
			Opening,
			Open,
			Closing,
			Closed,
		}

		private object statusLocker = new object();
		
		public DoorStatus Status { get; private set; }
		public int TimeToOpen { get; private set; }
		public int TimeToClose { get; private set; }
		
		//public ISynchronizeInvoke SyncObj { get; set; }
		public object SyncObj { get; set; }

		private TweakedEvent<EventHandler> DoorOpeningEvent;//private EventHandler DoorOpeningEvent;
		private TweakedEvent<EventHandler<AsyncCompletedEventArgs>> DoorOpenedEvent;
		//private EventHandler<AsyncCompletedEventArgs> DoorOpenedEvent;
		private TweakedEvent<EventHandler> DoorClosingEvent;
		private TweakedEvent<EventHandler> DoorClosedEvent;
		
		public AsyncDoor(int timeToOpen, int timeToClose)
		{
			Status = DoorStatus.Closed;
			TimeToOpen = timeToOpen;
			TimeToClose = timeToClose;
			
			var funcCreateEntry = (Func<EventHandler,IEventEntry<EventHandler>>)
			(
				//(handler) => new WeakSyncedEventEntry<EventHandler>(handler, this.SyncObj)
				(handler) => TweakedEvent.ToWeakSynced(this.SyncObj, handler)
			);
			
			DoorOpeningEvent = new FuncTweakedEvent<EventHandler>(funcCreateEntry);
			//DoorOpenedEvent = null;
			DoorOpenedEvent = new SyncedEvent<EventHandler<AsyncCompletedEventArgs>>();
			DoorClosingEvent = new FuncTweakedEvent<EventHandler>(funcCreateEntry);
			DoorClosedEvent = new FuncTweakedEvent<EventHandler>(funcCreateEntry);
		}

		/// <summary>Raised when the door starts to open</summary>
		public event EventHandler DoorOpening
		{
			//[System.Runtime.CompilerServices.MethodImpl(MethodImplOptions.Synchronized)] 
			//add { TweakedEvent.Add(ref DoorOpeningEvent, value; }
			//remove { DoorOpeningEvent -= value; }

			add { TweakedEvent.Add(ref DoorOpeningEvent, value); }
			remove { TweakedEvent.Remove(ref DoorOpeningEvent, value); }
		}

		/// <summary>Raised after the door is opened</summary>
		public event EventHandler<AsyncCompletedEventArgs> DoorOpened
		{
			//add { DoorOpenedEvent += value; }
			//remove { DoorOpenedEvent -= value; }
			add { TweakedEvent.Add(ref DoorOpenedEvent, value); }
			remove { TweakedEvent.Remove(ref DoorOpenedEvent, value); }
		}
		
		/// <summary>Raised when the door starts to close</summary>
		public event EventHandler DoorClosing
		{
			//add { DoorClosingEvent += value; }
			//remove { DoorClosingEvent -= value; }
			add { TweakedEvent.Add(ref DoorClosingEvent, value); }
			remove { TweakedEvent.Remove(ref DoorClosingEvent, value); }
		}

		/// <summary>Raised after the door is closed</summary>
		public event EventHandler DoorClosed
		{
			//add { DoorClosedEvent += value; }
			//remove { DoorClosedEvent -= value; }
			add { TweakedEvent.Add(ref DoorClosedEvent, value); }
			remove { TweakedEvent.Remove(ref DoorClosedEvent, value); }
		}
		
		protected virtual void OnDoorOpening()
		{
			DoorOpeningEvent.Raise(this, EventArgs.Empty);
		}
		
		protected virtual void OnDoorOpened()
		{
			//System.Windows.Forms.MessageBox.Show(System.Threading.Thread.CurrentThread.ManagedThreadId.ToString());
			//DoorOpenedEvent.RaiseEvent(this, null);
			DoorOpenedEvent.Raise(this, null);
			//DoorOpened(this, EventArgs.Empty);
		}
		
		protected virtual void OnDoorClosing()
		{
			DoorClosingEvent.Raise(this, EventArgs.Empty);
		}
		
		protected virtual void OnDoorClosed()
		{
			DoorClosedEvent.Raise(this, EventArgs.Empty);
		}

		/// <summary>Starts to open the door</summary>
		/// <returns>false if the door can't be opened now</returns>
		public bool Open()
		{
			lock (statusLocker)
			{
				// Only closed doors can be opened
				if ( Status != DoorStatus.Closed ) return false;
				Status = DoorStatus.Opening;
			}
			
			ThreadPool.QueueUserWorkItem(OpenAsync, null);

			OnDoorOpening();
			
			return true;
		}

		private void OpenAsync(object state)
		{
			Thread.Sleep(TimeToOpen);

			lock (statusLocker) Status = DoorStatus.Open;

			OnDoorOpened();
		}

		/// <summary>Starts to close the door</summary>
		/// <returns>false if the door can't be closed now</returns>
		public bool Close()
		{
			lock (statusLocker)
			{
				// Only open doors can be closed
				if ( Status != DoorStatus.Open ) return false;
				Status = DoorStatus.Closing;
			}

			ThreadPool.QueueUserWorkItem(CloseAsync, null);
			
			OnDoorClosing();
			
			return true;
		}

		private void CloseAsync(object state)
		{
			Thread.Sleep(TimeToClose);

			lock (statusLocker) Status = DoorStatus.Closed;

			OnDoorClosed();
		}
	}
}
