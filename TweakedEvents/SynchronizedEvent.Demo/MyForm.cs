﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using JpLabs.TweakedEvents;
using System.Windows.Forms.Integration;

namespace JpLabs.SynchronizedEvent.Demo
{
	public partial class MyForm : Form
	{
		class UpdateLabel
		{
			public Label Label { get; set; }
			
			public void Update(object sender, AsyncCompletedEventArgs e)
			{
				var door = (AsyncDoor)sender;
				Label.Text = door.Status.ToString();
				Label.ForeColor = Color.BlueViolet;

				System.Diagnostics.Trace.WriteLine("UpdateLabel.Update: " + door.Status.ToString());
				
				//[ThreadStatic]
				//private static bool inCrossThreadSafeCall;
				//System.Windows.Forms.Control.checkForIllegalCrossThreadCalls = Debugger.IsAttached;
				//at System.Windows.Forms.Control.get_Handle()
			}

			public void FailedHandler(object sender, AsyncCompletedEventArgs e)
			{
				throw new InvalidOperationException("Test Exception");
			}
		}
		
		private AsyncDoor door;
		private UpdateLabel updateLabelObj;
		private bool areEventsAttached;
		
		public MyForm ()
		{
			InitializeComponent();

			door = new AsyncDoor(1000, 1500);
			door.SyncObj = this;

			//var wpfChild = new System.Windows.Controls.Label() { Content = "{WPF label}" };
			//var wpfChild = new System.Windows.Controls.Frame();
			var wpfChild = new WpfControl();
			wpfChild.InitializeComponent();

			wpfElementHost.Child = wpfChild;
			
			AttachEvents();
		}
		
		private void AttachEvents()
		{
			areEventsAttached = true;
			btnAttachDetach.Text = "Detach Events";
			
			lblDoorStatus.Text = door.Status.ToString();
			lblDoorStatus.ForeColor = Color.Blue;
			
			//{
			//    var ugh = SynchronizationContext.Current;
				
			//    var foo = (Func<SynchronizationContext>)(
			//        () => {
			//            return SynchronizationContext.Current;
			//        }
			//    );
			//    var asyncResult0 = foo.BeginInvoke(null, null);
			//    var bar = foo.EndInvoke(asyncResult0);
			//}
			
			door.DoorOpening += TweakedEvent.ToSynced(this, door_OnDoorOpening);//((EventHandler)door_OnDoorOpening).ToSynced(this);
			
			/*
			door.DoorOpened += door_OnDoorOpened;
			//door.DoorOpened -= new EventHandler(door_OnDoorOpened);
			/* /
			var dlg0 = new EventHandler(door_OnDoorOpened);
			//var dlg1 = (EventHandler)Delegate.CreateDelegate(
			//    typeof(EventHandler),
			//    this, //.lblDoorStatus,
			//    dlg0.Method,
			//    false
			//);
			door.DoorOpenedEvent.Add(new SynchronizedEvent<EventHandler>.EventEntry(dlg0, this));
			/* /
			door.DoorOpenedEvent.Add(
				(sender, e) => { this.UpdateLabelText("The door is open!", Color.BlueViolet); }
				, this.lblDoorStatus
			);
			/*/
			this.updateLabelObj = new UpdateLabel() { Label = lblDoorStatus };
			//door.DoorOpenedEvent.Add(
			//    updateLabelObj.Update//, this.lblDoorStatus
			//);
			door.DoorOpened += updateLabelObj.Update;

			//door.DoorOpened += TweakedEvent.ToSynced<AsyncCompletedEventArgs>(null, this.updateLabelObj.FailedHandler);
			//door.DoorOpened += TweakedEvent.ToSynced<AsyncCompletedEventArgs>(this, this.updateLabelObj.FailedHandler);
			//door.DoorOpened += TweakedEvent.ToWeakSynced<AsyncCompletedEventArgs>(null, this.updateLabelObj.FailedHandler);
			//door.DoorOpened += TweakedEvent.ToWeakSynced<AsyncCompletedEventArgs>(this, this.updateLabelObj.FailedHandler);
			door.DoorOpened += TweakedEvent.ToWeak<AsyncCompletedEventArgs>(this.updateLabelObj.FailedHandler);

			//var syncObj = System.Windows.Threading.Dispatcher.CurrentDispatcher;
			//door.DoorOpened += TweakedEvent.ToSynced<AsyncCompletedEventArgs>(syncObj, this.updateLabelObj.FailedHandler);

			door.DoorOpened += TweakedEvent.ToSynced<AsyncCompletedEventArgs>(wpfElementHost.Child, this.updateLabelObj.FailedHandler);
			//door.DoorOpened += TweakedEvent.ToSynced<AsyncCompletedEventArgs>(wpfLabel.Dispatcher, this.updateLabelObj.FailedHandler);
			
			//door.DoorOpened += this.updateLabelObj.DoNothing;
			
			//var handler = TweakedEvent.ToWeak<AsyncCompletedEventArgs>(this.updateLabelObj.Update);
			//door.DoorOpened += handler;
			//handler.Dispose();
			//*/
			
			door.DoorClosing	+= door_OnDoorClosing;
			
			if (this.IsHandleCreated) {
				door.DoorClosed		+= door_OnDoorClosed;
			} else {
				this.HandleCreated	+= new EventHandler(MyForm_HandleCreated);
			}
		}

		private void DetachEvents()
		{
			areEventsAttached = false;
			btnAttachDetach.Text = "Attach Events";
			lblDoorStatus.ForeColor = Color.Red;
			
			door.DoorOpening	-= door_OnDoorOpening;
			
			door.DoorClosing	-= door_OnDoorClosing;
			door.DoorClosed		-= door_OnDoorClosed;

			this.updateLabelObj = null;
			GC.Collect();
			//door.DoorOpened		-= this.updateLabelObj.Update;
			//door.DoorOpened		-= this.updateLabelObj.DoNothing;
		}

		private void MyForm_HandleCreated(object sender, EventArgs e)
		{
			var f = (Action)(
				() => { door.DoorClosed += door_OnDoorClosed; }
			);
			var asyncResult = f.BeginInvoke(null, null);
			f.EndInvoke(asyncResult);
		}

		private void btnOpenDoor_Click(object sender, EventArgs e)
		{
			// -----
			/*
			
			//var dialog = new MyDialog();
			//dialog.ShowDialog(this);
			
			Control ctrl = lblInvoke; //dialog;
			//ctrl.Dispose();
			//pnlInvoke.Parent = null;
			pnlInvoke.Dispose();
			
			var f = (Action)(
				() => {

					System.Diagnostics.Debug.Print(ctrl.InvokeRequired.ToString());
					try
					{
						ctrl.Invoke((Delegate)(Action)(
							delegate {
								System.Diagnostics.Debug.Print("it works!");
							}
						));
					} catch (Exception ex) {
						throw;
					}
				}
			);
			var asyncResult = f.BeginInvoke(null, null);
			//f.EndInvoke(asyncResult);
			
			//*/
			// ---

			//MessageBox.Show(System.Threading.Thread.CurrentThread.ManagedThreadId.ToString());
			if (!door.Open()) MessageBox.Show(this, "Door can't be opened now");
		}

		private void btnCloseDoor_Click ( object sender, EventArgs e )
		{
			if (!door.Close()) MessageBox.Show(this, "Door can't be closed now");
		}

		private void btnAttachDetach_Click(object sender, EventArgs e)
		{
			if (areEventsAttached) {
				DetachEvents();
			} else {
				AttachEvents();
			}
		}

		private void door_OnDoorOpening(object sender, EventArgs args)
		{
			string msg = "Opening the door...";

			Console.WriteLine ( msg );

			UpdateLabelText(msg, Color.Orange);
		}

		private void door_OnDoorOpened(object sender, EventArgs args)
		{
			string msg = "Door opened.";

			Console.WriteLine ( msg );

			UpdateLabelText(msg, Color.Green);
		}

		private void door_OnDoorClosing(object sender, EventArgs args)
		{
			string msg = "Closing Door...";

			Console.WriteLine ( msg );

			UpdateLabelText(msg, Color.Orange);
		}

		private void door_OnDoorClosed(object sender, EventArgs args)
		{
			string msg = "Door closed.";

			Console.WriteLine ( msg );

			UpdateLabelText(msg, Color.Black);
		}

		/// <summary>
		/// Updates the label with the provided text and color
		/// </summary>
		private void UpdateLabelText ( string text, Color color )
		{
			lblDoorStatus.Text = text;
			lblDoorStatus.ForeColor = color;
		}

		private void btnPopup_Click(object sender, EventArgs e)
		{
			using (var pop = new Foo())
			{
				pop.ShowDialog(this);
				//pop.Show(this);
			}
		}
	}
}
