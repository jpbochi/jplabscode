﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using JpLabs.SynchronizedEvent.Demo;
using JpLabs.TweakedEvents;
using System.ComponentModel;
using System.Threading.Tasks;

namespace JpLabs.TweakedEvents.WpfDemo
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
        private AsyncDoor door;
        private bool areEventsAttached;

		public MainWindow()
		{
			InitializeComponent();

            door = new AsyncDoor(1000, 1500);
            door.SyncObj = this;

			AttachEvents();
		}

        private void AttachEvents()
        {
			areEventsAttached = true;
			//btnAttachDetach.Text = "Detach Events";
			
			lblDoorStatus.Content = door.Status.ToString();
			lblDoorStatus.Foreground = new SolidColorBrush(Colors.Blue);
			
            door.DoorOpening += TweakedEvent.ToSynced(this, door_OnDoorOpening);//((EventHandler)door_OnDoorOpening).ToSynced(this);
            
			door.DoorOpened += door_OnDoorOpened;

			//door.DoorOpened += TweakedEvent.ToSynced<AsyncCompletedEventArgs>(null, this.updateLabelObj.FailedHandler);
			//door.DoorOpened += TweakedEvent.ToSynced<AsyncCompletedEventArgs>(this, this.updateLabelObj.FailedHandler);
			//door.DoorOpened += TweakedEvent.ToWeakSynced<AsyncCompletedEventArgs>(null, this.updateLabelObj.FailedHandler);
			//door.DoorOpened += TweakedEvent.ToWeakSynced<AsyncCompletedEventArgs>(this, this.updateLabelObj.FailedHandler);
			//door.DoorOpened += TweakedEvent.ToWeak<AsyncCompletedEventArgs>(this.FailedHandler);

			//var syncObj = System.Windows.Threading.Dispatcher.CurrentDispatcher;
			//door.DoorOpened += TweakedEvent.ToSynced<AsyncCompletedEventArgs>(syncObj, this.updateLabelObj.FailedHandler);

			//door.DoorOpened += TweakedEvent.ToSynced<AsyncCompletedEventArgs>(this, this.FailedHandler);
			door.DoorOpened += TweakedEvent.ToSynced<AsyncCompletedEventArgs>(this.Dispatcher, this.FailedHandler);

            door.DoorClosing	+= door_OnDoorClosing;
			door.DoorClosed		+= door_OnDoorClosed;
        }

		private void btnOpen_Click(object sender, RoutedEventArgs e)
		{
			var t = Task.Factory.StartNew(
				() => {
					try
					{
						var dispatcherOperation = this.Dispatcher.BeginInvoke((Delegate)(EventHandler<AsyncCompletedEventArgs>)FailedHandler, this, null);

						dispatcherOperation.Wait();

						//this.Dispatcher.Invoke((Delegate)(EventHandler<AsyncCompletedEventArgs>)FailedHandler, this, null);
					} catch {
						throw;
					}
				}
			);

			//if (!door.Open()) MessageBox.Show(this, "Door can't be opened now");
		}

		private void btnClose_Click(object sender, RoutedEventArgs e)
		{
			if (!door.Close()) MessageBox.Show(this, "Door can't be closed now");
		}

		public void FailedHandler(object sender, AsyncCompletedEventArgs e)
		{
			throw new InvalidOperationException("Test Exception");
		}

        private void door_OnDoorOpening(object sender, EventArgs args)
        {
            string msg = "Opening the door...";

            Console.WriteLine ( msg );

            UpdateLabelText(msg, Colors.Orange);
        }

        private void door_OnDoorOpened(object sender, EventArgs args)
        {
            string msg = "Door opened.";

            Console.WriteLine ( msg );

            UpdateLabelText(msg, Colors.Green);
        }

        private void door_OnDoorClosing(object sender, EventArgs args)
        {
            string msg = "Closing Door...";

            Console.WriteLine ( msg );

            UpdateLabelText(msg, Colors.Orange);
        }

        private void door_OnDoorClosed(object sender, EventArgs args)
        {
            string msg = "Door closed.";

            Console.WriteLine ( msg );

            UpdateLabelText(msg, Colors.Black);
        }

        /// <summary>
        /// Updates the label with the provided text and color
        /// </summary>
        private void UpdateLabelText ( string text, Color color )
        {
            lblDoorStatus.Content = text;
            lblDoorStatus.Foreground = new SolidColorBrush(color);
        }
	}
}
