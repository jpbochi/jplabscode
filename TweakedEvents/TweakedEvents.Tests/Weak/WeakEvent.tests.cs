﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using JpLabs.TweakedEvents;
using Moq;
using Xunit;

namespace JpLabs.TweakedEventsTests
{
	public sealed class WeakEvent_Tests : BaseTweakedEvent_Tests<EventHandler,EventArgs>
	{
		public WeakEvent_Tests() : base(new WeakEvent<EventHandler>())
		{}

		protected override void AssertEventIsEmpty(TweakedEvent<EventHandler> eventStore)
		{
			Assert.IsType(typeof(WeakEvent<EventHandler>), eventStore);

			Assert.True(eventStore.IsEmpty());
		}

		protected override void AssertEventContainsASingleEntry(TweakedEvent<EventHandler> eventStore)
		{
			Assert.IsType(typeof(WeakEvent<EventHandler>), eventStore);

			Assert.False(eventStore.IsEmpty());
		}

		private event EventHandler TestEvent
		{
			add { base.TestEvent_Add(value); }
			remove { base.TestEvent_Remove(value); }
		}

		[Fact]
		public void Raise_ClearsCollectedHandlers()
		{
			TestEvent += GetNullListener().Handler;
			GC.Collect();
			GC.WaitForPendingFinalizers();

			TestEventStore.Raise(null, null);

			AssertEventIsEmpty(TestEventStore);
		}
	}
}
