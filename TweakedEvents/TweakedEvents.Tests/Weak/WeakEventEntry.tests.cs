﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Moq;
using Xunit;
using JpLabs.TweakedEvents;
using System.Linq.Expressions;
using JpLabs.TweakedEvents.Weak;

namespace JpLabs.TweakedEventsTests
{
	public sealed class WeakEventEntry_Tests : BaseTweakedEventEntry_Tests<EventHandler,EventArgs>
	{
		protected override BaseEventEntry<EventHandler> GetNewEntry(EventHandler handler)
		{
			return new WeakEventEntry<EventHandler>(handler);
		}

		[Fact]
		public override void Invoke_CallsAnonymousHandler()
		{
			Mock<IEventListener<EventArgs>> mockListener;
			var handler = CreateAnonymousHandler(out mockListener);

			// The target object of anonymous methods is usually never referenced to. So, weak events don't accept them.
			Assert.Throws<ArgumentException>( () => GetNewEntry(handler) );
		}

		[Fact]
		public void IsDisposed_AutomaticallyDisposes_IfHandlerWasCollected()
		{
			var entry = new WeakEventEntry<EventHandler>(GetNullHandler());
			GC.Collect();
			GC.WaitForPendingFinalizers();

			Assert.True(entry.IsDisposed);
		}

		[Fact]
		public void Invoke_CausesDispose_IfHandlerWasCollected()
		{
			var entry = new WeakEventEntry<EventHandler>(GetNullHandler());
			GC.Collect();
			GC.WaitForPendingFinalizers();

			var isDisposed = entry.Invoke(null, null);

			Assert.True(isDisposed);
			Assert.True(entry.IsDisposed);
		}
	}
}
