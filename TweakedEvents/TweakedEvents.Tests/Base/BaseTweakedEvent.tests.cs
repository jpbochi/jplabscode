﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JpLabs.TweakedEvents;
using Xunit;
using Moq;
using System.Linq.Expressions;

namespace JpLabs.TweakedEventsTests
{
	public abstract class BaseTweakedEvent_Tests<TEH,TArgs> : TestMother<TEH,TArgs>
		where TEH : class
		where TArgs : EventArgs
	{
		private TweakedEvent<TEH> eventTestEvent;

		public BaseTweakedEvent_Tests(TweakedEvent<TEH> initialEventValue)
		{
			this.eventTestEvent = initialEventValue;
		}

		protected abstract void AssertEventIsEmpty(TweakedEvent<TEH> eventStore);
		protected abstract void AssertEventContainsASingleEntry(TweakedEvent<TEH> eventStore);

		protected TweakedEvent<TEH> TestEventStore
		{
			get { return eventTestEvent; }
		}

		protected void TestEvent_Add(TEH handler)
		{
			TweakedEvent.Add(ref eventTestEvent, handler);
		}

		protected void TestEvent_Remove(TEH handler)
		{
			TweakedEvent.Remove(ref eventTestEvent, handler);
		}

		[Fact]
		public virtual void AddOne()
		{
			TestEvent_Add(GetHandler(GetNullListener<TArgs>()));

			AssertEventContainsASingleEntry(eventTestEvent);
		}

		[Fact]
		public virtual void AddTwo()
		{
			TestEvent_Add(GetHandler(GetNullListener<TArgs>()));
			TestEvent_Add(GetHandler(GetNullListener<TArgs>()));

			AssertEventContainsASingleEntry(eventTestEvent);
		}

		[Fact]
		public virtual void RemoveSameHandler()
		{
			var handler = GetHandler(GetNullListener<TArgs>());

			TestEvent_Add(handler);
			TestEvent_Remove(handler);

			AssertEventIsEmpty(eventTestEvent);
		}

		[Fact]
		public virtual void RemoveEquivalentHandler()
		{
			var listener = GetNullListener<TArgs>();

			TestEvent_Add(GetHandler(listener));
			TestEvent_Remove(GetHandler(listener));

			AssertEventIsEmpty(eventTestEvent);
		}

		[Fact]
		public virtual void Raise()
		{
			var mockListener = new Mock<IEventListener<TArgs>>();
			TestEvent_Add(GetHandler(mockListener.Object));
			var evArgs = GetNewEventArgs();

			eventTestEvent.Raise(this, evArgs);

			mockListener.Verify(l => l.Handler(this, evArgs), Times.Once());
			AssertEventContainsASingleEntry(eventTestEvent);
		}

		[Fact]
		public virtual void RaisePropagatesException()
		{
			var mockListener = new Mock<IEventListener<TArgs>>();
			mockListener.Setup( l => l.Handler(It.IsAny<object>(), It.IsAny<TArgs>()) ).Throws<ApplicationException>();

			TestEvent_Add(GetHandler(mockListener.Object));

			var ex = Assert.Throws<TweakedEventInvocationException>( () => eventTestEvent.Raise(this, EventArgs.Empty) );
			Assert.IsType(typeof(ApplicationException), ex.InvocationException);
		}
	}
}
