﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Moq;
using Xunit;
using JpLabs.TweakedEvents;
using System.Linq.Expressions;

namespace JpLabs.TweakedEventsTests
{
	public abstract class BaseTweakedEventEntry_Tests<TEH,TArgs> : TestMother<TEH,TArgs>
		where TEH : class
		where TArgs : EventArgs
	{
		public interface IEventDoubleListener
		{
			void Handler1(object sender, TArgs args);
			void Handler2(object sender, TArgs args);
		}

		private static class StaticListener
		{
			public static readonly Queue<Tuple<object,TArgs>> CallHistory = new Queue<Tuple<object,TArgs>>();

			public static void Handler(object sender, TArgs args)
			{
				CallHistory.Enqueue(Tuple.Create(sender, args));
			}
		}

		protected abstract BaseEventEntry<TEH> GetNewEntry(TEH handler);

		protected virtual BaseEventEntry<TEH> GetNewEntry()
		{
			return GetNewEntry(GetNullHandler());
		}

		protected void AssertEquals(BaseEventEntry<TEH> entry1, BaseEventEntry<TEH> entry2)
		{
			Assert.True(entry1.Equals(entry2));
			Assert.True(entry2.Equals(entry1));
			Assert.True(entry1.Equals((IEventEntry)entry2));
			Assert.True(entry2.Equals((IEventEntry)entry1));
		}

		protected void AssertNotEquals(BaseEventEntry<TEH> entry1, BaseEventEntry<TEH> entry2)
		{
			Assert.False(entry1.Equals(entry2));
			Assert.False(entry2.Equals(entry1));
			Assert.False(entry1.Equals((IEventEntry)entry2));
			Assert.False(entry2.Equals((IEventEntry)entry1));
		}

		protected TEH CreateStaticHandler(out Queue<Tuple<object,TArgs>> outCalls)
		{
			outCalls = StaticListener.CallHistory;

			return GetHandler(null, (object o) => StaticListener.Handler(null, null));
		}

		protected TEH CreateAnonymousHandler(out Mock<IEventListener<TArgs>> outMockListener)//out Queue<Tuple<object,TArgs>> outCalls)
		{
			var mockListener = new Mock<IEventListener<TArgs>>();//var calls = new Queue<Tuple<object,TArgs>>();
			outMockListener = mockListener;

			var func = (Action<object,TArgs>)(
				(sender, e) => { mockListener.Object.Handler(sender, e); } //calls.Enqueue(Tuple.Create(sender, e));  }
			);

			return (TEH)(object)Delegate.CreateDelegate(typeof(TEH), func.Target, func.Method);
		}

		[Fact]
		public virtual void Create()
		{
			var entry = GetNewEntry();

			Assert.NotNull(entry);
			Assert.False(entry.IsDisposed);
		}

		[Fact]
		public virtual void Dispose()
		{
			var entry = GetNewEntry();

			entry.Dispose();

			Assert.True(entry.IsDisposed);
		}

		[Fact]
		public virtual void Create_WithNullHandler_CreatesADisposedEntry()
		{
			var entry = GetNewEntry(null);

			Assert.NotNull(entry);
			Assert.True(entry.IsDisposed);
		}

		[Fact]
		public virtual void Invoke_CallsHandler()
		{
			var mockListener = new Mock<IEventListener<TArgs>>();
			var entry = GetNewEntry(GetHandler(mockListener.Object));

			var args = GetNewEventArgs();

			var isDisposed = entry.Invoke(this, args);

			Assert.False(isDisposed);
			mockListener.Verify(l => l.Handler(this, args), Times.Once());
		}

		[Fact]
		public virtual void Invoke_DoesntCallHandler_IfEntryIsDisposed()
		{
			var mockListener = new Mock<IEventListener<TArgs>>();
			var entry = GetNewEntry(GetHandler(mockListener.Object));
			entry.Dispose();

			var args = GetNewEventArgs();

			var isDisposed = entry.Invoke(this, args);

			Assert.True(isDisposed);
			mockListener.Verify(l => l.Handler(this, args), Times.Never());
		}

		[Fact]
		public virtual void Invoke_CallsStaticHandler()
		{
			Queue<Tuple<object,TArgs>> calls;
			var handler = CreateStaticHandler(out calls);

			var entry = GetNewEntry(handler);
			var args = GetNewEventArgs();

			calls.Clear();
			var isDisposed = entry.Invoke(this, args);
			var lastCall = calls.Dequeue();

			Assert.False(isDisposed);
			Assert.Equal(0, calls.Count);
			Assert.Equal(Tuple.Create<object,TArgs>(this, args), lastCall);
		}

		[Fact]
		public virtual void Invoke_CallsAnonymousHandler()
		{
			Mock<IEventListener<TArgs>> mockListener; //Queue<Tuple<object,TArgs>> calls;
			var handler = CreateAnonymousHandler(out mockListener);

			var entry = GetNewEntry(handler);
			var args = GetNewEventArgs();

			//calls.Clear();
			var isDisposed = entry.Invoke(this, args);
			//var lastCall = calls.Dequeue();

			Assert.False(isDisposed);
			//Assert.Equal(0, calls.Count);
			//Assert.Equal(Tuple.Create<object,TArgs>(this, args), lastCall);
			mockListener.Verify(l => l.Handler(this, args), Times.Once());
		}

		[Fact]
		public virtual void Invoke_PropagatesException()
		{
			var handler = GetThrowingHandler<ApplicationException>();

			var entry = GetNewEntry(handler);

			var ex = Assert.Throws<TweakedEventInvocationException>( () => entry.Invoke(null, null) );
			Assert.IsType(typeof(ApplicationException), ex.InvocationException);
		}

		[Fact]
		public virtual void EqualsHandler_WithSameTargets_AndSameMethods()
		{
			var listener = GetNullListener<TArgs>();
			var entry = GetNewEntry(GetHandler(listener));

			Assert.True(entry.EqualsHandler(GetHandler(listener)));
		}

		[Fact]
		public virtual void NotEqualsHandler_WithDifferentTarget()
		{
			var entry = GetNewEntry();

			Assert.False(entry.EqualsHandler(GetHandler(GetNullListener<TArgs>())));
		}

		[Fact]
		public virtual void NotEqualsHandler_WithDifferentMethods()
		{
			var listener = new Mock<IEventDoubleListener>().Object;
			var entry = GetNewEntry(GetHandler(listener, l => l.Handler1(null, null)));

			Assert.False(entry.EqualsHandler(GetHandler(listener, l => l.Handler2(null, null))));
		}

		[Fact]
		public virtual void EqualsOther()
		{
			var listener = GetNullListener<TArgs>();
			var entry1 = GetNewEntry(GetHandler(listener));
			var entry2 = GetNewEntry(GetHandler(listener));

			AssertEquals(entry1, entry2);
		}

		[Fact]
		public virtual void Equals_IfBothDisposed()
		{
			var entry1 = GetNewEntry();
			var entry2 = GetNewEntry();

			entry1.Dispose();
			entry2.Dispose();

			AssertEquals(entry1, entry2);
		}

		[Fact]
		public virtual void NotEqualsDisposedOther()
		{
			var listener = GetNullListener<TArgs>();
			var entry1 = GetNewEntry(GetHandler(listener));
			var entry2 = GetNewEntry(GetHandler(listener));

			entry2.Dispose();

			AssertNotEquals(entry1, entry2);
		}

		[Fact]
		public virtual void NotEqualsOtherWithDifferentTarget()
		{
			var entry1 = GetNewEntry();
			var entry2 = GetNewEntry();

			AssertNotEquals(entry1, entry2);
		}

		[Fact]
		public virtual void NotEqualsOtherWithDifferentMethods()
		{
			var listener = new Mock<IEventDoubleListener>().Object;
			var entry1 = GetNewEntry(GetHandler(listener, l => l.Handler1(null, null)));
			var entry2 = GetNewEntry(GetHandler(listener, l => l.Handler2(null, null)));

			AssertNotEquals(entry1, entry2);
		}
	}
}
