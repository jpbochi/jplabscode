﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using JpLabs.TweakedEvents;
using Moq;
using Xunit;
using System.Collections;
using System.Diagnostics;
using JpLabs.TweakedEvents.Synced;
using System.Threading;
using System.ComponentModel;

namespace JpLabs.TweakedEventsTests
{
	public sealed class SyncedEvent_Tests : BaseTweakedEvent_Tests<EventHandler,EventArgs>
	{
		public SyncedEvent_Tests() : base(new SyncedEvent<EventHandler>())
		{}

		protected override void AssertEventIsEmpty(TweakedEvent<EventHandler> eventStore)
		{
			Assert.IsType(typeof(SyncedEvent<EventHandler>), eventStore);

			Assert.True(eventStore.IsEmpty());
		}

		protected override void AssertEventContainsASingleEntry(TweakedEvent<EventHandler> eventStore)
		{
			Assert.IsType(typeof(SyncedEvent<EventHandler>), eventStore);

			Assert.False(eventStore.IsEmpty());
		}

		[Fact]
		public void CreateEntry_ReturnsASyncContextEventEntry_IfSyncObjIsNull()
		{
			var handler = GetNullHandler();

			var entry = SyncedEvent<EventHandler>.CreateEntry(handler, null);

			Assert.IsType(typeof(SyncContextEventEntry<EventHandler>), entry);
		}

		[Fact]
		public void CreateEntry_ReturnsASyncContextEventEntry_IfSyncObjIsASynchronizationContext()
		{
			var handler = GetNullHandler();
			var syncContext = GetNull<SynchronizationContext>();

			var entry = SyncedEvent<EventHandler>.CreateEntry(handler, syncContext);

			Assert.IsType(typeof(SyncContextEventEntry<EventHandler>), entry);
		}

		[Fact]
		public void CreateEntry_ReturnsASyncContextEventEntry_IfSyncObjIsAISynchronizeInvoke()
		{
			var handler = GetNullHandler();
			var syncInvoke = GetNull<ISynchronizeInvoke>();

			var entry = SyncedEvent<EventHandler>.CreateEntry(handler, syncInvoke);

			Assert.IsType(typeof(SyncInvokeEventEntry<EventHandler>), entry);
		}
	}
}
