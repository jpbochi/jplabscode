﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Moq;
using Xunit;
using JpLabs.TweakedEvents;
using System.Linq.Expressions;
using System.Threading;
using JpLabs.TweakedEvents.Synced;

namespace JpLabs.TweakedEventsTests
{
	public sealed class SyncContextEventEntry_Tests : BaseTweakedEventEntry_Tests<EventHandler,EventArgs>
	{
		#warning TODO: Test SyncContextEventEntry decoration capabilities

		protected override BaseEventEntry<EventHandler> GetNewEntry(EventHandler handler)
		{
			return new SyncContextEventEntry<EventHandler>(handler);
		}

		private BaseEventEntry<EventHandler> GetNewEntry(EventHandler handler, SynchronizationContext syncContext)
		{
			return new SyncContextEventEntry<EventHandler>(handler, syncContext);
		}

		private SynchronizationContext GetWorkingSynchronizationContext()
		{
			var mockSyncContext = new Mock<SynchronizationContext>();
			mockSyncContext
				.Setup( ctx => ctx.Send(It.IsAny<SendOrPostCallback>(), It.IsAny<object>()) )
				.Callback( (SendOrPostCallback callback, object state) => callback(state) );
			
			return mockSyncContext.Object;
		}

		[Fact]
		public void EqualsOther_WithSameISynchronizeInvoke()
		{
			var syncContext = GetNull<SynchronizationContext>();
			var listener = GetNullListener<EventArgs>();

			var entry1 = GetNewEntry(GetHandler(listener), syncContext);
			var entry2 = GetNewEntry(GetHandler(listener), syncContext);

			AssertEquals(entry1, entry2);
		}

		[Fact]
		public void NotEqualsOther_WithDifferentISynchronizeInvoke()
		{
			var listener = GetNullListener<EventArgs>();

			var entry1 = GetNewEntry(GetHandler(listener), GetNull<SynchronizationContext>());
			var entry2 = GetNewEntry(GetHandler(listener), GetNull<SynchronizationContext>());

			AssertNotEquals(entry1, entry2);
		}

		[Fact]
		public void Invoke_Calls_SynchronizationContext_Send()
		{
			var handler = GetNullHandler();
			var mockSyncContext = new Mock<SynchronizationContext>();

			var entry = GetNewEntry(handler, mockSyncContext.Object);

			entry.Invoke(null, null);

			mockSyncContext.Verify( ctx => ctx.Send(It.IsAny<SendOrPostCallback>(), It.IsAny<object>()), Times.Once());
		}

		[Fact]
		public void Invoke_CallsHandler_ThroughSynchronizationContext()
		{
			var mockListener = new Mock<IEventListener<EventArgs>>();
			var handler = GetHandler(mockListener.Object);
			var syncContext = GetWorkingSynchronizationContext();

			var entry = GetNewEntry(handler, syncContext);
			var args = GetNull<EventArgs>();

			entry.Invoke(this, args);

			mockListener.Verify(l => l.Handler(this, args), Times.Once());
		}

		[Fact]
		public void Invoke_PropagatesException_ThroughSynchronizationContext()
		{
			var handler = GetThrowingHandler<ApplicationException>();
			var syncContext = GetWorkingSynchronizationContext();

			var entry = GetNewEntry(handler, syncContext);

			var ex = Assert.Throws<TweakedEventInvocationException>( () => entry.Invoke(null, null) );
			Assert.IsType(typeof(ApplicationException), ex.InvocationException);
		}
	}
}
