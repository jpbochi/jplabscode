﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Moq;
using Xunit;
using JpLabs.TweakedEvents;
using System.Linq.Expressions;
using System.Threading;
using JpLabs.TweakedEvents.Synced;
using System.ComponentModel;

namespace JpLabs.TweakedEventsTests
{
	public sealed class SyncInvokeEventEntry_Tests : BaseTweakedEventEntry_Tests<EventHandler,EventArgs>
	{
		#warning TODO: Test SyncInvokeEventEntry decoration capabilities

		protected override BaseEventEntry<EventHandler> GetNewEntry(EventHandler handler)
		{
			return GetNewEntry(handler, null);
		}

		private BaseEventEntry<EventHandler> GetNewEntry(EventHandler handler, ISynchronizeInvoke syncInvoke)
		{
			return new SyncInvokeEventEntry<EventHandler>(handler, syncInvoke);
		}

		private ISynchronizeInvoke GetWorkingSynchronizeInvoke()
		{
			var mockSyncInvoke = new Mock<ISynchronizeInvoke>();
			mockSyncInvoke.SetupGet( s => s.InvokeRequired ).Returns(true);
			mockSyncInvoke
				.Setup( s => s.Invoke(It.IsAny<Delegate>(), It.IsAny<object[]>()) )
				.Callback( (Delegate method, object[] _args) => method.DynamicInvoke(_args) );
			
			return mockSyncInvoke.Object;
		}

		[Fact]
		public void EqualsOther_WithSameISynchronizeInvoke()
		{
			var syncInvoke = GetNull<ISynchronizeInvoke>();
			var listener = GetNullListener<EventArgs>();

			var entry1 = GetNewEntry(GetHandler(listener), syncInvoke);
			var entry2 = GetNewEntry(GetHandler(listener), syncInvoke);

			AssertEquals(entry1, entry2);
		}

		[Fact]
		public void NotEqualsOther_WithDifferentISynchronizeInvoke()
		{
			var listener = GetNullListener<EventArgs>();

			var entry1 = GetNewEntry(GetHandler(listener), GetNull<ISynchronizeInvoke>());
			var entry2 = GetNewEntry(GetHandler(listener), GetNull<ISynchronizeInvoke>());

			AssertNotEquals(entry1, entry2);
		}

		[Fact]
		public void Invoke_Calls_ISynchronizeInvoke_InvokeRequired()
		{
			var handler = GetNullHandler();
			var mockSyncInvoke = new Mock<ISynchronizeInvoke>();

			var entry = GetNewEntry(handler, mockSyncInvoke.Object);

			entry.Invoke(null, null);

			mockSyncInvoke.Verify( s => s.InvokeRequired, Times.Once());
		}

		[Fact]
		public void Invoke_DoesntCallHandler_IfInvokeRequiredReturnsFalse()
		{
			var handler = GetNullHandler();
			var mockSyncInvoke = new Mock<ISynchronizeInvoke>();
			mockSyncInvoke.SetupGet( s => s.InvokeRequired ).Returns(false);

			var entry = GetNewEntry(handler, mockSyncInvoke.Object);

			entry.Invoke(null, null);

			mockSyncInvoke.Verify( s => s.Invoke(It.IsAny<Delegate>(), It.IsAny<object[]>()), Times.Never());
		}

		[Fact]
		public void Invoke_CallHandler_IfInvokeRequiredReturnsTrue()
		{
			var handler = GetNullHandler();
			var mockSyncInvoke = new Mock<ISynchronizeInvoke>();
			mockSyncInvoke.SetupGet( s => s.InvokeRequired ).Returns(true);

			var entry = GetNewEntry(handler, mockSyncInvoke.Object);

			entry.Invoke(null, null);

			mockSyncInvoke.Verify( s => s.Invoke(It.IsAny<Delegate>(), It.IsAny<object[]>()), Times.Once());
		}

		[Fact]
		public void Invoke_CallsHandler_ThroughISynchronizeInvoke()
		{
			var mockListener = new Mock<IEventListener<EventArgs>>();
			var handler = GetHandler(mockListener.Object);
			var syncInvoke = GetWorkingSynchronizeInvoke();

			var entry = GetNewEntry(handler, syncInvoke);
			var args = GetNull<EventArgs>();

			entry.Invoke(this, args);

			mockListener.Verify(l => l.Handler(this, args), Times.Once());
		}

		[Fact]
		public void Invoke_PropagatesException_ThroughISynchronizeInvoke()
		{
			var handler = GetThrowingHandler<ApplicationException>();
			var syncInvoke = GetWorkingSynchronizeInvoke();

			var entry = GetNewEntry(handler, syncInvoke);

			var ex = Assert.Throws<TweakedEventInvocationException>( () => entry.Invoke(null, null) );
			Assert.IsType(typeof(ApplicationException), ex.InvocationException);
		}
	}
}
