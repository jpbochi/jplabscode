﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Moq;
using System.Diagnostics;
using Xunit;
using System.Linq.Expressions;

namespace JpLabs.TweakedEventsTests
{
	public class TestMother
	{
		protected T GetNull<T>() where T : class
		{
			return new Mock<T>().Object;
		}

		protected IEventListener<EventArgs> GetNullListener()
		{
			return GetNull<IEventListener<EventArgs>>();
		}

		protected IEventListener<TEventArgs> GetNullListener<TEventArgs>() where TEventArgs : EventArgs
		{
			return GetNull<IEventListener<TEventArgs>>();
		}
	}

	public abstract class TestMother<TEH,TArgs> : TestMother
		where TEH : class
		where TArgs : EventArgs
	{
		protected virtual TEH GetNullHandler()
		{
			return GetHandler(GetNullListener<TArgs>());
		}

		protected virtual TEH GetHandler(IEventListener<TArgs> listener)
		{
			return GetHandler(listener, l => l.Handler(null, null));
		}

		protected virtual TEH GetHandler<T>(T listener, Expression<Action<T>> callHandlerExpr)
		{
			var methodInfo = ((MethodCallExpression)callHandlerExpr.Body).Method;

			return (TEH)(object)Delegate.CreateDelegate(typeof(TEH), listener, methodInfo);
		}
		
		protected virtual TEH GetThrowingHandler<TException>() where TException : Exception, new()
		{
			var mockListener = new Mock<IEventListener<TArgs>>();
			mockListener
				.Setup( l => l.Handler(It.IsAny<object>(), It.IsAny<TArgs>()) )
				.Throws<TException>();

			return GetHandler(mockListener.Object);
		}

		protected virtual TArgs GetNewEventArgs()
		{
			return new Mock<TArgs>().Object;
		}
	}
}
