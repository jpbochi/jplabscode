﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JpLabs.TweakedEventsTests
{
	public interface IEventListener
	{
		void Handler(object sender, EventArgs args);
	}

	public interface IEventListener<TEventArgs> where TEventArgs : EventArgs
	{
		void Handler(object sender, TEventArgs args);
	}
}
