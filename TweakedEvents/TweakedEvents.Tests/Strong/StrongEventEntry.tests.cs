﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Moq;
using Xunit;
using JpLabs.TweakedEvents;
using System.Linq.Expressions;

namespace JpLabs.TweakedEventsTests
{
	public sealed class StrongEventEntry_Tests : BaseTweakedEventEntry_Tests<EventHandler,EventArgs>
	{
		protected override BaseEventEntry<EventHandler> GetNewEntry(EventHandler handler)
		{
			return new StrongEventEntry<EventHandler>(handler);
		}
	}
}
