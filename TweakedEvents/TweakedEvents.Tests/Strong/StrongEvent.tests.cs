﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using JpLabs.TweakedEvents;
using Moq;
using Xunit;
using System.Collections;
using System.Diagnostics;

namespace JpLabs.TweakedEventsTests
{
	public sealed class StrongEvent_Tests : BaseTweakedEvent_Tests<EventHandler,EventArgs>
	{
		public StrongEvent_Tests() : base(null)
		{}

		protected override void AssertEventIsEmpty(TweakedEvent<EventHandler> eventStore)
		{
			Assert.Null(eventStore);
		}

		protected override void AssertEventContainsASingleEntry(TweakedEvent<EventHandler> eventStore)
		{
			Assert.IsType(typeof(StrongEvent<EventHandler>), eventStore);

			//Unfortunately, we can't see the handler count
			Assert.False(eventStore.IsEmpty());
		}
	}
}
