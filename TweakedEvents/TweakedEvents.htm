﻿<!--
http://www.codeproject.com/KB/cs/WeakEvents.aspx
http://www.codeproject.com/script/Articles/SubmitStep1.aspx
 -->

<head>
	<style type="text/css">
		.green
		{
			color: #008000;
		}
		.red
		{
			color: #CC3300;
		}
	</style>
</head>

<pre>
Title:       Tweaked Events
Author:      jpbochi
Email:       jpbochi@gmail.com
Member ID:   2628692
Language:    C#
Platform:    Anything that runs .NET
Technology:  .NET2.0, .NET3.0, .NET3.5, C#
Level:       Intermediate
Description: Weak Events, Synced Events, and more.
Section:     Languages &gt; C# &gt; Delegates and Events  
License:     MIT
</pre>

<h2>Introduction</h2>

<p>
	<a href='http://msdn.microsoft.com/en-us/library/awbftdfh.aspx'>C# events</a> are
	one of my favorite language features. Coupled with .NET <a href='http://msdn.microsoft.com/en-us/library/ms173171.aspx'>
		delegates</a>, the observer pattern can be implemented so transparently
	that the pattern becomes invisible. And it&#39;s a very flexible feature. Apart from 
	using different delegate signatures, it&#39;s possible to overload the <code>add</code> and <code>remove</code>
	operators in order to achieve a deep level of customization. Here, I&#39;ll describe 
	a framework that I&#39;ve created to help people customize events.</p>
<p>
	Quite a few&nbsp;observer pattern implementations have a common problem. If the observer 
	fails to unregister from an observable, a strong reference to the observer will 
	be kept in the observable. Garbage collection systems will not collect the 
	forgotten observers until the the observable is collected. This is one big 
	source of memory leaks. C# events and Java listeners are both prone to this 
	problem. <a href='http://www.codeproject.com/KB/cs/WeakEvents.aspx'>
	Daniel Grunwald&#39;s Weak Events</a> solve the obserser leak problem using 
	a customized event implementation. In his article, he discusses extensively how 
	the <a href='http://msdn.microsoft.com/en-gb/library/aa970850.aspx'>
	weak event pattern</a> can be implemented in C#. His solution is very 
	reusable, but not very extensible. The code in the article you&#39;re reading was inspired by 
	Daniel&#39;s work, and is an attempt to make it much more extensible.</p>
<p>
	Any customized C# event works by overriding the <code>add</code> and <code>remove</code>
	event accessors, and implementing an internal <a href='http://msdn.microsoft.com/en-us/magazine/cc163533.aspx'>
	custom store</a> for the event handler delegates. This technique can be used to customize
	events in many diverse ways. I created a base 
	extensible framework that should be able to support&nbsp; a broad array of event types. 
	I implemented a couple of these types, and let others suggested for future work. 
	The framework is also able to merge behaviors from different event types 
	facilitating the use of event
	<a href='http://en.wikipedia.org/wiki/Decorator_pattern'>decorators</a>.</p>
<p>
	This is an open source initiative, so suggestion, bug fixes, and code 
	additions are more than welcome. In the rest of the text, I&#39;ll show how to use 
	my code, how it works, and how you can extend it.&nbsp;</p>
<h2>What is the difference between delegates and events?</h2>

<p>
	Before I proceed, I want to answer this question very clearly. The short 
	answer is this. Events and delegates are completely different concepts. 
	Delegates can be used without events, but events can&#39;t be used without 
	delegates.
	Delegates are <a href="http://en.wikipedia.org/wiki/Function_object">function 
	objects</a>. C# events are specialized properties with unusual accessors. 
	Regular properties have <code>get</code> and <code>set</code>. Events have <code>add</code> and <code>remove</code> (even if you don&#39;t 
	explicitly 
	declare them). Accessors are nothing more than a set of methods bound together 
	by the same name.</p>
<p>
	Looking at the accessors&#39; names (<code>add</code> and <code>remove</code>) I 
	could suppose that they are related to collections (or lists, sets, etc.). Being 
	bolder, I&#39;ll dare to affirm that they are meant to be used only with 
	collection-like data structures. In other words, I&#39;m saying that events are 
	interfaces to collections. Being more specific, I say that <i>events are 
	interfaces to a collection of delegates</i>.</p>
<p>
	Alright, I know that non-customized events can only hold a single delegate, not a collection of them. 
	Does that invalidate my statement? No, not really. Enter multicasting. Delegates have this very interesting 
	ability of being merged into a single multicast instance. Such operations work 
	in an immutable way, i.e., the addition (or subtraction) of two delegates 
	generate a new delegate without modifying the operands. Strictly speaking, <i>
	any delegate instance is an immutable collection of singlecast function objects</i>. Invocation features, like 
	multicasting, are built inside delegates, not events.</p>
<p>
	Then, a class that declares a non-customized event is declaring a protected 
	delegate field and exposing two operations to it. External components will be 
	able to <code>add</code> and <code>remove</code> only the delegates they know 
	about. The code that has access to the delegate beneath the event is also able 
	to invoke and reset its value. </p>

<h2>
	Concepts /
	Vocabulary</h2>
<p>
	I&#39;ll try to avoid confusion by using the vocabulary of definitions listed in this extense
	<a href='http://www.codeproject.com/KB/cs/event_fundamentals.aspx'>article</a>. 
	I&#39;ll add a couple of my own.</p>
<p>
	<b>Event Store</b> - It&#39;s the data structure resposible for holding a 
	collection of <i>Event Entries</i> (defined below). They must be invokable, just 
	like a multicast delegate. Stores are 
	also responsible for creating entries from delegates. Any class derived from <code>TweakedEvent</code> 
	is an <i>Event Store</i>;</p>
<p>
	<b>Event Entry</b> - It&#39;s a single item that can be contained in an <i>Event Store</i>. 
	It works like a singlecast function object. It&#39;s usually a wrapper around a 
	delegate instance. Some entries can wrap other entries, working as decorators. Any 
	subclass of <code>BaseEventEntry</code> is an Event Entry;</p>

<h2>
	Using Tweaked Events
</h2>
<p>
	Before I get to the implementation details, let me show a basic example of how 
	can you use Tweaked Events. Consider that you have an event source like the one 
	below. It has a single event named <code>StatusChanged</code>, and an method (<code>OnStatusChanged</code>) 
	that raises it. Naturally, the recommended pattern of invoking delegates is 
	used.</p>
<pre>
class RegularEventSource
{
	public event EventHandler StatusChanged;

	private void OnStatusChanged(EventArgs e)
	{
		var dlg = StatusChanged;
		if (dlg != null) dlg(this, e);
	}
}
</pre>

<p>
	Now, look at an equivalent code using Tweaked Events 
	below.</p>
<pre>
class TweakedEventSource
{
	private TweakedEvent&lt;EventHandler&gt; eventStatusChanged;

	public event EventHandler StatusChanged
	{
		add    { TweakedEvent.Add(ref eventStatusChanged, value); }
		remove { TweakedEvent.Remove(ref eventStatusChanged, value); }
	}

	private void OnStatusChanged(EventArgs e)
	{
		eventStatusChanged.Raise(this, e);
	}
}
</pre>

<p>
	Let&#39;s observe the differences. First, a <code>private</code> field has to be expicitly 
	declared. It will hold our event store. My suggested naming practice for such 
	fields is prefixing the name of the event with <i>event</i>. So, the field is named <code>eventStatusChanged</code>.</p>
<p>
	&nbsp;The second difference is the explicit implementation of the  
	accessors (<code>add</code> and <code>remove</code>). In order to transparently 
	handle <code>null</code> values and use proper synchronization, I created two <code>static</code> 
	helper methods, <code>TweakedEvent.Add</code> and <code>TweakedEvent.Remove</code>. 
	They will execute a lock-free synchronization that updates the tweaked event store 
	passed by reference. I&#39;ll talk more 
	about this synchronization later. These methods are recommended for all users of 
	Tweaked Events. I don&#39;t currently see any reason to implement <code>add</code> and <code>remove</code> 
	in any other way.</p>
<p>
	The last difference in this example is the event raising code. I created the <code>Raise</code> 
	extension method that handles <code>null</code> values for you. Since it&#39;s an extension method, it&#39;s capable of handling <code>
	null</code> values for you. This simple trick can be done
	<a href="http://kohari.org/2009/02/07/eventhandler-extension-method/">for regular 
	delegates too</a>.</p>
<p>
	The question now is: what have we achieved so far? Almost nothing. Both <code>TweakedEventSource</code> 
	and <code>RegularEventSource</code> have the same behavior, with different overheads. 
	The only difference might be the lock-free synchronization, if you are not compiling 
	this with C# 4 (again, I&#39;ll get into this later).</p>
<p>
	So, if we want to get the real benefits of Tweaked Events, there&#39;s only a small 
	step that has to be done. We have to choose an event 
	type. Suppose we want to transform <code>TweakedEventSource.StatusChanged</code> 
	into a weak event (just like Daniel&#39;s). All we need to do is initialize the event 
	store field 
	in the class constructor, as follows. When delegates are added, the event store 
	will wrap then into <code>WeakEventEntry</code> instances. Every other detail 
	work as it should.</p>
<pre>
public TweakedEventSource()
{
	eventStatusChanged = new WeakEvent&lt;EventHandler&gt;();
}</pre>

<h2>Events Variations</h2>
<p>The only event variation I mentioned so far was Weak Events. As I said, this is 
	only one of many possible event variations. Later in this article, I will introduce 
	<i>Synced Events</i>. There are many others, though. Here&#39;s a sample list of 
	ways events can vary. The base code is probably able to cover all of them with very few 
	modifications. If you envision a different type, please, tell me. I&#39;d love to 
	hear about it.</p>
<ul>
	<li>Event entry variations:<ul>
		<li>Weak references to handler delegates (Weak Events);</li>
		<li>Synchronized invocations (<a href='http://msdn.microsoft.com/en-us/library/system.componentmodel.isynchronizeinvoke.aspx'>ISynchronizeInvoke</a>,
			<a href='http://msdn.microsoft.com/en-us/library/system.threading.synchronizationcontext.aspx'>SynchronizationContext</a>,
			<a href='http://msdn.microsoft.com/en-us/library/system.windows.threading.dispatcher.aspx'>Dispatcher</a>,
			<a href='http://msdn.microsoft.com/en-us/library/system.threading.tasks.taskscheduler.aspx'>TaskScheduler</a>, etc.);</li>
		<li>Asynchronous invocations (BeginInvoke-like);</li>
		</ul>
	</li>
	<li>Event store variations:<ul>
		<li>Ordered invocations (perhaps with a priority queue);</li>
		<li>Concurrent invocations (i.e., all event handlers called concurrently);</li>
		</ul>
	</li>
	<li>Exception handling:<ul>
		<li><a href='http://msdn.microsoft.com/en-us/library/system.aggregateexception.aspx'>Aggregate exceptions</a> (as 
			opposed to 
			throwing the first exception that happens);</li>
		<li>Attachable handler (e.g., for logging or auditing purposes);</li>
		</ul>
	</li>
	<li>Custom <a href='http://msdn.microsoft.com/en-us/library/h846e9b3.aspx'>code 
		access permision</a>;</li>
	<li>Memento Events (it remembers all invocations; when a handler/entry is added, the 
		past invocations are repeated to the new handler/entry);</li>
</ul>

<p>Notice that some variations are typically implemented in event entries, whereas 
	others are implemented in stores. In some complex cases, might involve some 
	coordination between both classes.</p>

<h2>
	Tweaked Event features</h2>

<p>
	In this section, I&#39;ll discuss some of the features that come with Tweaked Events 
	framework out of the box.</p>
<h3>
	Convertibility / Equatability</h3>
<p>
	One important detail of Tweaked Event Stores 
	is that thay can only hold Tweaked Event 
	Entries. They are not designed to hold delegates. In order to add delegates from 
	an event store, we need to wrap them into event entries.
	Like I said earlier, <i>event stores are responsible for converting delegates 
	into event entries</i>. Tweaked event store classes will override the <code>CreateEntry</code> method, which 
	does this job.</p>
<p>
	Remove operations are trickier. There must be a way to match a delegate to be 
	removed with one equivalent event entry. That&#39;s why all <i>event entries are 
	equatable to delegates</i>. For remove operations to work, the <code>
	EqualsHandler</code> method has to be 
	correctly coded in all event entry types.</p>
<h3>Event Entry Composition / Decoration</h3>
<p>
	Event entries are not restricted to be wrappers around delegates. In fact, 
	anything that&#39;s invocable could be held inside an entry. Well, event entries are 
	invocable themselves. So, what kind of monster is an entry that wraps other 
	entries? An entry decorator is the answer! This kind of construction is almost 
	the definition of the <a href="http://en.wikipedia.org/wiki/Decorator_pattern">
	decorator pattern</a>.</p>
<p>
	It&#39;s possible to write an entry type that can either wrap a delegate or another 
	entry. The <code>SynchEventEntry</code> that I created follows this pattern. It&#39;s meant to 
	invoke its inner entry (or delegate) in a synchronized way, i.e., it will 
	actually run in another thread.</p>
<h3>
	Immutability 
	of Event Stores</h3>
<p>
	One fact about delegates is that they are immutable. 
	Just like numbers and strings, you can&#39;t alter a delegate. You can only create 
	new ones. For example, you can add (or 
	subtract) two delegates into a new third delegate. When a delegate is passed to 
	the <code>add</code> accessor of a regular event, it&#39;s added to the current 
	delegate inside the event, and the current event is updated.</p>
<p>
	Daniel&#39;s 
	followed a different strategy in his Weak Events. They are mutable. The store field 
	is designed to be 
	initialized once and never to be overwritten. So, there&#39;s no need to worry about it being 
	null or being substituted for another instance. Instead, entries are added (or removed) from 
	its inner collection directly.</p>
<p>For reasons that I&#39;ll explain later, I decided to make 
	Tweaked Events immutable. By that, I mean that event stores are immutable. Any 
	store update (add, remove, clear) operation is composed by the creation of a new 
	value and a subsequent update of the store variable. Regular events work in 
	exactly the same way. Naturally, there are pros and cons of store immutability. 
	The main differences revolve around concurrency and synchronization, so I&#39;ll 
	discuss that now.</p>
<h4>
	Synchronization in Add, Remove and Invoke operations 
	for Immutable Event Stores</h4>
<p>
	Immutable objects fit very well in multithreaded applications. 
	No synchronization is needed inside them because nothing inside them will ever 
	change. However, a variable can be used to share 
	immutable objects. The objects themselves won&#39;t change, but the variable can be 
	updated with a new object. Regular events work like this. Its shared value (i.e., a 
	delegate) is immutable, but the value might change (on add and remove). In order 
	to avoid concurrency issues, the access to the shared state has to be 
	synchronized somehow.</p>
<p>
	Let&#39;s start with <b>concurrent writes</b> first. It happens when two or more threads are 
	<a href="http://en.wikipedia.org/wiki/Race_condition">racing</a> to write a new value to the same variable. The last one to finish might 
	undo 
	the work of the others. Depending on the purpose of the software component, this might be 
	perfectly fine. 
	In most cases, though, it&#39;s plainly 
	incorrect. Events qualify as such cases. Two threads can race trying to add (or 
	remove) handlers to the same event. Without proper synchronization, the 
	resulting event might not contain one of the handlers, which is not correct.</p>
<p>
	Until C# 3, a simple lock was used to control access to the event accessors.&nbsp;Only 
	one thread can update an event at a time. This approach is too simplistic. In C# 4, 
	events
	got a
	little
	overhaul
	(as 
	<a href='http://blogs.msdn.com/cburrows/archive/2010/03/05/events-get-a-little-overhaul-in-c-4-part-i-locks.aspx'>explained</a>
	<a href='http://blogs.msdn.com/cburrows/archive/2010/03/08/events-get-a-little-overhaul-in-c-4-part-ii-semantic-changes-and.aspx'>by</a>
	<a href='http://blogs.msdn.com/cburrows/archive/2010/03/18/events-get-a-little-overhaul-in-c-4-part-iii-breaking-changes.aspx'>Chris</a>
	<a href='http://blogs.msdn.com/b/cburrows/archive/2010/03/30/events-get-a-little-overhaul-in-c-4-afterward-effective-events.aspx'>Burrows</a>). 
	
	
	A
	<a href='http://en.wikipedia.org/wiki/Compare-and-swap'>compare-and-swap</a> 
	lock-free mechanism is intrinsically implemented by the C# 4 compiler. This 
	change introduces some breaking changes, but it&#39;s so much better then the older 
	ones that its advantages outweigh the breaks it might insert in working code. I 
	also wrote an article about this lock-free 
	synchronization
	<a href='http://jp-labs.blogspot.com/2010/03/lock-free-synchronization-in-c-4-events.html'>
	here</a>.</p>
<p>
	<b>Multiple reads</b> in sequence of a shared variable may cause another problem. If the shared state is updated between the reads, unexpected things 
	might happen. That&#39;s why there&#39;s a
	<a href='http://blogs.msdn.com/b/ericlippert/archive/2009/04/29/events-and-races.aspx'>
	recommended pattern</a> for invoking event delegates. You should read the 
	delegate only once, and do all operations (check for null and invocation) using 
	the copied reference.</p>
<p>
	In order to solve all these problems, Tweaked Events have three static methods (<code>TweakedEvent.Add,</code> <code>TweakedEvent.Remove</code>, 
	and <code>TweakedEvent.Raise</code>). All of them are thread-safe and null-safe. The 
	first two 
	mimic the lock-free 
	synchronization mechanism implemented in C# 4 events. The latter method provides 
	a simple safe way to invoke tweaked events.</p>
<h4>
	Synchronization for Mutable Event Stores</h4>
<p>
	Let&#39;s now compare how mutable event stores have to deal with the concurrency 
	problems I mentioned above. In add and remove, a compare-and swap can&#39;t be used. 
	The variable holding the event store doesn&#39;t change, it&#39;s the event store itself 
	that changes. An entry will be either added or removed from there. Adding or 
	removing items from a mutable list might cause an internal allocation of new 
	resized array. This is definitely not an atomic operation, so a lock has to be 
	used. The advantage is that the operation may be very fast to execute, while an 
	immutable store is almost obliged to copy every entry for every add or remove. 
	One is faster, the other doesn&#39;t need locks.</p>
<p>
	Before taking a side, let&#39;s look at invocation. We saw that all that&#39;s needed to 
	invoke an immutable stores is to ensure a single read. We keep a reference to 
	the current store in a local variable and that&#39;s it. On the other hand, mutable 
	stores might change while the invocation is going on. A lock around the 
	invocation code could solve the problem, but it creates another. If we do that, 
	the event will be locked while user code is running. This is very dangerous. 
	User code can take an arbitrarily amount of time to execute. Locking has to 
	occur only for very short periods. The only alternative is creating a temporary 
	local 
	copy of all entries, and running the invocation on the copy. Of course, a lock 
	is needed during the copy.</p>
<p>
	Here&#39;s a summary, if you got lost in my reasoning.</p>
<p>
<table>
	<tr>
		<td style="width: 125px;text-align:center;">Operation</td>
		<td style="width: 220px;text-align:center;">Immutable Store</td>
		<td style="width: 220px;text-align:center;">Mutable Store</td>
	</tr>
	<tr>
		<td rowspan="2">Add/remove</td>
		<td><span class="red">Full copy</span> of entries every time</td>
		<td><span class="green">Very fast</span> most of the times</td>
	</tr>
	<tr>
		<td><span class="green">Lock-free</span> compare-and-swap</td>
		<td><span class="red">Locks required</span></td>
	</tr>
	<tr>
		<td rowspan="2">Invoke</td>
		<td><span class="green">Simple</span> reference copy</td>
		<td><span class="red">Full copy</span> of entries every time</td>
	</tr>
	<tr>
		<td><span class="green">Lock-free</span></td>
		<td><span class="red">Lock required</span> around the copy</td>
	</tr>
</table>
</p>
<p>
	&nbsp;Immutable store might be closely defeated on add and remove, but they 
	definitely are better at invocation. If we consider that invokes are much more 
	common than adds or removes, immutability wins easily. And there&#39;s one last 
	comment I would like to make on this subject. I said that immutable stores 
	require a full copy in add and remove. This is not completely true. Operation on 
	immutable structures don&#39;t always require a deep cloning of its content. This is 
	a complex subject, though. If you are interested, I suggest you read
	<a href="http://blogs.msdn.com/b/ericlippert/archive/tags/immutability/">Eric&#39;s 
	posts</a>.</p>
<h4>
	New Tweaked Event Stores Implementations</h4>
<p>
	This framework is designed to be extensible. New event types can and should be 
	implemented. Although immutability is not enforced, development of new event 
	types have to have immutability in mind. State change in stores and entries is 
	strongly discouraged, but it might be acceptable is some situations. Naturally, 
	restrictions are not a good thing. If you feel uncomfortable to code immutable 
	structures now, you might learn to love their advantages later. For one, they 
	are naturally thread-safe (at least internally).</p>

<h3>
	Listener-side Tweaked 
	Events</h3>
<p>
	Have you ever wished you could add a customized event entry to a regular 
	event? There are times when you simply can&#39;t change the event source. Until now, 
	I only talked about source-side events. There&#39;s another side of the story, 
	though. Some situations call for listener-side events. I&#39;m proud to say that 
	Tweaked Events support that neatly.</p>
<p>
	Looking from the outside, events accept only delegate of a given type. Suppose 
	you have a delegate handler and wants to tweak it somehow (e.g., make it a weak 
	delegate).The best that can be done is to put it inside a wrapper object. Then 
	you create a delegate that points to a method of the wrapper, and pass this 
	delegate to the event. Such a solution is detailed discussed by Daniel Grunwald suggested in his 
	
	<a href="http://www.codeproject.com/KB/cs/WeakEvents.aspx#heading0007">
	WeakEvents article</a>. Sacha Barber also used a weak event 
	entry wrapper (the WeakEventProxy class) in his
	<a href="http://www.codeproject.com/KB/WPF/CinchV2_1.aspx">CinchV2</a>. Both of 
	them created separate implementations for listener-side 
	events and source-side events. In Tweaked Events, the same code solve both 
	problems.</p>
<p>
	Just as the others, Tweaked Events supports listener-side events through wrapper 
	objects. The difference is that <i>Tweaked Event Entries are wrappers</i>. The base event entry class (<code>BaseEventEntry</code>) has the 
	<code>ToEventHandler</code> method that returns a delegate that can be added to 
	any event. This way, any tweaked event entry implementation will support 
	listener-side events without a single extra line of code.</p>
<pre>
//This will add a regular delegate
button.Click += Button_Click;

//This is a regular delegate
button.Click += TweakedEvent.ToWeak(Button_Click);
</pre>
<h2>
	Tweaked Event interfaces and classes</h2>
<p>
	Here is the code of the basic interfaces. They are <code>ITweakedEvent</code>
	and <code>IEventEntry</code> both in a generic and in a non-generic version. The 
	former is meant for Tweaked Event Stores. The latter is meant for Tweaked Event 
	Entries. Observe how immutability is enforced in the method signatures.</p>
<pre>
public interface ITweakedEvent
{
	void Raise(object sender, EventArgs e);
}
	
public interface ITweakedEvent&lt;TEventHandler&gt; : ITweakedEvent where TEventHandler : class
{
	bool IsEmpty();
	ITweakedEvent&lt;TEventHandler&gt; Clear();

	ITweakedEvent&lt;TEventHandler&gt; Combine(TEventHandler handler);
	ITweakedEvent&lt;TEventHandler&gt; Subtract(TEventHandler handler);
	ITweakedEvent&lt;TEventHandler&gt; Combine(IEventEntry&lt;TEventHandler&gt; entry);
	ITweakedEvent&lt;TEventHandler&gt; Subtract(IEventEntry&lt;TEventHandler&gt; entry);
}

public interface IEventEntry : IEquatable&lt;IEventEntry&gt;, IDisposable
{
	/// &lt;summary&gt;Invoke the entry and returns the value of IsDisposed&lt;/summary&gt;
	bool Invoke(object sender, EventArgs e);
	bool IsDisposed { get; }
}

public interface IEventEntry&lt;TEventHandler&gt; : IEventEntry where TEventHandler : class
{
	/// &lt;summary&gt;Compares this IEventEntry with a delegate for equality&lt;/summary&gt;
	/// &lt;param name="handler"&gt;A delegate to be compared with&lt;/param&gt;
	/// &lt;returns&gt;True if this IEventEntry is equivalent to the handler&lt;/returns&gt;
	bool EqualsHandler(TEventHandler handler);
}
</pre>
<p>
	Take notice of the generic constraint <code>where TEventHandler : class</code>. 
	The constraint that would really fit is this: <code>where TEventHandler : 
	Delegate</code>. Unfortunately, it is not a supported constraint in C# generics. 
	The most restrictive constraint available is <code>class</code>. Anyways, the 
	type of <code>TEventHandler</code> can be (and is) validated at runtime, in the 
	static constructor of the base classes.</p>
<p>
	The code below is a summary of the <code>TweakedEvent&lt;&gt;</code> class. 
	It contains the base implementation of <code>ITweakedEvent</code>. 
	All current store implementation are subclasses of this abstract class. <code>
	TEH</code> is used as an abbreviation of <code>TEventHandler</code>.</p>
<pre>
public abstract class TweakedEvent&lt;TEH&gt; : ITweakedEvent&lt;TEH&gt; where TEH : class
{
	//Constructors
	static TweakedEvent() { ... } // Will validade the type of TEH
	public TweakedEvent() { ... }
	protected TweakedEvent(IEventEntry&lt;TEH&gt;[] initialEntries) { ... }

	// Abstract Methods
	public abstract IEventEntry&lt;TEH&gt; CreateEntry(TEH handler);
	protected abstract ITweakedEvent&lt;TEH&gt; CreateNew(IEventEntry&lt;TEH&gt;[] entries);

	// Virtual Methods
	public virtual ITweakedEvent&lt;TEH&gt; Combine(TEH handler) { ... }
	public virtual ITweakedEvent&lt;TEH&gt; Subtract(TEH handler) { ... }
	public virtual ITweakedEvent&lt;TEH&gt; Combine(IEventEntry&lt;TEH&gt; entry) { ... }
	public virtual ITweakedEvent&lt;TEH&gt; Subtract(IEventEntry&lt;TEH&gt; entry) { ... }

	// Public Methods
	public bool IsEmpty() { ... }
	public ITweakedEvent&lt;TEH&gt; Clear() { ... }

	void ITweakedEvent.Raise(object sender, EventArgs e) { ... }
}</pre>

<p>
	And here is the summarized code of the <code>BaseEventEntry&lt;&gt;</code> class. 
	It&#39;s the base implementation of all event entries. The most important feature it 
	provides is the implicit conversion to an event handler delegate. This feature 
	enables any derived classes to be used as listener-side event wrappers.</p>
<pre>
public abstract class BaseEventEntry&lt;TEH&gt; : IEventEntry&lt;TEH&gt; where TEH : classs
{
	// Abstract Methods / IEventEntry&lt;TEH&gt; members		
	abstract public bool Equals(IEventEntry other);
	abstract public bool EqualsHandler(TEH handler);
	abstract public bool Invoke(object sender, EventArgs e);
	abstract public void Dispose();
	abstract public bool IsDisposed { get; }

	// Conversion to a TEventHandler delegate
	public static implicit operator TEH(BaseEventEntry&lt;TEH&gt; entry) { ... }

	public TEH ToEventHandler() { ... }
}
</pre>
<h2>Synchronized Events (Synced Events, for short)</h2>

<p>
	If you are (or was) a Windows Forms developer, you probably learned that all 
	access to Controls has to be synchronized. WinForms controls are not designed to 
	support requests coming from arbitrary threads. All requests have to be executed 
	in the same thread were the control handle was created. This is a very
	<a href="http://weblogs.asp.net/justin_rogers/pages/126345.aspx">tricky subject</a> 
	full of <a href="http://www.ikriv.com/en/prog/info/dotnet/MysteriousHang.html">gotchas</a>, 
	and you don&#39;t need any knowledge of it to understand my following example.</p>
<p>
	Suppose you build a simple WinForms application that has a single window. This 
	window has a button that, when pressed, will cause a long operation to be 
	executed. You don&#39;t want to block the UI, so the operation will run 
	asynchronously. When it finishes you want to update some other controls in that 
	window. Since you can&#39;t update the controls from a background thread, you&#39;ll 
	have to marshall the code to the UI thread. You can achieve that by calling the 
	<code>Control.Invoke</code> method.</p>
<p>
	Now, suppose you used something like
	<a href="http://msdn.microsoft.com/en-us/library/system.componentmodel.backgroundworker.aspx">BackgroundWorker</a> 
	to run the asynchronous task. The code that would update the UI would naturally 
	be placed in an handler of the <span>
	<a href="http://msdn.microsoft.com/en-us/library/system.componentmodel.backgroundworker.runworkercompleted.aspx">
	RunWorkerCompleted</a></span> event. Imagine that the BackGroundWorker class 
	assumed that all handlers of the RunWorkerCompleted event were going to make UI 
	updates. It could automatically call the handlers in a synchronized way. By 
	doing this, the user code would not have to worry about doing the 
	synchronization stuff. Of course, you probably know that BackgroundWorker is 
	indeed coded like this. Both RunWorkerCompleted and ProgressChanged events are 
	called in a synchronized way by means of a
	<a href="http://msdn.microsoft.com/en-us/library/system.threading.synchronizationcontext.aspx">
	SynchronizationContext</a>.</p>
<p>
	What if you need the async operation is coming from a different source like a 
	WCF service. Actually, any network messages will usually come to your 
	application asynchronously. In order to reflect this change in the UI, 
	synchronization is necessary.</p>
<p>
	Does all this apply only to WinForms? Of course not, WPF UI updates also have 
	to be synchronized. And it&#39;s not necessarily restricted to UI. The
	<a href="http://msdn.microsoft.com/en-us/library/microsoft.win32.systemevents.aspx">SystemEvents</a> 
	class is one example. All its events are synchronized, and it can be used in 
	applications of any kind.</p>
<p>
	If you are implementing an event source class and wants to provide synced 
	events, Tweaked Events and a few extra lines of code are all you need. If you 
	are at the listener side, a very small tweak will enable synchronization in your 
	code.</p>
<h2>Future Work</h2>

<p>
	New event types will be implemented as needed. Despite that, the most critical 
	task at hand is to improve the unit tests. The existing ones are barely a start. 
	When I started this code, I wasn&#39;t a loyal adopter of TDD. If you liked my code 
	and want to contribute in any way, you are very welcome to contact me.</p>
<h2>History</h2>

<ul>
	<li>2010.09.?? - Initial submission;</li>
</ul>

