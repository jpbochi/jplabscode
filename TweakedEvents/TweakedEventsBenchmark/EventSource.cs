// Copyright (c) 2008 Daniel Grunwald
// 
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use,
// copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following
// conditions:
// 
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
// HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
// WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
// OTHER DEALINGS IN THE SOFTWARE.

using System;
using JpLabs.TweakedEvents;

namespace TweakedEventsBenchmark
{
	public interface IEventSource
	{
		event EventHandler Event;
		void RaiseEvent();
	}

	public class NormalEventSource : IEventSource
	{
		public event EventHandler Event;
		
		public void RaiseEvent()
		{
			var local = Event;
			if (local != null) local(this, EventArgs.Empty);
		}
	}
	
	public class SmartEventSource : IEventSource
	{
		#pragma warning disable 0618 //SmartWeakEvent is obsolete
		JpLabs.TweakedEvents.SmartWeakEvent.SmartWeakEvent<EventHandler> _event = new JpLabs.TweakedEvents.SmartWeakEvent.SmartWeakEvent<EventHandler>();
		#pragma warning restore 0618

		public event EventHandler Event
		{
			add { _event.Add(value); }
			remove { _event.Remove(value); }
		}

		public void RaiseEvent()
		{
			_event.Raise(this, EventArgs.Empty);
		}
	}
	
	public class FastSmartEventSource : IEventSource
	{
		#pragma warning disable 0618 //FastSmartWeakEvent is obsolete
		JpLabs.TweakedEvents.SmartWeakEvent.FastSmartWeakEvent<EventHandler> _event = new JpLabs.TweakedEvents.SmartWeakEvent.FastSmartWeakEvent<EventHandler>();
		#pragma warning restore 0618
		
		public event EventHandler Event
		{
			add { _event.Add(value); }
			remove { _event.Remove(value); }
		}
		
		public void RaiseEvent()
		{
			_event.Raise(this, EventArgs.Empty);
		}
	}

	public class WeakEventSource : IEventSource
	{
		TweakedEvent<EventHandler> _event = new WeakEvent<EventHandler>();
		
		public event EventHandler Event
		{
			add { TweakedEvent.Add(ref _event, value); }
			remove { TweakedEvent.Remove(ref _event, value); }
		}
		
		public void RaiseEvent()
		{
			_event.Raise(this, EventArgs.Empty);
		}
	}

	public class SyncedEventSource : IEventSource
	{
		TweakedEvent<EventHandler> _event = new SyncedEvent<EventHandler>();
		
		public event EventHandler Event
		{
			add { TweakedEvent.Add(ref _event, value); }
			remove { TweakedEvent.Remove(ref _event, value); }
		}
		
		public void RaiseEvent()
		{
			_event.Raise(this, EventArgs.Empty);
		}
	}
	
	public class WeakSyncedEventSource : IEventSource
	{
		TweakedEvent<EventHandler> _event = new WeakSyncedEvent<EventHandler>();
		
		public event EventHandler Event
		{
			add { TweakedEvent.Add(ref _event, value); }
			remove { TweakedEvent.Remove(ref _event, value); }
		}
		
		public void RaiseEvent()
		{
			_event.Raise(this, EventArgs.Empty);
		}
	}

	public class EventListener
	{
		IEventSource source;
		
		public EventListener(IEventSource source)
		{
			this.source = source;
		}
		
		public void Attach()
		{
			source.Event += source_Event;
		}

		public void Detach()
		{
			source.Event -= source_Event;
		}
		
		void source_Event(object sender, EventArgs e)
		{
			Console.WriteLine("Event was called!");
		}
		
		~EventListener()
		{
			Console.WriteLine("EventListener was garbage-collected!");
		}
	}
}
