﻿using System;
using System.Diagnostics;

namespace JpLabs.Geometry
{
	internal static class ShapeIntersectsRealExt
	{
		//static public bool Intersects(this IShape s1, IShape s2)
		//{
		//    if (s1.IsVoid || s2.IsVoid)	return false;
		//    if (s1 is BoundingBoxInt)	return Intersects((BoundingBoxInt)s1, s2);
		//    if (s1 is PointInt)			return Intersects((PointInt)s1, s2);
		//    throw new NotImplementedException();
		//}

		static public bool Intersects(this BoundingBoxReal b, IShape s)
		{
			if (b.IsVoid || s.IsVoid)	return false;
			if (b.IsPunctual)			return Intersects(b.Lower, s);
			if (s is BoundingBoxReal)	return Intersects(b, (BoundingBoxReal)s);
			if (s is PointReal)			return Intersects(b, (PointReal)s);
			throw new NotImplementedException();
		}

		static public bool Intersects(this PointReal p, IShape s)
		{
			if (s.IsVoid)				return false;
			if (s is BoundingBoxReal)	return Intersects((BoundingBoxReal)s, p);
			if (s is PointReal)			return Intersects((PointReal)s, p);
			if (s == null) throw new NullReferenceException();
			throw new NotImplementedException();
		}
		
		static public bool Intersects(this BoundingBoxReal b1, BoundingBoxReal b2)
		{
			if (b1.IsVoid || b2.IsVoid)	return false;
			if (b1.IsPunctual)			return Intersects(b2, b1.Lower);
			if (b2.IsPunctual)			return Intersects(b1, b2.Lower);
			
			if (b1.Dimensions != b2.Dimensions) throw new ArgumentException("Boxes have to have the same number of dimensions");

			for (int i=0; i<b1.Dimensions; i++) {
				if (!(b1.Lower[i] <= b2.Upper[i])
				||  !(b2.Lower[i] <= b1.Upper[i])) return false;
			}
			return true;
		}

		static public bool Intersects(this BoundingBoxReal b, PointReal p)
		{
			if (b.IsVoid)		return false;
			if (b.IsPunctual)	return Intersects(b.Lower, p);
			
			if (b.Dimensions != p.Dimensions) throw new ArgumentException("'b' has to have the same number of dimensions as 'p'");

			for (int i=0; i<b.Dimensions; i++) {
			    if (!(b.Lower[i] <= p[i])
			    ||  !(b.Upper[i] >= p[i])) return false;
			}
			return true;
		}

		static public bool Intersects(this PointReal p, BoundingBoxReal b)
		{
			return Intersects(b, p);
		}

		static public bool Intersects(this PointReal p1, PointReal p2)
		{
			return p1 == p2;
		}
	}
}
