﻿using System;
using System.Linq;

using TNumeric = System.Double;

namespace JpLabs.Geometry
{

	public class BoundingBoxReal : IBoundingBox<TNumeric>
	{
		readonly PointReal lower;
		readonly PointReal upper;
		double? volume;
		double? surface;
		
		public PointReal Lower							{ get {return lower;} }
		public PointReal Upper							{ get {return upper;} }
		IPoint<TNumeric> IBoundingBox<TNumeric>.Lower	{ get {return lower;} }
		IPoint<TNumeric> IBoundingBox<TNumeric>.Upper	{ get {return upper;} }
		IPoint IBoundingBox.Lower						{ get {return lower;} }
		IPoint IBoundingBox.Upper						{ get {return upper;} }
		
		public bool IsPunctual	{ get; protected set; }
		public bool IsVoid		{ get; protected set; }		
		public int Dimensions	{ get; protected set; }
		
		IPoint<TNumeric> IBoundingBox<TNumeric>.Center	{ get { return this.Center; } }
		IPoint IBoundingBox.Center					{ get { return this.Center; } }

		static public BoundingBoxReal VoidBoundingBox = new BoundingBoxReal();
		
		public BoundingBoxReal(PointReal p1, PointReal p2)
		{
			if (p1.Dimensions != p2.Dimensions) throw new ArgumentException("Points must have same number of dimensions");
			
			Dimensions = p1.Dimensions;
			
			TNumeric[] vLower = new TNumeric[Dimensions];
			TNumeric[] vUpper = new TNumeric[Dimensions];
			for (int i=0; i<Dimensions; i++) {
				if (p1[i] < p2[i]) {
					vLower[i] = p1[i];
					vUpper[i] = p2[i];
				} else {
					vLower[i] = p2[i];
					vUpper[i] = p1[i];
				}
			}
			this.lower = new PointReal(vLower);
			this.upper = new PointReal(vUpper);
			
			IsPunctual = (this.lower == this.upper);
			//if (IsPunctual) upper = lower; //ensure single PointInt instance (update: PointInt is not a class anymore)
		}

		public BoundingBoxReal(PointReal p)
		{
			Dimensions = p.Dimensions;
			lower = p;
			upper = p;
			IsPunctual = true;
			IsVoid = false;
		}

		protected BoundingBoxReal()
		{
			IsVoid = true;
		}
		
		IBoundingBox IShape.GetBoundingBox()
		{
			return this;
		}

		bool IShape.Contains(IShape other)
		{
			return this.Contains(other);
		}

		bool IShape.Intersects(IShape other)
		{
			return this.Intersects(other);
		}
		
		public PointReal Center
		{
			get {
				if (this.IsVoid) return default(PointReal);
				if (this.IsPunctual) return lower;
				var r = new TNumeric[Dimensions];
				for (int i=0; i<Dimensions; i++ ) r[i] = (Upper[i] + Lower[i]) / 2d;
				return new PointReal(r);
			}
		}
		
		private VectorReal DiffVector
		{
			get {
				int dim = Dimensions;
				var v = new TNumeric[dim];
				for (int i=0; i<dim; i++ ) v[i] = upper[i] - lower[i];
				return new VectorReal(v);
			}
		}
		
		public double Volume
		{
			get {
				if (this.IsVoid) return 0;
				if (volume == null) {
					/*
					volume = DiffVector.Aggregate((val1, val2) => val1 * val2);
					/*/
					//Optimization attempt
					{
						var diff = DiffVector;
						int dim = Dimensions;
						TNumeric vol = diff[0];
						for (int i=1; i<dim; i++) vol *= diff[i];
						volume = vol;
					}
					//*/
				}
				return volume.Value;
			}
		}
		
		public double Surface
		{
			get {
				if (this.IsVoid) return 0;
				if (surface == null) {
					var diff = DiffVector;
					var dim = Dimensions;
					if (dim == 2) {
						surface = diff[0] + diff[1];
						surface *= 2;
					} else if (dim == 3) {
						surface = diff[0] * diff[1];
						surface += diff[0] * diff[2];
						surface += diff[1] * diff[2];
						surface *= 2;
					} else {
						/*
						var q = from i in Enumerable.Range(0, 2)
								from j in Enumerable.Range(0, i)
								select new {I = i, J = j};
						/*/
						//give just an estimate
						surface = diff.Sum();
						//*/
					}
				}
				return surface.Value;
			}
		}
		
		internal IBoundingBox<TNumeric> Merge(BoundingBoxReal other)
		{
			return this + other;
		}

		internal IBoundingBox<TNumeric> Intersection(BoundingBoxReal other)
		{
			return this & other;
		}

		IBoundingBox IBoundingBox.Merge(IBoundingBox other)
		{
			return this + (BoundingBoxReal)other;
		}

		IBoundingBox IBoundingBox.Intersection(IBoundingBox other)
		{
			return this & (BoundingBoxReal)other;
		}
		
		/// <summary> + = Merge operator </summary>
		static public BoundingBoxReal operator +(BoundingBoxReal b1, BoundingBoxReal b2)
		{
			if (b1 == null || b1.IsVoid) return b2;
			if (b2 == null || b2.IsVoid) return b1;
			if (b1.Dimensions != b2.Dimensions) throw new ArgumentException("Boxes must have same number of dimensions");
			
			var dim = b1.Dimensions;
			var low = new TNumeric[dim];
			var upp = new TNumeric[dim];
			var b1Lower = b1.Lower; var b1Upper = b1.Upper;
			var b2Lower = b2.Lower; var b2Upper = b2.Upper;
			for (int i=0; i<dim; i++) {
				low[i] = Math.Min(b1Lower[i], b2Lower[i]);
				upp[i] = Math.Max(b1Upper[i], b2Upper[i]);
			}
			return new BoundingBoxReal(new PointReal(low), new PointReal(upp));
		}

		/// <summary> & = Insertection operator </summary>
		static public BoundingBoxReal operator &(BoundingBoxReal b1, BoundingBoxReal b2)
		{
			if (b1 == null || b1.IsVoid) return VoidBoundingBox;
			if (b2 == null || b2.IsVoid) return VoidBoundingBox;
			if (b1.Dimensions != b2.Dimensions) throw new ArgumentException("Boxes must have same number of dimensions");
			
			var dim = b1.Dimensions;
			var low = new TNumeric[dim];
			var upp = new TNumeric[dim];
			var b1Lower = b1.Lower; var b1Upper = b1.Upper;
			var b2Lower = b2.Lower; var b2Upper = b2.Upper;
			for (int i=0; i<dim; i++) {
				low[i] = Math.Max(b1Lower[i], b2Lower[i]);
				upp[i] = Math.Min(b1Upper[i], b2Upper[i]);
				if (low[i] > upp[i]) return VoidBoundingBox;
			}
			
			return new BoundingBoxReal(new PointReal(low), new PointReal(upp));
		}

		static public bool operator >(BoundingBoxReal bContainer, BoundingBoxReal bContained)
		{
			return bContainer.Contains(bContained);
		}

		static public bool operator <(BoundingBoxReal bContained, BoundingBoxReal bContainer)
		{
			return bContained.Contains(bContainer);
		}

		public override string ToString()
		{
			if (IsVoid)		return "{box: void}";
			if (IsPunctual)	return string.Concat("{box: ", Lower.ToString(), "}");
			
			return string.Concat("{box: lower = ", Lower.ToString(), ", upper = ", Upper.ToString(), "}");
		}
	}
}
