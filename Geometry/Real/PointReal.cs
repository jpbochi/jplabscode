﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using TNumeric = System.Double;

namespace JpLabs.Geometry
{
	//sealed public class PointReal : IPoint<TNumeric>, IEnumerable<TNumeric>, ICloneable
	public struct PointReal : IPoint<TNumeric>, IEnumerable<TNumeric>, ICloneable
	{
		private readonly TNumeric[] v;
		
		public TNumeric X { get {return v[Core.DimX];} }
		public TNumeric Y { get {return v[Core.DimY];} }
		public TNumeric Z { get {return v[Core.DimZ];} }
		public TNumeric W { get {return v[Core.DimW];} }

		public readonly int Dimensions;
		int IShape.Dimensions { get { return Dimensions; } }
		
		public PointReal(int dimensions)
		{
			Dimensions = dimensions;
		    v = new TNumeric[Dimensions];
		}

		public PointReal(params TNumeric[] p)
		{
			if (p == null) throw new ArgumentNullException();
			
			Dimensions = p.Length;
			v = new TNumeric[Dimensions];
			p.CopyTo(v, 0);
		}
		
		public TNumeric this[int dim]
		{
			get { return v[dim]; }
		}

		IComparable IPoint.this[int dim]
		{
		    get { return v[dim]; }
		}

		static public VectorReal operator -(PointReal p1, PointReal p2)
		{
		    if (p1.Dimensions != p2.Dimensions) throw new ArgumentException("Points have to have the same number of dimensions");
		    var dim = p1.Dimensions;
		    var r = new TNumeric[dim];
		    for (int i=0; i<dim; i++) r[i] = p1[i] - p2[i];
		    return new VectorReal(r);
		}
		
		static public PointReal operator -(PointReal p, VectorReal v)
		{
			if (p.Dimensions != v.Dimensions) throw new ArgumentException("'p' has to have the same number of dimensions as 'v'");
			var dim = p.Dimensions;
			var r = new TNumeric[dim];
			for (int i=0; i<dim; i++) r[i] = p[i] - v[i];
			return new PointReal(r);
		}

		static public PointReal operator +(PointReal p, PointReal v)
		{
			if (p.Dimensions != v.Dimensions) throw new ArgumentException("'p' has to have the same number of dimensions as 'v'");
			var dim = p.Dimensions;
			var r = new TNumeric[dim];
			for (int i=0; i<dim; i++) r[i] = p[i] + v[i];
			return new PointReal(r);
		}
		
		#if POINT_IS_CLASS
		static public bool operator ==(PointReal point1, PointReal point2)
		{
			return Equals(point1, point2);
		}		
		static public bool operator !=(PointReal point1, PointReal point2)
		{
			return !Equals(point1, point2);
		}		
		
		public override bool Equals(object obj)
		{
			if (obj is PointReal) return Equals(this, (PointReal)obj);
			return false;
		}

		static bool Equals(PointReal p, PointReal q)
		{
			if (((object)p) == null) return ((object)q) == null;
			if (((object)q) == null) return false;
			
			return p.SequenceEqual(q);
		}
		#else
		static public bool operator ==(PointReal p1, PointReal p2)
		{
			return p1.Equals(p2);
		}
		static public bool operator !=(PointReal p1, PointReal p2)
		{
			return !p1.Equals(p2);
		}
		
		public override bool Equals(object obj)
		{
			if (obj is PointReal) return Equals((PointReal)obj);
			return false;
		}
		
		public bool Equals(PointReal other)
		{
			//return this.SequenceEqual(other);
			
			if (this.Dimensions != other.Dimensions) return false;
			
			for (int i=0; i<Dimensions; i++) if (v[i] != other[i]) return false;
			
			return true;
		}
		#endif

		public override int GetHashCode()
		{
			/*
			int shiftStep = 32 / (Dimensions + 1);
			unchecked {
				//unsafe {
				//    double num = 0d;
				//    //long num2 = *((long*) &num);
				//    var num3 = (void*)&num;
				//}
				int hash = 0;
				for (int i=0; i<Dimensions; i++) hash += v[i].GetHashCode() << (i * shiftStep);
				return ~hash;
			}
			/*/
			int hash = 0;
			for (int i=0; i<Dimensions; i++) hash ^= v[i].GetHashCode(); //bitwise XOR
			return hash;
			//*/
		}

		public override string ToString()
		{
			return this.Aggregate(
				new StringBuilder(),
				(str, value) => ((str.Length == 0) ? str.Append("{") : str.Append(",")).Append(value)
			).Append("}").ToString();
		}

		IVector IPoint.Subtract(IPoint other)
		{
			return this - (PointReal)other;
		}

		bool IShape.IsVoid
		{
			get { return false; }
		}

		IBoundingBox IShape.GetBoundingBox()
		{
			return new BoundingBoxReal(this);
		}

		bool IShape.Contains(IShape other)
		{
			return this.Contains(other);
		}

		bool IShape.Intersects(IShape other)
		{
			return this.Intersects(other);
		}

		public IEnumerator<TNumeric> GetEnumerator() { return ((IEnumerable<TNumeric>)v).GetEnumerator(); }
		IEnumerator IEnumerable.GetEnumerator() { return v.GetEnumerator(); }

		public object Clone()
		{
			return new PointReal(this.v);
		}
	}
}
