﻿//#define POINT_IS_CLASS

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using TNumeric = System.Int32;
using System.ComponentModel;
using JpLabs.Geometry.Int;
using System.IO;
using System.Text.RegularExpressions;

namespace JpLabs.Geometry
{
	//*
	[DataContract(Namespace="http://jplabs.bochi.it/geometry")]
	#if POINT_IS_CLASS
	sealed public class PointInt : IPoint<TNumeric>, IEnumerable<TNumeric>, ICloneable
	#else
	[TypeConverter(typeof(PointIntConverter))]
	public struct PointInt : IPoint<TNumeric>, IEnumerable<TNumeric>, ICloneable
	#endif
	{
		#if POINT_IS_CLASS
		public static PointInt Null { get { return new PointInt(0); } }
		#else
		public static PointInt Null { get { return default(PointInt); } }
		#endif

		[DataMember(EmitDefaultValue=false)]private readonly TNumeric[] v;
		[IgnoreDataMember]public int Dimensions;
		
		public TNumeric X { get {return v[Core.DimX];} }
		public TNumeric Y { get {return v[Core.DimY];} }
		public TNumeric Z { get {return v[Core.DimZ];} }
		public TNumeric W { get {return v[Core.DimW];} }

		int IShape.Dimensions { get { return Dimensions; } }

		[OnDeserialized]
		private void OnDeserialized(StreamingContext context)
		{
			Dimensions = (v == null) ? 0 : v.Length;
		}
		
		public PointInt(TNumeric dimensions)
		{
			Dimensions = dimensions;
		    v = new TNumeric[Dimensions];
		}

		public PointInt(TNumeric x, TNumeric y)
		{
			Dimensions = 2;
		    v = new [] {x, y};
		}
		
		public PointInt(TNumeric x, TNumeric y, TNumeric z)
		{
			Dimensions = 3;
		    v = new [] {x, y, z};
		}
		
		public PointInt(TNumeric x, TNumeric y, TNumeric z, TNumeric w)
		{
			Dimensions = 4;
		    v = new [] {x, y, z, w};
		}

		internal PointInt(bool reuseArray, params TNumeric[] p)
		{
			if (p == null) throw new ArgumentNullException();
			
			Dimensions = p.Length;
			
			if (reuseArray) {
				v = p;
			} else {
				v = new TNumeric[Dimensions];
				p.CopyTo(v, 0);
			}
		}

		internal PointInt(params TNumeric[] p)
		{
			Dimensions = p.Length;
			v = p;
		}
		
		internal TNumeric[] ToArray()
		{
			return v;
		}
		
		public TNumeric this[int index]
		{
			get { return v[index]; }
		}

		IComparable IPoint.this[int index]
		{
		    get { return v[index]; }
		}

		static public VectorInt operator -(PointInt p1, PointInt p2)
		{
			var dim = Math.Min(p1.Dimensions, p2.Dimensions);
			var r = new TNumeric[dim];
			for (int i=0; i<dim; i++) r[i] = p1[i] - p2[i];
			return new VectorInt(r);
		}
		
		static public PointInt operator -(PointInt p, VectorInt v)
		{
			var dim = Math.Min(p.Dimensions, v.Dimensions);
			var r = new TNumeric[dim];
			for (int i=0; i<dim; i++) r[i] = p[i] - v[i];
			return new PointInt(r);
		}

		static public PointInt operator +(PointInt p, VectorInt v)
		{
			var dim = Math.Min(p.Dimensions, v.Dimensions);
			var r = new TNumeric[dim];
			for (int i=0; i<dim; i++) r[i] = p[i] + v[i];
			return new PointInt(r);
		}
		
		#if POINT_IS_CLASS
		static public bool operator ==(PointInt point1, PointInt point2)
		{
			return Equals(point1, point2);
		}		
		static public bool operator !=(PointInt point1, PointInt point2)
		{
			return !Equals(point1, point2);
		}		
		
		public override bool Equals(object obj)
		{
			if (obj is PointInt) return Equals(this, (PointInt)obj);
			return false;
		}

		static bool Equals(PointInt p, PointInt q)
		{
			if (((object)p) == null) return ((object)q) == null;
			if (((object)q) == null) return false;
			
			int dim = p.Dimensions;
			if (dim != q.Dimensions) return false;
			for (int i=0; i<dim; i++) if (p[i] != q[i]) return false;
			return true;
		}
		#else
		static public bool operator ==(PointInt p1, PointInt p2)
		{
			return p1.Equals(p2);
		}
		static public bool operator !=(PointInt p1, PointInt p2)
		{
			return !p1.Equals(p2);
		}
		
		public override bool Equals(object obj)
		{
			if (obj is PointInt) return Equals((PointInt)obj);
			return false;
		}
		
		public bool Equals(PointInt other)
		{
			if (this.Dimensions != other.Dimensions) return false;
			for (int i=0; i<Dimensions; i++) if (v[i] != other[i]) return false;
			return true;
		}
		#endif

		public override int GetHashCode()
		{
			//int rotateBits = 32 / (Dimensions + 1); //rotateBits for 3 dimensions == 8 bits
			const int rotateBits = 8; //i.e., one byte

			if (v == null) return -1;
			
			int hash = v[0];
			for (int i=1; i<Dimensions; i++) hash ^= v[i].RotateLeft(i * rotateBits); //bitwise XOR
			return hash;
		}

		public override string ToString()
		{
			return this.Aggregate(
				new StringBuilder(),
				(str, value) => ((str.Length == 0) ? str.Append("{") : str.Append(",")).Append(value)
			).Append("}").ToString();
			//	(str, value) => ((str.Length == 0) ? str.Append("[") : str.Append(",")).Append(value)
			//).Append("]").ToString();
		}

		private static readonly Regex pointRegex = new Regex(@"\A\{(?<n>[-+]?\b\d+\b)(?:,(?<n>[-+]?\b\d+\b))+\}\z", RegexOptions.Compiled | RegexOptions.CultureInvariant);

		internal static TNumeric[] ParseInternal(string value)
		{
			var results = pointRegex.Match(value);
			if (!results.Success) throw new FormatException();

			return results.Groups["n"].Captures.Cast<Capture>().Select(c => int.Parse(c.Value)).ToArray();
		}

		public static PointInt Parse(string value)
		{
			return new PointInt(ParseInternal(value));
		}

		IVector IPoint.Subtract(IPoint other)
		{
			return this - (PointInt)other;
		}

		bool IShape.IsVoid
		{
			get { return false; }
		}

		IBoundingBox IShape.GetBoundingBox()
		{
			return new BoundingBoxInt(this);
		}

		bool IShape.Contains(IShape other)
		{
			return this.Contains(other);
		}

		bool IShape.Intersects(IShape other)
		{
			return this.Intersects(other);
		}

		public IEnumerator<TNumeric> GetEnumerator()
		{
			if (v == null) return Enumerable.Empty<TNumeric>().GetEnumerator();

			return ((IEnumerable<TNumeric>)v).GetEnumerator();
		}

		IEnumerator IEnumerable.GetEnumerator() { return GetEnumerator(); }

		public object Clone()
		{
			return new PointInt(this.v);
		}
	}
	//*/
}
