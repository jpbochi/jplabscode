﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Text.RegularExpressions;

namespace JpLabs.Geometry
{
	[DataContract(Namespace="http://jplabs.bochi.it/geometry")]
	public struct VectorInt : IVector, IEnumerable<int>
	{
		[DataMember(EmitDefaultValue=false)]private readonly int[] v;
		[IgnoreDataMember]private int dimensions;
		
		public int X { get {return v[Core.DimX];} }
		public int Y { get {return v[Core.DimY];} }
		public int Z { get {return v[Core.DimZ];} }
		
		public int Dimensions { get { return dimensions; } }

		[OnDeserialized]
		private void OnDeserialized(StreamingContext context)
		{
			dimensions = (v == null) ? 0 : v.Length;
		}

		public VectorInt(int x, int y) : this()
		{
			dimensions = 2;
			v = new int[] { x, y };
		}
		
		public VectorInt(int x, int y, int z) : this()
		{
			dimensions = 3;
			v = new int[] { x, y, z};
		}

		internal VectorInt(params int[] p) : this()
		{
			dimensions = p.Length;
			v = p;//v = new int[Dimensions];
			//p.CopyTo(v, 0);
		}
		
		internal int[] InternalArray
		{
			get { return v; }
		}
		
		public int this[int dim]
		{
			get { return v[dim]; }
		}

		IComparable IVector.this[int dim]
		{
		    get { return v[dim]; }
		}

		static public VectorInt operator -(VectorInt v1, VectorInt v2)
		{
			if (v1.Dimensions != v2.Dimensions) throw new ArgumentException("Vectors have to have the same number of dimensions");
			var dim = Math.Min(v2.dimensions, v2.dimensions);;
			var r = new int[dim];
			for (int i=0; i<dim; i++) r[i] = v1[i] - v2[i];
			return new VectorInt(r);
		}

		static public VectorInt operator +(VectorInt v1, VectorInt v2)
		{
			if (v1.Dimensions != v2.dimensions) throw new ArgumentException("Vectors have to have the same number of dimensions");
			var dim = Math.Min(v2.dimensions, v2.dimensions);;
			var r = new int[dim];
			for (int i=0; i<dim; i++) r[i] = v1[i] + v2[i];
			return new VectorInt(r);
		}

		public double Length
		{
			get { return Math.Sqrt(LengthSquared); }
		}

		public double LengthSquared
		{
			/*
			get { return v.Sum(value => value * value); }
			/*/
			//without LINQ it might be faster
			get {
				int len = 0;
				for (int i=0; i<dimensions; i++) len += v[i] * v[i];
				return len;
			}
			//*/
		}
		
		static public bool operator ==(VectorInt v1, VectorInt v2)
		{
			return v1.Equals(v2);
		}		
		static public bool operator !=(VectorInt v1, VectorInt v2)
		{
			return !v1.Equals(v2);
		}
		
		public override bool Equals(object obj)
		{
			if (obj is VectorInt) return Equals((VectorInt)obj);
			return false;
		}

		public bool Equals(VectorInt other)
		{
			//return this.SequenceEqual(other);
			if (this.Dimensions != other.Dimensions) return false;
			for (int i=0; i<Dimensions; i++) if (v[i] != other[i]) return false;
			return true;
		}

		public override int GetHashCode()
		{
			//int rotateBits = 32 / (Dimensions + 1); //rotateBits for 3 dimensions == 8 bits
			const int rotateBits = 8; //i.e., one byte

			if (v == null) return -1;
			
			int hash = v[0];
			for (int i=1; i<Dimensions; i++) hash ^= v[i].RotateLeft(i * rotateBits); //bitwise XOR
			return hash;
		}

		public override string ToString()
		{
			return this.Aggregate(
				new StringBuilder(),
				(str, value) => ((str.Length == 0) ? str.Append("{") : str.Append(",")).Append(value)
			).Append("}").ToString();
		}

		public static VectorInt Parse(string value)
		{
			return new VectorInt(PointInt.ParseInternal(value));
		}

		public IEnumerator<int> GetEnumerator() { return v.AsEnumerable<int>().GetEnumerator(); }
		IEnumerator IEnumerable.GetEnumerator() { return v.GetEnumerator(); }
	}
}
