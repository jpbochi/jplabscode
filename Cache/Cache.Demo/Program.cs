﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace JpLabs.Cache.Demo
{
	class Program
	{
		static IAsyncCache<string,string> TheCache;
		
		static void Main(string[] args)
		{
			TheCache = new AsyncCache<string,string>();
			
			const int NUM_THREADS = 15;
			Thread[] t = new Thread[NUM_THREADS];
			
			TheCache.AddValueLoadedHandler("0", OnLoadValue);
			TheCache.AddValueLoadedHandler("1", OnLoadValue);
			TheCache.ValueLoaded += OnLoadAny;
			TheCache.AddValueLoadedHandler("2", OnLoadValue);
			TheCache.RemoveValueLoadedHandler("2", OnLoadValue);
			
			for (int i=0; i<t.Length; i++) {
				t[i] = new Thread(ThreadCallback) { Name = string.Format("t[{0}]", i) };
			}
			
			for (int i=0; i<t.Length; i++) {
				Thread.Sleep(100);
				t[i].Start((i % 2).ToString());
			}
			
			TheCache.BeginReload("0", LoadValue);
			Thread.Sleep(0);
			TheCache.BeginReload("1", LoadValue);
			Thread.Sleep(0);
			TheCache.BeginReload("2", LoadValue);
			
			for (int i=0; i<t.Length; i++) t[i].Join();
			
			Thread.Sleep(1500);
		}

		static void OnLoadAny(object sender, KeyPairEventArgs<string,string> args)
		{
			MyConsole.WriteLine(string.Format("Key='{0}' - OnLoadAny event raised", args.Key));
		}
		
		static void OnLoadValue(object sender, KeyPairEventArgs<string,string> args)
		{
			MyConsole.WriteLine(string.Format("Key='{0}' - OnLoadValue event raised", args.Key));
		}
		
		static void ThreadCallback(object arg)
		{
			string key = (string)arg;
			//string value = TheCache.GetOrLoad(key, LoadValue);
			
			TheCache.BeginLoad(key, LoadValue);
			Thread.Sleep(50);
			string value = TheCache.EndLoad(key);
			
			MyConsole.WriteLine(string.Format("Key='{0}' - Value='{1}'", key, value));
		}

		static string LoadValue(string key)
		{
			MyConsole.WriteLine(string.Format("Key='{0}' - LoadValue started", key));
			Thread.Sleep(200);
			MyConsole.WriteLine(string.Format("Key='{0}' - LoadValue ended", key));
		    return key + ".Value";
		}
	}
}
