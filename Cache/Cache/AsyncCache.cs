﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading;
using JpLabs.CustomEvents;
using JpLabs.CustomEvents.SmartWeakEvent;
using AsyncResult = System.Runtime.Remoting.Messaging.AsyncResult;

namespace JpLabs.Cache
{
	public static class MyConsole
	{
		[System.Diagnostics.Conditional("DEBUG")]
		public static void WriteLine(string value)
		{
			var t = Thread.CurrentThread;
			Console.WriteLine(
				string.Format(
					"Thread='{0}'\t| {1}",
					string.IsNullOrEmpty(t.Name) ? t.ManagedThreadId.ToString() : t.Name,
					value
				)
			);
		}
	}
	
	public interface IAsyncCache<TKey,TValue> : IDictionary<TKey, TValue>
	{
		object SyncRoot { get; }
		Func<TKey,TValue> DefaultLoadValueFunc { get; set; }
		
		TValue GetOrLoadValue(TKey key);
		TValue GetOrLoadValue(TKey key, Func<TKey,TValue> loadValueFunc);
		
		void BeginLoad(TKey key);
		void BeginLoad(TKey key, Func<TKey,TValue> loadValueFunc);
		TValue EndLoad(TKey key);
		
		TValue Reload(TKey key);
		TValue Reload(TKey key, Func<TKey,TValue> loadValueFunc);
		void BeginReload(TKey key);
		void BeginReload(TKey key, Func<TKey,TValue> loadValueFunc);
		
		bool IsLoadingKey(TKey key);
		bool ContainsOrIsLoadingKey(TKey key);

		event EventHandler<KeyPairEventArgs<TKey,TValue>> ValueLoaded;
		
		void AddValueLoadedHandler(TKey key, EventHandler<KeyPairEventArgs<TKey,TValue>> handler);
		void RemoveValueLoadedHandler(TKey key, EventHandler<KeyPairEventArgs<TKey,TValue>> handler);
	}
	
	public class KeyPairEventArgs<TKey,TValue> : EventArgs
	{
		public TKey Key { get; set; }
		public TValue Value { get; set; }
	}

	public class AsyncCache<TKey,TValue> : IAsyncCache<TKey,TValue>
	{
		private class LoadingJob
		{
			public TKey Key { get; set; }
			public IAsyncResult AsyncResult { get; set; }
			public Exception Exception { get; set; }

			public bool IsLoadCompleted { get; set; }
		}

		//TOREVIEW: consider using ReaderWriterLockSlim
		//http://www.albahari.com/threading/part3.aspx#_ReaderWriterLock

		protected object syncRoot = new object();
		protected object eventSyncRoot = new object();
		
		protected IDictionary<TKey,TValue> innerDict;
		private IDictionary<TKey,LoadingJob> loadingJobs;
		private CustomEvent<EventHandler<KeyPairEventArgs<TKey,TValue>>> onLoadAnyKeyEvent;
		private IDictionary<TKey,ICustomEvent<EventHandler<KeyPairEventArgs<TKey,TValue>>>> onLoadEventColl;

		public AsyncCache()
		{
			innerDict = CreateInnerDictionary();
			loadingJobs = new Dictionary<TKey,LoadingJob>();
			onLoadAnyKeyEvent = CreateValueLoadedEvent();
			onLoadEventColl = new Dictionary<TKey,ICustomEvent<EventHandler<KeyPairEventArgs<TKey,TValue>>>>();
		}

		public virtual Object SyncRoot { get { return syncRoot; } }

		private Func<TKey,TValue> _defaultLoadValueFunc;
		public Func<TKey,TValue> DefaultLoadValueFunc
		{
			get { return _defaultLoadValueFunc; }
			set { lock(syncRoot) _defaultLoadValueFunc = value; }
		}

		public event EventHandler<KeyPairEventArgs<TKey,TValue>> ValueLoaded
		{
			add    { onLoadAnyKeyEvent += value; }
			remove { onLoadAnyKeyEvent -= value; }
		}
		
		protected virtual IDictionary<TKey,TValue> CreateInnerDictionary()
		{
			return new Dictionary<TKey,TValue>();
		}

		protected virtual CustomEvent<EventHandler<KeyPairEventArgs<TKey,TValue>>> CreateValueLoadedEvent()
		{
			//return new FastSmartWeakEvent<EventHandler<KeyPairEventArgs<TKey,TValue>>>();
			return new SyncedEvent<EventHandler<KeyPairEventArgs<TKey,TValue>>>();
		}

		protected virtual void OnLoad(TKey key, TValue value)
		{
			ICustomEvent<EventHandler<KeyPairEventArgs<TKey,TValue>>> onLoadSpecificKeyEvent;
			bool hasSpecificKeyEvent;
			lock (eventSyncRoot) hasSpecificKeyEvent = onLoadEventColl.TryGetValue(key, out onLoadSpecificKeyEvent);
			
            var eventArgs = new KeyPairEventArgs<TKey,TValue>() { Key = key, Value = value };
            
            //Raise specific key OnLoad event
            if (hasSpecificKeyEvent) onLoadSpecificKeyEvent.Raise(this, eventArgs);
            
            //Raise generic OnLoad event
			onLoadAnyKeyEvent.Raise(this, eventArgs);
		}
		
		#region Private Methods
		
		private void ThrowAddNotSupported()
		{
			throw new NotSupportedException("GetOrLoadValue, BeginLoad, Reload, or BeginReload methods should be used to insert/update values");
		}

		private LoadingJob InternalBeginLoad(TKey key, Func<TKey,TValue> loadValueFunc)
		{
			MyConsole.WriteLine(string.Format("Key='{0}' - InternalBeginLoad called", key));

			lock (syncRoot) {
				IAsyncResult asyncResult = loadValueFunc.BeginInvoke(key, LoadValueCallback, key);
				var loadingJob = new LoadingJob() { Key = key, AsyncResult = asyncResult };
				loadingJobs.Add(key, loadingJob);
				return loadingJob;
			}
		}

		private void LoadValueCallback(IAsyncResult asyncResult)
		{
		    Thread.Sleep(50);

            TKey key = (TKey)asyncResult.AsyncState;
            TValue value = default(TValue);
		    LoadingJob loadingJob;

            lock (syncRoot) {
				loadingJob = loadingJobs[key];

				Thread.Sleep(50);

				MyConsole.WriteLine(string.Format("Key='{0}' - LoadValue.EndInvoke called", (TKey)asyncResult.AsyncState));

				// Retrieve the delegate and call EndInvoke (http://msdn.microsoft.com/en-us/library/2e08f6yc.aspx)
				Func<TKey,TValue> caller = (Func<TKey,TValue>)((AsyncResult)asyncResult).AsyncDelegate;
				try
				{
					value = caller.EndInvoke(asyncResult);

					MyConsole.WriteLine(string.Format("Key='{0}' - LoadValue.EndInvoke returned", (TKey)asyncResult.AsyncState));
					Thread.Sleep(50);

					innerDict[key] = value;
				} catch (Exception e) {
					loadingJob.Exception = e;
				}
				
				loadingJobs.Remove(key);
				
				loadingJob.IsLoadCompleted = true;
				Monitor.PulseAll(syncRoot);
            }
			
			if (loadingJob.Exception != null) {
				OnLoad(key, value);
			}
		}

		private TValue InternalEndLoad(LoadingJob loadingJob)
		{
			TKey key = loadingJob.Key;

			Thread.Sleep(50);
			MyConsole.WriteLine(string.Format("Key='{0}' - Wait started", key));
			
			int iterations = 0;
			lock (syncRoot) {
				Thread.Sleep(50);
				//while (!result.EndInvokeCalled) {
				//while (!((AsyncResult)loadingJob.AsyncResult).EndInvokeCalled) {
				while (!loadingJob.IsLoadCompleted) {
					iterations++;
					Thread.Sleep(50);
					Monitor.Wait(syncRoot);
				}
			}
			
			Thread.Sleep(50);
			MyConsole.WriteLine(string.Format("Key='{0}' - Wait ended after {1} iteractions", key, iterations));
			
			if (loadingJob.Exception != null) {
				throw loadingJob.Exception;
			} else {
				return innerDict[key];
			}
		}
		
		#endregion
		
		#region IAsyncCache<TKey,TValue> Members

		public TValue GetOrLoadValue(TKey key)
		{
			return GetOrLoadValue(key, DefaultLoadValueFunc);
		}

		public TValue GetOrLoadValue(TKey key, Func<TKey,TValue> loadValueFunc)
		{
			if (loadValueFunc == null) throw new ArgumentNullException("loadValueFunc");
			
			TValue value;
			LoadingJob loadingJob;
			lock (syncRoot) {
				if (innerDict.TryGetValue(key, out value)) {
					MyConsole.WriteLine(string.Format("Key='{0}' - Value returned from cache", key));
					
					return value;
				} else if (!loadingJobs.TryGetValue(key, out loadingJob)) {
					loadingJob = InternalBeginLoad(key, loadValueFunc);
				}
			}
			
			return InternalEndLoad(loadingJob);
		}

		public void BeginLoad(TKey key)
		{
			BeginLoad(key, DefaultLoadValueFunc);
		}

		public void BeginLoad(TKey key, Func<TKey, TValue> loadValueFunc)
		{
			if (loadValueFunc == null) throw new ArgumentNullException("loadValueFunc");
			
			lock (syncRoot) {
				//Ignore the call if key has a loaded (or loading) value
				if (!innerDict.ContainsKey(key) && !loadingJobs.ContainsKey(key)) {
					InternalBeginLoad(key, loadValueFunc);
				}
			}
		}

		public TValue EndLoad(TKey key)
		{
			TValue value;
			LoadingJob loadingJob;
			lock (syncRoot) {
				if (innerDict.TryGetValue(key, out value)) {
					MyConsole.WriteLine(string.Format("Key='{0}' - Value returned from cache", key));
					
					return value;
				} else if (!loadingJobs.TryGetValue(key, out loadingJob)) {
					throw new ApplicationException("EndLoad called before BeginLoad started");
				}
			}
			
			return InternalEndLoad(loadingJob);
		}

		public TValue Reload(TKey key)
		{
			return Reload(key, DefaultLoadValueFunc);
		}

		public TValue Reload(TKey key, Func<TKey,TValue> loadValueFunc)
		{
			if (loadValueFunc == null) throw new ArgumentNullException("loadValueFunc");
			
			BeginReload(key, loadValueFunc);
			return EndLoad(key);
		}

		public void BeginReload(TKey key)
		{
			BeginReload(key, DefaultLoadValueFunc);
		}

		public void BeginReload(TKey key, Func<TKey,TValue> loadValueFunc)
		{
			if (loadValueFunc == null) throw new ArgumentNullException("loadValueFunc");
			
			lock (syncRoot) {
				//Ignore the call if the key has a loading value
				if (!loadingJobs.ContainsKey(key)) {
					MyConsole.WriteLine(string.Format("Key='{0}' - SignalReload received", key));
					InternalBeginLoad(key, loadValueFunc);
				} else {
					MyConsole.WriteLine(string.Format("Key='{0}' - SignalReload ignored", key));
				}
			}
		}

		public bool IsLoadingKey(TKey key)
		{
			lock (syncRoot) {
				return loadingJobs.ContainsKey(key);
			}
		}

		public bool ContainsOrIsLoadingKey(TKey key)
		{
			lock (syncRoot) {
				return innerDict.ContainsKey(key) || loadingJobs.ContainsKey(key);
			}
		}

		public void AddValueLoadedHandler(TKey key, EventHandler<KeyPairEventArgs<TKey,TValue>> handler)
		{
			ICustomEvent<EventHandler<KeyPairEventArgs<TKey,TValue>>> onLoadEvent;
			lock (eventSyncRoot) {
				if (onLoadEventColl.TryGetValue(key, out onLoadEvent)) {
					//Update custom event
					onLoadEventColl[key] = onLoadEvent.Combine(handler);
				} else {
					//Insert custom event
					onLoadEvent = CreateValueLoadedEvent().Combine(handler);
					onLoadEventColl.Add(key, onLoadEvent);
				}
			}
		}

		public void RemoveValueLoadedHandler(TKey key, EventHandler<KeyPairEventArgs<TKey,TValue>> handler)
		{
			ICustomEvent<EventHandler<KeyPairEventArgs<TKey,TValue>>> onLoadEvent;
			lock (eventSyncRoot) {
				if (onLoadEventColl.TryGetValue(key, out onLoadEvent)) {
					//Generate new custom event
					onLoadEvent = onLoadEvent.TakeOff(handler);
					
					if (onLoadEvent.IsEmpty()) {
						//Remove custom event if it's empty
						onLoadEventColl.Remove(key);
					} else {
						//Else, update it
						onLoadEventColl[key] = onLoadEvent;
					}
				}
			}
		}

		#endregion

		#region IDictionary<TKey,TValue> Members

		public void Add(TKey key, TValue value)
		{
			ThrowAddNotSupported(); //lock(locker) innerDict.Add(key, value);
		}

		public bool ContainsKey(TKey key)
		{
			lock(syncRoot) return innerDict.ContainsKey(key);
		}

		public ICollection<TKey> Keys
		{
			get { lock(syncRoot) return innerDict.Keys; }
		}

		public bool Remove(TKey key)
		{
			//TOREVIEW: What to do with the values being loaded?
			lock(syncRoot) return innerDict.Remove(key);
		}

		public bool TryGetValue(TKey key, out TValue value)
		{
			lock(syncRoot) return innerDict.TryGetValue(key, out value);
		}

		public ICollection<TValue> Values
		{
			get { lock(syncRoot) return innerDict.Values; }
		}

		public TValue this[TKey key]
		{
			get
			{
				lock(syncRoot) return innerDict[key];
			}
			set
			{
				ThrowAddNotSupported(); //lock(locker) innerDict[key] = value;
			}
		}

		#endregion

		#region ICollection<KeyValuePair<TKey,TValue>> Members

		public void Add(KeyValuePair<TKey, TValue> item)
		{
			ThrowAddNotSupported(); //lock(locker) innerDict.Add(item);
		}

		public void Clear()
		{
			//TOREVIEW: What to do with the values being loaded?
			lock(syncRoot) innerDict.Clear();
		}

		public bool Contains(KeyValuePair<TKey, TValue> item)
		{
			lock(syncRoot) return innerDict.Contains(item);
		}

		public void CopyTo(KeyValuePair<TKey, TValue>[] array, int arrayIndex)
		{
			lock(syncRoot) innerDict.CopyTo(array, arrayIndex);
		}

		public int Count
		{
			get { lock(syncRoot) return innerDict.Count; }
		}

		public bool IsReadOnly
		{
			get { return false; }
		}

		public bool Remove(KeyValuePair<TKey, TValue> item)
		{
			//TOREVIEW: What to do with the values being loaded?
			lock(syncRoot) return innerDict.Remove(item);
		}

		#endregion

		#region IEnumerable<KeyValuePair<TKey,TValue>> Members

		public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator()
		{
			lock(syncRoot) return ((IEnumerable<KeyValuePair<TKey, TValue>>)innerDict.ToArray()).GetEnumerator();
		}

		#endregion

		#region IEnumerable Members

		System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
		{
			lock(syncRoot) return this.GetEnumerator();
		}

		#endregion
	}
}
