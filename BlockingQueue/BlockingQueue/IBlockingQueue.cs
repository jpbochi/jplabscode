﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JpLabs.BlockingQueue
{
	public delegate bool FuncGetVolatileFlag();

	public interface IBlockingQueue
	{
		Type ItemType { get; }
		int Count { get; }
		void Clear();
		
		/// <summary>Max number of item in the queue (-1 means there's no limit)</summary>
		int MaxQueueDepth { get; }
		
		/// <summary>Returns false when there's something wrong, e.g., queue full</summary>
		bool TryEnqueue(object item);
		
		/// <summary>Returns false when there's no message to read</summary>
		bool TryDequeue(out object item);

		/// <summary>Returns false when a timeout or an abort happens</summary>
		bool WaitEnqueue(object item, int millisecondsTimeout, FuncGetVolatileFlag ExternalAbortSignaled);
		
		/// <summary>Returns false when a timeout or an abort happens</summary>
		bool WaitDequeue(out object item, int millisecondsTimeout, FuncGetVolatileFlag ExternalAbortSignaled);

	    void Pulse();
	    void PulseAll();
	}
}
