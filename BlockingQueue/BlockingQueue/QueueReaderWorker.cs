﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Threading;

namespace JpLabs.BlockingQueue
{
	public class QueueReaderWorker : BackgroundWorker
	{
		public IBlockingQueue CurrentQueue {get; protected set;}
		
		public QueueReaderWorker() : this(null)
		{}

		public QueueReaderWorker(IBlockingQueue startingQueue)
		{
			CurrentQueue = startingQueue;
			base.WorkerReportsProgress = false;
			base.WorkerSupportsCancellation = true;
		}
		
		public new void CancelAsync()
		{
			base.CancelAsync();
			CurrentQueue.Pulse();
		}

		protected override void OnDoWork(DoWorkEventArgs e)
		{
			object item;
			CurrentQueue = (e.Argument as IBlockingQueue) ?? CurrentQueue;
			if (CurrentQueue == null) throw new InvalidOperationException("No queue to read from");
			while (CurrentQueue.WaitDequeue(out item, Timeout.Infinite, delegate {return CancellationPending;})) {
				base.OnDoWork(new DoWorkEventArgs(item));
			}
		}
	}
}
