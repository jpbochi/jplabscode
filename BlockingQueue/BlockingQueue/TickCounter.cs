﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JpLabs.BlockingQueue
{
	/// <summary>
	/// Unfinished attempt of simple performance counter
	/// TODO: finish it or throw it away
	/// </summary>
	public class TickCounter
	{
		Queue<DateTime> ticks;
		int capacity;
		
		public TickCounter(int capacity)
		{
			this.capacity = capacity;
			ticks = new Queue<DateTime>(capacity);
		}
		
		public void Tick()
		{
			DateTime tickTime = DateTime.Now;
			lock (ticks) {
				if (ticks.Count == capacity) ticks.Dequeue();
				ticks.Enqueue(tickTime);
			}
		}
		
		public TimeSpan TimePerTick()
		{
			int ticksCount;
			DateTime lastTick;
			lock (ticks) {
				ticksCount = ticks.Count;
				lastTick = ticks.Peek();
			}
			if (ticksCount == 0)
				return TimeSpan.Zero;
			else
				return TimeSpan.FromTicks((DateTime.Now - lastTick).Ticks / (long)ticksCount);
		}

		public double TicksPerSecond()
		{
			int ticksCount;
			DateTime lastTick;
			lock (ticks) {
				ticksCount = ticks.Count;
				lastTick = ticks.Peek();
			}
			if (ticksCount == 0)
				return 0;
			else
				return (double)ticksCount / (DateTime.Now - lastTick).TotalSeconds;
		}
	}
}
