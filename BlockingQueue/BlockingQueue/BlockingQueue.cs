﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;

namespace JpLabs.BlockingQueue
{
	/// <summary>
	/// A BlockingQueue for multithreading
	/// TODO:
	///		- TryPeek() and WaitPeek()
	///		- Reduce repeated code (WaitEnqueue() and WaitDequeue() are far too similar)
	///		- Transactions!
	/// </summary>
	public class BlockingQueue<T> : IBlockingQueue
	{
		Queue<T> queue;
		int maxQueueDepth;
		object qLocker = new object();
		const int MINIMUM_WAIT = 1732; //nothing special

		public BlockingQueue() : this(new Queue<T>())
		{}

		public BlockingQueue(IEnumerable<T> collection) : this(new Queue<T>(collection))
		{}
		
		private BlockingQueue(Queue<T> q)
		{
			queue = q;
			maxQueueDepth = -1;
		}

		bool UnsafeTryEnqueue(object item)
		{
			queue.Enqueue((T)item);
			return true;
		}

		bool UnsafeTryEnqueue(T item)
		{
			queue.Enqueue(item);
			return true;
		}

		bool UnsafeTryDequeue(out object untypedItem)
		{
			T typedItem;
			bool retValue = UnsafeTryDequeue(out typedItem);
			untypedItem = typedItem;
			return retValue;
		}
		
		bool UnsafeTryDequeue(out T item)
		{
			if (queue.Count == 0) {
				item = default(T);
				return false;
			} else {
				try {
					item = queue.Dequeue();
					return true;
				} catch (InvalidOperationException) {
					//queue is empty
					item = default(T);
					return false;
				}
			}
		}

		Type IBlockingQueue.ItemType { get { return typeof(T); } }

		public int MaxQueueDepth
		{
			get { lock (qLocker) return maxQueueDepth; }
			set {
				if (value <= 0 && value != -1) throw new ArgumentOutOfRangeException();
				lock (qLocker) maxQueueDepth = value;
			}
		}
		
		public int Count
		{
			get { lock (qLocker) return queue.Count; }
		}
		
		public void Clear()
		{
			lock (qLocker) queue.Clear();
		}

		public bool TryEnqueue(object item)
		{
			lock (qLocker) {
				if (!(item is T)) throw new ApplicationException("Can't enqueue this type");
				if (maxQueueDepth == -1 || queue.Count >= maxQueueDepth) {
					return false; //Queue full! Plz, try again in a few miliseconds...
				} else {
					bool success = UnsafeTryEnqueue((T)item);
					if (success) {
						Monitor.Pulse(qLocker);
						//inboundCounter.Tick();
					}
					return success;
				}
			}
		}

		public bool TryDequeue(out object item)
		{
			lock (qLocker) {
				bool success = UnsafeTryDequeue(out item);
				if (success) {
					Monitor.Pulse(qLocker);
					//outboundCounter.Tick();
				}
				/// REMARK:
				/// If there were a way to enqueue a item without making a Pulse (from outside of this class)
				/// Then it would be necessary to Pulse once in a while to ensure a TryDequeue call wouldn't get blocked
				/// Or use a short timeout trying to dequeue, as I did
				return success;
			}
		}
		
	    public void Pulse()
	    {
			lock (qLocker) Monitor.Pulse(qLocker);
	    }

	    public void PulseAll()
	    {
			lock (qLocker) Monitor.PulseAll(qLocker);
	    }

		public bool WaitEnqueue(object item, int longTimeout, FuncGetVolatileFlag ExternalAbortSignaled)
		{
			int shortTimeout
				= (longTimeout == Timeout.Infinite)
				? MINIMUM_WAIT
				: Math.Min(MINIMUM_WAIT, longTimeout);

			lock (qLocker)
			{
				Stopwatch watch = (longTimeout == Timeout.Infinite) ? null : Stopwatch.StartNew();
				//Only one thread is allowed to TryEnqueue/TryDequeue at a time
				while (!UnsafeTryEnqueue(item))
				{
					//Wait for a pulse, an abort or a long timeout
					if (!ExternalAbortSignaled())
					{
						Monitor.Wait(qLocker, shortTimeout);
						/// REMARK:
						/// There is a time interval between the actual timeout and the returning of Wait
						/// In this interval, if there were a Pulse, it might get lost
					}
					if (ExternalAbortSignaled()) return false;
					if (watch != null && watch.ElapsedMilliseconds >= longTimeout) return false;
				}
				return true;
			}
		}

		public bool WaitDequeue(out object item, int longTimeout, FuncGetVolatileFlag ExternalAbortSignaled)
		{
			int shortTimeout
				= (longTimeout == Timeout.Infinite)
				? MINIMUM_WAIT
				: Math.Min(MINIMUM_WAIT, longTimeout);
			
			lock (qLocker) {
				Stopwatch watch = (longTimeout == Timeout.Infinite) ? null : Stopwatch.StartNew();
				//Only one thread is allowed to TryEnqueue/TryDequeue at a time
				while (!UnsafeTryDequeue(out item)) {
					//Wait for a pulse, an abort or a long timeout
					if (!ExternalAbortSignaled()) {
						Monitor.Wait(qLocker, shortTimeout);
						/// REMARK:
						/// There is a time interval between the actual timeout and the returning of Wait
						/// In this interval, if there were a Pulse, it might get lost
					}
					if (ExternalAbortSignaled()) return false;
					if (watch != null && watch.ElapsedMilliseconds >= longTimeout) return false;
				}
				return true;
			}
		}
	}
}
