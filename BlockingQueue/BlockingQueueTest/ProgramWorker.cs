﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JpLabs.BlockingQueue;
using System.Threading;
using System.ComponentModel;

namespace JpLabs.BlockingQueueTest
{
	class ProgramWorker
	{
		static volatile bool ShouldStop;
		static Random rnd = new Random();
		
		static void Main(string[] args)
		{
			BlockingQueue<string> q = new BlockingQueue<string>();
			
			
			Thread p1 = new Thread(Producer);
			Thread p2 = new Thread(Producer);
			Thread p3 = new Thread(Producer);
			QueueReaderWorker w1 = new QueueReaderWorker();
			QueueReaderWorker w2 = new QueueReaderWorker();
			QueueReaderWorker w3 = new QueueReaderWorker();
			w1.DoWork += Consume;
			w2.DoWork += Consume;
			w3.DoWork += Consume;
			
			ShouldStop = false;
			
			Console.WriteLine("Starting...");
			{
				p1.Start(new Pair("P1", q));
				p2.Start(new Pair("P2", q));
				p3.Start(new Pair("P3", q));
				w1.RunWorkerAsync(q);
				w2.RunWorkerAsync(q);
				w3.RunWorkerAsync(q);
			}
			
			ConsoleKeyInfo key;
			do {
				key = Console.ReadKey(true);
			} while (key.Key != ConsoleKey.Escape);
			
			Console.WriteLine("Stopping...");
			{
				ShouldStop = true;
				w1.CancelAsync();
				w2.CancelAsync();
				w3.CancelAsync();
				p1.Join();
				p2.Join();
				p3.Join();
			}
			Console.WriteLine("Stopped");
			Console.WriteLine("Items remaining: {0}", q.Count);
		}
		
		static void Producer(object obj)
		{
			Pair p = (Pair)obj;
			string myName = (string)p.First;
			IBlockingQueue q = (IBlockingQueue)p.Second;
			
			int itemSeed = rnd.Next(short.MaxValue);
			while (!ShouldStop) {
				Thread.Sleep(rnd.Next(10, 100));
				
				string item = itemSeed.ToString();
				itemSeed++;
				if (q.WaitEnqueue(item, Timeout.Infinite, () => ShouldStop)) {
					Console.WriteLine("Write by {0} | {1}", myName, item.ToString());
					Thread.Sleep(rnd.Next(500, 2000));
				}
			}
		}

		static void Consume(object sender, DoWorkEventArgs e)
		{
			object item = e.Argument;
			Console.WriteLine("\t\t\t{0} | Read", item.ToString());
			Thread.Sleep(rnd.Next(500, 2000));
		}
	}
}
