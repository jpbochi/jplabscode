﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JpLabs.BlockingQueue;
using System.Threading;

namespace JpLabs.BlockingQueueTest
{
	class Pair
	{
		public object First {get; set;}
		public object Second {get; set;}
		
		public Pair(object first, object second)
		{
			First = first;
			Second = second;
		}
	}
	
	class ProgramSimple
	{
		static volatile bool ShouldStop;
		static Random rnd = new Random();
		
		static void Main(string[] args)
		{
			BlockingQueue<string> q = new BlockingQueue<string>();
			
			Thread p1 = new Thread(Producer);
			Thread p2 = new Thread(Producer);
			Thread p3 = new Thread(Producer);
			Thread c1 = new Thread(Consumer);
			Thread c2 = new Thread(Consumer);
			Thread c3 = new Thread(Consumer);
			
			Console.WriteLine("Starting...");
			{
				ShouldStop = false;
				p1.Start(new Pair("P1", q));
				p2.Start(new Pair("P2", q));
				p3.Start(new Pair("P3", q));
				c1.Start(new Pair("C1", q));
				c2.Start(new Pair("C2", q));
				c3.Start(new Pair("C3", q));
			}
			
			ConsoleKeyInfo key;
			do {
				key = Console.ReadKey(true);
			} while (key.Key != ConsoleKey.Escape);
			
			Console.WriteLine("Stopping...");
			{
				ShouldStop = true;
				p1.Join();
				p2.Join();
				p3.Join();
				c1.Join();
				c2.Join();
				c3.Join();
			}
			Console.WriteLine("Stopped");
			Console.WriteLine("Items remaining: {0}", q.Count);
		}
		
		static void Producer(object obj)
		{
			Pair p = (Pair)obj;
			string myName = (string)p.First;
			IBlockingQueue q = (IBlockingQueue)p.Second;
			
			int itemSeed = rnd.Next(short.MaxValue);
			while (!ShouldStop) {
				Thread.Sleep(rnd.Next(10, 100));
				
				string item = itemSeed.ToString();
				itemSeed++;
				if (q.WaitEnqueue(item, Timeout.Infinite, () => ShouldStop)) {
					Console.WriteLine("Write by {0} | {1}", myName, item.ToString());
					Thread.Sleep(rnd.Next(500, 2000));
				}
			}
		}
		
		static void Consumer(object obj)
		{
			Pair p = (Pair)obj;
			string myName = (string)p.First;
			IBlockingQueue q = (IBlockingQueue)p.Second;
			
			while (!ShouldStop) {
				Thread.Sleep(rnd.Next(10, 100));
				
				object item;
				if (q.WaitDequeue(out item, Timeout.Infinite, () => ShouldStop)) {
					Console.WriteLine("\t\t\t{1} | Read by {0}", myName, item.ToString());
					Thread.Sleep(rnd.Next(500, 2000));
				}
			}
		}
	}
}
